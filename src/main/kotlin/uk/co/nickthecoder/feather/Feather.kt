package uk.co.nickthecoder.feather

import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.parser.LibraryClassLoader
import java.io.File
import java.io.FileOutputStream
import java.util.jar.Attributes
import java.util.jar.JarEntry
import java.util.jar.JarOutputStream
import java.util.jar.Manifest


object Feather {

    private class Permissive : SandBox {
        override fun isAllowed(klass: Class<*>) = true
        override fun clone() = Permissive()
    }

    fun configuration(debug: Boolean) = CompilerConfiguration().apply {
        sandBox = Permissive()
        includeFeatherRuntime()
        includeExit()
        impliedImportPackages.add("java.lang")
        simplifyMain = true
        allowCommands = true
        errorHandler = { position, message -> System.err.println("$position\n    $message") }
        classLoader = LibraryClassLoader()
        this.debug = debug
    }

    @JvmStatic
    fun main(vararg argv: String) {

        // Has the special argument "--" been found yet.
        // Note this will NOT allow filenames of feather scripts to begin with "-".
        // It is used just before the arguments for the feather script's main method.
        var endOfArgs = false

        // The list of files passed to the compiler
        val files = mutableListOf<File>()

        // The class name containing the "main" method.
        var main: String? = null

        // The directory name where class files are created
        var compileClassDirectory: String? = null

        // The filename of the destination jar file
        var toJar: String? = null

        // Should the classes from uk.co.nickthecoder.runtime be included in the jar file?
        var addRuntime = false

        // Include debug symbols (line numbers available within stack traces)
        var debug = true


        // The arguments to be passed to the Feather script's main method
        val featherArguments = mutableListOf<String>()

        for (arg in argv) {

            if (endOfArgs || !arg.startsWith('-')) {

                if (endOfArgs) {
                    // An argument to be passed to the feather script
                    featherArguments.add(arg)

                } else {
                    // A filename of a feather script
                    val f = File(arg)
                    if (f.exists()) {
                        files.add(f)
                    } else {
                        System.err.println("File $f not found")
                        System.exit(-2)
                    }
                }

            } else if (arg == "--") {
                endOfArgs = true

            } else {
                val withoutPrefix = if (arg.startsWith("--")) arg.substring(2) else arg.substring(1)
                val eq = withoutPrefix.indexOf('=')
                val (name, value) = if (eq >= 0) {
                    Pair(withoutPrefix.substring(0, eq), withoutPrefix.substring(eq + 1))
                } else {
                    Pair(withoutPrefix, "")
                }

                when (name) {
                    "h", "help" -> usage()
                    "examples" -> examples()
                    "main" -> main = value
                    "jar" -> toJar = value
                    "addruntime" -> addRuntime = true
                    "no_debug" -> debug = false
                    "compile" -> compileClassDirectory = value
                }

            }
        }

        // Command line has been parsed, now compile (and maybe run)...

        val configuration = configuration(debug)
        val classes = try {
            FeatherCompiler(configuration).compileFiles(files)
        } catch (e: CompilationFailed) {
            System.exit(-10)
            return // Just to keep the compiler happy ;-)
        }

        val classLoader = object : ClassLoader(configuration.classLoader) {
            override fun findClass(name: String): Class<*>? {
                val bytes = classes[name] ?: return null
                return defineClass(name, bytes, 0, bytes.size)
            }
        }
        if (main == null) {
            for (className in classes.keys) {
                val klass = classLoader.loadClass(className)
                try {
                    klass.getMethod("main", arrayOf<String>().javaClass)
                    main = className
                } catch (e: Exception) {
                }
            }
        }

        if (toJar != null) {
            if (toJar.isEmpty()) {
                System.err.println("Jar file not specified")
                error()
            }

            val manifest = Manifest()
            with(manifest.mainAttributes) {
                put(Attributes.Name.MANIFEST_VERSION, "1.0")
                if (main != null) put(Attributes.Name.MAIN_CLASS, main)
            }

            val fileOutputStream = FileOutputStream(toJar)
            val jarOutputStream = JarOutputStream(fileOutputStream, manifest)

            val now = System.currentTimeMillis()

            classes.forEach { className, byteCode ->
                val jarEntry = JarEntry(className.replace(".", "/") + ".class")
                jarEntry.setTime(now)
                jarOutputStream.putNextEntry(jarEntry)
                jarOutputStream.write(byteCode, 0, byteCode.size)
                jarOutputStream.closeEntry()
            }

            if (addRuntime) {
                for (name in listOf(
                    "System",
                    "Print",
                    "Extensions",
                    "CharRange", "CharProgression", "CharProgression\$CharProgressionIterator",
                    "IntRange", "IntProgression", "IntProgression\$IntProgressionIterator",
                    "LongRange", "LongProgression", "LongProgression\$LongProgressionIterator",
                    "CharSequenceIterator",

                    "exit/Exit",

                    "command/Command", "command/Command.CommandPart",
                    "command/CommandLineBuilder", "command/ShCommandLineBuilder",
                    "command/CommandResult",
                    "command/CommandRunner",
                    "command/DefaultCommandRunner", "command/DefaultCommandRunner\$ConsumeOutput"
                )) {
                    val resourceName = "uk/co/nickthecoder/feather/runtime/$name.class"
                    val byteCode = classLoader.getResource(resourceName)?.readBytes() ?: continue
                    val jarEntry = JarEntry(resourceName)
                    jarEntry.setTime(now)
                    jarOutputStream.putNextEntry(jarEntry)
                    jarOutputStream.write(byteCode, 0, byteCode.size)
                    jarOutputStream.closeEntry()
                }
            }

            jarOutputStream.close()
            fileOutputStream.close()

        } else if (compileClassDirectory != null) {

            val dir = if (compileClassDirectory.isBlank()) File(".") else File(compileClassDirectory)
            if (!dir.exists()) {
                System.err.println("Directory $dir does not exit")
                System.exit(-3)
            }
            classes.forEach { className, byteCode ->
                val file = File(dir, className.replace(".", File.separator) + ".class")
                file.parentFile.mkdirs()
                file.writeBytes(byteCode)
            }

        } else {

            if (main == null) {
                System.err.println("No main method found")
                System.exit(-4)
            }

            val klass = classLoader.loadClass(main)
            val mainMethod = try {
                klass.getMethod("main", arrayOf<String>().javaClass)
            } catch (e: Exception) {
                null
            }
            if (mainMethod == null) {
                System.err.println("Class $main has no main method")
                System.exit(-5)
            } else {
                if (mainMethod.isStatic()) {
                    mainMethod.invoke(null, featherArguments.toTypedArray())
                } else {
                    System.err.println("Class $main does not have a STATIC main method")
                }
            }
        }
    }

    @JvmStatic
    fun examples() {
        println(EXAMPLES)
        System.exit(0)
    }

    @JvmStatic
    fun usage() {
        println(USAGE)
        System.exit(0)
    }

    @JvmStatic
    fun error() {
        System.err.println(USAGE)
        System.exit(-1)
    }
}

private val USAGE = """
Usage : feather [OPTIONS] FEATHER-FILE... [ -- PROGRAM_ARGUMENTS... ]

Options :
    --main=CLASS   : The fully qualified name of the class containing the main method.
                     If this is omitted, then the first class containing a main method is used.
                     
    --jar=FILENAME : Compiles the script(s) to a .jar files. The script is NOT executed.
    
    --compile      : Compiles the scripts to .class file(s) in the current directory
    --compile=DIR  : Compiles the scripts to .class file(s) in the given directory
    
    --addruntime   : Includes the classes from feather-runtime.jar into the jar.
                     Only applicable when used with the --jar option.
                      
    --no_debug     : Excludes debug symbols (line numbers won't be available within stack traces)

    --examples     : Prints examples of how to use this command line program.
                     All later arguments are ignored.
    
    -h, --help     : Prints this message. All later arguments are ignored.

If either --jar or --compile are specified, the script is NOT run, and PROGRAM_ARGUMENTS are ignored.
"""

val EXAMPLES = """
Examples :

    Runs a single script with no arguments :
    
        feather examples/HelloWorld.feather

    Runs a single script passing "Hello" to its main method :
    
        feather MyScript.feather -- Hello

    Runs the "main" method in class MyProgram (Helper.feather is also compiled) :
    
        feather --main=MyProgram MyProgram.feather Helper.feather

    Creates a jar file :
        
        feather --jar=helloWorld.jar examples/HelloWorld.feather
        java -jar helloWorld.jar
    
    Creates a jar file, and includes the feather-runtime into the jar :
    If you omit --addruntime, then the java command would fail, because
    examples/HelloWorld.feather uses the "println" function from feather-runtime's System class. 
        
        feather --jar=hello.jar --addruntime examples/Hello.feather
        java -jar hello.jar
    
    Creates a class file in the current directory :
    If Foo.feather contains more than one class, then multiple .class files will be created.
    Note, if the classes are in packages, then the appropriate sub-directories will be created. 

        feather --compile Foo.feather

    Creates a class file in the current directory : 
    If Foo.feather contains more than one class, then multiple .class files will be created.
    Note, if the classes are in packages, then the appropriate sub-directories will be created. 
    
        feather --compile Foo.feather
        
"""
