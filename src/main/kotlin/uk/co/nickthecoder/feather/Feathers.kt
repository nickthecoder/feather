package uk.co.nickthecoder.feather

import uk.co.nickthecoder.feather.internal.isStatic
import java.io.File
import java.lang.System.err
import java.lang.reflect.Method

/**
 * Compiles a single class from a script file given as the first parameter.
 * All other parameters are passes to the script's main method.
 *
 * This is particularly useful for creating simple command line scripts.
 * Prefix such scripts with the shebang similar to :
 *
 *      #!/bin/feathers
 */
object Feathers {

    private class Permissive : SandBox {
        override fun isAllowed(klass: Class<*>) = true
        override fun clone() = Permissive()
    }

    @JvmStatic
    fun main(vararg args: String) {
        if (args.isEmpty()) {
            err.println("No feather script specified")
            err.println(USAGE)
            System.exit(-1)
        }
        val scriptPath = args[0]
        val featherArguments = args.toList().subList(1, args.size)

        val configuration = Feather.configuration(true)
        // To use exit from your script : exit( n ) where n is an integer.
        configuration.includeExit()
        configuration.includeFeatherRuntime()
        with (configuration.impliedImportPackages) {
            add("java.util")
            add("java.io")
            add("java.text")
        }

        val classes = try {
            FeatherCompiler(configuration).compileFiles(listOf(File(scriptPath)))
        } catch (e: CompilationFailed) {
            System.exit(-10)
            return // Just to keep the compiler happy ;-)
        }

        val classLoader = object : ClassLoader(configuration.classLoader) {
            override fun findClass(name: String): Class<*>? {
                val bytes = classes[name] ?: return null
                return defineClass(name, bytes, 0, bytes.size)
            }
        }

        // Look for a class with a main method
        for (className in classes.keys) {
            val klass = classLoader.loadClass(className)
            val mainMethod: Method? = try {
                klass.getMethod("main", arrayOf<String>().javaClass)
            } catch (e: Exception) {
                throw e
            }
            if (mainMethod?.isStatic() == true) {
                mainMethod.invoke(null, featherArguments.toTypedArray())
                System.exit(0)
            }

        }
        System.err.println("No main method found. static fun( argv : String... )")
        System.exit(-1)
    }

}

private val USAGE = "Usage : feathers FEATHER-FILE"
