
## Enum Class

A simple enum :

    enum class Rainbow {
        RED, ORANGE, YELLOW, GREEN, BLUE, INDIGO, VIOLET
    }

Here's a more complicated example, where each enum value has an attribute :

    enum class Rainbow( val color: String ) {
        RED( "#ff0000" ),
        ORANGE( "..." ),
        YELLOW( "#ffff00" ),
        GREEN( "00ff00" ),
        BLUE( "0000ff" ),
        INDIGO( "..." ),
        VIOLET( "...")
    }

Here, `color` is a field. We could use it like so :

    for ( hue in Rainbow.values() ) {
        println( "Hue : ${hue.name()} Color : ${hue.color}" )
    }

In this example `hue` is one of the `Rainbow` enum values.
