## Feather's Runtime

In addition to the compiler, Feather also include some useful classes/functions that you can use from within Feather
scripts.

It includes various niceties, such as :

* `println` (instead of Java's verbose `System.out.println`)
* `ranges` so that for loops can iterate over ranges of `int`s `char`s etc.
* A subset of java.lang.System
* exit. The same as java.lang.System.exit, but within its own package,
  so that it must be explicitly enabled if required.
  (You don't want a script/macro being able to terminate your entire application!)

The runtime classes are optional, but in most cases, the runtime should be included.

    featherCompiler.configuration.includeRuntime = true

Ensure that the feather-runtime jar file is included in your classpath. If your compile-time classpath is different from
your runtime classpath, you must include it in both.

Sorry, there's no documentation for the runtime, so for now, please look at the
[Java source code](https://gitlab.com/nickthecoder/feather/-/tree/master/feather-runtime/src/main/java/uk/co/nickthecoder/feather/runtime)

