## Sandbox

One of Feather's goals, is to provide a *safe* language, suitable for embedding
in an application, were the scripts only have access to certain classes, and
cannot, for example, scan the filesystem, run arbitrary commands,
access the internet etc.

Feather achieves this through the use of a *Sandbox*. The Sandbox has a list of
classes/packages, which are *safe*.
During compilation, every time a class is used, it is checked against the sandbox,
and compilation is aborted with an error if a class outside the sandbox is used.

### Limitations

The sandbox is only as good as the list of allowed classes/packages.

If you grant access to the `File` class, then scripts are free to scan the filesystem.
If you grant access to anything in `java.lang.reflect`, then it's easy to escape the sandbox.

There are more subtle ways that the sandbox is too permissive.

Imagine you use Feather as a scripting language in a drawing application,
with an `Image` class in the allowed list.
If `Image` has a save method, which takes a file path (as a string), then scripts
will be able to destroy data, by saving images overwriting existing files.

The recommended solution to this subtle problem is to create a `public` API,
which does NOT include the ability to save an `Image`.
The non-public implementation of `Image` may include the *save* method, but as
the implementation is not in the allowed list, Feather scripts cannot use the *save*
method.

If you *want* scripts to be able to save images, then Feather offers no support.
It is up to you to ensure that saving of those images cannot do harm.

This is hard to do well - there is no silver bullet to make scripting safe and easy!
