// feather (top-level)

plugins {
    kotlin("jvm") version "1.9.10"
    id("org.jetbrains.dokka") version "1.9.10"
    application
}

val featherVersion: String by project

allprojects {
    apply(plugin = "org.jetbrains.dokka")
    apply(plugin = "org.jetbrains.kotlin.jvm")

    kotlin {
        jvmToolchain(11)
    }

    repositories {
        mavenCentral()
    }

    version = featherVersion
    group = "uk.co.nickthecoder"
}

subprojects {
    apply(plugin = "maven-publish")
}

application {
    mainClass.set("uk.co.nickthecoder.feather.Feather")
}

dependencies {
    implementation(project(":feather-core"))
    implementation(project(":feather-runtime"))
}

tasks.dokkaHtmlMultiModule {
    moduleName.set("Feather")
}

// The following tasks publish dokka and the zip file to my webserver.

task<Exec>("publishDokka") {
    dependsOn(":dokkaHtmlMultiModule")
    commandLine("./publishDokka.feathers", "build/dokka/htmlMultiModule/", "../api/feather-$featherVersion")
}

task<Exec>("publishZip") {
    dependsOn(":distZip")
    commandLine("cp", "build/distributions/feather-${featherVersion}.zip", "../download/")
}

task<Exec>("ntc") {
    dependsOn(":publishZip", "publishDokka")
    commandLine("echo", "Done")
}

defaultTasks("installDist")
