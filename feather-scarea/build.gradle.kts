// feather-runtime

repositories {
    // My other projects hosted on gitlab.com
    maven {
        name = "scarea"
        url = uri("https://gitlab.com/api/v4/projects/15723319/packages/maven")
    }
}

dependencies {
    val scareaVersion: String by project
    implementation(project(":feather-core"))
    implementation("uk.co.nickthecoder:scarea-syntax:$scareaVersion")
}

// For uploading maven artifacts to gitlab.com
publishing {
    repositories {
        publications {
            create<MavenPublication>("feather-scarea") {
                from(components["kotlin"])
            }
        }
        maven {
            // 16531834 is the GitLab project ID of project nickthecoder/feather
            url = uri("https://gitlab.com/api/v4/projects/16531834/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = providers.gradleProperty("gitLabPrivateToken").get()
                // The password is stored in ~/.gradle/gradle.properties
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}
