package uk.co.nickthecoder.feather.scarea

import org.antlr.v4.runtime.*
import org.antlr.v4.runtime.tree.ErrorNode
import org.antlr.v4.runtime.tree.ParseTreeWalker
import org.antlr.v4.runtime.tree.TerminalNode
import uk.co.nickthecoder.feather.parser.FeatherLexer
import uk.co.nickthecoder.feather.parser.FeatherParser
import uk.co.nickthecoder.feather.parser.FeatherParserBaseListener
import uk.co.nickthecoder.scarea.HighlightRange
import uk.co.nickthecoder.scarea.PairedHighlightRange
import uk.co.nickthecoder.scarea.ScareaPosition
import uk.co.nickthecoder.scarea.syntax.AntlrSyntaxHighlighter
import uk.co.nickthecoder.scarea.syntax.SyntaxHighlighter
import uk.co.nickthecoder.scarea.syntax.endPosition
import uk.co.nickthecoder.scarea.syntax.startPosition

abstract class AntlrSyntaxHighlighter : SyntaxHighlighter() {

    override fun createRanges(text: String): List<HighlightRange> {
        return createWorker().createRanges(text)
    }

    protected abstract fun createWorker(): Worker

    protected abstract inner class Worker {

        protected val ranges = mutableListOf<HighlightRange>()

        fun highlightPair(tokenA: Token?, tokenB: Token?) {
            tokenA ?: return
            tokenB ?: return

            val highlightName = when (tokenA.text) {
                "\"", "\"\"\"" -> "string"
                "{" -> "brace"
                "[" -> "bracket"
                "<" -> "angle"
                "\${" -> "brace"
                else -> "paren"
            }
            highlightPair(tokenA, tokenB, highlightName)
        }

        fun highlightPair(tokenA: Token?, tokenB: Token?, highlightName: String) {
            tokenA ?: return
            tokenB ?: return

            val a = PairedHighlightRange(
                tokenA.startPosition(),
                tokenA.endPosition(),
                highlights[highlightName]!!,
                transient = true,
                owner = this@AntlrSyntaxHighlighter,
                opening = null
            )
            val b = PairedHighlightRange(
                tokenB.startPosition(),
                tokenB.endPosition(),
                highlights[highlightName]!!,
                transient = true,
                owner = this@AntlrSyntaxHighlighter,
                opening = a
            )

            ranges.add(a)
            ranges.add(b)
        }

        fun highlightPair(nodeA: TerminalNode?, nodeB: TerminalNode?) {
            nodeA ?: return
            nodeB ?: return

            highlightPair(nodeA.symbol, nodeB.symbol)
        }

        fun highlightPair(nodeA: TerminalNode?, nodeB: TerminalNode?, highlightName: String) {
            nodeA ?: return
            nodeB ?: return

            highlightPair(nodeA.symbol, nodeB.symbol, highlightName)
        }

        fun highlight(from: ScareaPosition, to: ScareaPosition, highlightName: String) {
            val highlight = highlights[highlightName] ?: return

            ranges.add(
                HighlightRange(from, to, highlight, transient = true, owner = this@AntlrSyntaxHighlighter)
            )
        }

        fun highlight(ctx: ParserRuleContext?, highlightName: String = "keyword") {
            ctx ?: return
            highlight(ctx.startPosition(), ctx.endPosition(), highlightName)
        }

        fun highlight(token: Token?, highlightName: String = "keyword") {
            token ?: return
            highlight(token.startPosition(), token.endPosition(), highlightName)
        }

        fun highlight(node: TerminalNode?, highlightName: String = "keyword") {
            node ?: return
            highlight(node.symbol, highlightName)
        }

        abstract fun createRanges(text: String): List<HighlightRange>

    }
}

class FeatherSyntaxHighlighter : AntlrSyntaxHighlighter() {

    companion object {
        val instance = FeatherSyntaxHighlighter()
    }

    override fun createWorker(): Worker {
        return FeatherWorker()
    }

    private inner class FeatherWorker : Worker() {

        override fun createRanges(text: String): List<HighlightRange> {

            val lexer = CommentedFeatherLexer(CharStreams.fromString(text))
            lexer.removeErrorListeners()
            val tokens = CommonTokenStream(lexer)
            val parser = FeatherParser(tokens)
            parser.removeErrorListeners()

            val listener = Listener()
            try {
                val tree = parser.featherFile()
                ParseTreeWalker.DEFAULT.walk(listener, tree)
            } catch (e: Throwable) {
                e.printStackTrace()
                // Do nothing
            }

            return ranges
        }

        private inner class CommentedFeatherLexer(source: CharStream) : FeatherLexer(source) {

            override fun nextToken(): Token {
                val token = super.nextToken()
                if (token.channel == 1) {
                    highlight(token, "comment")
                }

                return token
            }
        }

        private inner class Listener : FeatherParserBaseListener() {


            override fun visitErrorNode(node: ErrorNode) {
                highlight(node.symbol, "error")
            }

            override fun exitPackageHeader(ctx: FeatherParser.PackageHeaderContext) {
                highlight(ctx.PACKAGE())
            }

            override fun exitImportHeader(ctx: FeatherParser.ImportHeaderContext) {
                highlight(ctx.IMPORT())
                highlight(ctx.importAlias()?.AS())
            }

            override fun exitClassModifiersAndName(ctx: FeatherParser.ClassModifiersAndNameContext) {
                highlight(ctx.CLASS())
                highlight(ctx.INTERFACE())
                highlight(ctx.ABSTRACT())
            }

            override fun exitConstructorParameter(ctx: FeatherParser.ConstructorParameterContext) {
                highlight(ctx.VAL())
                highlight(ctx.VAR())
            }

            override fun exitClassBody(ctx: FeatherParser.ClassBodyContext) {
                highlightPair(ctx.LCURL(), ctx.RCURL())
            }

            override fun exitFieldDeclarationDescription(ctx: FeatherParser.FieldDeclarationDescriptionContext) {
                highlight(ctx.STATIC())
                highlight(ctx.VAL())
                highlight(ctx.VAR())
            }

            override fun exitMethodValueParameters(ctx: FeatherParser.MethodValueParametersContext) {
                highlightPair(ctx.LPAREN(), ctx.RPAREN())
            }

            override fun exitBlock(ctx: FeatherParser.BlockContext) {
                highlightPair(ctx.LCURL(), ctx.RCURL())
            }

            override fun exitLocalVariableDeclaration(ctx: FeatherParser.LocalVariableDeclarationContext) {
                highlight(ctx.VAL())
                highlight(ctx.VAR())
            }

            override fun exitThrowStatement(ctx: FeatherParser.ThrowStatementContext) {
                highlight(ctx.THROW())
            }

            override fun exitReturnStatement(ctx: FeatherParser.ReturnStatementContext) {
                highlight(ctx.RETURN())
            }

            override fun exitContinueStatement(ctx: FeatherParser.ContinueStatementContext) {
                highlight(ctx.CONTINUE())
            }

            override fun exitBreakStatement(ctx: FeatherParser.BreakStatementContext) {
                highlight(ctx.BREAK())
            }

            override fun exitNamedInfix(ctx: FeatherParser.NamedInfixContext) {
                highlight(ctx.IS())
                highlight(ctx.NOT_IS())
            }

            override fun exitArrayAccess(ctx: FeatherParser.ArrayAccessContext) {
                highlightPair(ctx.LSQUARE(), ctx.RSQUARE())
            }

            override fun exitValueArguments(ctx: FeatherParser.ValueArgumentsContext) {
                highlightPair(ctx.LPAREN(), ctx.RPAREN())
            }

            override fun exitTypeArguments(ctx: FeatherParser.TypeArgumentsContext) {
                highlightPair(ctx.LANGLE(), ctx.RANGLE())
            }

            override fun exitThisExpression(ctx: FeatherParser.ThisExpressionContext) {
                highlight(ctx.THIS())
            }

            override fun exitWithDescription(ctx: FeatherParser.WithDescriptionContext) {
                highlight(ctx.WITH())
                highlightPair(ctx.LPAREN(), ctx.RPAREN())
            }

            override fun exitReceiverDescription(ctx: FeatherParser.ReceiverDescriptionContext) {
                highlight(ctx.APPLY())
            }

            override fun exitParenthesizedExpression(ctx: FeatherParser.ParenthesizedExpressionContext) {
                highlightPair(ctx.LPAREN(), ctx.RPAREN())
            }

            override fun exitIfExpression(ctx: FeatherParser.IfExpressionContext) {
                if (ctx.ELSE() == null) {
                    highlight(ctx.IF())
                } else {
                    highlightPair(ctx.IF(), ctx.ELSE(), "keyword")
                }
            }

            override fun exitForSetup(ctx: FeatherParser.ForSetupContext) {
                highlight(ctx.FOR())
                highlightPair(ctx.LPAREN(), ctx.RPAREN())
            }

            override fun exitWhileExpression(ctx: FeatherParser.WhileExpressionContext) {
                highlight(ctx.WHILE())
                highlightPair(ctx.LPAREN(), ctx.RPAREN())
            }

            override fun exitDoWhileExpression(ctx: FeatherParser.DoWhileExpressionContext) {
                highlightPair(ctx.DO(), ctx.WHILE(), "keyword")
                highlightPair(ctx.LPAREN(), ctx.RPAREN())
            }

            override fun exitTryExpression(ctx: FeatherParser.TryExpressionContext) {
                highlight(ctx.TRY())
            }

            override fun exitCatchBlock(ctx: FeatherParser.CatchBlockContext) {
                highlight(ctx.CATCH())
                highlightPair(ctx.LPAREN(), ctx.RPAREN())
            }

            override fun exitFinallyBlock(ctx: FeatherParser.FinallyBlockContext) {
                highlight(ctx.FINALLY())
            }

            override fun exitNullLiteral(ctx: FeatherParser.NullLiteralContext) {
                highlight(ctx)
            }

            override fun exitBooleanLiteral(ctx: FeatherParser.BooleanLiteralContext?) {
                highlight(ctx)
            }

            override fun exitCharacterLiteral(ctx: FeatherParser.CharacterLiteralContext) {
                highlight(ctx, "string")
            }

            override fun exitDoubleLiteral(ctx: FeatherParser.DoubleLiteralContext?) {
                highlight(ctx, "number")
            }

            override fun exitFloatLiteral(ctx: FeatherParser.FloatLiteralContext?) {
                highlight(ctx, "number")
            }

            override fun exitByteLiteral(ctx: FeatherParser.ByteLiteralContext?) {
                highlight(ctx, "number")
            }

            override fun exitShortLiteral(ctx: FeatherParser.ShortLiteralContext?) {
                highlight(ctx, "number")
            }

            override fun exitIntLiteral(ctx: FeatherParser.IntLiteralContext?) {
                highlight(ctx, "number")
            }

            override fun exitLongLiteral(ctx: FeatherParser.LongLiteralContext?) {
                highlight(ctx, "number")
            }

            override fun exitLineStringLiteral(ctx: FeatherParser.LineStringLiteralContext) {
                highlightPair(ctx.QUOTE_OPEN(), ctx.QUOTE_CLOSE())
            }

            override fun exitMultiLineStringLiteral(ctx: FeatherParser.MultiLineStringLiteralContext) {
                highlightPair(ctx.TRIPLE_QUOTE_OPEN(), ctx.TRIPLE_QUOTE_CLOSE())
            }

            override fun exitLineStringContent(ctx: FeatherParser.LineStringContentContext) {
                highlight(ctx, "string")
            }

            override fun exitLineStringExpression(ctx: FeatherParser.LineStringExpressionContext) {
                highlightPair(ctx.LineStrExprStart(), ctx.RCURL())
            }

            override fun exitMultiLineStringContent(ctx: FeatherParser.MultiLineStringContentContext) {
                highlight(ctx, "string")
            }

            override fun exitMultiLineStringExpression(ctx: FeatherParser.MultiLineStringExpressionContext) {
                highlightPair(ctx.MultiLineStrExprStart(), ctx.RCURL())
            }

            override fun exitCommandLiteral(ctx: FeatherParser.CommandLiteralContext) {
                highlightPair(ctx.COMMAND_OPEN(), ctx.COMMAND_CLOSE())
            }

            override fun exitCommandContent(ctx: FeatherParser.CommandContentContext) {
                highlight(ctx, "string")
            }

            override fun exitCommandExpression(ctx: FeatherParser.CommandExpressionContext) {
                highlightPair(ctx.CommandExprStart(), ctx.RCURL())
            }

            override fun exitGenericSpec(ctx: FeatherParser.GenericSpecContext) {
                highlightPair(ctx.LANGLE(), ctx.RANGLE())
            }
        }
    }

}
