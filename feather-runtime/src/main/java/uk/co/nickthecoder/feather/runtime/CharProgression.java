package uk.co.nickthecoder.feather.runtime;

import java.util.Iterator;
import java.util.Objects;

public class CharProgression implements Iterable<Character> {

    public final char start;
    public final char endInclusive;
    public final int step;

    public CharProgression(char start, char endInclusive, int step) {
        if (step == 0) throw new IllegalArgumentException("Step cannot be zero");
        this.start = start;
        this.endInclusive = endInclusive;
        this.step = step;
    }

    public CharProgression step(int step) {
        if (this.step < 0) {
            return new CharProgression(start, endInclusive, -step);
        } else {
            return new CharProgression(start, endInclusive, step);
        }
    }

    public boolean isEmpty() {
        if (step < 0) {
            return start < endInclusive;
        } else {
            return start > endInclusive;
        }
    }

    public boolean equals(Object other) {
        if (other instanceof CharProgression) {
            CharProgression otherProgression = (CharProgression) other;
            return (isEmpty() && (otherProgression).isEmpty()) ||
                    (start == otherProgression.start &&
                            endInclusive == otherProgression.endInclusive &&
                            step == otherProgression.step
                    );
        } else {
            return false;
        }
    }

    public int hashCode() {
        if (isEmpty()) return -1;
        return Objects.hash(start, endInclusive, step);
    }

    public String toString() {
        if ( step == 1 ) {
            return "'" + start + "'..'" + endInclusive + "'";
        } else {
            return "'" + start + "'..'" + endInclusive + "' step " + step;
        }
    }

    @Override
    public Iterator<Character> iterator() {
        return new CharProgressionIterator();
    }

    private class CharProgressionIterator implements Iterator<Character> {

        private char next = start;

        public boolean hasNext() {
            return step < 0 ? next >= endInclusive : next <= endInclusive;
        }

        public Character next() {
            char result = next;
            next += step;
            return result;
        }
    }
}
