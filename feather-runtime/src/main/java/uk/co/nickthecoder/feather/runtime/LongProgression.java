package uk.co.nickthecoder.feather.runtime;

import java.util.Iterator;
import java.util.Objects;

public class LongProgression implements Iterable<Long> {

    public final long start;
    public final long endInclusive;
    public final long step;

    public LongProgression(long start, long endInclusive, long step) {
        if (step == 0) throw new IllegalArgumentException("Step cannot be zero");
        this.start = start;
        this.endInclusive = endInclusive;
        this.step = step;
    }

    public LongProgression step(long step) {
        if (this.step < 0) {
            return new LongProgression(start, endInclusive, -step);
        } else {
            return new LongProgression(start, endInclusive, step);
        }
    }

    public boolean isEmpty() {
        if (step < 0) {
            return start < endInclusive;
        } else {
            return start > endInclusive;
        }
    }

    public boolean equals(Object other) {
        if (other instanceof LongProgression) {
            LongProgression otherProgression = (LongProgression) other;
            return (isEmpty() && (otherProgression).isEmpty()) ||
                    (start == otherProgression.start &&
                            endInclusive == otherProgression.endInclusive &&
                            step == otherProgression.step
                    );
        } else {
            return false;
        }
    }

    public int hashCode() {
        if (isEmpty()) return -1;
        return Objects.hash(start, endInclusive, step);
    }

    public String toString() {
        if ( step == 1 ) {
            return start + ".." + endInclusive;
        } else {
            return start + ".." + endInclusive + " step " + step;
        }
    }

    @Override
    public Iterator<Long> iterator() {
        return new LongProgressionIterator();
    }

    private class LongProgressionIterator implements Iterator<Long> {

        private long next = start;

        public boolean hasNext() {
            return step < 0 ? next >= endInclusive : next <= endInclusive;
        }

        public Long next() {
            long result = next;
            next += step;
            return result;
        }
    }
}
