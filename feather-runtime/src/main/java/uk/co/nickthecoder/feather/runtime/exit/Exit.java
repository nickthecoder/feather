package uk.co.nickthecoder.feather.runtime.exit;

/**
 * Allows access to Java's System.exit()
 * This is in its own package, so that it does not have to be enabled when package `runtime` is enabled.
 */
public class Exit {

    public static void exit(int status) {
        java.lang.System.exit(status);
    }
    public static void exit() {
        java.lang.System.exit(0);
    }

}
