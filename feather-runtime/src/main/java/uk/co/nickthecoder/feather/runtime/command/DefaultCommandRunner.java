package uk.co.nickthecoder.feather.runtime.command;

import java.io.*;

public class DefaultCommandRunner implements CommandRunner {

    private final String[] shell;

    private final CommandLineBuilder commandBuilder;

    public DefaultCommandRunner(CommandLineBuilder commandBuilder, String... shell) {
        this.commandBuilder = commandBuilder;
        this.shell = shell;
    }

    @Override
    public String commandLine( Command command ) {
        return commandBuilder.buildCommandLine(command);
    }

    @Override
    public CommandResult run(Command command) {
        return run(command, null, null);
    }

    @Override
    public CommandResult run(Command command, Consumer outConsumer, Consumer errConsumer) {
        String commandLine = commandBuilder.buildCommandLine(command);
        String[] allArgs = new String[shell.length + 1];
        java.lang.System.arraycopy(shell, 0, allArgs, 0, shell.length);
        allArgs[shell.length] = commandLine;

        ProcessBuilder pb = new ProcessBuilder(allArgs);

        if (command.directory != null) {
            pb.directory(command.directory);
        }
        if (command.env != null) {
            pb.environment().putAll(command.env);
        }

        if (outConsumer == null) {
            pb.redirectOutput(ProcessBuilder.Redirect.INHERIT);
        }
        if (errConsumer == null) {
            pb.redirectError(ProcessBuilder.Redirect.INHERIT);
        }

        Process process;
        try {
            process = pb.start();
        } catch (IOException e) {
            return new CommandResult(commandLine, null, CommandResult.STATE_FAILED, -1, "", "");
        }

        ConsumeOutput consumeOut = null;
        ConsumeOutput consumeErr = null;
        if (outConsumer != null) {
            consumeOut = new ConsumeOutput(outConsumer, process.getInputStream());
            consumeOut.start();
        }
        if (errConsumer != null) {
            consumeErr = new ConsumeOutput(errConsumer, process.getErrorStream());
            consumeErr.start();
        }

        try {
            int exitStatus = process.waitFor();
            if (consumeOut != null) {
                consumeOut.join();
            }
            if (consumeErr != null) {
                consumeErr.join();
            }
            String out;
            String err;
            if (consumeOut == null) {
                out = "";
            } else {
                out = consumeOut.consumer.end();
            }
            if (consumeErr == null) {
                err = "";
            } else {
                err = consumeErr.consumer.end();
            }

            return new CommandResult(commandLine, process, CommandResult.STATE_FINISHED, exitStatus, out, err);
        } catch (InterruptedException e) {
            return new CommandResult(commandLine, process, CommandResult.STATE_INTERRUPTED, -1, "", "");
        }
    }


    @Override
    public CommandResult collect(Command command) {
        return run(command, new Collector(), new Collector());
    }

    static class ConsumeOutput extends Thread {
        private final Consumer consumer;
        private final BufferedReader reader;

        public ConsumeOutput(Consumer consumer, InputStream is) {
            this.consumer = consumer;
            reader = new BufferedReader(new InputStreamReader(is));
        }

        @Override
        public void run() {
            try {
                char[] buffer = new char[8 * 1024];
                consumer.begin();
                int chars = reader.read(buffer);
                while (chars >= 0) {
                    consumer.consume(buffer, chars);
                    chars = reader.read(buffer);
                }
            } catch (IOException e) {
                // Do nothing
            }
        }
    }

    static class Collector implements Consumer {
        private final StringWriter out = new StringWriter();

        @Override
        public void begin() {
        }

        @Override
        public void consume(char[] buffer, int chars) {
            out.write(buffer, 0, chars);
        }

        @Override
        public String end() {
            try {
                out.close();
            } catch (Exception e) {
                // Do nothing
            }
            return out.toString();

        }
    }
}
