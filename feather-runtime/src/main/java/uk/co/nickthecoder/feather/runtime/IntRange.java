package uk.co.nickthecoder.feather.runtime;

public final class IntRange extends IntProgression {

    public static final IntRange EMPTY = new IntRange(1, 0);

    public IntRange(int start, int endInclusive) {
        super(start, endInclusive, 1);
    }

    public static IntRange exclusiveRange(int start, int endInclusive) {
        if (endInclusive == Integer.MIN_VALUE) return EMPTY;
        return new IntRange(start, endInclusive - 1);
    }

    public boolean contains(int value) {
        return start <= value && value <= endInclusive;
    }

    public IntProgression step(int step) {
        return new IntProgression(start, endInclusive, step);
    }

    public IntProgression backwards() {
        return new IntProgression(start, endInclusive, -1);
    }
}
