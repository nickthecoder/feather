package uk.co.nickthecoder.feather.runtime;

/**
 * A class that hold two values, the first of generic type F, the second of generic type S.
 * Duo are immutable.
 *
 * I would like to call this Pair, but I don't want a name clash with Kotlin.
 * I also don't want Feather's runtime to depend on Kotlin's runtime, so
 * I cannot use Kotlin's Pair.
 * Therefore Feather has Duo.
 * @param <F> The first type
 * @param <S> The second type
 */
final public class Duo<F, S>  {

    public Duo(F first, S second ) {
        this.first = first;
        this.second = second;
    }

    final F first;
    final S second;

}
