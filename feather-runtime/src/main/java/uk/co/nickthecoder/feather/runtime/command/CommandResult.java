package uk.co.nickthecoder.feather.runtime.command;

public class CommandResult {

    public static final int STATE_FAILED = 1;
    public static final int STATE_INTERRUPTED = 2;
    public static final int STATE_FINISHED = 2;

    public final String commandLine;
    public final Process process;
    public final int state;
    public final int exitStatus;
    public final String out;
    public final String err;

    public CommandResult(String commandLine, Process process, int state, int exitStatus, String out, String err) {
        this.commandLine = commandLine;
        this.process = process;
        this.state = state;
        this.exitStatus = exitStatus;
        this.out = out;
        this.err = err;
    }

    public boolean ok() {
        return state == STATE_FINISHED && exitStatus == 0;
    }
}
