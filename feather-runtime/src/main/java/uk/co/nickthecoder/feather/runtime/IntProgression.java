package uk.co.nickthecoder.feather.runtime;

import java.util.Iterator;
import java.util.Objects;

public class IntProgression implements Iterable<Integer> {

    public final int start;
    public final int endInclusive;
    public final int step;

    public IntProgression(int start, int endInclusive, int step) {
        if (step == 0) throw new IllegalArgumentException("Step cannot be zero");
        this.start = start;
        this.endInclusive = endInclusive;
        this.step = step;
    }

    public IntProgression step(int step) {
        if (this.step < 0) {
            return new IntProgression(start, endInclusive, -step);
        } else {
            return new IntProgression(start, endInclusive, step);
        }
    }

    public boolean isEmpty() {
        if (step < 0) {
            return start < endInclusive;
        } else {
            return start > endInclusive;
        }
    }

    public boolean equals(Object other) {
        if (other instanceof IntProgression) {
            IntProgression otherProgression = (IntProgression) other;
            return (isEmpty() && (otherProgression).isEmpty()) ||
                    (start == otherProgression.start &&
                            endInclusive == otherProgression.endInclusive &&
                            step == otherProgression.step
                    );
        } else {
            return false;
        }
    }

    public int hashCode() {
        if (isEmpty()) return -1;
        return Objects.hash(start, endInclusive, step);
    }

    public String toString() {
        if (step == 1) {
            return start + ".." + endInclusive;
        } else {
            return start + ".." + endInclusive + " step " + step;
        }
    }

    @Override
    public Iterator<Integer> iterator() {
        return new IntProgressionIterator();
    }

    private class IntProgressionIterator implements Iterator<Integer> {

        private int next = start;

        public boolean hasNext() {
            return step < 0 ? next >= endInclusive : next <= endInclusive;
        }

        public Integer next() {
            int result = next;
            next += step;
            return result;
        }
    }
}
