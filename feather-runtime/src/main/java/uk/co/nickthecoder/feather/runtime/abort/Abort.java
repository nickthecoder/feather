package uk.co.nickthecoder.feather.runtime.abort;

/**
 * To end a script, call [abort]. This will throw an [Aborted] exception.
 * It is up to the scripts caller to catch this exception, and handle it appropriately.
 *
 * I recommend you use the same convention as Java's system.exit
 * i.e. A non-zero exit status implies an error.
 *
 */
public class Abort {
    public static void abort() throws Aborted {
        throw new Aborted(0);
    }

    public static void abort(int status) throws Aborted {
        throw new Aborted(status);
    }
}
