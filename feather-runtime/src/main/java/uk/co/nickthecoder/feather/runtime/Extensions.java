package uk.co.nickthecoder.feather.runtime;

import kotlin.text.Charsets;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;

public class Extensions {

    public static boolean isUpperCase(char c) {
        return Character.isUpperCase(c);
    }

    public static boolean isLowerCase(char c) {
        return Character.isLowerCase(c);
    }

    public static char toUpperCase(char c) {
        return Character.toUpperCase(c);
    }

    public static char toLowerCase(char c) {
        return Character.toLowerCase(c);
    }

    public static boolean isAlphabetic(char c) {
        return Character.isAlphabetic(c);
    }

    public static boolean isDigit(char c) {
        return Character.isDigit(c);
    }

    public static boolean isLetter(char c) {
        return Character.isLetter(c);
    }

    public static boolean isLetterOrDigit(char c) {
        return Character.isLetterOrDigit(c);
    }

    /**
     * This is DIFFERENT from Java's Character.isWhiteSpace.
     * Java considers 160 (non breaking space) NOT to be whitespace.
     * I've chosen for feather to go against this, and consider it to be whitespace.
     * So Feather is consistent with Kotlin (and Unicode apparently), and IMHO common sense!
     */
    public static boolean isWhitespace(char c) {
        return Character.isWhitespace(c) || Character.isSpaceChar(c);
    }

    public static boolean isSpaceChar(char c) {
        return Character.isSpaceChar(c);
    }

    public static String toString(char c) {
        return Character.toString(c);
    }


    public static int toInt(double value) {
        return (int) value;
    }

    public static int toInt(float value) {
        return (int) value;
    }

    public static int toInt(String str) {
        return Integer.parseInt(str);
    }

    public static int toInt(String str, int defaultValue) {
        try {
            return Integer.parseInt(str);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static long toLong(double value) {
        return (long) value;
    }

    public static long toLong(float value) {
        return (long) value;
    }

    public static long toLong(String str) {
        return Long.parseLong(str);
    }

    public static long toLong(String str, long defaultValue) {
        try {
            return Long.parseLong(str);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static float toFloat(String str) {
        return Float.parseFloat(str);
    }

    public static float toFloat(String str, float defaultValue) {
        try {
            return Float.parseFloat(str);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static double toDouble(String str) {
        return Double.parseDouble(str);
    }

    public static double toDouble(String str, double defaultValue) {
        try {
            return Double.parseDouble(str);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static String times(String str, int times) {
        return String.valueOf(str).repeat(Math.max(0, times));
    }

    public static String times(char c, int times) {
        return String.valueOf(c).repeat(Math.max(0, times));
    }

    public static List<String> splitLines(String str) {
        return splitTerminator(str, "\n");
    }

    public static List<String> splitTerminator(String str, String regex) {
        if (str.length() == 0) {
            return new ArrayList<>();
        } else {
            if (str.endsWith(regex)) {
                return Arrays.asList(str.substring(0, str.length() - regex.length()).split(regex));
            } else {
                return Arrays.asList(str.split(regex));
            }
        }
    }

    /**
     * Allows for loops to iterate over CharSequences.
     */
    public static CharSequenceIterator iterator(CharSequence cs) {
        return new CharSequenceIterator(cs);
    }


    public static IntRange rangeTo(int start, int endInclusive) {
        return new IntRange(start, endInclusive);
    }

    public static IntRange until(int start, int endExclusive) {
        return IntRange.exclusiveRange(start, endExclusive);
    }

    public static IntProgression downTo(int start, int endInclusive) {
        return new IntProgression(start, endInclusive, -1);
    }

    /**
     * Creates an IntRange from start to endInclusive if endInclusive >= start,
     * otherwise it creates a downwards IntProgression (with step -1).
     * Note, there is no similar method which uses an exclusive end.
     */
    public static IntProgression to(int start, int endInclusive) {
        if (endInclusive >= start) {
            return new IntRange(start, endInclusive);
        } else {
            return new IntProgression(start, endInclusive, -1);
        }
    }

    public static CharRange rangeTo(char start, char endInclusive) {
        return new CharRange(start, endInclusive);
    }

    public static CharRange until(char start, char endExclusive) {
        return CharRange.exclusiveRange(start, endExclusive);
    }

    public static CharProgression downTo(char start, char endInclusive) {
        return new CharProgression(start, endInclusive, -1);
    }

    /**
     * Creates a CharRange from start to endInclusive if endInclusive >= start,
     * otherwise it creates a downwards CharProgression (with step -1).
     * Note, there is no similar method which uses an exclusive end.
     */
    public static CharProgression to(char start, char endInclusive) {
        if (start >= endInclusive) {
            return new CharRange(start, endInclusive);
        } else {
            return new CharProgression(start, endInclusive, -1);
        }
    }

    public static LongRange rangeTo(long start, long endInclusive) {
        return new LongRange(start, endInclusive);
    }

    public static LongRange until(long start, long endExclusive) {
        return LongRange.exclusiveRange(start, endExclusive);
    }

    public static LongProgression downTo(long start, long endInclusive) {
        return new LongProgression(start, endInclusive, -1);
    }

    /**
     * Creates a LongRange from start to endInclusive if endInclusive >= start,
     * otherwise it creates a downwards LongProgression (with step -1).
     * Note, there is no similar method which uses an exclusive end.
     */
    public static LongProgression to(long start, long endInclusive) {
        if (start >= endInclusive) {
            return new LongRange(start, endInclusive);
        } else {
            return new LongProgression(start, endInclusive, -1);
        }
    }


    /**
     * This is useful if you wish to iterate over the index values of a list.
     */
    public static IntRange indices(List<?> list) {
        return new IntRange(0, list.size() - 1);
    }

    public static IntRange indices(boolean[] list) {
        return new IntRange(0, list.length - 1);
    }

    public static IntRange indices(char[] list) {
        return new IntRange(0, list.length - 1);
    }

    public static IntRange indices(byte[] list) {
        return new IntRange(0, list.length - 1);
    }

    public static IntRange indices(short[] list) {
        return new IntRange(0, list.length - 1);
    }

    public static IntRange indices(int[] list) {
        return new IntRange(0, list.length - 1);
    }

    public static IntRange indices(long[] list) {
        return new IntRange(0, list.length - 1);
    }

    public static IntRange indices(float[] list) {
        return new IntRange(0, list.length - 1);
    }

    public static IntRange indices(double[] list) {
        return new IntRange(0, list.length - 1);
    }

    public static <T> IntRange indices(T[] list) {
        return new IntRange(0, list.length - 1);
    }

    /*
     * Creates a array using vararg parameters.
     * This doesn't do what I want - it creates an array of Object, not an array of T.
     * This has knock-on consequences.
     * Therefore, I have implemented it directly in byte code. See ArrayOf classs.

    @SafeVarargs
    public static <T> T[] arrayOf(T... items) {
        return items;
    }
    */

    public static boolean[] arrayOf(boolean... items) {
        return items;
    }
    public static char[] charArrayOf(char... items) {
        return items;
    }
    public static short[] shortArrayOf(short... items) {
        return items;
    }
    public static int[] intArrayOf(int... items) {
        return items;
    }
    public static long[] longArrayOf(long... items) {
        return items;
    }
    public static float[] floatArrayOf(float... items) {
        return items;
    }
    public static double[] doubleArrayOf(double... items) {
        return items;
    }


    /**
     * Create a 1D array (using an ArrayList).
     * <p>
     * The size of the 1d array is n.
     */
    public static <T> List<T> arrayList(int n) {
        List<T> result = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            result.add(null);
        }
        return result;
    }

    /**
     * Create a 2D array (using ArrayLists). The result is an array list of columns,
     * where each item in each column is null.
     * <p>
     * The size of the 2d array is (x,y)
     */
    public static <T> List<List<T>> arrayList(int x, int y) {
        List<List<T>> result = new ArrayList<>(x);
        for (int i = 0; i < x; i++) {
            List<T> column = new ArrayList<>(y);
            for (int j = 0; j < y; j++) {
                column.add(null);
            }
            result.add(column);
        }
        return result;
    }

    /**
     * Create a 3D array (using ArrayLists). The result is an array list of columns,
     * where each item in each column is null.
     * <p>
     * The size of the 3d array is (x,y,z).
     */
    public static <T> List<List<List<T>>> arrayList(int x, int y, int z) {
        List<List<List<T>>> result = new ArrayList<>(x);
        for (int i = 0; i < x; i++) {
            List<List<T>> slice = new ArrayList<>(y);
            for (int j = 0; j < y; y++) {
                List<T> bar = new ArrayList<>(z);
                for (int k = 0; k < z; k++) {
                    bar.add(null);
                }
                slice.add(bar);
            }
            result.add(slice);
        }
        return result;
    }

    /**
     * Creates a mutable List containing the items specified.
     */
    @SafeVarargs
    public static <T> List<T> listOf(T... items) {
        return new ArrayList<>(Arrays.asList(items));
    }

    /**
     * Creates a mutable Set containing the items specified.
     */
    @SafeVarargs
    public static <T> Set<T> setOf(T... items) {
        return new HashSet<>(Arrays.asList(items));
    }

    /**
     * Creates a mutable Map containing the items specified.
     */
    @SafeVarargs
    public static <K, V> Map<K, V> mapOf(Duo<K, V>... entries) {
        HashMap<K, V> result = new HashMap<>();
        for (Duo<K, V> entry : entries) {
            result.put(entry.first, entry.second);
        }
        return result;
    }

    /**
     * Use as an infix function within a call to mapOf.
     * Alas, at the time of writing this, Feather cannot guess the type parameters,
     * so you cannot do : mapOf( "Hello" to "world", "Hi", "all" )
     * But that is the plan!
     * NOTE. Feather currently has an infix operator => which performs the role.
     */
    public static <K, V> Duo<K, V> to(K key, V value) {
        return new Duo<>(key, value);
    }

    public static <T> T[] toArray(Collection<T> collection) {
        @SuppressWarnings("unchecked")
        T[] result = (T[]) collection.toArray();
        return result;
    }

    /**
     * Returns a copy of the array as a list.
     * <p>
     * This is not supported for arrays of primitive data types.
     */
    public static <T> List<T> toList(T[] array) {
        return new ArrayList<>(Arrays.asList(array));
    }

    public static <T> List<T> toList(Collection<T> collection) {
        return new ArrayList<>(collection);
    }

    /**
     * Creates a new list, with the items in reverse order.
     */
    public static <T> List<T> reversed(List<T> list) {
        List<T> newList = toList(list);
        Collections.reverse(newList);
        return newList;
    }

    public static <T> Set<T> toSet(Collection<T> collection) {
        return new HashSet<>(collection);
    }

    public static List<Integer> toList(int[] array) {
        List<Integer> result = new ArrayList<>(array.length);
        for (int e : array) {
            result.add(e);
        }
        return result;
    }

    public static boolean[] toBooleanArray(Collection<Boolean> list) {
        boolean[] result = new boolean[list.size()];
        int index = 0;
        for (Boolean o : list) {
            result[index] = o;
            index++;
        }
        return result;
    }

    public static char[] toCharArray(Collection<Character> list) {
        char[] result = new char[list.size()];
        int index = 0;
        for (Character o : list) {
            result[index] = o;
            index++;
        }
        return result;
    }

    public static short[] toShortArray(Collection<Short> list) {
        short[] result = new short[list.size()];
        int index = 0;
        for (Short o : list) {
            result[index] = o;
            index++;
        }
        return result;
    }

    public static int[] toIntArray(Collection<Integer> list) {
        int[] result = new int[list.size()];
        int index = 0;
        for (Integer o : list) {
            result[index] = o;
            index++;
        }
        return result;
    }

    public static long[] toLongArray(Collection<Long> list) {
        long[] result = new long[list.size()];
        int index = 0;
        for (Long o : list) {
            result[index] = o;
            index++;
        }
        return result;
    }

    public static float[] toFloatArray(Collection<Float> list) {
        float[] result = new float[list.size()];
        int index = 0;
        for (Float o : list) {
            result[index] = o;
            index++;
        }
        return result;
    }

    public static double[] toDoubleArray(Collection<Double> list) {
        double[] result = new double[list.size()];
        int index = 0;
        for (Double o : list) {
            result[index] = o;
            index++;
        }
        return result;
    }

    // java.util.File  extension functions

    public static List<File> dir(File dir) {
        File[] result = dir.listFiles();
        if (result == null) return new ArrayList<>();
        Arrays.sort(result);
        return Arrays.asList(result);
    }

    public static String extension(File file) {
        String name = file.getName();
        int dot = name.lastIndexOf(".");
        if (dot > 0) {
            return name.substring(dot + 1);
        } else {
            return "";
        }
    }

    public static String nameWithoutExtension(File file) {
        String name = file.getName();
        int dot = name.lastIndexOf(".");
        if (dot > 0) {
            return name.substring(0, dot);
        } else {
            return name;
        }
    }


    public static String pathWithoutExtension(File file) {
        String p = file.getParent();
        if (p == null) {
            return nameWithoutExtension(file);
        } else {
            return p + File.separator + nameWithoutExtension(file);
        }
    }

    // Note, there's a bug in Java 7 on Windows, so upgrade to Java 8 or above.
    public static File homeDirectory() {
        return new File(java.lang.System.getProperty("user.home"));
    }

    public static String readText(File file) throws IOException {
        return readText(file, Charsets.UTF_8);
    }

    public static String readText(File file, Charset charset) throws IOException {
        try (Reader reader = new InputStreamReader(new FileInputStream(file))) {
            StringWriter buffer = new StringWriter();
            char[] cArray = new char[1024];
            var chars = reader.read(cArray, 0, cArray.length);
            while (chars >= 0) {
                buffer.write(cArray, 0, chars);
                chars = reader.read(cArray, 0, cArray.length);
            }
            return buffer.toString();
        }
    }

    public static BufferedReader reader( InputStream in ) {
        return new BufferedReader(new InputStreamReader(in));
    }

    public static void writeText(File file, String str) throws IOException {
        writeText(file, str, Charsets.UTF_8);
    }

    public static void writeText(File file, String str, Charset charset) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(str.getBytes(Charsets.UTF_8));
        }
    }

}
