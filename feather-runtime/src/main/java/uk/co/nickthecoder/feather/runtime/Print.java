package uk.co.nickthecoder.feather.runtime;

public class Print {

    private Print() {
    }

    public static void println() {
        java.lang.System.out.println();
    }

    public static void println(Object obj) {
        java.lang.System.out.println(obj);
    }

    public static void print(Object obj) {
        java.lang.System.out.print(obj);
    }

    public static void println(String str) {
        java.lang.System.out.println(str);
    }

    public static void print(String str) {
        java.lang.System.out.print(str);
    }

}
