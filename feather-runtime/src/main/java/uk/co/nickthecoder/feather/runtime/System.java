package uk.co.nickthecoder.feather.runtime;

import java.io.InputStream;
import java.io.PrintStream;

/**
 * A version of java.reflect.System, which excludes fields/methods which could be considered dangerous.
 */
public class System {

    private System() {
    }

    public static InputStream getIn() {
        return java.lang.System.in;
    }

    public static PrintStream getOut() {
        return java.lang.System.out;
    }

    public static PrintStream getErr() {
        return java.lang.System.err;
    }

    public static long currentTimeMillis() {
        return java.lang.System.currentTimeMillis();
    }

    public static long nanoTime() {
        return java.lang.System.nanoTime();
    }

    public static String lineSeparator() {
        return java.lang.System.lineSeparator();
    }

}
