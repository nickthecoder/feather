package uk.co.nickthecoder.feather.runtime.command;

/**
 * Consumes output from a Command.
 * The value of CommandResult.out (or CommandResult.err) is this.toString()
 */
public interface Consumer {
    void begin();

    void consume(char[] buffer, int chars);

    /**
     * Called after the process has ended.
     * The String return value is placed into CommandResult.out (or .err).
     *
     * @return An empty string, or a representation of the command's output.
     */
    String end();
}
