package uk.co.nickthecoder.feather.runtime.command;

/**
 * The command line string is build by repeatedly calling appendText and/or append, followed by toString().
 * This is similar in style to a StringBuilder (and concrete implementation will probably use a StringBUilder),
 * but this has one more trick, it can automatically escape special characters.
 *
 */
public interface CommandLineBuilder {

    String buildCommandLine( Command command );

}
