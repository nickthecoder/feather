package uk.co.nickthecoder.feather.runtime.abort;

public class Aborted extends Throwable {

    int exitStatus;

    Aborted(int exitStatus) {
        this.exitStatus = exitStatus;
    }
}
