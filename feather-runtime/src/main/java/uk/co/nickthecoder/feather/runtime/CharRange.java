package uk.co.nickthecoder.feather.runtime;

public class CharRange extends CharProgression {

    public static final CharRange EMPTY = new CharRange('b', 'a');

    public CharRange(char start, char endInclusive) {
        super(start, endInclusive, 1);
    }

    public static CharRange exclusiveRange(char start, char endInclusive) {
        if (endInclusive == Character.MIN_VALUE) return EMPTY;
        return new CharRange(start, (char) (endInclusive - 1));
    }

    public boolean contains(char value) {
        return start <= value && value <= endInclusive;
    }

    public CharProgression step(int step) {
        return new CharProgression(start, endInclusive, step);
    }

    public CharProgression backwards() {
        return new CharProgression(start, endInclusive, -1);
    }

}
