package uk.co.nickthecoder.feather.runtime;

import java.util.Iterator;

/**
 * Allows a Feather for loop to iterate over CharSequences (which includes Strings).
 */
public class CharSequenceIterator implements Iterator<Character> {

    private CharSequence cs;
    private int index = 0;

    public CharSequenceIterator(CharSequence cs) {
        this.cs = cs;
    }

    public boolean hasNext() {
        return index < cs.length();
    }

    public Character next() {
        return cs.charAt(index++);
    }

}
