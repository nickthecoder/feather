package uk.co.nickthecoder.feather.runtime.command;

/**
 * See [Command] for details on how to choose the CommandRunner, and how the [DefaultCommandRunner] behaves.
 */
public interface CommandRunner {

    public CommandResult run(Command command, Consumer outConsumer, Consumer errConsumer);

    public CommandResult run(Command command);

    public CommandResult collect(Command command);

    public String commandLine( Command command );

}
