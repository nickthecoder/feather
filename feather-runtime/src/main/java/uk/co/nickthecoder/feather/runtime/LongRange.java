package uk.co.nickthecoder.feather.runtime;

public class LongRange extends LongProgression {

    public static final LongRange EMPTY = new LongRange(1, 0);

    public LongRange(long start, long endInclusive) {
        super(start, endInclusive, 1);
    }

    public static LongRange exclusiveRange(long start, long endInclusive) {
        if (endInclusive == Long.MIN_VALUE) return EMPTY;
        return new LongRange(start, endInclusive - 1);
    }

    public boolean contains(long value) {
        return start <= value && value <= endInclusive;
    }

    public LongProgression step(long step) {
        return new LongProgression(start, endInclusive, step);
    }

    public LongProgression backwards() {
        return new LongProgression(start, endInclusive, -1);
    }

}
