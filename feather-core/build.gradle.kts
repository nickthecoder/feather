// feather-core

dependencies {
    val antlrVersion: String by project
    val asmVersion: String by project
    val xbeanVersion: String by project
    val junitVersion: String by project

    implementation(project(":feather-runtime"))

    // Run createFeathers whenever dependencies are changed
    implementation("org.antlr:antlr4-runtime:$antlrVersion")
    implementation("org.ow2.asm:asm:$asmVersion")
    implementation("org.apache.xbean:xbean-classloader:$xbeanVersion")

    testImplementation("junit:junit:$junitVersion")
    testImplementation("org.ow2.asm:asm-util:$asmVersion")
}

// For uploading maven artifacts to gitlab.com
publishing {
    publications {
        create<MavenPublication>("feather-core") {
            from(components["kotlin"])
        }
    }
    repositories {
        maven {
            // 16531834 is the GitLab project ID of project nickthecoder/feather
            url = uri("https://gitlab.com/api/v4/projects/16531834/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = providers.gradleProperty("gitLabPrivateToken").get()
                // The password is stored in ~/.gradle/gradle.properties
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}
