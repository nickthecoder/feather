package uk.co.nickthecoder.feather

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.feather.helper.compareByteCode

//internal fun asmName(name: String) = name.replace('.', '/')

/**
 * Compare the byte code of a compiled Java class with the equivalent feather class.
 * For simple language features, I should be able to make them identical!
 *
 * Note, if you are using a different Java compiler to the one I tested with, then it is possible
 * that some tests will fail. This is because different Java compilers could generate different
 * byte code form the same source code.
 *
 * For example, should stringA + stringB use String.concat(String) or use StringBuilder?
 * (my current JavaCompiler always uses StringBuilder, even when concatenating only two strings,
 * which seems sub-optimal to me!)
 */
class TestCompareByteCode : TestCase() {

    fun testEmpty() {
        compareByteCode("Empty")
    }

    fun testAbstractClass() {
        compareByteCode("AbstractClass")
    }

    fun testConstants() {
        compareByteCode("Constants")
    }

    fun testArguments() {
        compareByteCode("Arguments")
    }

    fun testMaths() {
        compareByteCode("Maths")
    }

    fun testComparisons() {
        compareByteCode("Comparisons")
    }
/*
    fun testStrings() {
        compareByteCode("Strings")
    }
*/

    fun testMisc() {
        compareByteCode("Misc")
    }

    fun testImplements() {
        compareByteCode("Implements")
    }

    fun testExtends() {
        compareByteCode("Extends")
    }

    fun testExtendsInterface() {
        compareByteCode("ExtendsInterface")
    }

    fun testWhile() {
        compareByteCode("While")
    }

    fun testDoWhile() {
        compareByteCode("DoWhile")
    }

    fun testIf() {
        compareByteCode("If")
    }

    fun testContinue() {
        compareByteCode("Continue")
    }

    fun testBreak() {
        compareByteCode("Break")
    }

    fun testAssignments() {
        compareByteCode("Assignments")
    }

    fun testCallConstructor() {
        compareByteCode("CallConstructor")
    }

    fun testThis() {
        compareByteCode("This")
        compareByteCode("ThisB")
        compareByteCode("ThisC")
    }

    fun testInterfaceMethod() {
        compareByteCode("InterfaceMethod")
    }

    fun testRanges() {
        compareByteCode("Ranges")
    }

    fun testLogic() {
        compareByteCode("Logic")
    }

    fun testIs() {
        compareByteCode("Is")
    }

    fun testAs() {
        compareByteCode("As")
    }

    fun testIncrement() {
        compareByteCode("Increment")
    }

    fun testImportStatic() {
        compareByteCode("ImportStatic")
    }

    fun testImportStaticStar() {
        compareByteCode("ImportStaticStar")
    }

    fun testMain() {
        compareByteCode("Main")
    }

    fun testExtendsAbstract() {
        compareByteCode("ExtendsAbstract")
    }

    fun testGetter() {
        compareByteCode("Getter")
    }

    fun testThrow() {
        compareByteCode("Throw")
    }

    fun testIntRangeIterator() {
        compareByteCode("IntRangeIterator")
    }

    fun testArray() {
        compareByteCode("Array")
    }

    fun testVarArg() {
        compareByteCode("VarArg")
    }

    fun testNarrower() {
        compareByteCode("Narrower")
    }

    fun testNarrower2() {
        compareByteCode("Narrower2")
    }

    fun testAnnotation() {
        compareByteCode("Annotation")
    }

    fun testFails() {
        compareByteCode("Fails")
    }

    fun testImpliedStatic() {
        compareByteCode("ImpliedStatic")
    }

    fun testClassConstant() {
        compareByteCode("ClassConstant")
    }

    fun testStaticInterface() {
        compareByteCode("StaticInterface")
    }

    /**
     * Test Feather implementing a Java interface, which has a default method implementation.
     * Note that Java and Kotlin do this differently.
     *
     * It seems that there is no special code required in the base class, when implementing a Java interface,
     * which has a default implementation. Hmm.
     */
    fun testImplementDefault() {
        compareByteCode("ImplementDefault")
    }

    /**
     * Test Feather implementing a Kotlin interface, which has a default method implementation.
     * Note that Java and Kotlin do this differently.
     *
     * A Kotlin interface has its default implementations as a static method within another class.
     */
    fun testImplementDefault2() {
        // Alas, we can't do this test easily, because Kotlin orders the methods differently from Java (and Feather),
        // and it also includes some additional metadata, which will cause the "diff" to fail.

        //compareByteCode("ImplementDefault2")
    }

    /**
     * A basic enum
     */
    fun testEnum() {
        compareByteCode("Enum")
    }

    /**
     * An enum with a single field
     */
    fun testEnum2() {
        compareByteCode("Enum2")
    }

}
