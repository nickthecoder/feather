package uk.co.nickthecoder.feather.internal.keywords

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectThrowAt

class TestThrow : TestCase() {

    fun testSingleThrow() {
        expectThrowAt(listOf(7), """
            import java.lang.*
            
            class Foo() {
            
                fun test() {
                
                    throw IllegalStateException( "Bye" )
                }
            }
            """.trimIndent()
        )
    }
}

