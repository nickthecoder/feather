package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.Rock
import uk.co.nickthecoder.feather.helper.expectErrorAt
import uk.co.nickthecoder.feather.helper.expectValue
import uk.co.nickthecoder.feather.helper.expectValueNoSandbox
import uk.co.nickthecoder.feather.internal.resolveGenericType
import uk.co.nickthecoder.feather.internal.special.MethodWrapper

/**
 * This is a simplified version of real-world code, which Feather could not call.
 * It came from Tickle's Stage.findRolesByType
 *
 * See testFindByType
 */
class TestFindByType {
    val objects = listOf<Any>("Hello", "World", 1.0, 1.1, 2, 3)

    fun <T : CharSequence> findByType(type: Class<T>): List<T> {
        @Suppress("UNCHECKED_CAST")
        return objects.filter { type.isInstance(it) }.map { it as T }
    }
}

class TestGenerics : TestCase() {

    private fun <T> foo() = ArrayList<ArrayList<T>>()
    fun testResolveGenericType() {
        val method = this.javaClass.getDeclaredMethod("foo")
        val methodWrapper = MethodWrapper(method)
        val typeArgs = listOf(String::class.java)
        val returnType = methodWrapper.genericReturnType()
        val resolvedReturnType = resolveGenericType(methodWrapper, typeArgs, returnType)
        assertEquals("java.util.ArrayList<java.util.ArrayList<java.lang.String>>", resolvedReturnType.toString())
    }

    fun testIntRangeIterator() {

        expectValue(
            3, """
            class Foo() {
                fun test() : int {
                    var range = 2..10
                    var iterator = range.iterator() // Local variable is Iterator<Integer>
                    if ( iterator.hasNext() ) {
                        return iterator.next() + 1 // next() returns Integer, auto cast to int.
                    }
                    return 0
                }
            }
            """.trimIndent()
        )
    }

    fun testExample0() {

        expectValue(3.0, """
            class Foo() {
                fun test() : double {
                    val doubleExample = GenericExample.doubleExampleA
                    val v = doubleExample.get()
                    return v
                }
            }
            """.trimIndent()
        )
    }

    fun testExample1() {
        expectValue(3.0, """
            class Foo() {
                fun test() : double {
                    val doubleExample = GenericExample.doubleExampleA
                    val v = doubleExample.value
                    return v
                }
            }
            """.trimIndent()
        )

    }

    fun testExample2() {

        expectValue(14.0, """
            class Foo() {
                fun test() : double {
                    val doubleExample = GenericExample.doubleExampleB
                    doubleExample.set(14)
                    return doubleExample.value
                }
            }
            """.trimIndent()
        )
    }

    fun testExample3() {
        expectValue(13.0, """
            class Foo() {
                fun test() : double {
                    val doubleExample = GenericExample.doubleExampleB
                    doubleExample.value = 13
                    return doubleExample.value
                }
            }
            """.trimIndent()
        )
    }

    fun testListOfString() {
        expectValue("Hello", """
            import java.util.*
            class Foo() {
                fun test() : String {
                    val list = ArrayList<String>()
                    list.add( "Hello" )
                    return list.get(0)
                }
            }
            """.trimIndent()
        )
    }

    fun testMissingTypeArguments() {
        expectErrorAt(3, 19, "Wrong number of type parameters", """
            import java.util.*
            class Foo() {
                fun test() : String {
                    val list = ArrayList()
                    list.add( "Hello" )
                    return list.get(0)
                }
            }
            """.trimIndent()
        )
    }

    fun testDeclareListOfString() {
        expectValue(ArrayList<String>(), """
            import java.util.*
            class Foo() {
                var foo : List<String> = ArrayList<String>()
                fun test() : List<String> {
                    return foo
                }
            }
            """.trimIndent()
        )
    }

    fun testDeclareLocalListOfString() {
        expectValue(ArrayList<String>(), """
            import java.util.*
            class Foo() {
                fun test() : List<String> {
                    var foo : List<String> = ArrayList<String>()
                    return foo
                }
            }
            """.trimIndent()
        )
    }

    fun testConstructor() {
        expectValue(2, """
            import java.util.*
            class Foo( val list : List<String> ) {
                fun foo() = list.size()
            }
            class Test {
                fun test() = Foo( listOf<String>("Hello", "World") ).foo()
            }
            """.trimIndent()
        )
    }
    // TODO Check that List<String> isn't compatible with List<Int> etc.

    /**
     * This one worked - so we can CALL findByType
     */
    fun testFindByType1() {
        expectValueNoSandbox("[Hello, World]", """
            import uk.co.nickthecoder.feather.TestFindByType
            class Test {
                fun test() : String {
                    return TestFindByType().findByType<String>(String).toString()
                }
            }
            """.trimIndent()
        )
    }

    /**
     * This one failed - the return type wasn't evaluated correctly???
     *
     * Not a Class nor a ParameterizedType : T : class sun.reflect.generics.reflectiveObjects.TypeVariableImpl
     */
     fun testFindByType2() {
        expectValueNoSandbox("Hello,World,", """
            import uk.co.nickthecoder.feather.TestFindByType
            import java.lang.StringBuilder
            class Test {
                fun test() : String {
                    val buffer = StringBuilder()
                    for( item in TestFindByType().findByType<String>(String) ) {
                        buffer.append( item.toString() )
                        buffer.append(",")
                    }
                    return buffer.toString()
                }
            }
            """.trimIndent()
        )
    }



    /**
     * A real-world test which failed. Not failing now. Maybe I missed the type parameter???
     */
    fun testRoleByClass() {
        expectValueNoSandbox(
            1, """
            import uk.co.nickthecoder.feather.helper.*
            import java.util.*
            class Test {
                fun test() : Object {
                    var count = 0
                    val roles = Stage().findRolesByClass<Rock>( Rock )
                    for (role in roles) {
                        count ++
                    }
                    return count
                }
            }
            """.trimIndent()
        )
        expectValueNoSandbox(
        2, """
            import uk.co.nickthecoder.feather.helper.*
            import java.util.*
            class Test {
                fun test() : Object {
                    var count = 0
                    val roles = Stage().findRolesByClass<Ship>( Ship )
                    for (role in roles) {
                        count ++
                    }
                    return count
                }
            }
            """.trimIndent()
        )
        expectValueNoSandbox(
            "Rock", """
            import uk.co.nickthecoder.feather.helper.*
            import java.util.*
            class Test {
                fun test() : Object {
                    return Stage().findRoleByClass<Rock>( Rock ).getClass().simpleName
                }
            }
            """.trimIndent()
        )
    }


    /**
     * As above, but this time, we are looking for instances of MetaDataType.
     */
    fun testRoleByMetaDataType() {
        expectValue(
            "a Bar", """
            class Bar() : Role() {
                override fun toString() = "a Bar"
            }
            
            class Foo() {
                fun test() : String {
                    val stage = Stage()
                    stage.roles.add( Bar() )
                    
                    return stage.findRoleByClass<Bar>( Bar ).toString()
                    //for ( gate in stage.findRolesByClass<Bar>( Bar ) ) {
                    //    println( "Found one" )
                    //}
                }
            }
            """.trimIndent()
        )

        expectValue(
            "a Bar", """
            class Bar() : Role() {
                override fun toString() = "a Bar"
            }
            
            class Foo() {
                fun test() : String {
                    val stage = Stage()
                    stage.roles.add( Bar() )
                    
                    for (role in stage.findRolesByClass<Bar>( Bar ) ) {
                        return role.toString()
                    }
                    return "Not found"
                }
            }
            """.trimIndent()
        )

        // Same again, but Bar is defined in a later file.
        expectValue(
            "a Bar", """
           
            class Foo() {
                fun test() : String {
                    val stage = Stage()
                    stage.roles.add( Bar() )
                    
                    for (role in stage.findRolesByClass<Bar>( Bar ) ) {
                        return role.toString()
                    }
                    return "Not found"
                }
            }
            """.trimIndent(),
            """
            class Bar() : Role() {
                override fun toString() = "a Bar"
            }
            """.trimIndent()

        )
    }

    fun testListItem() {
        expectValue(
            "a Bar", """
           
            class Foo() {
                fun test() : String {
                    val items = listOf<Bar>(Bar())
                    return Rand.listItem<Bar>( items ).toString()
                }
            }
            """.trimIndent(),
            """
            class Bar() : Role() {
                override fun toString() = "a Bar"
            }
            """.trimIndent()

        )
    }

}
