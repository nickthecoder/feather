package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectError
import uk.co.nickthecoder.feather.helper.expectErrorAt

/**
 * Here we have various "wrong" scripts, and how we expect Feather to report them.
 */
class TestSyntaxErrors : TestCase() {

    fun testKeywordAsVarName() {

        expectErrorAt(
            2, 12, "mismatched input 'class",
            """
            class Foo() {
                fun test() {
                    val class = 1
                }
            }
            """.trimIndent()
        )
    }

    /**
     * While creating generics, I left off a close bracket, which gave an unexpected exception.
     * Index -1 out of bounds for length 0 (popping in exitForExpression).
     * We were popping off an empty stack!
     *
     * Now, the reported error is in a weird place, but I don't control that. Antrl does.
     * (end of the line containing "for"
     */
    fun testUnclosedParen() {
        expectError(
            "mismatched input", """
            import uk.co.nickthecoder.feather.TestFindByType
            class Test {
                fun test() : String {
                    val buffer = StringBuild()
                    for( item in TestFindByType().findByType<String>(String) ) {
                        buffer.append( item.toString() )
                        buffer.append(","
                    }
                    return buffer.toString()
                }
            }
            """.trimIndent()
        )
    }

}
