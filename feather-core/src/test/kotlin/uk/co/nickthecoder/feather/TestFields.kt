package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectErrorAt
import uk.co.nickthecoder.feather.helper.expectValue

/**
 * Similar tests to [TestLocalVariables]
 */
class TestFields : TestCase() {

    fun testNormal() {
        expectValue(1, """
            class Foo() {
                var foo = 1 // Var or var makes no difference.
                fun test() : int {
                    return foo
                }
            }
            """.trimIndent()
        )
        expectValue(1, """
            class Foo() {
                val foo = 1 // Var or var makes no difference.
                fun test() : int {
                    return foo
                }
            }
            """.trimIndent()
        )
    }

    fun testAlterVar() {
        expectValue(2, """
            class Foo() {
                var foo = 1
                fun test() : int {
                    foo ++ // A var, so we can alter it.
                    return foo
                }
            }
        """.trimIndent())
    }

    fun testAlterVal() {
        expectErrorAt(3, 12, "The value cannot be changed", """
            class Foo() {
                val foo = 1
                fun test() : int {
                    foo ++ // A val, so this should fail to compile
                    return foo
                }
            }
        """.trimIndent())
    }

    fun testSetVal() {
        expectErrorAt(3, 12, "The value cannot be changed", """
            class Foo() {
                val foo = 1
                fun test() : int {
                    foo = 2 // A val, so this should fail to compile
                    return foo
                }
            }
        """.trimIndent())
    }

    fun testValWithoutInitialValue() {
        expectErrorAt(1, 4, "val (foo) must be initialised", """
            class Foo() {
                val foo : int
                fun test() : int {
                    return 1
                }
            }
        """.trimIndent())
    }

    fun testVarWithoutType() {
        expectErrorAt(1, 4, "Cannot determine the type", """
            class Foo() {
                var foo
                fun test() : int {
                    return 1
                }
            }
        """.trimIndent())
    }



    fun testFromStaticFun() {
        expectErrorAt(3, 15, "Cannot access a non-static field", """
            class Foo() {
                var foo = 1
                static fun test() : int {
                    return foo
                }
            }
            """.trimIndent()
        )
    }


    fun testSelf() {
        expectErrorAt(3, 18, "Cannot access a non-static field", """
            class Foo() {
                val foo = 1
                fun test() : Object {
                    return Foo.foo
                }
            }
            """.trimIndent()
        )
    }

    fun testPlainMutableField() {
        expectValue(-1,"""
            class Foo() {
                fun test() : Object {
                    return Plain().mutableField
                }
            }
            """.trimIndent()
        )
    }

    fun testPlainFinalField() {
        expectValue(1,"""
            class Foo() {
                fun test() : Object {
                    return Plain().finalField
                }
            }
            """.trimIndent()
        )
    }

    fun testPlainGetter() {
        expectValue(2,"""
            class Foo() {
                fun test() : Object {
                    return Plain().getter
                }
            }
            """.trimIndent()
        )
    }

    fun testPlainGetSetter() {
        expectValue(3,"""
            class Foo() {
                fun test() : Object {
                    return Plain().getSetter
                }
            }
            """.trimIndent()
        )
    }


    fun testPlainStaticField() {
        expectValue(101,"""
            class Foo() {
                fun test() : Object {
                    return Plain.staticFinalField
                }
            }
            """.trimIndent()
        )
    }

    fun testPlainStaticGetter() {
        expectValue(102,"""
            class Foo() {
                fun test() : Object {
                    return Plain.staticGetter
                }
            }
            """.trimIndent()
        )
    }
    fun testPlainStaticImportGetter() {
        expectValue(102,"""
            import static uk.co.nickthecoder.feather.helper.Plain.*
            class Foo() {
                fun test() : Object {
                    return staticGetter
                }
            }
            """.trimIndent()
        )
    }

    fun testPlainStaticGetSetter() {
        expectValue(103,"""
            class Foo() {
                fun test() : Object {
                    return Plain.staticGetSetter
                }
            }
            """.trimIndent()
        )
    }


    fun testChangeInInit() {
        expectValue(2, """
            class Foo() {
                var foo = 1
                init {
                    foo = 2
                }
                fun test() : int {
                    return foo
                }
            }
            """.trimIndent()
        )
    }


    fun testNullString() {
        expectValue(null, """
            class Foo() {
                var foo : String = null
                fun test() : String {
                    return foo
                }
            }
            """.trimIndent()
        )
    }
}
