package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectValue

class TestEnum : TestCase() {

    fun testValueOf() {
        expectValue("RED", """
            enum class Color {
                RED, GREEN, BLUE;
            }
            class Foo() {
                fun baz( str : String ) : Color = Color.valueOf( str )
                fun test() : String = baz( "RED" ).toString()
            }
            """.trimIndent()
        )
    }

    fun testValues() {
        expectValue("[RED, GREEN, BLUE]", """
            enum class Color {
                RED, GREEN, BLUE;
            }
            class Foo() {
                fun test() : String = Color.values().toString()
            }
            """.trimIndent()
        )
    }


    fun testName() {
        expectValue("RED", """
            enum class Color {
                RED, GREEN, BLUE;
            }
            class Foo() {
                fun test() : String = Color.RED.name()
            }
            """.trimIndent()
        )
    }

    fun testOrdinal() {
        expectValue(0, """
            enum class Color {
                RED, GREEN, BLUE;
            }
            class Foo() {
                fun test() = Color.RED.ordinal()
            }
            """.trimIndent()
        )
        expectValue(1, """
            enum class Color {
                RED, GREEN, BLUE;
            }
            class Foo() {
                fun test() = Color.GREEN.ordinal()
            }
            """.trimIndent()
        )
    }

}
