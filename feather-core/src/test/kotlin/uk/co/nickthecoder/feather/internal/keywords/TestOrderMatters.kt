package uk.co.nickthecoder.feather.internal.keywords

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectValue

/**
 * These tests ensure that various operations still work, even when the types they used
 * will only be known later in the parsing phase.
 */
class TestOrderMatters : TestCase() {
    fun testFunctionType() {
        expectValue(
            4, """
            class Foo() {
                fun test() : int {
                    val f = Bar::add<int,int>
                    return f(1, 3)
                }
            }
            """.trimIndent(),
            """
            class Bar() {
                static fun add( a : int, b : int ) = a + b                
            }
            """.trimIndent()
        )

        // Same again, but without explicit parameter types.
        expectValue(
            4, """
            class Foo() {
                fun test() : int {
                    val f = Bar::add
                    return f(1, 3)
                }
            }
            """.trimIndent(),
            """
            class Bar() {
                static fun add( a : int, b : int ) = a + b                
            }
            """.trimIndent()
        )
    }

    fun testAdd() {
        expectValue(
            3, """
            class Foo() {
                fun test() : int {
                    return Bar().one() + Bar().two()
                }
            }
            """.trimIndent(),
            """
            class Bar() {
                fun one() = 1
                fun two() = 2
            }
            """.trimIndent()
        )
    }

    fun testAddDoubles() {
        expectValue(
            3.0, """
            class Foo() {
                fun test() : double {
                    return Bar().one() + Bar().two()
                }
            }
            """.trimIndent(),
            """
            class Bar() {
                fun one() = 1.0
                fun two() = 2.0
            }
            """.trimIndent()
        )
    }


    fun testDivide() {
        expectValue(
            2, """
            class Foo() {
                fun test() : int {
                    return Bar().five() / Bar().two()
                }
            }
            """.trimIndent(),
            """
            class Bar() {
                fun five() = 5
                fun two() = 2
            }
            """.trimIndent()
        )
    }
    fun testDivide2() {
        expectValue(
            2.5, """
            class Foo() {
                fun test() : double {
                    return Bar().five() / Bar().two()
                }
            }
            """.trimIndent(),
            """
            class Bar() {
                fun five() = 5.0
                fun two() = 2.0
            }
            """.trimIndent()
        )
    }

    /**
     * exitWhileExpressions was using "resolveType", and therefore did not work when the
     * boolean expression was typed from a yet-to-be-parsed method.
     */
    fun testWhile() {
        expectValue(
            3, """
            class Foo() {
                fun test() : int {
                    var i = 0
                    while( Bar.notFinished() ) {
                        i ++
                    }
                    return i
                }
            }
            """.trimIndent(),
            """
            class Bar() {
                static var count = 0
                static fun notFinished() = count ++ < 3
                    
            }
            """.trimIndent()
        )
    }


    /**
     * exitWhileExpressions was using "resolveType", and therefore did not work when the
     * boolean expression was typed from a yet-to-be-parsed method.
     */
    fun testDoWhile() {
        expectValue(
            4, """
            class Foo() {
                fun test() : int {
                    var i = 0
                    do {
                        i ++
                    } while( Bar.notFinished() )
                     
                    return i
                }
            }
            """.trimIndent(),
            """
            class Bar() {
                static var count = 0
                static fun notFinished() = count ++ < 3
                    
            }
            """.trimIndent()
        )
    }
    /**
     */
    fun testDivideEquals() {
        expectValue(
            2.5, """
            class Foo() {
                fun test() : double {
                   Bar.a /= 2
                   return Bar.a
                }
            }
            """.trimIndent(),
            """
            class Bar() {
                static var a = 5.0
            }
            """.trimIndent()
        )
    }

    fun testConstructorTypeArgs() {
        expectValue(
            "Hello",
            """
            class Test() {
                fun test() : String {
                    val block = Bar.neighbourhood.blockAt()
                    val foo = Foo( "Hello" )
                    block.occupants.add( foo )
                    return foo.item()
                }
            }
            """.trimIndent(),

            """
            class Bar() {
                static var neighbourhood = TestNeighbourhood<Foo>()
            }
            """.trimIndent(),

            """
            class Foo( val str : String ) {
                fun item() : String {
                    val block = Bar.neighbourhood.blockAt()
                    if (block.occupants.size() == 0) return null
                    return block.occupants[0].str
                }
            }
            """.trimIndent()
        )
    }

}