package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectErrorAt
import uk.co.nickthecoder.feather.helper.expectValue

class TestExtends : TestCase() {

    fun testNoDefaultFeather() {
        // Would prefer it if the error column was 10 or 12
        expectErrorAt(1, 0, "No constructor Bar()", """
            class Bar( a : int ) {}
            class Foo : Bar {
            }
        """.trimIndent()
        )

        expectErrorAt(1, 12, "No constructor Bar()", """
            class Bar( a : int ) {}
            class Foo : Bar() {
            }
        """.trimIndent()
        )
    }

    fun testNoDefaultJava() {
        expectErrorAt(1, 12, "No constructor Greeter()", """
            // Padding!
            class Foo : Greeter() {
            }
        """.trimIndent()
        )
    }

    fun testCantCast() {
        expectErrorAt(1, 12, "No constructor Bar(boolean)", """
                class Bar( a : int ) {}
                class Foo : Bar(true) {
                }
            """.trimIndent()
        )

        expectErrorAt(1, 12, "No constructor Greeter(boolean)", """
                class Bar( a : int ) {}
                class Foo : Greeter(true) {
                }
            """.trimIndent()
        )
    }

    fun testExtendFinal() {
        expectErrorAt(1, 0, "Cannot extend final class", """
                // Padding!
                class Foo : Complex(1.0,1.0) {
                }
            """.trimIndent()
        )
    }

    fun testSuperMethods() {
        expectValue("Hello, Nick", """
            class Chat() : Greeter( "Hello" ) {
                fun test() : String = greet( "Nick" )
            }
            """.trimIndent()
        )
    }

    fun testOverrideReturnType() {
        expectValue("1.0", """
            import java.lang.Double
            
            class Gen() : NumberGen {
                override fun gen() : Double = Double(1)
            }
            class Foo() {
                fun test() = (Gen() as NumberGen).gen().toString()
            }
            """.trimIndent()
        )
    }

    /**
     * Make a static call to the same class in the call to the super's constructor.
     * When I created this test, currentClass was null, and a NPE was thrown.
     */
    fun testCallInsideConstructor() {
        expectValue( 1, """
            class Bar( var a : int ) {}
            class Foo() : Bar( one() ) {
                fun test() = a
                static fun one() = 1
            }
            """.trimIndent()
        )
    }

    fun testBothInits() {
        expectValue( 10, """
            class Bar() {
                var b = 1
                init {
                    b = 3
                }
            }
            class Foo() : Bar {
                init {
                    b = 10
                }
                fun test() = b
            }
            """.trimIndent()
        )
    }


    /**
     * Hmm, a real-world version of this crashed at runtime, but this seems to run ok.
     * It still shouldn't compile though, as Greeter.cantOverrideThis is not open!?! (final in java-speak).
     */
    fun testCantOverride() {
        expectErrorAt(4, 4, "Cannot override", """
                class Foo : Greeter("Hi") {
                    fun test() : String {
                        return cantOverrideThis( "..." )
                    }
                    override fun cantOverrideThis( foo : String ) : String {
                        return "Hello"
                    }
                }
            """.trimIndent()
        )
    }

}
