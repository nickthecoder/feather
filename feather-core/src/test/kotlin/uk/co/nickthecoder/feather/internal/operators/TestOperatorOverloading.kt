package uk.co.nickthecoder.feather.internal.operators

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.Complex
import uk.co.nickthecoder.feather.helper.expectValue
import uk.co.nickthecoder.feather.helper.testExpression

class TestOperatorOverloading : TestCase() {

    fun testComplex() {
        testExpression(Complex(3.0, 5.0), "Complex(1.0, 2.0) + Complex(2.0, 3.0)")
        testExpression(Complex(-1.0, -1.0), "Complex(1.0, 2.0) - Complex(2.0, 3.0)")
        testExpression(Complex(3.0, 6.0), "Complex(1.0, 2.0) * 3")
        testExpression(Complex(1.0, 2.0), "Complex(3.0, 6.0) / 3")
        testExpression(Complex(-1.0, -2.0), "-Complex(1.0, 2.0)")
    }

    fun testString() {
        testExpression("AbcAbcAbcAbc", """ "Abc" * 4 """)
    }

    fun testChar() {
        testExpression("aaaa", """ 'a' * 4 """)
    }

    fun testPlusAssign() {
        expectValue(
            "1.0,2.0",
            """
            class Foo() {
                fun test() : String {
                    val a = VectorTest( 0, 1 )
                    a += VectorTest( 1, 1 )
                    return a.toString()
                }
            }
            """.trimIndent()
        )
    }
    fun testMinusAssign() {
        expectValue(
            "-1.0,0.0",
            """
            class Foo() {
                fun test() : String {
                    val a = VectorTest( 0, 1 )
                    a -= VectorTest( 1, 1 )
                    return a.toString()
                }
            }
            """.trimIndent()
        )
    }
    fun testTimesAssign() {
        expectValue(
            "3.0,8.0",
            """
            class Foo() {
                fun test() : String {
                    val a = VectorTest( 1, 2 )
                    a *= VectorTest( 3, 4 )
                    return a.toString()
                }
            }
            """.trimIndent()
        )
    }
    fun testDivAssign() {
        expectValue(
            "2.0,3.0",
            """
            class Foo() {
                fun test() : String {
                    val a = VectorTest( 4, 12 )
                    a /= VectorTest( 2, 4 )
                    return a.toString()
                }
            }
            """.trimIndent()
        )
    }
    fun testRemAssign() {
        expectValue(
            "1.0,3.0",
            """
            class Foo() {
                fun test() : String {
                    val a = VectorTest( 5, 15 )
                    a %= VectorTest( 2, 4 )
                    return a.toString()
                }
            }
            """.trimIndent()
        )
    }
}
