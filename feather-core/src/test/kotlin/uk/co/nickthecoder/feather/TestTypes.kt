package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.*
import uk.co.nickthecoder.feather.internal.*

class TestTypes : TestCase() {

    fun testAutoCast() {
        assertTrue("Cast int to byte", canAutoCast(intPrimitive, bytePrimitive))
        assertTrue("Cast int to long", canAutoCast(intPrimitive, longPrimitive))

        assertFalse("Cast int to char", canAutoCast(intPrimitive, charPrimitive))
    }

    fun testNumberAsSuperType() {
        assertEquals(voidPrimitive, findCommonSuperType(voidPrimitive, voidPrimitive))

        assertEquals(Number::class.java, findCommonSuperType(doubleClass, integerClass))
        assertEquals(Number::class.java, findCommonSuperType(doubleClass, intPrimitive))
        assertEquals(Number::class.java, findCommonSuperType(doublePrimitive, integerClass))
    }

    fun testSuperInterface() {
        assertEquals(Shape2d::class.java, findCommonSuperType(Circle::class.java, Square::class.java))
    }

    /**
     * I had problems in my FooCAD project such as :
     * It would make the type of result Object, not Shape2d.
     */
    fun testShapeCommonInterface() {

        expectValue(
            true, """
            class Foo() {
                fun test() : bool {
                    return true // We only care that it compiles!!!
                }
                
                fun foo(f : bool) : Shape2d {
                
                    val result = if (f) {
                        Square()
                    } else {
                        Circle()
                    }
                    
                    return result
                }
            }
            """.trimIndent()
        )
    }

    /**
     * A Bug : If a function and a val have the same name, the type of the val errors if the type isn't explicit.
     */
    fun testFunctionAndVariableType() {
        expectValue(
            "Hello", """
            class Foo() {
                fun foo() : String = "Hello"
                fun test() : String {
                    val foo = foo()
                    return foo
                }
            }       
            """
        )
    }

    /**
     * Is a function's return type checked against the actual type returned by the expression?
     * At time of writing, both of these fail.
     */
    fun testCheckFunctionReturnType1() {
        expectErrorAt(
            1, 4, "Expected type java.lang.CharSequence but found Test",
            """
            class Test() {
                fun test() : CharSequence = this
            }
            """.trimIndent()
        )
    }

    fun testCheckFunctionReturnType2() {
        expectErrorAt(
            1, 4, "Expected type java.lang.String but found java.lang.CharSequence",
            """
            class Test() {
                fun test() : String = "Hello" as CharSequence
            }
            """.trimIndent()
        )
    }
}
