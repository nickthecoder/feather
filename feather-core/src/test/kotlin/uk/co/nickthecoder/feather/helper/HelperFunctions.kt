package uk.co.nickthecoder.feather.helper

import junit.framework.TestCase
import org.objectweb.asm.ClassReader
import org.objectweb.asm.util.ASMifier
import org.objectweb.asm.util.Textifier
import org.objectweb.asm.util.TraceClassVisitor
import uk.co.nickthecoder.feather.AllowList
import uk.co.nickthecoder.feather.CompilationFailed
import uk.co.nickthecoder.feather.FeatherCompiler
import uk.co.nickthecoder.feather.internal.isStatic
import uk.co.nickthecoder.feather.parser.LibraryClassLoader
import java.io.PrintWriter
import java.io.StringWriter
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method

val standardCompiler = FeatherCompiler().apply {
    with(configuration) {
        impliedImportPackages.add("uk.co.nickthecoder.feather.helper")
        includeFeatherRuntime()
        allowCommands = true
        simplifyMain = true
        (sandBox as AllowList).apply {
            allowedPackages.add("uk.co.nickthecoder.feather.helper")
            allowedPackages.add("uk.co.nickthecoder.feather")
            allowedPackages.add("java.lang")
            allowedPackages.add("java.util")
        }
    }
}

val noSandBoxCompiler = FeatherCompiler().apply {
    with(configuration) {
        impliedImportPackages.add("uk.co.nickthecoder.feather.helper")
        sandBox = AllowAll()
        includeFeatherRuntime()
        allowCommands = true
        simplifyMain = true
        // Makes testing easier, by allowing 3rd party jars to be used within the tests.
        classLoader = LibraryClassLoader()
    }
}

fun compareByteCode(vararg classNames: String) {
    compareByteCodePackage("compare", *classNames)
}

fun compareByteCodePackage(subPackageName: String?, vararg classNames: String) {
    compareByteCodePackage(noSandBoxCompiler, subPackageName, * classNames)
}

fun compareByteCodePackage(compiler: FeatherCompiler, subPackageName: String?, vararg classNames: String) {

    val packageFilePrefix = if (subPackageName == null) "" else "$subPackageName/"
    val packagePrefix = if (subPackageName == null) "" else ".$subPackageName"

    val sourceCodes = classNames.map { className ->
        val file = "${packageFilePrefix}Feather$className.feather"
        Pair(
            "Feather$className",
            FeatherCompiler::class.java.getResource(file)?.readText()
                ?: throw IllegalArgumentException("Feather class ${file} not found")
        )
    }
    val featherByteCodes = compiler.compileFromNamedStrings(sourceCodes)

    for (name in classNames) {
        val javaByteCode = FeatherCompiler::class.java.getResource("${packageFilePrefix}Java$name.class").readBytes()
        val javaOut = dumpByteCode(javaByteCode).replace("Java", "Feather")
        val featherName = FeatherCompiler::class.java.`package`.name + packagePrefix + ".Feather" + name
        val featherByteCode = featherByteCodes[featherName]
            ?: throw IllegalStateException("Class $featherName not found. ${featherByteCodes.keys.joinToString()}")
        val featherOut = dumpByteCode(featherByteCode)

        // Can aid debugging by seeing what ASM code should have been called.
        //if (javaOut != featherOut) println(dumpASM(javaByteCode))

        // Compare the raw byte code
        //if (javaOut != featherOut) assertEquals(javaByteCode.toHex(), featherByteCode.toHex() )

        TestCase.assertEquals(javaOut, featherOut)
    }

}

fun dumpByteCode(byteCode: ByteArray): String {
    val parsingOptions = ClassReader.SKIP_DEBUG // or 0

    val writer = StringWriter()
    val visitor = TraceClassVisitor(null, Textifier(), PrintWriter(writer, true))
    ClassReader(byteCode).accept(visitor, parsingOptions)
    return writer.toString()
}

fun dumpASM(byteCode: ByteArray): String {
    val parsingOptions = ClassReader.SKIP_DEBUG // or 0

    val writer = StringWriter()
    val visitor = TraceClassVisitor(null, ASMifier(), PrintWriter(writer, true))
    ClassReader(byteCode).accept(visitor, parsingOptions)
    return writer.toString()
}


fun ByteArray.toHex(): String? {
    val HEX_ARRAY = "0123456789ABCDEF".toCharArray()
    val hexChars = CharArray(this.size * 2)
    for (j in this.indices) {
        val v: Int = this[j].toInt() and 0xFF
        hexChars[j * 2] = HEX_ARRAY.get(v ushr 4)
        hexChars[j * 2 + 1] = HEX_ARRAY.get(v and 0x0F)
    }
    return String(hexChars)
}

fun expectErrorAt(line: Int, column: Int, err: String, vararg scripts: String) {
    expectErrorAt(standardCompiler, line, column, err, * scripts)
}

fun expectErrorAt(compiler: FeatherCompiler, line: Int, column: Int, err: String, vararg scripts: String) {
    expectErrorAt(compiler, line, column, err, scripts.toList())
}

fun expectError(err: String, script: String) {
    expectErrorAt(standardCompiler, null, null, err, listOf(script))
}
fun expectError(compiler: FeatherCompiler, err: String, script: String) {
    expectErrorAt(compiler, null, null, err, listOf(script))
}

fun expectErrorAt(compiler: FeatherCompiler, line: Int?, column: Int?, err: String, scripts: List<String>) {

    try {
        compiler.compileFromStrings(scripts)
        TestCase.fail("Expected a FeatherException, but no exception was thrown")

    } catch (e: CompilationFailed) {
        if (e.errors.isEmpty()) {
            TestCase.fail("Expected an error but found none")
        }
        val fe = e.errors.first()
        if (line != null && line != fe.pos.line) {
            for (script in scripts) {
                println(script)
            }
            TestCase.fail("Expected an error on line $line, found ${fe.message} on line ${fe.pos.line}")
        }
        if (column != null && column != fe.pos.column) {
            TestCase.fail("Expected an error in column $column, found ${fe.message} on column ${fe.pos.column}")
        }
        if (fe.message?.startsWith(err) != true) {
            TestCase.fail("Expected an error message beginning with '$err'. Found '${fe.message}'")
        }

    } catch (e: Exception) {
        e.printStackTrace()
        TestCase.fail("Expected a CompilationFailed, but found $e")
    }

}


/**
 * Compile the [scripts], and call the method called "test", and check the return value
 * is the same as the [expected] value.
 *
 * Note the method test() can be static, or non-static (in which case the class must have
 * a constructor with no arguments).
 */
fun expectValue(expected: Any?, vararg scripts: String) {
    expectValue(standardCompiler, expected, *scripts)
}

fun expectValueNoSandbox(expected: Any?, vararg scripts: String) {
    expectValue(noSandBoxCompiler, expected, *scripts)
}

fun expectValue(compiler: FeatherCompiler, expected: Any?, vararg scripts: String) {

    val method = findTestMethod(compiler, * scripts)
    val actual = try {
        if (method.isStatic()) {
            method.invoke(null)
        } else {
            val instance = method.declaringClass.newInstance()
            method.invoke(instance)
        }
    } catch (e: Exception) {
        val map = compiler.compileFromStrings(scripts.toList())
        println( "**Byte code may help resolve this failed test**" )
        for (compiledCode in map.values) {
            println(dumpByteCode(compiledCode))
        }
        throw e
    }

    if (expected != actual) {
        // Useful to have a peek at the bytecode when a test fails!
        val map = compiler.compileFromStrings(scripts.toList())
        println( "**Byte code may help resolve this failed test**" )
        for (compiledCode in map.values) {
            println(dumpByteCode(compiledCode))
        }
    }
    TestCase.assertEquals("Incorrect type", expected?.javaClass, actual?.javaClass)
    TestCase.assertEquals(expected, actual)
}

fun expectThrowAt(expectedLines: List<Int>, vararg scripts: String) {
    expectThrowAt(standardCompiler, expectedLines, null, * scripts)
}

fun expectThrowAt(line: Int, exceptionType: Class<*>, vararg scripts: String) {
    expectThrowAt(standardCompiler, listOf(line), exceptionType, * scripts)
}

fun expectThrowAt(compiler: FeatherCompiler, expectedLines: List<Int>, errorType: Class<*>?, vararg scripts: String) {

    val method = findTestMethod(compiler, * scripts)
    try {
        val actual = if (method.isStatic()) {
            method.invoke(null)
        } else {
            val instance = method.declaringClass.newInstance()
            method.invoke(instance)
        }

        TestCase.fail("No exception thrown : actual value = $actual")

    } catch (e: InvocationTargetException) {
        val cause = e.cause !!
        //cause.printStackTrace()
        val entries = cause.stackTrace
        expectedLines.forEachIndexed { index, expectedLine ->
            if (entries[index].lineNumber != expectedLine) {
                TestCase.fail("Expected a exception at line $expectedLine, but found : ${entries[index].lineNumber} ( ${entries[index]} )")
            }
        }
        if (errorType != null && errorType != cause.javaClass) {
            TestCase.fail("Expected message : ${errorType.simpleName} but found ${cause.javaClass.simpleName}")
        }
    }
}

fun findTestMethod(compiler: FeatherCompiler, vararg scripts: String): Method {

    val classes = try {
        compiler.compileFromStrings(scripts.toList())
    } catch (e: CompilationFailed) {
        System.err.println("CompilationFailed Caused by ...")
        e.errors.forEach {
            it.printStackTrace()
        }
        throw e
    }
    val classLoader = object : ClassLoader(compiler.configuration.classLoader) {
        override fun findClass(name: String): Class<*>? {
            val bytes = classes[name] ?: return null
            try {
                return defineClass(name, bytes, 0, bytes.size)
            } catch (e: Throwable) {
                println(dumpByteCode(classes[name]!!))
                throw e
            }
        }
    }

    classes.keys.forEach { name ->
        val klass = classLoader.loadClass(name)
        val method = try {
            klass.getMethod("test")
        } catch (e: Throwable) {
            //e.printStackTrace()
            null
        }

        if (method != null) {
            return method
        }
    }

    // Dump the first class file's bytcode
    classes.entries.forEach { println(dumpByteCode(it.value)) }
    TestCase.fail("No method test() found")
    throw IllegalStateException()
}

fun compileClasses(compiler: FeatherCompiler, vararg scripts: String): List<Class<*>> {

    val classesAsByteArrays = try {
        compiler.compileFromStrings(scripts.toList())
    } catch (e: CompilationFailed) {
        System.err.println("CompilationFailed Caused by ...")
        e.errors.forEach {
            it.printStackTrace()
        }
        throw e
    }
    val classLoader = object : ClassLoader(compiler.configuration.classLoader) {
        override fun findClass(name: String): Class<*>? {
            val bytes = classesAsByteArrays[name] ?: return null
            try {
                return defineClass(name, bytes, 0, bytes.size)
            } catch (e: Throwable) {
                println(dumpByteCode(classesAsByteArrays[name]!!))
                throw e
            }
        }
    }

    val result = mutableListOf<Class<*>>()
    classesAsByteArrays.keys.forEach { name ->
        result.add(classLoader.loadClass(name))
    }

    return result
}

fun compileClass(compiler: FeatherCompiler, script: String) = compileClasses(compiler, script).first()

fun classForExpression(exp: String) = """
    class Foo() {
        fun test() = $exp
    }
""".trimIndent()

fun testExpression(expected: Any?, expression: String) {
    expectValue(expected, classForExpression(expression))
}
