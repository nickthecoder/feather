package uk.co.nickthecoder.feather.helper

open class Greeter( val greeting : String ) {

    open fun greet( name : String ) = "$greeting, $name"

    fun cantOverrideThis( foo : String? ) = foo

}
