package uk.co.nickthecoder.feather.internal.keywords

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectValue

/**
 * Note, many of these test cases are very similar to those in [TestApply].
 */
class TestWith : TestCase() {

    fun testValue() {
        expectValue(11, """
            class Foo() {
                fun test() : int {
                    return with ( "Hello" ) {
                        this + " World" // This is the result of the "with"
                    }.length()
                }
            }
            """.trimIndent()
        )
    }

    fun testMethodCall() {
        expectValue(5, """
            class Foo() {
                fun test() : int {
                    with( "Hello" ) {
                        return length()
                    }
                }
            }
            """.trimIndent()
        )
    }

    fun testField() {
        expectValue(-1, """
            class Foo() {
                fun test() : int {
                    with( Plain() ) {
                        return mutableField
                    }
                }
            }
            """.trimIndent()
        )
    }

    fun testGetter() {
        expectValue(2, """
            class Foo() {
                fun test() : int {
                    with ( Plain() ) {
                        return getter
                    }
                }
            }
            """.trimIndent()
        )
    }

    fun testWhichIsAccessed() {
        expectValue( -1, """
            class Foo() : Plain() {
                fun test() : int {
                    mutableField = 100
                    with( Plain() ) {
                        return mutableField // From Plain, not Foo(), so -1
                    }
                }
            }
            """.trimIndent()
        )
    }

    fun testAlsoSeeFoo() {
        // Can still access classes fields
        expectValue( 42, """
            class Foo() : Plain() {
                val extra = 42
                fun test() : int {
                    mutableField = 100
                    with ( Plain() ) {
                        return extra
                    }
                }
            }
            """.trimIndent()
        )

        // Can still access classes methods
        expectValue( 43, """
            class Foo() : Plain() {
                fun extra() = 43
                fun test() : int {
                    mutableField = 100
                    with ( Plain() ) {
                        return extra()
                    }
                }
            }
            """.trimIndent()
        )
    }


    /* this@ not implemented yet.
    fun testThisAt() {
        expectValue( -2, """
            class Foo() : Plain() {
                val extra = 42
                fun test() : int {
                    mutableField = 100
                    with ( Plain() ) {
                        mutableField = this@Foo.mutableField - 1
                        return mutableField
                    }
                }
            }
            """.trimIndent()
        )
    }
    */

}
