package uk.co.nickthecoder.feather.internal.operators

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectValue

/**
 * Tests the == operator.
 *
 * See [TestEqual] The tests should be the same, with == replaced by != and the expected result reversed.
 */
class TestEqual : TestCase() {

    fun testEqualsNonNullNonNull() {

        expectValue(
            true,
            """
            class Foo() {
                fun test() : bool {
                    val foo : String = "Hello"
                    val bar : String = "Hello"
                    return foo == bar
                }
            }
            """.trimIndent()
        )

        expectValue(
            false,
            """
            class Foo() {
                fun test() : bool {
                    val foo : String = "Hello"
                    val bar : String = "World"
                    return foo == bar
                }
            }
            """.trimIndent()
        )

    }

    fun testEqualsNullNull() {
        expectValue(
            true,
            """
            class Foo() {
                fun test() : bool {
                    val foo : String = null
                    val bar : String = null
                    return foo == bar
                }
            }
            """.trimIndent()
        )
    }

    fun testEqualsNonNullNull() {
        expectValue(
            false,
            """
            class Foo() {
                fun test() : bool {
                    val foo : String = "Hello"
                    val bar : String = null
                    return foo == bar
                }
            }
            """.trimIndent()
        )

    }

    fun testEqualsNullNonNull() {
        expectValue(
            false,
            """
            class Foo() {
                fun test() : bool {
                    val foo : String = null
                    val bar : String = "Hello"
                    return foo == bar
                }
            }
            """.trimIndent()
        )
    }

    /**
     * I had a real-world bug, where comparing a value with null was causing weird errors.
     * The first worked okay, but the second was probably attempting to call CharSequence.equals( null )+
     */
    fun testInterfaceEqualsNull() {
        expectValue(
            true,
            """
                class Foo() {
                    fun test() : bool {
                        var cs : CharSequence = null
                        return cs == null
                    }
                }
            """
        )
        expectValue(
            false,
            """
                class Foo() {
                    fun test() : bool {
                        var cs : CharSequence = "Hello"
                        return cs == null
                    }
                }
            """
        )

    }

}
