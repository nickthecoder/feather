package uk.co.nickthecoder.feather.helper

/**
 * A simple example class using generics for testing generics in Feather.
 *
 */
class GenericExample<T : Number>(initialValue: T) {

    @JvmField
    var value: T = initialValue

    fun set(v: T) {
        value = v
    }

    fun get() = value

    companion object {
        @JvmStatic
        val doubleExampleA = GenericExample<Double>(3.0)

        @JvmStatic
        val doubleExampleB = GenericExample<Double>(6.0)

        @JvmStatic
        val intExample = GenericExample<Int>(4)
    }
}

/**
 * Help test a real-world bug. This is a cut down version of the Neighbourhood class in Tickle.
 */
class TestNeighbourhood<T>() {

    private val block = TestBlock<T>()
    fun blockAt() = block

}

class TestBlock<T> {
    val occupants = mutableListOf<T>()
}

/**
 * A real-world example which Feather got wrong.
 * The tests must use "Rock" as the type parameter T.
 */
class Stage {
    val roles = mutableListOf(Rock(), Ship(), Ship())

    fun <T : Role> findRolesByClass(type: Class<T>): List<T> {
        val result = mutableListOf<T>()
        @Suppress("UNCHECKED_CAST")
        return roles.filter { type.isInstance(it) }.map { it as T }
    }
    fun <T : Role> findRoleByClass(type: Class<T>) : T? {
        for (role in roles) {
            @Suppress("UNCHECKED_CAST")
            if (role.javaClass == type) return role as T
        }
        return null
    }
}
open class Role {}
class Rock() : Role()
class Ship() : Role()

/**
 * This is a real-world API that showed a flaw in Feather. See TestGenerics.testListItem.
 */
class Rand {
    companion object {
        @JvmStatic
        fun <T> listItem(list: List<T>) = list.first() // The real implementation returned a random item.
    }
}

/**
 * Tests the += operator
 */
class VectorTest(var x: Double, var y: Double) {

    operator fun plusAssign(other: VectorTest) {
        x += other.x
        y += other.y
    }

    operator fun minusAssign(other: VectorTest) {
        x -= other.x
        y -= other.y
    }

    operator fun timesAssign(other: VectorTest) {
        x *= other.x
        y *= other.y
    }

    operator fun divAssign(other: VectorTest) {
        x /= other.x
        y /= other.y
    }

    operator fun remAssign(other: VectorTest) {
        x %= other.x
        y %= other.y
    }

    override fun toString() = "$x,$y"
}
