package uk.co.nickthecoder.feather.helper

interface NumberGen {
    fun gen(): Number
}

interface NumberGen2 {
    fun genRange(a: Double, b: Double): Number
}
