package uk.co.nickthecoder.feather.internal.operators

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectValue

/**
 * Java Lists can be accesses as arrays, using the [] syntax.
 * These tests the get and set operations of the [] syntax.
 */
class TestListAccess : TestCase() {

    fun testArrayList() {
        expectValue( "World", """
            import java.util.*
            class Foo() {
                fun test() : String {
                    val list = ArrayList<String>().apply {
                        add( "Hello" )
                        add( "World" )
                        add( "!" )
                    }
                    return list[1]
                }
            }
        """.trimIndent())
    }


    fun testSetArrayList() {
        expectValue( "Nick", """
            import java.util.*
            class Foo() {
                fun test() : String {
                    val list = ArrayList<String>().apply {
                        add( "Hello" )
                        add( "World" )
                        add( "!" )
                    }
                    list[1] = "Nick"
                    return list[1]
                }
            }
        """.trimIndent())
    }

    fun testIntArrayList() {
        expectValue( 42, """
            import java.util.*
            class Foo() {
                fun test() : int {
                    val list = ArrayList<int>().apply {
                        add( 1 )
                        add( 3 )
                        add( 5 )
                    }
                    list[1] = 42
                    return list[1]
                }
            }
        """.trimIndent()
        )
    }

    fun testAdjustDouble() {
        expectValue(
            2, """
            import java.util.*
            class Foo() {
                fun test() : int {
                    //val list = ArrayList<double>().apply { add( 1.0 ) }
                    val list = ArrayList<double>()
                    list.add( 1.0 )

                    val foo = "foo"
                    list[0]++
                    val bar = "bar"
                    return list[0]
                }
            }
        """.trimIndent()
        )
    }

    fun testAdjustArrayList() {
        expectValue(
            4, """
            import java.util.*
            class Foo() {
                fun test() : int {
                    val list = ArrayList<int>().apply {
                        add( 1 )
                        add( 3 )
                        add( 5 )
                    }
                    list[1]++
                    list[1]++
                    list[1]--
                    return list[1]
                }
            }
        """.trimIndent())
    }

    fun testPlusEquals() {
        expectValue(6, """
            import java.util.*
            class Foo() {
                fun test() : int {
                    val list = ArrayList<int>().apply {
                        add( 1 )
                        add( 3 )
                        add( 5 )
                    }
                    list[1]+=3
                    return list[1]
                }
            }
        """.trimIndent())
    }

}
