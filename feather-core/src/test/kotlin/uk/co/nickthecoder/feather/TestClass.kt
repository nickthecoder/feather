package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectErrorAt

class TestClass : TestCase() {

    fun testEmpty() {
        expectErrorAt(0, 5, "mismatched", "class")

        expectErrorAt(0, 6, "missing", """
            class {
            }
        """.trimIndent())
    }

    fun testExtendsTwo() {
        expectErrorAt(1, 0, "Cannot extend from more than one class", """
            
            class Foo : StringBuilder, StringBuffer {
            }
        """.trimIndent())
    }

}
