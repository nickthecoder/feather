package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectValue
import java.util.*

/**
 * Tests Feathers support for Java array types.
 * The syntax differs from Java, which makes is *look* like Array is a class, but it isn't.
 * The underlying type is a Java array.
 *
 * The JVM handles arrays of Objects differently from arrays of primitive types, and therefore
 * we should test both kinds of arrays.
 *
 */
class TestArrays : TestCase() {

    fun testArrayOfString() {
        expectValue(
            // NOTE, the last element is not assigned, and therefore should have the "default" value.
            "[hello, hello, there, null]", """
            class Foo() {
                fun test() : String {
                    var array = Array<String>(4) // Tests array creation
                    array[0] = "hello"
                    array[1] = array[0] // Tests array getter and setter.
                    array[2] = "there"
                    
                    return array.toString()
                }
            }
            """.trimIndent()
        )
    }

    fun testArrayOfBoolean() {
        expectValue(
            // NOTE, the last element is not assigned, and therefore should have the "default" value.
            "[true, true, false, false]", """
            class Foo() {
                fun test() : String {
                    var array = Array<bool>(4) // Tests array creation
                    array[0] = true
                    array[1] = array[0] // Tests array getter and setter.
                    array[2] = false

                    return array.toString()
                }
            }
            """.trimIndent()
        )
    }
    fun testArrayOfInt() {
        expectValue(
            // NOTE, the last element is not assigned, and therefore should have the "default" value.
            "[1, 1, 2, 0]", """
            class Foo() {
                fun test() : String {
                    var array = Array<int>(4) // Tests array creation
                    array[0] = 1
                    array[1] = array[0] // Tests array getter and setter.
                    array[2] = 2

                    return array.toString()
                }
            }
            """.trimIndent()
        )
    }

    /**
     * The function arrayOf<T>(...) is implemented directly in byte code. See uk.co.nickthecoder.feather.internal.special.ArrayOf class.
     */
    fun testArrayOfFakeFunction() {
        expectValue(
            // NOTE, the last element is not assigned, and therefore should have the "default" value.
            "[Hello, World]", """
            class Foo() {
                fun test() : String {
                    return arrayOf<String>("Hello", "World" ).toString()
                }
            }
            """.trimIndent()
        )
    }

    fun testArrayOfStringSize() {
        expectValue(
            4, """
            class Foo() {
                fun test() : int {
                    var array = Array<String>(4)
                    
                    return array.size()
                }
            }
            """.trimIndent()
        )
    }

    fun testArrayOfIntSize() {
        expectValue(
            4, """
            class Foo() {
                fun test() : int {
                    var array = Array<int>(4)
                    
                    return array.size()
                }
            }
            """.trimIndent()
        )
    }

    fun testSortStrings() {
        expectValue(
            "[abc, def, hij]", """
            class Foo() {
                fun test() : String {
                    val array = arrayOf<String>("def", "abc", "hij")
                    array.sort()
                    return array.toString()
                }
            }
            """.trimIndent()
        )
    }
    fun testSortChars() {
        expectValue(
            "[a, b, c]", """
            class Foo() {
                fun test() : String {
                    val array = charArrayOf('b', 'a', 'c')
                    array.sort()
                    return array.toString()
                }
            }
            """.trimIndent()
        )
    }
    fun testSortInts() {
        expectValue(
            "[1, 3, 10]", """
            class Foo() {
                fun test() : String {
                    val array = intArrayOf(3, 1, 10)
                    array.sort()
                    return array.toString()
                }
            }
            """.trimIndent()
        )
    }

    /**
     * NOTE, Kotlin's array.hashCode is NOT the same as Java's.
     * Java states that an array's hashCode should be the same as a List's hashCode.
     * It seems that Kotlin doesn't respect this???
     * Feather uses Java's implementation, and therefore we convert the array to a list before calling hashCode()..
     */
    fun testHashCodeStrings() {
        expectValue(
            arrayOf("def", "abc", "hij").toList().hashCode(), """
            class Foo() {
                fun test() : int {
                    val array = arrayOf<String>("def", "abc", "hij")
                    return array.hashCode()
                }
            }
            """.trimIndent()
        )
    }
    fun testHashCodeInts() {
        expectValue(
            arrayOf(3, 1, 10).toList().hashCode(), """
            class Foo() {
                fun test() : int {
                    val array = intArrayOf(3, 1, 10)
                    return array.hashCode()
                }
            }
            """.trimIndent()
        )
    }

    /**
     * Ensure that arrays can be declared as a Type. In this case, an argument to "foo()"
     */
    fun testArrayArgType() {
        expectValue(
            3, """
            class Foo() {
                fun foo( array : Array<int> ) = array.size() 
                fun test() : int {
                    val array = intArrayOf(3, 1, 10)
                    return foo(array)
                }
            }
            """.trimIndent()
        )

        // As above, but we also declare the variable explicitly as Array<int>
        expectValue(
            3, """
            class Foo() {
                fun foo( array : Array<int> ) = array.size()
                fun test() : int {
                    val array : Array<int> = intArrayOf(3, 1, 10)
                    return foo(array)
                }
            }
            """.trimIndent()
        )

    }

    fun test2DArrayOfString() {
        expectValue(
            "[[Hello, World], [Bye, Now]]", """
            class Foo() {
                fun test() : String {
                    val array = Array<String>(2,2)
                    array[0][0] = "Hello"
                    array[0][1] = "World"
                    array[1][0] = "Bye"
                    array[1][1] = "Now"
                    
                    return array.toString()
                }
            }
            """.trimIndent()
        )
    }

    fun testJaggedArray() {
        expectValue(
            "[[Hello, World], [Bye]]", """
            class Foo() {
                fun test() : String {
                    // Alas, Feather cannot deduce the types (yet?), so the following line is unwieldy :-(
                    val array = arrayOf<Array<String>>( arrayOf<String>("Hello", "World"), arrayOf<String>( "Bye" ) )
                    return array.toString()
                }
            }
            """.trimIndent()
        )
    }

    /**
     * An alternate version of testJaggedArray, with a sightly less unwieldy code.
     */
    fun testJaggedArray2() {
        expectValue(
            "[[Hello, World], [Bye]]", """
            class Foo() {
                fun test() : String {
                    val array = Array<String>(2,0)
                    array[0] = arrayOf<String>("Hello", "World")
                    array[1] = arrayOf<String>( "Bye" )
                    return array.toString()
                }
            }
            """.trimIndent()
        )
    }

    /**
     * Ensures that deepHashCode is used for 2d arrays
     */
    fun test2DHashCode() {
        expectValue(
            Arrays.deepHashCode(arrayOf(arrayOf("Hello", "World"), arrayOf("Bye", "Now"))), """
            class Foo() {
                fun test() : int {
                    val array = Array<String>(2,2)
                    array[0][0] = "Hello"
                    array[0][1] = "World"
                    array[1][0] = "Bye"
                    array[1][1] = "Now"
                    
                    return array.hashCode()
                }
            }
            """.trimIndent()
        )
    }

    fun testFromStaticContext() {
        expectValue(
            "Hello", """
            class Foo() {
                static val words = arrayOf<String>("Hello", "World")
                fun test() = words[0]
            }
            """.trimIndent()
        )
    }

    // todo Multi-dimensional arrays have not been tested
    // todo deepHashCode has not been tested.
    // todo We should probably use deepToString for Array<Object>
    // todo No tests for array equality (not implement (correctly?) yet)
    // todo array.indices has not been implemented
}
