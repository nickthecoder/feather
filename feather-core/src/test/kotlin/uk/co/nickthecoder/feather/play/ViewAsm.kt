package uk.co.nickthecoder.feather.play

import uk.co.nickthecoder.feather.FeatherCompiler
import uk.co.nickthecoder.feather.helper.dumpByteCode

fun main(vararg argv: String) {

    println("Byte Code for class ${argv[0]} ...")

    val javaByteCode = FeatherCompiler::class.java.classLoader.getResource("${argv[0]}.class")?.readBytes()
    println(dumpByteCode(javaByteCode!!))

}
