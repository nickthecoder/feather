package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectThrowAt
import uk.co.nickthecoder.feather.helper.expectValue

class TestNull : TestCase() {

    /**
     * This test is primarily designed to check for compile-time problems.
     * Is `null` considered a suitable value as one path of an if, where the other is a String.
     * Oddly, when this test was created, the first two passed, and the third and fourth failed.
     */
    fun testIfThenStringElseNull1() {
        expectValue(
            "Hello", """
            class Test() {
                fun test() = if (true) "Hello" else null
            }
            """.trimIndent()
        )
    }

    fun testIfThenStringElseNull2() {

        expectValue(
            null, """
            class Test() {
                fun test() = if (false) "Hello" else null
            }
            """.trimIndent()
        )
    }

    fun testIfThenStringElseNull3() {

        expectValue(
            "Hello", """
            class Test() {
                fun test() : String {
                     val result = if (true) "Hello" else null
                     return result
                 }
            }
            """.trimIndent()
        )
    }

    fun testIfThenStringElseNull4() {

        expectValue(
            null, """
            class Test() {
                fun test() : String {
                     val result = if (false) "Hello" else null
                     return result
                 }
            }
            """.trimIndent()
        )
    }

    /**
     */
    fun testIfThenNullReturnType() {
        // This is the simple case
        expectValue(
            "Hello!",
            """
            class Test() {
                fun foo() = if (true) "Hello" else null
                fun test() : String {
                     return foo() + "!"
                 }
            }
            """.trimIndent()
        )
        // This is the tricky case. A NPE should be thrown as we are performing append on a null string.
        expectThrowAt(4, NullPointerException::class.java,
            """
            class Test() {
                fun foo() = if (false) "Hello" else null
                fun test() : String {
                     return foo() + "!"
                 }
            }
            """.trimIndent()
        )
    }

}
