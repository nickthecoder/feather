package uk.co.nickthecoder.feather.play;

import uk.co.nickthecoder.feather.runtime.command.Command;
import uk.co.nickthecoder.feather.runtime.command.Command.CommandPart;

public class CreateCommand {
    public Command createCommand() {
        return new Command(
                new CommandPart("echo 'Hello ", true),
                new CommandPart("O'Reilly", false),
                new CommandPart("'", true)
        );
    }

}
