package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectErrorAt
import uk.co.nickthecoder.feather.helper.expectValue
import uk.co.nickthecoder.feather.helper.standardCompiler

class TestInterface : TestCase() {

    fun testEmpty() {
        expectErrorAt(0, 9, "mismatched", "interface")

        expectErrorAt(0, 10, "missing", """
            interface {
            }
        """.trimIndent())
    }

    fun testField() {
        expectErrorAt(1, 4, "An interface cannot contain a field", """
            interface Foo {
                val a = 1
            }
        """.trimIndent())
    }

    fun testMethodExpression() {
        expectErrorAt(1, 14, "An interface cannot contain a function body", """
            interface Foo {
                fun one() = 1
            }
        """.trimIndent())
    }

    fun testMethodBody() {
        expectErrorAt(1, 20, "An interface cannot contain a function body", """
            interface Foo {
                fun one() : Int { return 1 }
            }
        """.trimIndent())
    }

    fun testExtendClass() {
        expectErrorAt(1, 0, "An interface cannot extend a class", """
            
            interface Foo : StringBuilder {}
        """.trimIndent())

        expectErrorAt(1, 0, "An interface cannot extend a class", """
            class Bar {}
            interface Foo : Bar {}
        """.trimIndent())

        expectErrorAt(2, 0, "An interface cannot extend a class", """
            package my.pack
            class Bar {}
            interface Foo : Bar {}
        """.trimIndent())
    }

    fun testConstructor() {
        expectErrorAt(0, 13, "An interface cannot have a constructor", """
            interface Foo() { }
        """.trimIndent())

        expectErrorAt(0, 13, "An interface cannot have a constructor", """
            interface Foo( a : Int ) { }
        """.trimIndent())

    }


    /**
     * When this test was created, interfaces which extended other interfaces didn't work as expected.
     * Methods declared in the super interface weren't seen.
     * So in this test calling Middle.boo() failed.
     * Also, declaring Bottom.boo() was impossible, because the "override" keyword didn't work, as it
     * did see that Middle *does* have a boo() method.
     */
    fun testExtendsMethods() {
        standardCompiler.compileFromStrings("""
            interface Top {
                fun boo()
            }
            
            interface Middle : Top {
                fun bar()
            }
            
            class Bottom : Middle {
                override fun bar() {
                    println( "Bottom.bar" )
                }
                override fun boo() {
                    println( "Bottom.boo" )
                }
            }
            
            class Test {
                fun test( a : Top, b : Middle ) {
                    //a.boo()
                    b.boo()
                    //b.bar()
                }
            }
        """.trimIndent()
        )

    }

    fun testDefaultImpl() {
        expectValue(
            1, """
                class Foo : ExampleInterface {
                    fun test() = one()
                }
            """
        )

        expectValue(
            4, """
                class Foo : ExampleInterface {
                    fun test() = length("abcd")
                }
            """
        )


        expectValue(
            7, """
                class Foo : ExampleInterface {
                    fun test() = lengthPlus("abcd",3)
                }
            """
        )

        expectValue(
            7, """
                class Foo : ExampleInterface {
                    fun test() = plusLength(3,"abcd")
                }
            """
        )

    }

    /**
     * Was failing with :
     * Method 'java.lang.String uk.co.nickthecoder.feather.helper.ExampleInterface.getHello()' must be InterfaceMethodref constant
     *
     * This was due to not setting the "isInterface" to true, when invoking a static method on an interface.
     */
    fun testStaticInInterface() {
        expectValue(
            "Hello", """
                class Foo {
                    fun test() = ExampleInterface.hello
                }
            """
        )
        // Same again, but with explicit getHello() method call,
        expectValue(
                "Hello", """
                class Foo {
                    fun test() = ExampleInterface.getHello()
                }
            """
        )
    }

}
