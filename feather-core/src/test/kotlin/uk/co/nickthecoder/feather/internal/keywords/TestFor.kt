package uk.co.nickthecoder.feather.internal.keywords

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectValue
import uk.co.nickthecoder.feather.helper.expectValueNoSandbox

class TestFor : TestCase() {

    fun testSum() {
        expectValue(9, """
            class Foo() {
                fun test() : int {
                    var sum = 0
                    for ( a in 2..4 ) {
                        sum = sum + a
                    }
                    return sum
                }
            }
        """.trimIndent())
    }

    fun testFromFooCAD() {
        expectValue(0, """
            class Foo() {
                fun test() : int {
                    val pieces = 12
                    val angle = 30.0
                    for ( i in 1..4 ) {
                        // val foo : int = angle*i // Fails
                        //val foo = angle*i // Works
                        angle*i // Fails
                    }
                    return 0
                }
            }
        """.trimIndent())
    }

    /**
     * The optimised for loop has an extra test before the loop starts, as well as at the bottom of
     * the loop. So we need an additional test case.
     */
    fun testEmptyRange() {
        expectValue(0, """
            class Foo() {
                fun test() : int {
                    var count = 0
                    for ( a in 8..4 ) {
                        count ++
                    }
                    return count
                }
            }
        """.trimIndent())
    }

    fun testCounter() {
        expectValue(3, """
            class Foo() {
                fun test() : int {
                    var sum = 0
                    for ( i, a in 0..2 ) {
                        sum = sum + i
                    }
                    return sum
                }
            }
        """.trimIndent())
    }


    fun testBreak() {
        expectValue(9, """
            class Foo() {
                fun test() : int {
                    var sum = 0
                    for ( a in 2..10 ) {
                        sum = sum + a
                        if ( a == 4 ) break
                    }
                    return sum
                }
            }
        """.trimIndent())
    }

    fun testContinue() {
        // 2 + 3 + 5 + 6
        expectValue(16, """
            class Foo() {
                fun test() : int {
                    var sum = 0
                    for ( a in 2..6 ) {
                        if ( a == 4 ) continue
                        sum = sum + a
                    }
                    return sum
                }
            }
        """.trimIndent())
    }

    fun testStep() {
        // 2 + 4 + 6
        expectValue(12, """
            class Foo() {
                fun test() : int {
                    var sum = 0
                    for ( a in 2..6 step 2) {
                        sum = sum + a
                    }
                    return sum
                }
            }
        """.trimIndent())
    }

    fun testDownTo() {
        // 5 + 4 + 3 ( break on 2 )
        expectValue(12, """ 
            class Foo() {
                fun test() : int {
                    var sum = 0
                    for ( a in 5 downTo 1 ) {
                        if ( a == 2 ) break
                        sum = sum + a
                    }
                    return sum
                }
            }
        """.trimIndent())
    }

    fun testStrings() {
        expectValue(">H\ne\nl\nl\no\n", """ 
            class Foo() {
                fun test() : String {
                    var result = ">"
                    for ( c in "Hello" ) {
                        result = result + c + '\n' 
                    }
                    return result
                }
            }
        """.trimIndent())
    }

    fun testArrays() {
        expectValue("abcd", """ 
            class Foo() {
                fun test() : String {
                    val str = "ab cd"
                    var result = ""
                    for ( foo in str.split(" ") ) {
                        result = result + foo
                    }
                    return result
                }
            }
        """.trimIndent())
    }

    /**
     * Ensure that a body who's last statement is non-void, the unused value is popped off the stack.
     * In this case, the result of foo() must be popped.
     */
    fun testNonVoidBody() {
        expectValue(
            1, """
            class Foo() {
                fun foo() = 3
                fun test() : int {
                    for ( a in 1..2 ) {
                        foo()
                    }
                    return 1
                }
            }
        """.trimIndent()
        )
    }


    /**
     * This just tests that f.isFile() compiles.
     * Previous to writing this test, "file" was of type Object, not File, so f.isFile failed.
     */
    fun testForArray() {
        expectValueNoSandbox(
            1, """
            import java.io.File
            class Foo() {
                fun test() : int {
                    val files = File(".").listFiles()
                    for (file in files) {
                        if (file.isFile()) {}
                    }
                    return 1
                }
            }
        """.trimIndent()
        )
    }

    /**
     * Very similar to [testForArray], but the type of the "iterable" expression is an implied type.
     * i.e. we need to compile the second method to determine its type.
     */
    fun testForImplied() {
        expectValueNoSandbox(
            1, """
            import java.io.File
            class Foo() {
                fun test() : int {
                    val files = File(".").listFiles()
                    for (file in files()) {
                        file.isFile()
                    }
                    return 1
                }
                fun files() = listOf<File>(File("."))
            }
        """.trimIndent()
        )
    }

    fun testIterator() {
        expectValue(
            6, """
            class Foo() {
                fun test() : int {
                    var sum = 0
                    for ( a in (1..3).iterator() ) {
                        sum += a
                    }
                    return sum
                }
            }
            """.trimIndent()
        )
    }
}
