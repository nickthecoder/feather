package uk.co.nickthecoder.feather.internal.operators

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectValue

/**
 * Tests the != operator
 *
 * See [TestEqual] The tests should be the same, with == replaced by != and the expected result reversed.
 */
class TestNotEqual : TestCase() {

    fun testNotqualsNonNullNonNull() {

        expectValue(
            false,
            """
            class Foo() {
                fun test() : bool {
                    val foo : String = "Hello"
                    val bar : String = "Hello"
                    return foo != bar
                }
            }
            """.trimIndent()
        )

        expectValue(
            true,
            """
            class Foo() {
                fun test() : bool {
                    val foo : String = "Hello"
                    val bar : String = "World"
                    return foo != bar
                }
            }
            """.trimIndent()
        )

    }

    fun testNotEqualsNullNull() {
        expectValue(
            false,
            """
            class Foo() {
                fun test() : bool {
                    val foo : String = null
                    val bar : String = null
                    return foo != bar
                }
            }
            """.trimIndent()
        )
    }

    fun testNotEqualsNonNullNull() {
        expectValue(
            true,
            """
            class Foo() {
                fun test() : bool {
                    val foo : String = "Hello"
                    val bar : String = null
                    return foo != bar
                }
            }
            """.trimIndent()
        )

    }

    fun testNotEqualsNullNonNull() {
        expectValue(
            true,
            """
            class Foo() {
                fun test() : bool {
                    val foo : String = null
                    val bar : String = "Hello"
                    return foo != bar
                }
            }
            """.trimIndent()
        )
    }
    fun testInterfaceNotEqualsNull() {
        expectValue(
            false,
            """
                class Foo() {
                    fun test() : bool {
                        var cs : CharSequence = null
                        return cs != null
                    }
                }
            """
        )
        expectValue(
            true,
            """
                class Foo() {
                    fun test() : bool {
                        var cs : CharSequence = "Hello"
                        return cs != null
                    }
                }
            """
        )

    }
}