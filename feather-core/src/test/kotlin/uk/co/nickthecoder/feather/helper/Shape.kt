package uk.co.nickthecoder.feather.helper

interface Shape

interface Shape2d : Shape

class Square : Shape2d

class Circle : Shape2d
