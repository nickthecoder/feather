package uk.co.nickthecoder.feather.helper

/**
 * Used by TestInterface, to make sure that default implementations are handled correctly.
 */
interface ExampleInterface {

    fun one() = 1

    fun length( a : String ) = a.length

    fun lengthPlus( a : String, b : Int ) = a.length + b

    fun plusLength( b : Int, a : String ) = a.length + b

    companion object {
        @JvmStatic
        var hello = "Hello"
    }
}

