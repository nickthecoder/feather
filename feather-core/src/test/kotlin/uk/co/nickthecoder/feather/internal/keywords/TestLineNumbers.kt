package uk.co.nickthecoder.feather.internal.keywords

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectThrowAt

class TestLineNumbers : TestCase() {

    fun testDivideByZero() {
        expectThrowAt(listOf(5), """
            class Foo() {
            
                fun test() : int {
                
                    return 1 ~/ 0
                }
            }
            """.trimIndent()
        )
    }

    fun testTwoMethods() {
        expectThrowAt(listOf(3,6), """
            class Foo() {
                fun foo() : int {
                    return 1 ~/ 0
                }
                fun test() : int {
                    return foo()
                }
            }
            """.trimIndent()
        )
    }
    fun testTwoExpressionMethods() {
        expectThrowAt(listOf(2,3), """
            class Foo() {
                fun foo() = 1 ~/ 0
                fun test() = foo()
            }
            """.trimIndent()
        )
    }

    fun testNotReturns() {
        expectThrowAt(listOf(3,6), """
            class Foo() {
                fun foo() {
                    1 ~/ 0
                }
                fun test() {
                    foo()
                }
            }
            """.trimIndent()
        )
    }
}
