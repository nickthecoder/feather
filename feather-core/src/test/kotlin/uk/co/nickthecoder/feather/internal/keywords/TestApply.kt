package uk.co.nickthecoder.feather.internal.keywords

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectValue


/**
 * Note, many of these test cases are very similar to those in [TestWith].
 */
class TestApply : TestCase() {

    fun testValue() {
        expectValue(5, """
            class Foo() {
                fun test() : int {
                    return "Hello".apply {
                        // Do nothing in particular
                        this + " World" // Ignored. Result of "apply" is "Hello" itself
                    }.length()
                }
            }
            """.trimIndent()
        )
    }

    fun testMethodCall() {
        expectValue(5, """
            class Foo() {
                fun test() : int {
                    "Hello".apply {
                        return length()
                    }
                }
            }
            """.trimIndent()
        )
    }

    fun testField() {
        expectValue(-1, """
            class Foo() {
                fun test() : int {
                    Plain().apply {
                        return mutableField
                    }
                }
            }
            """.trimIndent()
        )
    }

    fun testGetter() {
        expectValue(2, """
            class Foo() {
                fun test() : int {
                    Plain().apply {
                        return getter
                    }
                }
            }
            """.trimIndent()
        )
    }

    fun testWhichIsAccessed() {
        expectValue( -1, """
            class Foo() : Plain() {
                fun test() : int {
                    mutableField = 100
                    Plain().apply {
                        return mutableField // From Plain, not Foo(), so -1
                    }
                }
            }
            """.trimIndent()
        )
    }

    fun testAlsoSeeFoo() {
        // Can still access classes fields
        expectValue( 42, """
            class Foo() : Plain() {
                val extra = 42
                fun test() : int {
                    mutableField = 100
                    Plain().apply {
                        return extra
                    }
                }
            }
            """.trimIndent()
        )

        // Can still access classes methods
        expectValue( 43, """
            class Foo() : Plain() {
                fun extra() = 43
                fun test() : int {
                    mutableField = 100
                    Plain().apply {
                        return extra()
                    }
                }
            }
            """.trimIndent()
        )
    }

    fun testFromLaterScript() {
        // This one worked (were Bar is declared first)
        expectValue(
            3,
            """
            class Bar() {
                var a = 1
                fun bar() = a + 1
            }
            
            """.trimIndent(),
            """
            class Foo() : Plain() {
                fun test() : int {
                    Bar().apply {
                        a = 2
                        return bar()
                    }
                }
            }
            """.trimIndent()
        )

        // This one failed (where Bar is declared later)
        expectValue(
            3, """
            class Foo() : Plain() {
                fun test() : int {
                    Bar().apply {
                        a = 2
                        return bar()
                    }
                }
            }
            """.trimIndent(),

            """
            class Bar() {
                var a = 1
                fun bar() = a + 1
            }
            
            """.trimIndent()
        )
    }

    /* this@ not implemented yet.
    fun testThisAt() {
        expectValue( -2, """
            class Foo() : Plain() {
                val extra = 42
                fun test() : int {
                    mutableField = 100
                    Plain().apply {
                        mutableField = this@Foo.mutableField - 1
                        return mutableField
                    }
                }
            }
            """.trimIndent()
        )
    }
    */
}
