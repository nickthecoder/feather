package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.compileClass
import uk.co.nickthecoder.feather.helper.noSandBoxCompiler

@kotlin.annotation.Target(AnnotationTarget.FIELD)
annotation class FieldExample

@kotlin.annotation.Target(AnnotationTarget.FUNCTION)
annotation class MethodExample

/**
 */
class TestAnnotations : TestCase() {

    fun testField() {
        val klass = compileClass(
            noSandBoxCompiler,
            """
            import uk.co.nickthecoder.feather.FieldExample

            class Foo() {
                @FieldExample
                var foo = 1
            }
            """.trimIndent()
        )

        val field = klass.getField("foo")
        var annotationCount = 0
        for (an in field.annotations) {
            if (an is FieldExample) {
                annotationCount++
            }
        }
        assertEquals(1, annotationCount)
    }

    fun testMethod() {
        val klass = compileClass(
            noSandBoxCompiler,
            """
            import uk.co.nickthecoder.feather.MethodExample
            class Foo() {
                @MethodExample
                fun foo() = 1
                
                fun test() : int {
                   return foo()
                }
            }
            """.trimIndent()
        )

        val method = klass.getMethod("foo")
        var annotationCount = 0
        for (an in method.annotations) {
            if (an is MethodExample) {
                annotationCount++
            }
        }
        assertEquals(1, annotationCount)
    }

}