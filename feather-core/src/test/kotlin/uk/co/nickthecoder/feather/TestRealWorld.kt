package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectValue
import uk.co.nickthecoder.feather.helper.expectValueNoSandbox
import uk.co.nickthecoder.feather.internal.stringClass

/**
 * These are tests for things that failed using 3rd party classes,
 * they only work on my laptop. Sorry. Please remove them from your testing.
 */
class TestRealWorld : TestCase() {


    /**
     * Within while loops, the Block's nextVariableIndex wasn't being calculated correctly,
     * and therefore local variables would overlap other variables. The compilation would
     * fail quickly when a primitive type overlapped with a non-primitive type.
     */
    fun testGetFilm() {
        expectValue(1, """
            class Foo() {
                fun test() : int {
                    val html = "blah"
                    val h3Tag : String = "<h3>"
                
                    var start : int = 0
                    
                    while (start >= 0) {
            
                        val h3Index = html.indexOf( h3Tag, start )
                        if (false) return 1
                        
                        // If the next declaration is a primitive, then it failed to compile.
                        // If it were a string, then it compiled ok.
                        val foo = 1
                        
                        // Make sure the loop ends
                        start = -1
                    }
                    return 1
                }
            }
            """.trimIndent()
        )
    }

    /**
     * Super classes were not working for ParameterizedTypes, so :
     *
     *      parameters.add(intP)
     *
     * failed, as no methods from the super-interfaces of ObservableList<Parameter> were found.
     */
    fun testCollectionInterfaceAdd() {
        expectValueNoSandbox(
            1,
            """
            library /home/nick/lib/paratask/*.jar
            import uk.co.nickthecoder.paratask.*
            import uk.co.nickthecoder.paratask.parameters.*
      
            class Foo() : AbstractParameterizedTask("foo") {
                val intP = IntegerParameter( "int", 0 )
                
                init {
                    parameters.add(intP)
                }
                fun test() : int {
                    return 1 //intP.value
                }
                override fun run() {}
            }
            """.trimIndent()
        )
    }

    /**
     * At the time of writing, a MetaDataClass was only added to the MetaDataMap when the class was fully parsed.
     * Therefore reference to itself weren't resolved. Since then, the current class is put into the MetaDataMap
     * when the declaration of the class is complete
     * i.e. just after the class NAME... and before the fields and methods.
     */
    fun testSingleton() {
        expectValue(
            "Hello",
            """
            class Foo {
                var instance : Foo = null
                init {
                    instance = this
                }
                val hello = "Hello"
                fun test() = instance.hello
            }
            """.trimIndent()
        )
    }

    /**
     * While fixing [testCollectionInterfaceAdd], I stumbled on another bug...
     *
     *      intP.value
     *
     * failed, because the type was considered as T, not Integer.
     */
    fun testParameterValue() {
        expectValueNoSandbox(
            1, """
            library /home/nick/lib/paratask/*.jar
            import uk.co.nickthecoder.paratask.*
            import uk.co.nickthecoder.paratask.parameters.*
      
            class Foo() : AbstractParameterizedTask("foo") {
                fun test() : int {
                    return IntegerParameter( "int", 1 ).getValue()
                }
                override fun run() {}
            }
            """.trimIndent()
        )
    }

    /**
     * The same as [testParameterValue], but using .value instead of .getValue()
     */
    fun testParameterValue2() {
        expectValueNoSandbox(
            1, """
            library /home/nick/lib/paratask/*.jar
            import uk.co.nickthecoder.paratask.*
            import uk.co.nickthecoder.paratask.parameters.*
      
            class Foo() : AbstractParameterizedTask("foo") {
                fun test() : int {
                    return IntegerParameter( "int", 1 ).value
                }
                override fun run() {}
            }
            """.trimIndent()
        )
    }

    /**
     * In a real-world setting, I was surprised that the "trim()" was NOT applied to the first "if".
     * (The real world code didn't use trim() or Strings, but the structure was identical).
     * It would need brackets around the outer-most "if" for the result that I expected.
     * So, what is the correct precedence?
     * NOTE, if feather had a case statement, then the problem wouldn't have occurred!
     */
    fun ignore_testIfElseIfElseExtra() {
        expectValue( "Hello", """
            class Foo() {
                fun test() : String {
                    val foo = 0
                    return if (foo==0) {
                        "  Hello  "
                    } else if (foo==1) {
                        "  World  "
                    } else {
                        "   Bye   "
                    }.trim()
                }
            }
            """.trimIndent()
        )
        expectValue( "World", """
            class Foo() {
                fun test() : String {
                    val foo = 1
                    return if (foo==0) {
                        "  Hello  "
                    } else if (foo==1) {
                        "  World  "
                    } else {
                        "   Bye   "
                    }.trim()

                }
            }
            """.trimIndent()
        )
        expectValue( "Bye", """
            class Foo() {
                fun test() : String {
                    val foo = 2
                    return if (foo==0) {
                        "  Hello  "
                    } else if (foo==1) {
                        "  World  "
                    } else {
                        "   Bye   "
                    }.trim()
                }
            }
            """.trimIndent()
        )
    }

    /**
     * Compile two scripts, were the first script uses a type contained in the second script.
     * Failed because UnresolvedType.asmDescriptor wasn't trying to resolve the type.
     */
    fun testMultipleScripts() {
        expectValue(
            1,
            """
            class Foo {
                val bar : Bar = null // Doesn't work
                // val bar = Bar() // Works
                fun result() : int {
                    return 1
                }
            }
            """.trimIndent(),
            """
            class Bar {
                fun test() : int {
                    val foo = Foo()
                    return foo.result()
                }
            }
            """.trimIndent()
        )
    }

    /**
     * Same real-world problem as above, this time the unresolved type is used in a Generic Type.
     */
    fun testUnresolvedGenericType() {
        expectValue(
            1,
            """
            class Foo {
                val bar = arrayList<Bar>( 2, 2 )
                fun result() : int {
                    return 1
                }
            }
            """.trimIndent(),
            """
            class Bar {
                fun test() : int {
                    val foo = Foo()
                    return foo.result()
                }
            }
            """.trimIndent()
        )
    }
    /**
     * Same real-world problem as above, this time the unresolved type is used within an "as" statement.
     */
    fun testUnresolvedAs() {
        expectValue(
            1,
            """
            class Foo {
                val bar = arrayList<Bar>( 2, 2 )
                fun result() : int {
                    val obj : Object = Bar()
                    val bar = obj as Bar
                    return 1
                }
            }
            """.trimIndent(),
            """
            class Bar {
                fun test() : int {
                    return 1
                }
            }
            """.trimIndent()
        )
    }

    fun testCallThisParam() {
        expectValue(
            1,
            """
            class Foo {
                fun test() : int {
                    return Bar().bar(this)
                }
            }
            """.trimIndent(),
            """
            class Bar {
                fun bar( foo : Foo ) : int {
                    return 1
                }
            }
            """.trimIndent()
        )
    }

    fun testIfUnresolved() {
        expectValue(
            1,
            """
            class Foo {
                fun test() : int {
                    return if ( Bar().bar() ) 1 else 2
                }
            }
            """.trimIndent(),
            """
            class Bar {
                fun bar() = true
            }
            """.trimIndent()
        )
    }

    fun testUnresolvedTypeField() {
        expectValue(
            false,
            """
            class Foo {
                fun test() : bool {
                    // val s = MySquare() // Works
                    val s : MySquare = MySquare() // Fails
                    return s.isMine
                }
            }
            """.trimIndent(),
            """
            class MySquare {
                var isMine = false
            }
            """.trimIndent()
        )
    }

    fun testEqualsNull() {
        val dollar = "$"

        expectValue(
            false,
            """
            class Foo {
                fun test() : bool {
                    val hello = "Hello"
                    return hello == null
                }
            }
            """.trimIndent()
        )
        expectValue(
            true,
            """
            class Foo {
                fun test() : bool {
                    val hello : String = null
                    return hello == null
                }
            }
            """.trimIndent()
        )
    }

    fun testLiteralClass() {
        expectValue(
            stringClass,
            """
            class Foo {
                fun test() : Class {
                    return String
                }
            }
            """.trimIndent()
        )
    }

    fun testNullString() {
        expectValue(
            null as String?,
            """
            class Foo {
                fun test() : String {
                    return null
                }
            }
            """.trimIndent()
        )
    }

    fun testIsFromSecondSource() {
        expectValue(
            false,
            """
                class Foo {
                    fun test() : bool {
                        return this is Bar
                    }
                }
            """.trimIndent(),
            """
                class Bar {
                }
            """.trimIndent()
        )
    }

    /**
     * The two branches of the if have a Type of a MetaDataType, and at the time of writing the test,
     * feather couldn't work out the "common super class" (despite this case being really easy, as
     * both branches are of the same type).
     */
    fun testIfMetaDataBranches() {
        expectValue(
            "hello",
            """
                class Bar( val text : String ) {
                }

                class Foo {
                    fun test() : String {
                        val bar = if (true) {
                            Bar("hello")
                        } else {
                            Bar("world")
                        }
                        return bar.text
                    }
                }
            """.trimIndent()
        )
    }


    /**
     * This passed (when the scripts were parsed in the order that they were needed,
     * but the [testGrandparentExtendsField2] fails.
     */
    fun testGrandparentExtendsField1() {
        expectValue(
            "hello",
            """
                class Baz {
                    val text = "hello"
                }

            """.trimIndent(),
            """
                class Bar : Baz {
                }

            """.trimIndent(),
            """
                class Foo : Bar {
                    fun test() : String {
                        return text
                    }
                }
            """.trimIndent()


        )
    }

    /**
     * This failed, whereas [testGrandparentExtendsField1] passed.
     * NOTE, it didn't seem to make any difference what Bar extended,
     * The problem stemmed from Bar being unresolved while Foo was being compiled,
     * and Bar's super type was not yet set to anything.
     */
    fun testGrandparentExtendsField2() {
        expectValue(
            "hello",
            """
                class Foo : Bar {
                    fun test() : String {
                        return text
                    }
                }
            """.trimIndent(),
            """
                class Baz {
                    val text = "hello"
                }

            """.trimIndent(),
            """
                class Bar : Baz { // NOTE, using Baz(), it worked, but using just Baz with no brackets failed.
                }

            """.trimIndent()
        )
    }

    /**
     * When we compile Foo, Bar has not been compiled, and Bar's extendsType has not been set yet
     * (due to feather allowing the construct call be be empty, which means that the "implements" list
     * may or may not contain a super class (as opposed to only interfaced)).
     */
    fun testGrandparentOverride() {
        expectValue(
            "hello",
            """
                class Foo : Bar {
                    override fun hello() = "hello"
                    fun test() : String {
                        return hello()
                    }
                }
            """.trimIndent(),
            """
                class Baz {
                    fun hello() = "Hello"
                }

            """.trimIndent(),
            """
                class Bar : Baz {
                }

            """.trimIndent()
        )
    }

    /**
     * When we call a method, if the argument type is a sub-class of an unresolved type,
     * then the method isn't found.
     */
    fun testMethodCallUnresolvedSubClass() {
        expectValue(
            "hello",
            """
                class Test {
                    fun test() : String {
                        return foo( Bar() )
                    }
                    fun foo( a : Foo ) = "hello"
                }
                
                class Bar : Foo {
                }
            """.trimIndent(),
            """
                class Foo {
                    fun hello() = "Hello"
                }

            """.trimIndent()
        )
    }

    /**
     * The solution to this bug was to implement FeatherCompilerWriter, which can resolve the common super types
     * of types, including MetaDataTypes. The default implementation only worked on Classes or course.
     */
    fun testAsSubclass() {
        expectValue(
            "ok",

            """ 
                class Man : TileOccupant {
                    val level = 1
                }
            """.trimIndent(),

            """
                abstract class TileOccupant {
                    fun hello() = "Hello"
                }

            """.trimIndent(),

            """
                class Tile {
                    var region : Region
                    var occupant : TileOccupant
                }
            """.trimIndent(),

            """
                class Village {
                    fun test() : String {
                    
                        // Without this declaration, it compiled ok. Without the .apply it compiled ok.
                        val tile = Tile().apply {}
                        
                        val region = Region()
                        
                        if ( true ) { // Without this if, it compiled ok.
                            var highestMan : Man = null
                            for (t in region.tiles) {
                                val occ = t.occupant
                                if (occ is Man) {
                                    val man = occ as Man
                                    // Without this line it compiled ok.
                                    highestMan = man 
                                }
                            }
                        }
                        
                        return "ok"
                    }
                }
            """.trimIndent(),

            """
                import java.util.*
                
                class Region() {
                    val tiles = ArrayList<Tile>()
                }
            """.trimIndent()

        )
    }

}
