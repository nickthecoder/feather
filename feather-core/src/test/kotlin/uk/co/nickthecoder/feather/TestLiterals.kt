package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.testExpression

class TestLiterals : TestCase() {
    val open = "\${"
    val close = "}"

    fun testBool() {
        testExpression(true, "true")
        testExpression(false, "false")
    }


    fun testByte() {
        // Oh how quaint, Kotlin doesn't have byte literal ;-) (Please correct me if I'm wrong).
        // Java doesn't either!
        testExpression(0.toByte(), "0B")
        testExpression(1.toByte(), "1B")
        testExpression(123.toByte(), "123B")

        testExpression(17.toByte(), "0x11_B") // Note when using hex, the suffix must be _B (Just B would clash with the hex digit)
        testExpression(3.toByte(), "0b11B")

        // NOTE that bytes are signed, so the value of these are actually -1
        testExpression(0xff.toByte(), "0xFF_B")
        testExpression(0xff.toByte(), "0b11111111B")

        // Check that we can also use a lower case b
        testExpression(0xff.toByte(), "0xFF_b")
        testExpression(0xff.toByte(), "0b11111111b")
    }

    fun testShort() {
        testExpression(0.toShort(), "0S")
        testExpression(1.toShort(), "1s")
        testExpression(123.toShort(), "123_S")

        testExpression(17.toShort(), "0x11_s")
        testExpression(3.toShort(), "0b11S")
    }

    fun testInt() {
        testExpression(0, "0")
        testExpression(1, "1")
        testExpression(123, "123")

        testExpression(17, "0x11")
        testExpression(3, "0b11")
    }

    fun testLong() {
        // Note lower and upper case L is allowed in Feather, but not Kotlin nor Java.
        // This seems weird to me, because float can use f or F.
        testExpression(0L, "0_L")
        testExpression(1L, "1L")
        testExpression(123L, "123l")

        testExpression(17L, "0x11_l")
        testExpression(3L, "0b11_L")
    }

    // TODO Float, double and char
    // TODO String and mutli-line string.

    fun testString() {

        testExpression("", """ "" """)
        testExpression("A", """ "A" """)
        testExpression("Hello", """ "Hello" """)
        testExpression("Hello 2", """ "Hello $open 1 + 1 $close" """)
    }

    fun testMultiLineString() {
        val q = "\"\"\""
        testExpression("", """ $q$q """)
        testExpression("A", """ ${q}A${q} """)
        testExpression("Hello", """ ${q}Hello${q} """)
        testExpression("Hello 2", """ ${q}Hello ${1 + 1}${q} """)
    }

    /**
     * It is possible for a smart string to contain other smart strings.
     * FeatherParser used to have a single variable : stringLiteralFirstPart
     * which wouldn't work for nested smart strings. It needs a stack of them.
     */
    fun testNestedStrings() {
        testExpression("Hello World", """ "Hello $open"World"$close" """)
        testExpression("World", """ "$open"World"$close" """)
    }

    /**
     * A single expression within a smart string uses a different path in StringBuilderStackEntry.
     */
    fun testSingleExpressions() {
        testExpression("true", """ "$open true $close" """) // Boolean.toString( boolean )
        testExpression("a", """ "$open 'a' $close" """) // Character.toString( char )
        testExpression("1", """ "$open 1b $close" """) // Byte.toString( byte )
        testExpression("1", """ "$open 1s $close" """) // Short.toString( short )
        testExpression("1", """ "$open 1 $close" """) // Integer.toString( int )
        testExpression("1", """ "$open 1l $close" """) // Long.toString( long )
        testExpression("1.0", """ "$open 1f $close" """) // Float.toString( float )
        testExpression("1.0", """ "$open 1.0 $close" """) // Double.toString( double )
        testExpression("Hello", """ "$open "Hello" $close" """) // String
        testExpression("(1.0,1.0i)", """ "$open Complex(1,1) $close" """) // Any object (Uses .toString)
    }
}
