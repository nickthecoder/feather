package uk.co.nickthecoder.feather.internal.keywords

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectErrorAt
import uk.co.nickthecoder.feather.helper.expectValue

class TestReturn : TestCase() {

    fun testCastNumbers() {

        expectValue(1, """
            class Foo {
                fun test() : int {
                    return 1 // No cast needed
                }
            }
            """.trimIndent()
        )

        expectValue(1, """
            class Foo {
                fun test() : int {
                    return 1.0 // autocast to int
                }
            }
            """.trimIndent()
        )
        expectValue(1.0, """
            class Foo {
                fun test() : double {
                    return 1 // autocast to double
                }
            }
            """.trimIndent()
        )

    }

    fun testNoReturnValue() {
        expectErrorAt(2, 8, "Expected a return value of type", """
            class Foo {
                fun test() : int {
                    return
                }
            }
            """.trimIndent()
        )
    }

    fun testWrongType() {
        expectErrorAt(2, 8, "Expected type java.lang.String but found int", """
            class Foo {
                fun test() : String {
                    return 1
                }
            }
            """.trimIndent()
        )
    }

    fun testValueButVoid() {
        expectErrorAt(2, 8, "Method does not have a return value", """
            class Foo {
                fun test() {
                    return 1
                }
            }
            """.trimIndent()
        )
    }

    fun testReturnIntAsObject() {
        expectValue(1, """
            class Foo {
                fun test() : Object {
                    return 1
                }
            }
            """.trimIndent()
        )
    }

}
