package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectErrorAt
import uk.co.nickthecoder.feather.helper.expectValue

/**
 * Similar tests to [TestFields], but foo is static.
 */
class TestStaticFields : TestCase() {

    fun testNormal() {
        expectValue(1, """
            class Foo() {
                static var foo = 1 // var or val makes no difference.
                fun test() : int {
                    return foo
                }
            }
            """.trimIndent()
        )
        expectValue(1, """
            class Foo() {
                static val foo = 1 // var or val makes no difference.
                fun test() : int {
                    return foo
                }
            }
            """.trimIndent()
        )
    }

    fun testAlterVar() {
        expectValue(2, """
            class Foo() {
                static var foo = 1
                fun test() : int {
                    foo ++ // A var, so we can alter it.
                    return foo
                }
            }
        """.trimIndent())
    }

    fun testAlterVal() {
        expectErrorAt(3, 12, "The value cannot be changed", """
            class Foo() {
                static val foo = 1
                fun test() : int {
                    foo ++ // A val, so this should fail to compile
                    return foo
                }
            }
        """.trimIndent())
    }

    fun testSetVal() {
        expectErrorAt(3, 12, "The value cannot be changed", """
            class Foo() {
                static val foo = 1
                fun test() : int {
                    foo = 2 // A val, so this should fail to compile
                    return foo
                }
            }
        """.trimIndent())
    }

    fun testValWithoutInitialValue() {
        expectErrorAt(1, 4, "val (foo) must be initialised", """
            class Foo() {
                static val foo : int
                fun test() : int {
                    return 1
                }
            }
        """.trimIndent())
    }

    fun testVarWithoutType() {
        expectErrorAt(1, 4, "Cannot determine the type", """
            class Foo() {
                static var foo
                fun test() : int {
                    return 1
                }
            }
        """.trimIndent())
    }


    fun testFromStaticFun() {
        expectValue(1, """
            class Foo() {
                static var foo = 1
                static fun test() : int {
                    return foo
                }
            }
            """.trimIndent()
        )
    }



    fun testStatic3rdParty() {
        expectValue(System.out, """
            class Foo() {
                fun test() : Object {
                    return System.out
                }
            }
            """.trimIndent()
        )
    }

    fun testStaticSelf() {
        expectValue(1, """
            class Foo() {
                static val foo = 1
                fun test() : Object {
                    return Foo.foo
                }
            }
            """.trimIndent()
        )
    }
}
