package uk.co.nickthecoder.feather.helper

open class Plain {

    @JvmField
    val finalField : Int = 1

    @JvmField
    var mutableField : Int = -1

    val getter = 2

    var getSetter = 3

    companion object {

        @JvmField
        val staticFinalField = 101

        @JvmField
        var staticMutableField = -101

        @JvmStatic
        val staticGetter = 102

        @JvmStatic
        var staticGetSetter = 103


    }
}
