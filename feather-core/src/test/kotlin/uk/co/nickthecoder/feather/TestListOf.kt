package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectValue

class TestListOf: TestCase() {

    fun testListOf() {
        expectValue("Hello", """
            class Foo() {
                fun test() : String {
                    val a = listOf<String>("Hello")
                    return a.get(0)
                }
            }
            """.trimIndent()
        )
    }

    /*
    fun testWithoutType() {
        expectValue("Hello", """
            class Foo() {
                fun test() : String {
                    val a = listOf("Hello")
                    return a.get(0)
                }
            }
            """.trimIndent()
        )
    }
    */
}
