package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectErrorAt

class TestPackage: TestCase() {

    fun testEmpty() {
        expectErrorAt(0, 7, "missing", "package")
        expectErrorAt(0, 7, "missing", "package\nclass Foo { }")
    }
}
