package uk.co.nickthecoder.feather.internal.keywords

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectValue
import uk.co.nickthecoder.feather.helper.testExpression

class TestIf: TestCase() {

    fun testSimpleExpression() {
        testExpression(1, "if (true) 1 else 2")
        testExpression(2, "if (false) 1 else 2")

        testExpression(1, """if ( "yes" == "yes") 1 else 2""")
        testExpression(2, """if ( "no" == "yes") 1 else 2""")

        testExpression(2, """if ( "yes" != "yes") 1 else 2""")
        testExpression(1, """if ( "no" != "yes") 1 else 2""")
    }

    fun testEmptyIf() {
        expectValue(1, """
            class Foo {
                fun foo() = 3

                fun test() : int {
                    if (false) {
                    } else {
                        foo()
                    }
                    return 1
                }
            }
            """.trimIndent()
        )
    }

    fun testEmptyElse() {
        expectValue(1, """
            class Foo {
                fun foo() = 3

                fun test() : int {
                    if (false) {
                        foo()
                    } else {
                    }
                    return 1
                }
            }
            """.trimIndent()
        )
    }
    /**
     * This used to throw a java.reflect.NegativeArraySizeException.
     * The "bodge" fix involved adding an extra statement after foo() which returned a void.
     */
    fun testNonVoid() {
        expectValue(1, """
            class Foo {
                fun foo() = 3

                fun test() : int {
                    if (true) {
                        foo()
                    }
                    return 1
                }
            }
            """.trimIndent()
        )
    }

    fun testNonVoidElse() {
        expectValue(1, """
            class Foo {
                fun foo() = 3

                fun test() : int {
                    if (false) {
                        val a = 1
                        a + 1
                    } else {
                        foo()
                    }
                    return 1
                }
            }
            """.trimIndent()
        )
    }

    fun testReturnIntAndDouble() {
        expectValue(1.0, """
            class Foo {
                fun test() : double {
                    return if (true) {
                        1
                    } else {
                        1.1
                    }
                }
            }
            """.trimIndent()
        )
    }

    /**
     * This caused al-formed byte code.
     * The "if" half should be cast to a double, but wasn't.
     */
    fun testIntAndDouble1() {
        expectValue(100.0, """
            class Foo {
                fun test() : double {
                    val b = 100
                    val a = if (true) b else b/2
                    return a
                }
            }
            """.trimIndent()
        )
    }
    /**
     * This caused al-formed byte code.
     * The "if" half should be cast to a double, but wasn't.
     */
    fun testIntAndDouble2() {
        expectValue(100.0, """
            class Foo {
                fun test() : double {
                    val b = 100
                    val a = if (false) b/2 else b
                    return a
                }
            }
            """.trimIndent()
        )
    }

    fun testBothBranchesNull() {
        expectValue(2.0, """
            class Foo {
                fun test() : double {
                    var a = 1
                    if (true) {
                        a += 1
                    } else {
                        a +=2
                        // If the follow is uncommented, then it failed.
                        // ""
                    }
                    return a
                }
            }
            """.trimIndent()
        )
    }


    fun testIfIntElseNull() {
        expectValue(2.0, """
            class Foo {
                fun test() : double {
                    var a = 1
                    if (true) {
                        a ++
                    } else {
                        a +=2
                        // If the follow is uncommented, then it failed.
                        // ""
                    }
                    return a
                }
            }
            """.trimIndent()
        )
    }

}
