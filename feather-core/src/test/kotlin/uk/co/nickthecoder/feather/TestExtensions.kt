package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectValue
import uk.co.nickthecoder.feather.helper.testExpression
import uk.co.nickthecoder.feather.runtime.CharRange
import uk.co.nickthecoder.feather.runtime.Extensions
import uk.co.nickthecoder.feather.runtime.IntRange
import uk.co.nickthecoder.feather.runtime.LongRange
import java.io.File

class TestExtensions : TestCase() {

    fun testString() {
        testExpression("abcabcabcabc", """ "abc" times 4 """)
    }

    fun testChar() {
        testExpression("aaaa", """ 'a' times 4 """)

        testExpression(CharRange('a', 'd'), """ 'a' rangeTo 'd' """)
        testExpression(CharRange.exclusiveRange('a', 'd'), """ 'a' until 'd' """)
    }

    fun testChar2() {
        for (c in 0.toChar()..255.toChar()) {
            testExpression(c.isDigit(), """'$c'.isDigit()""")
            testExpression(c.isLetter(), """'$c'.isLetter()""")
            testExpression(c.isLetterOrDigit(), """'$c'.isLetterOrDigit()""")
            testExpression(c.isUpperCase(), """'$c'.isUpperCase()""")
            testExpression(c.isLowerCase(), """'$c'.isLowerCase()""")

            testExpression(Character.isSpaceChar(c), """'$c'.isSpaceChar()""")
            // NOTE, Java and Kotlin differ on the results.
            // A non breaking space
            testExpression(c.isWhitespace(), """'$c'.isWhitespace()""")
            testExpression(Character.isWhitespace(c) || Character.isSpaceChar(c), """'$c'.isWhitespace()""")

            // TODO Hmm, is this a bug, or a flaw in this test. A CR fails these tests!
            // It's a very weird test though, so I'm happy to ignore it for now. I haven't even considered
            // if putting an ACTUAL carriage return character between single quotes should compile.
            // It does compile, but expected is a CR, and actual is LF. Hmm.
            // See testCarriageReturnConstant.
            if (c.toInt() != 13) {
                testExpression(c.toUpperCase(), """'$c'.toUpperCase()""")
                testExpression(c.toLowerCase(), """'$c'.toLowerCase()""")
            }
        }
    }

    // TODO Putting a char 13 (carriage return) inside single quotes feather returns a char 10 ( line feed )
    //fun testCarriageReturnConstant() {
    //    val c = 13.toChar()
    //    testExpression(c, """'$c'""")
    //}

    /**
     * Highlights the issues around the definition of isWhitespace for char 160 (non breaking space).
     * Java considers it false, whereas Kotlin (and Unicode) considers it true.
     * Feather sides with Kotlin, and considers NBSP to be whitespace.
     */
    fun testNBSPConstant() {
        val c = 160.toChar()
        testExpression(c, """'$c'""")

        testExpression(true, """'$c'.isWhitespace()""")
        // Feather agrees with Kotlin
        testExpression(c.isWhitespace(), """'$c'.isWhitespace()""")
        // But disagrees with Java.
        testExpression(! Character.isWhitespace(c), """'$c'.isWhitespace()""")

    }

    fun testInt() {
        testExpression(IntRange(1, 4), """ 1 rangeTo 4 """)
        testExpression(IntRange.exclusiveRange(1, 4), """ 1 until 4 """)

        testExpression(6, "3 * 2")
    }

    fun testLong() {
        testExpression(LongRange(1, 4), """ 1_l rangeTo 4_l """)
        testExpression(LongRange.exclusiveRange(1, 4), """ 1_l until 4_l """)
    }

    fun testSplit() {
        // I use toList, because assertEquals doesn't consider arrays the same even if the contents are the same.
        assertEquals(listOf("Hello", "World"), Extensions.splitLines("Hello\nWorld").toList())
        assertEquals(listOf("Hello", "World"), Extensions.splitLines("Hello\nWorld\n").toList())
    }

    fun testPathWithoutExtension() {
        assertEquals("./hello", Extensions.pathWithoutExtension(File("./hello.txt")))
        assertEquals("./.hello", Extensions.pathWithoutExtension(File("./.hello.txt")))
        assertEquals("./.config", Extensions.pathWithoutExtension(File("./.config")))
    }

    fun testNameWithoutExtension() {
        assertEquals("hello", Extensions.nameWithoutExtension(File("hello.txt")))
        assertEquals(".hello", Extensions.nameWithoutExtension(File(".hello.txt")))
        assertEquals(".config", Extensions.nameWithoutExtension(File(".config")))
    }

    fun testFileExtension() {
        assertEquals("txt", Extensions.extension(File("hello.txt")))
        assertEquals("txt", Extensions.extension(File(".hello.txt")))
        assertEquals("", Extensions.extension(File(".config")))
    }

    fun testStringIterator() {
        expectValue( "olleH", """
            class Foo() {

                fun test() : String {
                    var result = ""
                    
                    for ( c in "Hello" ) {
                        result = c.toString() + result
                    }
                    
                    return result
                }
            }
            """.trimIndent()
        )
    }

    fun testFeatherExtension() {
        expectValue( 'H', """
            package baz
            
            class Baz() {
                static fun firstChar( str : String ) = str.charAt(0)
            }
            """.trimIndent(),
            """
            import static baz.Baz.*
            
            class Foo() {

                fun test() : char {
                    return "Hello".firstChar()
                }
            }
            """.trimIndent()
        )
    }

    fun testFeatherFunctionExtension() {
        expectValue( 'H', """
            package baz
            
            class Baz() {
                static fun firstChar( function : ()->String ) = function().charAt(0)
            }
            """.trimIndent(),
            """
            import static baz.Baz.*
            
            class Foo() {                
                fun test() : char {
                    return this:>foo.firstChar()
                }
                
                fun foo() = "Hello"

            }
            """.trimIndent()
        )
    }
}
