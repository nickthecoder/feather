package uk.co.nickthecoder.feather.helper

class ExampleFunctionExtension {
    companion object {

        /**
         * Make sure that we can use extension function on the Function types!
         * See TestFunctionTypes.testFunctionExtension.
         */
        @JvmStatic
        fun uk.co.nickthecoder.feather.reflect.Function<String, Void, Void, Void, Void, Void, Void, Void, Void, String>.curryNick() =
            this.curry("Nick")
    }

}