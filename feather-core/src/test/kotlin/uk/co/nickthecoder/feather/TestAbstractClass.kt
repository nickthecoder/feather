package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectValue

class TestAbstractClass : TestCase() {

    fun testAbstractMethod() {
        expectValue(
            "Hello", """
            abstract class Abstract() {
                abstract fun foo() : String
            }
            class Foo() : Abstract {
                override fun foo() = "Hello"
                fun test() : String = foo()
            }
            """.trimIndent()
        )
    }

    fun testDoubleAbstractMethod() {
        expectValue(
            "Hello", """
            abstract class Base() {
                abstract fun foo() : String
            }
            abstract class Abstract() : Base() {
            }
            
            class Foo() : Abstract {
                override fun foo() = "Hello"
                fun test() : String = foo()
            }
            """.trimIndent()
        )
    }

}