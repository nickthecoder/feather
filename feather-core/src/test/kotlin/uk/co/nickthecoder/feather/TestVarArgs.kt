package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectErrorAt
import uk.co.nickthecoder.feather.helper.expectValue
import uk.co.nickthecoder.feather.internal.asmMethodDescription
import uk.co.nickthecoder.feather.internal.voidPrimitive

class VarArgConstruct(vararg val ints: Int) {
    fun get(index: Int) = ints[index]
}

class TestVarArgs : TestCase() {

    fun testFirst() {
        expectValue("Hello", """
            class Foo() {
                fun first( items : String... ) = items[0]
                
                fun test() = first( "Hello", "World" )
            }
        """.trimIndent())
    }

    fun testIgnore() {
        expectValue("Hello", """
            class Foo() {
                fun first( ignore : String, items : String... ) = items[0]
                
                fun test() = first( "Ignore", "Hello", "World" )
            }
        """.trimIndent())
    }

    fun testIgnoreDifferent() {
        expectValue("Hello", """
            class Foo() {
                fun first( ignore : int, items : String... ) = items[0]
                
                fun test() = first( 1, "Hello", "World" )
            }
        """.trimIndent())
    }

    fun testAutoCast() {
        expectValue(1, """
            class Foo() {
                fun first( items : int... ) = items[0]
                
                fun test() = first( 1.0, 2, 3.0 )
            }
        """.trimIndent())
    }

    fun testCannotCast() {
        expectErrorAt(3, 17, "No such method", """
            class Foo() {
                fun first( items : int... ) = items[0]
                
                fun test() = first( 1.0, 'c', 3.0 )
            }
        """.trimIndent())
    }

    fun testAsmMethodDescription() {
        val intArray = IntArray(0)
        assertEquals("([I)V", asmMethodDescription(listOf(intArray.javaClass), voidPrimitive))
    }


    fun testToList() {

        expectValue(42, """
            class Foo() { 
                fun test() : int {
                    val list = listOf<int>( 42, 2 )
                    return list[0]
                }
            }
        """.trimIndent())

    }


    fun testVarargWrongGenericParameterType() {

        expectErrorAt(2, 19, "No such method", """
            class Foo() { 
                fun test() : int {
                    val list = listOf<int>( 42, "Hello" )
                    return list[0]
                }
            }
        """.trimIndent())

    }

    fun testToListMissingTypeArgs() {

        expectErrorAt(2, 19, "No such method", """
            class Foo() { 
                fun test() : int {
                    val list = listOf( 42, 2 )
                    return list[0]
                }
            }
        """.trimIndent())

    }

    fun testMapOf() {

        expectValue("Meaning of Life", """
            class Foo() { 
                fun test() : String {
                    val map = mapOf<int,String>( 42 => "Meaning of Life", 0 => "Nothing" )
                    return map[42]
                }
            }
        """.trimIndent())

    }

    fun testSetOf() {

        expectValue("HashSet2", """
            class Foo() { 
                fun test() : String {
                    val set = setOf<String>( "Hello", "World" )
                    return set.getClass().simpleName + set.size()
                }
            }
        """.trimIndent())

    }

    fun testVarArgConstructorCall() {

        expectValue(42, """
            import uk.co.nickthecoder.feather.VarArgConstruct
            class Foo() { 
                fun test() : int {
                    val foo = VarArgConstruct( 42, 2 )
                    return foo.get(0)
                }
            }
        """.trimIndent())

    }

    /**
     * I wanted to test that b.value() worked. It does, but from real-world usage, I thought this test was going to fail.
     * Oh well, may as well keep the test now that it is written ;-)
     */
    fun testLoopVarType() {
        expectValue(2, """
            class Bar() {
                fun value() = 1
            }
            
            class Foo() {
                fun foo( bars : Bar... ) : int {
                    var result = 0
                    for ( b in bars ) {
                        result += b.value()
                    }
                    return result
                }
                fun test() : int {
                    return foo( Bar(), Bar() ) 
                }
            }
        """.trimIndent())
    }

}
