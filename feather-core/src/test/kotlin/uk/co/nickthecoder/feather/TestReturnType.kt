package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectErrorAt
import uk.co.nickthecoder.feather.helper.expectValue

class TestReturnType : TestCase() {

    fun testSimple() {
        expectValue(
            1, """
            interface Bar {
                fun bar() : int
            }
            
            class Foo() : Bar {
                override fun bar() = 1
                fun test() : int {
                    return bar()
                }
            }
            """.trimIndent()
        )
        expectValue(
            1, """
            interface Bar {
                fun bar() : int
            }
            
            class Foo() : Bar {
                override fun bar() : int = 1
                fun test() : int {
                    return bar()
                }
            }
            """.trimIndent()
        )
    }

    fun testFail() {
        expectErrorAt(
            5, 4, "Override return type mismatch. Expected java.lang.String but found int",
            """
            interface Bar {
                fun bar() : String
            }
            
            class Foo() : Bar {
                override fun bar() : int = 1
                fun test() : int {
                    return bar()
                }
            }
            """.trimIndent()
        )
    }

    /*
    fun testAutoCast() {
        expectErrorAt(
            5, 4, "The return type",
            """
            interface Bar {
                fun bar() : double
            }

            class Foo() : Bar {
                override fun bar() = 1
                fun test() : int {
                    bar()
                    return 1
                }
            }
            """.trimIndent()
        )
    }
    */

    fun testWasInt() {
        expectErrorAt(
            5, 4, "Override return type mismatch. Expected int but found void",
            """
            interface Bar {
                fun bar() : int
            }
            
            class Foo() : Bar {
                override fun bar() {}
                fun test() : int {
                    bar()
                    return 1
                }
            }
            """.trimIndent()
        )
    }

    fun testNoReturnValue() {
        expectValue(
            1, """
            interface Bar {
                fun bar() : Object
            }
            
            class Foo() : Bar {
                //override fun bar() : Object {return null} // This always worked
                override fun bar() {} // But this didn't work
                fun test() : int {
                    bar()
                    return 1
                }
            }
            """.trimIndent()
        )
    }
}
