package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectErrorAt
import uk.co.nickthecoder.feather.helper.expectValue

class TestMethods : TestCase() {

    fun testNormal() {
        expectValue(1, """
            class Foo() {
                fun test() : int {
                    return 1
                }
            }
            """.trimIndent()
        )
    }

    fun testMissingBrackets() {
        expectErrorAt(1, 12, "no viable alternative", """
            class Foo() {
                fun foo : Int = 42
            }
            """.trimIndent()
        )
    }

    fun testStaticBefore() {
        expectValue(2, """
            class Foo() {
                static fun bar() = 2
                
                fun test() : Object {
                    return Foo.bar()
                }

            }
            """.trimIndent()
        )
    }

    fun testStaticAfter() {
        expectValue(2, """
            class Foo() {
                fun test() : Object {
                    return Foo.bar()
                }
                static fun bar() = 2
            }
            """.trimIndent()
        )
    }

    fun testInterfaceImpl() {
        expectValue(0, """            
            class MyRunnable : Runnable {
                override fun run() {
                    
                }
            }
            
            class Foo() {
                fun test() : Object {
                    MyRunnable().run()
                    return 0
                }
            }
            """.trimIndent()
        )
    }

    fun testStaticMethodCall() {
        expectValue(
            1, """
            class Bar {
                static fun bar() = 1
            }
        """, """
            class Foo () {
                fun test() = Bar.bar()
            }
        """.trimIndent()
        )
    }

    fun testDuplicateMethod() {
        expectErrorAt(
            3, 20, "Duplicate method",
            """
                class Foo() {
                    fun test() = 1
                    fun test() = 2
                }
                """
        )

        expectErrorAt(
            3, 20, "Duplicate method",
            """
                class Foo() {
                    fun test(a : String) = 1
                    fun test(b : String) = 2
                }
                """
        )

        expectErrorAt(
            5, 20, "Duplicate method",
            """
                class Foo() {
                    fun test() = 1
                    
                    fun test(a : String) = 1 // Okay
                    fun test(b : String) = 2 // Should fail here
                }
                """
        )


        expectErrorAt(
            5, 20, "Duplicate method",
            """
                class Foo() {
                    fun test(a : String) = 1
                    
                    fun test(a : String, b : int) = 1 // Okay. Not the same as the one above
                    fun test(b : String, c : int) = 2 // Should fail here.
                }
                """
        )
    }
}
