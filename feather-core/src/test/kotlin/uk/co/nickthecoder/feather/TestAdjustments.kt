package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectValue

/**
 * Tests ++, -- etc
 * We should test each against fields, static fields, local variables, arrays and lists, as there are separate
 * implementations for each.
 *
 * Also, check doubles (and/or longs), because these require different op-codes when duplicating the
 * values.
 */
class TestAdjustments : TestCase() {

    fun testPostIncLocal() {
        expectValue(
            4, """
            class Foo() {
                fun test() : int {
                    var local = 3
                    local ++
                    return local
                }
            }
            """.trimIndent()
        )
    }
    fun testPostIncLocalLong() {
        expectValue(
            4L, """
            class Foo() {
                fun test() : long {
                    var local = 3L
                    local ++
                    return local
                }
            }
            """.trimIndent()
        )
    }
    fun testPostIncField() {
        expectValue(
            4, """
            class Foo() {
                var field = 3
                
                fun test() : int {
                    field ++                    
                    return field
                }
            }
            """.trimIndent()
        )
    }
    fun testPostIncFieldLong() {
        expectValue(
            4L, """
            class Foo() {
                var field = 3L
                
                fun test() : long {
                    field ++                    
                    return field
                }
            }
            """.trimIndent()
        )
    }
    fun testPostIncStaticField() {
        expectValue(
            4, """
            class Foo() {
                static var field = 3
                
                fun test() : int {
                    field ++                    
                    return field
                }
            }
            """.trimIndent()
        )
    }
    fun testPostIncStaticFieldLong() {
        expectValue(
            4L, """
            class Foo() {
                static var field = 3
                
                fun test() : long {
                    field ++                    
                    return field
                }
            }
            """.trimIndent()
        )
    }
    fun testPostIncArray() {
        expectValue(
            4, """
            class Foo() {
                fun test() : int {
                    val array = intArrayOf( 3 )
                    array[0] ++
                    return array[0]
                }
            }
            """.trimIndent()
        )
    }
    fun testPostIncArrayLong() {
        expectValue(
            4L, """
            class Foo() {
                fun test() : long {
                    val array = longArrayOf( 3L )
                    array[0] ++
                    return array[0]
                }
            }
            """.trimIndent()
        )
    }
    fun testPostIncList() {
        expectValue(
            4, """
            class Foo() {
                fun test() : int {
                    val list = listOf<int>( 3 )
                    list[0] ++
                    return list[0]
                }
            }
            """.trimIndent()
        )
    }
    fun testPostIncListLong() {
        expectValue(
            4L, """
            class Foo() {
                fun test() : long {
                    val list = listOf<long>( 3L )
                    list[0] ++
                    return list[0]
                }
            }
            """.trimIndent()
        )
    }

    fun testPostDecLocal() {
        expectValue(
            2, """
            class Foo() {
                fun test() : int {
                    var local = 3
                    local --
                    return local
                }
            }
            """.trimIndent()
        )
    }
    fun testPostDecField() {
        expectValue(
            2, """
            class Foo() {
                var field = 3
                
                fun test() : int {
                    field --              
                    return field
                }
            }
            """.trimIndent()
        )
    }
    fun testPostDecStaticField() {
        expectValue(
            2, """
            class Foo() {
                static var field = 3
                
                fun test() : int {
                    field --              
                    return field
                }
            }
            """.trimIndent()
        )
    }
    fun testPostDecArray() {
        expectValue(
            2, """
            class Foo() {
                fun test() : int {
                    val array = intArrayOf( 3 )
                    array[0] --
                    return array[0]
                }
            }
            """.trimIndent()
        )
    }
    fun testPostDecList() {
        expectValue(
            2, """
            class Foo() {
                fun test() : int {
                    val list = listOf<int>( 3 )
                    list[0] --
                    return list[0]
                }
            }
            """.trimIndent()
        )
    }

    /**
     * Checks that the "old" value is returned from the expression, and that the value has been adjusted.
     */
    fun testPostInc2Local() {
        expectValue(
            12, """
            class Foo() {
                fun test() : int {
                    var local = 3
                    return local++ * local
                }
            }
            """.trimIndent()
        )
    }
    fun testPostInc2Field() {
        expectValue(
            12, """
            class Foo() {
                var field = 3
                
                fun test() : int {
                    return field++ * field
                }
            }
            """.trimIndent()
        )
    }
    fun testPostInc2StaticField() {
        expectValue(
            12, """
            class Foo() {
                static var field = 3
                
                fun test() : int {
                    return field++ * field
                }
            }
            """.trimIndent()
        )
    }
    fun testPostInc2Array() {
        expectValue(
            12, """
            class Foo() {
                fun test() : int {
                    val array = intArrayOf( 3 )
                    return array[0]++ * array[0]
                }
            }
            """.trimIndent()
        )
    }
    fun testPostInc2List() {
        expectValue(
            12, """
            class Foo() {
                fun test() : int {
                    val list = listOf<int>( 3 )
                    return list[0]++ * list[0]
                }
            }
            """.trimIndent()
        )
    }


    /**
     * Checks that the "old" value is returned from the expression, and that the value has been adjusted.
     */
    fun testPostDec2Local() {
        expectValue(
            6, """
            class Foo() {
                fun test() : int {
                    var local = 3
                    return local-- * local
                }
            }
            """.trimIndent()
        )
    }
    fun testPostDec2Field() {
        expectValue(
            6, """
            class Foo() {
                var field = 3
                
                fun test() : int {
                    return field-- * field
                }
            }
            """.trimIndent()
        )
    }
    fun testPostDec2StaticField() {
        expectValue(
            6, """
            class Foo() {
                static var field = 3
                
                fun test() : int {
                    return field-- * field
                }
            }
            """.trimIndent()
        )
    }
    fun testPostDec2Array() {
        expectValue(
            6, """
            class Foo() {
                fun test() : int {
                    val array = intArrayOf( 3 )
                    return array[0]-- * array[0]
                }
            }
            """.trimIndent()
        )
    }
    fun testPostDec2List() {
        expectValue(
            6, """
            class Foo() {
                fun test() : int {
                    val list = listOf<int>( 3 )
                    return list[0]-- * list[0]
                }
            }
            """.trimIndent()
        )
    }

}
