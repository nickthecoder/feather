package uk.co.nickthecoder.feather.helper

import uk.co.nickthecoder.feather.SandBox

class AllowAll : SandBox {
    override fun isAllowed(klass: Class<*>) = true

    override fun clone() = AllowAll()
}