package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectErrorAt
import uk.co.nickthecoder.feather.helper.expectValue

/**
 * Similar tests to [TestFields]
 */
class TestLocalVariables : TestCase() {

    fun testNormal() {
        expectValue(1, """
            class Foo() {
                fun test() : int {
                    var foo = 1 // Var or var makes no difference.
                    return foo
                }
            }
            """.trimIndent()
        )
        expectValue(1, """
            class Foo() {
                fun test() : int {
                    val foo = 1 // Var or var makes no difference.
                    return foo
                }
            }
            """.trimIndent()
        )
    }

    fun testAlterVar() {
        expectValue(2, """
            class Foo() {
                fun test() : int {
                    var foo = 1
                    foo ++ // A var, so we can alter it.
                    return foo
                }
            }
        """.trimIndent())
    }

    fun testAlterVal() {
        expectErrorAt(3, 12, "The value cannot be changed", """
            class Foo() {
                fun test() : int {
                    val foo = 1 
                    foo ++ // A val, so this should fail to compile
                    return foo
                }
            }
        """.trimIndent())
    }

    fun testSetVal() {
        expectErrorAt(3, 12, "The value cannot be changed", """
            class Foo() {
                fun test() : int {
                    val foo = 1 
                    foo = 2 // A val, so this should fail to compile
                    return foo
                }
            }
        """.trimIndent())
    }

    fun testValWithoutInitialValue() {
        expectErrorAt(2, 8, "val (foo) must be initialised", """
            class Foo() {
                fun test() : int {
                    val foo : int
                    return 1
                }
            }
        """.trimIndent())
    }

    fun testVarWithoutType() {
        expectErrorAt(2, 8, "Cannot determine the type", """
            class Foo() {
                fun test() : int {
                    var foo
                    return 1
                }
            }
        """.trimIndent())
    }

    fun testVarNotDeclared() {
        expectErrorAt(2, 15, "Unknown identifier : bar", """
            class Foo() {
                fun test() : int {
                    return bar
                }
            }
        """.trimIndent())
    }

    fun testVarDeclaredLater() {
        expectErrorAt(2, 18, "Unknown identifier : baz", """
            class Foo() {
                fun test() : int {
                    val bar = baz
                    val baz = 1
                    return bar
                }
            }
        """.trimIndent())
    }

    fun testVarDeclaredTwice() {
        expectErrorAt(3, 12, "Duplicate local variable : bar", """
            class Foo() {
                fun test() : int {
                    val bar = 1
                    val bar = 2
                    return bar
                }
            }
        """.trimIndent())
    }

}
