package uk.co.nickthecoder.feather.helper

import java.util.*

class Complex(val real: Double, val imaginary: Double) {

    operator fun unaryMinus() = Complex(-real, -imaginary)

    operator fun plus(other: Complex) = Complex(real + other.real, imaginary + other.imaginary)
    operator fun minus(other: Complex) = Complex(real - other.real, imaginary - other.imaginary)

    operator fun plus(other: Double) = Complex(real + other, imaginary)
    operator fun minus(other: Double) = Complex(real - other, imaginary)

    operator fun times(other: Double) = Complex(real * other, imaginary * other)
    operator fun div(other: Double) = Complex(real / other, imaginary / other)

    override fun hashCode() = Objects.hash(real, imaginary)

    override fun equals(other: Any?): Boolean {
        return if (other is Complex) {
            real == other.real && imaginary == other.imaginary
        } else {
            false
        }
    }

    override fun toString() = "($real,${imaginary}i)"

}
