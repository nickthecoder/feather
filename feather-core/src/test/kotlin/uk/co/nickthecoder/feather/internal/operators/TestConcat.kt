package uk.co.nickthecoder.feather.internal.operators

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.testExpression

class TestConcat : TestCase() {

    fun testConcat() {
        testExpression("HelloWorld", """ "Hello" + "World" """)
        testExpression("Hello1", """ "Hello" + 1 """)
        testExpression("Hello1.0", """ "Hello" + 1.0 """)
        testExpression("Hello1.0", """ "Hello" + 1.0f """)
        testExpression("Hello1", """ "Hello" + 1b """)
        testExpression("HelloX", """ "Hello" + 'X' """)

        // Java can't do this ;-)
        testExpression("ABC", """ 'A' + 'B' + 'C' """)

    }

}
