package uk.co.nickthecoder.feather

import junit.framework.TestCase

class TestNew : TestCase() {

    // I use this when creating a newly failing test, so that it is easy to run just the one test.
    // Once it passes, I put the test elsewhere, and this is empty again.
    fun testNew() {

    }
}
