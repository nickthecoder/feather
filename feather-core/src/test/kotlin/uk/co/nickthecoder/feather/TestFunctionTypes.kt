package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectErrorAt
import uk.co.nickthecoder.feather.helper.expectValue

class TestFunctionTypes : TestCase() {

    /**
     * Used in the early stages of Function types, just to check the compiler can parse function types.
     * The function isn't actually used!
     */
    fun testDeclaration() {
        expectValue(
            "yes", """
            class Foo() {
                fun foo( func : (String) -> String ) {}
                fun bar( func : (int) -> float ) {}
                fun test() = "yes"
            }
            """.trimIndent()
        )
    }

    /**
     * The simplest use of a function type.
     */
    fun testImportedStatic() {
        expectValue(
            "[Hello, World]", """
            import static uk.co.nickthecoder.feather.runtime.Extensions.toUpperCase;

            class Foo() {
                fun test() : String {
                    val foo = ::splitLines<String>
                    return foo("Hello\nWorld").toString()
                }
            }
            """.trimIndent()
        )
    }

    /**
     * The implicit parameter types
     */
    fun testImplicitParameterTypes() {
        expectValue(
            "[Hello, World]", """
            import static uk.co.nickthecoder.feather.runtime.Extensions.toUpperCase;

            class Foo() {
                fun test() : String {
                    val foo = ::splitLines
                    return foo("Hello\nWorld").toString()
                }
            }
            """.trimIndent()
        )
    }


    /**
     * The simplest use of a function type.
     */
    fun testFunctionAsAField() {
        expectValue(
            "[Hello, World]", """
            import static uk.co.nickthecoder.feather.runtime.Extensions.toUpperCase;

            class Foo() {
                val foo = ::splitLines<String>

                fun test() : String {
                    return foo("Hello\nWorld").toString()
                }
            }
            """.trimIndent()
        )
    }

    /**
     * Currying a simple function
     */
    fun testCurry() {
        expectValue(
            "[Hello, World]", """
            import static uk.co.nickthecoder.feather.runtime.Extensions.toUpperCase;

            class Foo() {
                fun test() : String {
                    val foo = ::splitLines<String>
                    val bar = foo.curry("Hello\nWorld")
                    return bar().toString()
                }
            }
            """.trimIndent()
        )
    }

    /**
     * Using a primitive arg type, and return type.
     */
    fun testPrimitiveArg() {
        expectValue(
            false, """
            import static uk.co.nickthecoder.feather.runtime.Extensions.toUpperCase;

            class Foo() {
                fun test() : bool {
                    val foo = ::isUpperCase<char>
                    return foo('x')
                }
            }
            """.trimIndent()
        )
    }

    /**
     * Using a primitive arg type, and return type, but implicit
     */
    fun testPrimitiveArgImplicit() {
        expectValue(
            false, """
            import static uk.co.nickthecoder.feather.runtime.Extensions.toUpperCase;

            class Foo() {
                fun test() : bool {
                    val foo = ::isUpperCase<char>
                    return foo('x')
                }
            }
            """.trimIndent()
        )
    }

    /**
     * Currying with a primitive types.
     */
    fun testCurryPrimitive() {
        expectValue(
            false, """
            import static uk.co.nickthecoder.feather.runtime.Extensions.toUpperCase;

            class Foo() {
                fun test() : bool {
                    val foo = ::isUpperCase<char>
                    val bar = foo.curry( 'x' )
                    return bar()
                }
            }
            """.trimIndent()
        )
    }

    /**
     * Currying with the wrong type.
     */
    fun testCurryPrimitiveWrongType() {
        expectErrorAt(3, 21, "Wrong curry argument. Expected char", """
            class Foo() {
                fun test() : bool {
                    val foo = ::isUpperCase<char>
                    val bar = foo.curry( "x" )
                    return bar()
                }
            }
            """.trimIndent()
        )
    }


    /**
     * From a named (feather) class
     */
    fun testNamedClass() {
        expectValue(
            'e', """
            class Bar() {
                static fun nth(n: int, str: String) = str.charAt(n)
            }
            
            class Foo() {
                fun test() : char {
                    val func = Bar::nth<int,String>
                    return func(1, "Hello")
                }
            }
            """.trimIndent()
        )
    }
    /**
     * From a named (feather) class - implicit types
     */
    fun testNamedClassImplicit() {
        expectValue(
            'e', """
            class Bar() {
                static fun nth(n: int, str: String) = str.charAt(n)
            }
            
            class Foo() {
                fun test() : char {
                    val func = Bar::nth
                    return func(1, "Hello")
                }
            }
            """.trimIndent()
        )
    }

    /**
     * A function using a non-static method
     */
    fun testNonStatic() {
        expectValue(
            'e', """
            class Foo() {
                fun test() : char {
                    val func = String::charAt<int>
                    return func("Hello", 1)
                }
            }
            """.trimIndent()
        )
    }

    /**
     * A function using a non-static method, curried with its subject
     */
    fun testPreCurried() {
        expectValue(
            'e', """
            class Foo() {
                fun test() : char {
                    val func = "Hello":>charAt<int>
                    return func(1)
                }
            }
            """.trimIndent()
        )
    }
    /**
     * A function using a non-static method, curried with its subject - Implicit types
     */
    fun testPreCurriedImplicit() {
        expectValue(
            'e', """
            class Foo() {
                fun test() : char {
                    val func = "Hello":>charAt
                    return func(1)
                }
            }
            """.trimIndent()
        )
    }

    /**
     *
     */
    fun testPreCurriedFromExpression() {
        expectValue(
            'E', """
            class Foo() {
                fun bar() = "Hello"
                fun test() : char {
                    val func = bar().toUpperCase():>charAt<int>
                    return func(1)
                }
            }
            """.trimIndent()
        )
    }

    /**
     * This is from a real-world setting. (Tickle project).
     * At that time, function types were only being looked for in local variables, not fields.
     */
    fun testAction() {
        expectValue(
            true, """
            class FunctionAction( val x : ()->bool ) {
                fun begin() = true
                fun act() = x()
            }
            class Foo() {
                static fun alwaysTrue() = true
                fun test() : bool {
                    return FunctionAction( ::alwaysTrue ).act()
                }
            }
            
            """.trimIndent()
        )
    }


    /**
     * This is from a real-world setting. (Tickle project).
     * At that time, function types were only being looked for in local variables, not fields.
     */
    fun testAction2() {
        expectValue(
            true, """
            class FunctionAction( val x : ()->bool ) {
                fun act() = x()
            }
            class Foo() {
                fun walk() = true
                fun test() : bool {
                    return FunctionAction( this:>walk ).act()
                }
            }
            
            """.trimIndent()
        )
    }

    fun testZeroParameters() {
        expectValue(
            true, """
            class FunctionAction( val x : () ) {
                fun act() : bool {
                    x()
                    return true
                }
            }
            class Foo() {
                fun walk() {}
                fun test() : bool {
                    val a = this:>walk
                    return FunctionAction( a ).act()
                    //return FunctionAction( this:>walk ).act()
                }
            }
            
            """.trimIndent()
        )
    }

    /**
     * Testing that Function types can also have extension functions.
     * This worked as intended, but it doesn't work when I merge the two lines,
     * thus removing the unnecessary local variable.
     */
    fun testFunctionExtension() {
        expectValue(
            "Hello Nick", """
            import static uk.co.nickthecoder.feather.helper.ExampleFunctionExtension.*
            class Foo() {
                fun greet( name : String ) = "Hello " + name
                
                fun test() : String {
                    val curriedFunction : ()->String = this:>greet.curryNick()
                    return curriedFunction()
                }
            }
            """.trimIndent()
        )
    }

    /**
     * We still have the two lines and the unnecessary local variable, but not its
     * type is implied (not specified). This failed! Eek!
     * The bug was testing if the type was [ParameterizedTypeImplementaion] (the one defined in Feather),
     * rather than the interface [ParameterizedType] in java.lang.reflect.
     */
    fun testFunctionExtension2() {
        expectValue(
            "Hello Nick", """
            import static uk.co.nickthecoder.feather.helper.ExampleFunctionExtension.*
            class Foo() {
                fun greet( name : String ) = "Hello " + name
                
                fun test() : String {
                    val curriedFunction = this:>greet.curryNick()
                    return curriedFunction()
                }
            }
            """.trimIndent()
        )
    }

    /**
     * Even after I fixed the bug described above, this still failed. Oh no!
     * When I tested the statement in the ANTLR Preview, it was as expected.
     * However, the way I call a method/function is in exitMethodCall,
     * and here, the left of the second set of parentheses is an invokable expression.
     * The solution was to add code into exitInvokableExpression.
     */
    fun testFunctionExtension3() {
        expectValue(
            "Hello Nick", """
            import static uk.co.nickthecoder.feather.helper.ExampleFunctionExtension.*
            class Foo() {
                fun greet( name : String ) = "Hello " + name
                
                fun test() : String {
                    return this:>greet.curryNick()()
                }
            }
            """.trimIndent()
        )
    }

    /**
     * From a real-world bug report. The wrong return type of a Function didn't report a problem.
     */
    fun testFunctionTypeMatches() {
        expectErrorAt(6,26,"Cannot perform operation '='",
            """
            class Foo() {
                fun test() {
                    // This one is okay
                    val func1 : (int)->String = this:>returnsString
   
                    // Wrong return type
                    val func2 : (int) = this:>returnsString
                }
                fun returnsString( i : int ) = "Hello"
            }
            """.trimIndent()
        )
    }
    /**
     * Same as above, but without the int argument.
     */
    fun testFunctionTypeMatches2() {
        expectErrorAt(6,23,"Cannot perform operation '='",
            """
            class Foo() {
                fun test() {
                    // This one is okay
                    val func1 : ()->String = this:>returnsString
   
                    // Wrong return type
                    val func2 : () = this:>returnsString
                }
                fun returnsString() = "Hello"
            }
            """.trimIndent()
        )
    }
    /**
     * Now let's try passing the Function to a method
     */
    fun testFunctionTypeMatches3() {
        expectErrorAt(7,8,"No such method",
            """
            class Foo() {
                fun baz( function : ()->String ) {}
                fun test() {
                    // This one is okay
                    baz( this:>returnsString )
                    
                    
                    baz( this:>returnsNothing )
                }
                fun returnsString() = "Hello"
                fun returnsNothing() {}
            }
            """.trimIndent()
        )
    }

    /**
     * Same again, but using a static method
     */
    fun testFunctionTypeMatches4() {
        expectErrorAt(7,11,"No such method",
            """
            class Foo() {
                static fun baz( function : ()->String ) {}
                fun test() {
                    // This one is okay
                    Foo.baz( this:>returnsString )
                    
                    
                    Foo.baz( this:>returnsNothing )
                }
                fun returnsString() = "Hello"
                fun returnsNothing() {}
            }
            """.trimIndent()
        )
    }
    /**
     * Same again, but using a static import method
     */
    fun testFunctionTypeMatches5() {
        expectErrorAt(7,11,"No such method",
            """
            class Baz {
                static fun baz( function : ()->bool ) {}
            }
            """.trimIndent(),
            """
            import static Baz.*
            class Foo() {
                fun test() {
                    // This one is okay
                    baz( this:>returnsBool )
                    
                    
                    Foo.baz( this:>returnsNothing )
                }
                fun returnsBool() = true
                fun returnsNothing() {}
            }
            """.trimIndent()
        )
    }

}
