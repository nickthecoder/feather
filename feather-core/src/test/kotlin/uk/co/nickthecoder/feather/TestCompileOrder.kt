package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectErrorAt
import uk.co.nickthecoder.feather.helper.expectValue

class TestCompileOrder : TestCase() {

    fun testEasyOrder() {
        expectValue(1, """
            class Foo() {
                val a = 1
            }
            """.trimIndent(), """
            class Bar() {
                fun test() = Foo().a
            }
            """.trimIndent()
        )
    }

    // The "wrong" order, but type of Foo.a is known without having to compile it (it is a constant)
    fun testWrongOrder() {
        expectValue(1, """
            class Bar() {
                fun test() = Foo().a
            }
            """.trimIndent(), """
            class Foo() {
                val a = 1
            }
            """.trimIndent()
        )
    }

    //
    fun testWrongOrderHard() {
        expectValue(1, """
            class Bar() {
                fun test() = Foo().a()
            }
            """.trimIndent(), """
            class Foo() {
                fun a() = b() + c()
                fun b() = 0
                fun c() = 1
            }
            """.trimIndent()
        )
    }

    fun testRecursive() {
        expectErrorAt(1, 4, "Recursive type definition", """
            class Bar() {
                fun fib( a : int ) = if ( a == 0 ) 0 else a + fib( a ) 
                fun test() = fib( 4 )
            }
            """.trimIndent()
        )
        expectErrorAt(2, 4, "Recursive type definition", """
            class Bar() {
                fun test() = fib( 4 )
                fun fib( a : int ) = if ( a == 0 ) 0 else a + fib( a ) 
            }
            """.trimIndent()
        )
    }

    fun testRecursiveField() {
        expectErrorAt(
            1, 4, "Recursive type definition", """
            class Bar() {
                var a = b
                var b = a
            }
            """.trimIndent()
        )
    }

    /**
     * Both of these worked fine...
     */
    fun testExtendsTwice() {
        expectValue(
            "Hi, Nick. What's up?", """
            class Base : Greeter( "Hi" ) {
                override fun greet( name : String ) : String = super.greet(name) + ". What's up?"
            }
            class Foo {
                fun test() = Base().greet("Nick")
            }
        """.trimIndent()
        )

        expectValue(
            "Hi, Nick", """
            class Base : Greeter( "Hi" ) {
            }
            class Foo : Base {
                fun test() = greet("Nick")
            }
        """.trimIndent()
        )
    }

    /**
     * This is exactly the same as the one above, but with the scripts the other way round.
     * It failed. Base doesn't declare greet (its super class does though).
     */
    fun testExtendsTwice2() {
        expectValue(
            "Hi, Nick",
            """
            class Foo : Base {
                fun test() = greet("Nick")
            }
        """.trimIndent(),
            """
            class Base : Greeter( "Hi" ) {
            }
        """.trimIndent()
        )
    }

}
