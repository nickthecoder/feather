package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectValue
import uk.co.nickthecoder.feather.helper.testExpression
import uk.co.nickthecoder.feather.runtime.command.CommandResult

class TestCommands : TestCase() {

    fun testEcho() {
        testExpression("Hello\n", """ $( echo Hello ).eval() """)
        testExpression(CommandResult.STATE_FINISHED, """ $( echo Hello ).collect().state """)
        testExpression(0, """ $( echo Hello ).collect().exitStatus """)
        testExpression(true, """ $( echo Hello ).collect().ok() """)
    }

    fun testCommandNotFound() {
        testExpression(CommandResult.STATE_FINISHED, """ $( this_command_does_not_exist ).collect().state """)
        testExpression(127, """ $( this_command_does_not_exist ).collect().exitStatus """)
        testExpression(false, """ $( this_command_does_not_exist ).collect().ok() """)
    }

    fun testEscapeQuotes() {
        // The quoting and escaping are complicated! We are running the echo command with a single
        // command line argument within single quotes, who's value is Hello and the result of the
        // feather expression "it's nasty!".
        // The CommandBuilder will count the first quote before Hello, and when it gets the string "it's nasty!"
        // it will convert it to : "it'\''nasty!", so the command line becomes :
        // echo 'Hello it's\'' nasty!'
        // The shell interprets this a quoted string [Hello it], then a single quote then another quoted string [ nasty!].

        testExpression("Hello it's nasty!\n", " $( echo 'Hello \${\"it's nasty!\"}' ).eval() ")
    }

    fun testWithToString() {
        val dollar = "$"
        val script = """
            class Foo() {
                fun test() : String {
                    val name = this
                    return $(echo '${dollar}{name.toString()}').eval()
                }
                
                override fun toString() = "Hello"
            }
            """.trimIndent()

        expectValue("Hello\n", script)
    }

    /**
     * The same as the above, but without the toString().
     * This crashes bad!
     */
    fun testWithoutToString() {
        val dollar = "$"
        val script = """
            class Foo() {
                fun test() : String {
                    val name = this
                    return $(echo '${dollar}{name}').eval()
                }
                
                override fun toString() = "Hello"
            }
            """.trimIndent()

        expectValue("Hello\n", script)
    }


}