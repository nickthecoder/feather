package uk.co.nickthecoder.feather.internal.operators

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectValue

class TestIncDec : TestCase() {

    fun testPostInc() {
        var i = 3
        var j = i++
        expectValue(
            j++,
            """
            class Foo() {
                fun test() : int {
                    var i = 3
                    var j = i ++
                    return j ++
                }
            }
            """.trimIndent()
        )
    }
/* Pre inc/dec not implemented in feather yet
    fun testPreInc() {
        var i = 3
        var j = ++i
        expectValue(
            ++j,
            """
            class Foo() {
                fun test() : int {
                    var i = 3
                    var j = ++ i
                    return ++ j
                }
            }
            """.trimIndent()
        )
    }
*/
}