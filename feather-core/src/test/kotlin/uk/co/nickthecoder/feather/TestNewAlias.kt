package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectValue
import uk.co.nickthecoder.feather.helper.noSandBoxCompiler

/**
 * This is only here for historic reasons. There used to be special aliases for Map List and Set which created
 * ArrayList, HashMap and HashSet.
 * This feature has now been removed in favour of listOf, mapOf and setOf static methods.
 */
class TestNewAlias: TestCase() {

    fun testList() {
        expectValue( noSandBoxCompiler, ArrayList::class.java, """
            class Foo() {
                fun test() : Class = listOf<String>().getClass()
            }
            """.trimIndent()
        )
    }
    fun testMap() {
        expectValue( noSandBoxCompiler, HashMap::class.java, """
            class Foo() {
                fun test() : Class = mapOf<String, String>().getClass()
            }
            """.trimIndent()
        )
    }
    fun testSet() {
        expectValue( noSandBoxCompiler, HashSet::class.java, """
            class Foo() {
                fun test() : Class = setOf<String>().getClass()
            }
            """.trimIndent()
        )
    }
}
