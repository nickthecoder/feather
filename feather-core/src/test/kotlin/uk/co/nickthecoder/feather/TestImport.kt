package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectErrorAt
import uk.co.nickthecoder.feather.helper.expectValue

class TestImport : TestCase() {

    fun testEmpty() {
        expectErrorAt(0, 6, "mismatched", "import")
        expectErrorAt(0, 6, "mismatched", "import\nclass Foo { }")
    }

    fun testDenyFile() {
        expectErrorAt(0, 7, "The sandbox", "import java.io.File")
        expectErrorAt(0, 7, "The sandbox", "import java.io.File\nclass Foo { }")
    }

    fun testMissingAs() {
        expectErrorAt(0, 26, "missing", "import java.lang.String as")
        expectErrorAt(0, 26, "missing", "import java.lang.String as\nclass Foo { }")
    }

    fun testImportFromDifferentPackage() {
        expectValue(1, """
            package foo
            class Foo {
                fun one() = 1
            }
        """.trimIndent(), """
            package bar
            import foo.Foo
            
            class Bar {
                fun test() = Foo().one()
            }
            
        """.trimIndent()
        )
    }

    fun testImportAlias() {
        expectValue(1, """
            package foo
            class Foo {
                fun one() = 1
            }
        """.trimIndent(), """
            package bar
            import foo.Foo as Fooy
            
            class Bar {
                fun test() = Fooy().one()
            }
            
        """.trimIndent()
        )
    }

    fun testImportStaticWithoutPackageNames() {
        expectValue( 1, """
            class Foo {
                static fun one() = 1
            }
        """.trimIndent(), """
            import static Foo.*
            class Bar {
                fun test() = one()
            }
        """.trimIndent()
        )
    }

    fun testImportStaticWithPackageNames() {
        expectValue( 1, """
            package foo
            class Foo {
                static fun one() = 1
            }
        """.trimIndent(), """
            import static foo.Foo.*
            class Bar {
                fun test() = one()
            }
        """.trimIndent()
        )
    }

    /**
     * This was specifically for FooCAD's script extensions.
     */
    /* Fails, but I don't know if it *SHOULD* work. Hmm.
    fun testImportStaticSuperClass() {
        expectValue( 1, """
            package foo.v1
            class Foo {
                static fun one() = 1
            }
        """.trimIndent(), """
            package foo
            import foo.v1.Foo as Version1
            class Foo : Version1 {
            }
        """.trimIndent(), """
            import static foo.Foo.*
            class Bar {
                fun test() = one()
            }
        """.trimIndent()
        )
    }
    */
}
