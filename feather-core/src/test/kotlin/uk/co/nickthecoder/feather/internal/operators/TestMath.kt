package uk.co.nickthecoder.feather.internal.operators

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectValue
import uk.co.nickthecoder.feather.helper.testExpression

class TestMath : TestCase() {

    fun testInts() {
        testExpression( -1, "- 1" )
        testExpression( 1, "+1" )

        testExpression( 3, "1 + 2" )
        testExpression( 5, "8 - 3" )
        testExpression( 8, "2 * 4" )

        testExpression( 2, "8 ~/ 4" )
        testExpression( 2.0, "8 / 4" )
        testExpression( 4.5, "9 / 2" )
        testExpression( 4, "9 ~/ 2" )

        testExpression( 16, "2 ^ 4" )
    }

    // TODO add similar tests for bytes, shorts, longs and floats

    fun testDoubles() {
        testExpression( -1.0, "- 1.0" )
        testExpression( 1.0, "+ 1.0" )

        testExpression( 3.0, "1.0 + 2.0" )
        testExpression( 5.0, "8.0 - 3.0" )
        testExpression( 8.0, "2 * 4.0" )

        testExpression( 2.0, "8.0 ~/ 4.0" )
        testExpression( 2.0, "8.0 ~/ 4.0" )
        testExpression( 4.5, "9.0 / 2.0" )
        testExpression( 4.0, "9.0 ~/ 2.0" )

        testExpression( 16.0, "2.0 ^ 4.0" )
    }


    fun testIntAndDoubles() {
        testExpression( 3.0, "1 + 2.0" )
        testExpression( 5.0, "8 - 3.0" )
        testExpression( 8.0, "2 * 4.0" )

        testExpression( 2.0, "8 ~/ 4.0" )
        testExpression( 2.0, "8 ~/ 4.0" )
        testExpression( 4.5, "9 / 2.0" )
        testExpression( 4.0, "9 ~/ 2.0" )

        testExpression( 16.0, "2 ^ 4.0" )
    }


    fun testDoubleAndInt() {
        testExpression( 3.0, "1.0 + 2" )
        testExpression( 5.0, "8.0 - 3" )
        testExpression( 8.0, "2.0 * 4" )

        testExpression( 2.0, "8.0 ~/ 4" )
        testExpression( 2.0, "8.0 ~/ 4" ) // TODO Should this be 2.0 or 2 ?
        testExpression( 4.5, "9.0 / 2" )
        testExpression( 4.0, "9.0 ~/ 2" )

        testExpression( 16.0, "2.0 ^ 4" )
    }


    fun testDoubleAndDouble() {
        testExpression(3.0, "1.0 + 2.0")
        testExpression(5.0, "8.0 - 3.0")
        testExpression(8.0, "2.0 * 4.0")

        testExpression(2.0, "8.0 ~/ 4.0")
        testExpression(2.0, "8.0 ~/ 4.0") // TODO Should this be 2.0 or 2 ?
        testExpression(4.5, "9.0 / 2.0")
        testExpression(4.0, "9.0 ~/ 2.0")

        testExpression(16.0, "2.0 ^ 4.0")
    }

    /**
     * Use a List, so that number are boxed (non-primitives)
     */
    fun testBoxedDoubles() {
        expectValue(
            5.0, """
            class Foo() {
                fun test() : double {
                    val list = listOf<double>( 2.0, 3.0 )
                    return list[0] + list[1]
                }
            }
            """.trimIndent()
        )
        expectValue(
            -1.0, """
            class Foo() {
                fun test() : double {
                    val list = listOf<double>( 2.0, 3.0 )
                    return list[0] - list[1]
                }
            }
            """.trimIndent()
        )
        expectValue(
            6.0, """
            class Foo() {
                fun test() : double {
                    val list = listOf<double>( 2.0, 3.0 )
                    return list[0] * list[1]
                }
            }
            """.trimIndent()
        )
        expectValue(
            10.0, """
            class Foo() {
                fun test() : double {
                    val list = listOf<double>( 5.0, 2.0 )
                    return list[0] * list[1]
                }
            }
            """.trimIndent()
        )
    }
}
