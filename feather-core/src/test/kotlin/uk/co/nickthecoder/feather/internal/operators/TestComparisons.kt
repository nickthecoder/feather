package uk.co.nickthecoder.feather.internal.operators

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.testExpression

class TestComparisons : TestCase() {

    fun testEq() {
        testExpression(true, "1 == 1")
        testExpression(true, "1.0 == 1.0")
        testExpression(true, "1.0f == 1.0f")
        testExpression(true, "1.0 == 1") // Auto cast
        testExpression(true, "1 == 1.0") // Auto cast
        testExpression(true, "'a' == 'a'")

        testExpression(false, "1 == 2")
        testExpression(false, "1.0 == 2.0")
        testExpression(false, "1.0f == 2.0f")
        testExpression(false, "1.0 == 2") // Auto cast
        testExpression(false, "1 == 2.0") // Auto cast
        testExpression(false, "'a' == 'b'")

        testExpression( true, "\"HelloWorld\" == \"Hello\" + \"World\"")
        testExpression( false, "\"Hello World\" == \"Hello\" + \"World\"")
    }

    fun testNe() {
        testExpression(false, "1 != 1")
        testExpression(false, "1.0 != 1.0")
        testExpression(false, "1.0f != 1.0f")
        testExpression(false, "1.0 != 1") // Auto cast
        testExpression(false, "1 != 1.0") // Auto cast
        testExpression(false, "'a' != 'a'")

        testExpression(true, "1 != 2")
        testExpression(true, "1.0 != 2.0")
        testExpression(true, "1.0f != 2.0f")
        testExpression(true, "1.0 != 2") // Auto cast
        testExpression(true, "1 != 2.0") // Auto cast
        testExpression(true, "'a' != 'b'")

        testExpression( false, "\"HelloWorld\" != \"Hello\" + \"World\"")
        testExpression( true, "\"Hello World\" != \"Hello\" + \"World\"")
    }

    fun testGt() {
        testExpression(true, "2 > 1")
        testExpression(true, "2.0 > 1.0")
        testExpression(true, "2.0f > 1.0f")
        testExpression(true, "2.0 > 1") // Auto cast
        testExpression(true, "2 > 1.0") // Auto cast
        testExpression(true, "'b' > 'a'")

        testExpression(false, "1 > 2")
        testExpression(false, "1.0 > 2.0")
        testExpression(false, "1.0f > 2.0f")
        testExpression(false, "1.0 > 2") // Auto cast
        testExpression(false, "1 > 2.0") // Auto cast
        testExpression(false, "'a' > 'b'")

        testExpression(false, "2 > 2")
        testExpression(false, "2.0 > 2.0")
        testExpression(false, "2.0f > 2.0f")
        testExpression(false, "2.0 > 2") // Auto cast
        testExpression(false, "2 > 2.0") // Auto cast
        testExpression(false, "'b' > 'b'")
        testExpression(false, "'a' > 'b'")


        testExpression( true, "\"B\" > \"A\"")
        testExpression( false, "\"B\" > \"B\"")
        testExpression( false, "\"A\" > \"B\"")

    }

    fun testLe() {
        testExpression(false, "2 <= 1")
        testExpression(false, "2.0 <= 1.0")
        testExpression(false, "2.0f <= 1.0f")
        testExpression(false, "2.0 <= 1") // Auto cast
        testExpression(false, "2 <= 1.0") // Auto cast
        testExpression(false, "'b' <= 'a'")

        testExpression(true, "2 <= 2")
        testExpression(true, "2.0 <= 2.0")
        testExpression(true, "2.0f <= 2.0f")
        testExpression(true, "2.0 <= 2") // Auto cast
        testExpression(true, "2 <= 2.0") // Auto cast
        testExpression(true, "'b' <= 'b'")

        testExpression(true, "1 <= 2")
        testExpression(true, "1.0 <= 2.0")
        testExpression(true, "1.0f <= 2.0f")
        testExpression(true, "1.0 <= 2") // Auto cast
        testExpression(true, "1 <= 2.0") // Auto cast
        testExpression(true, "'a' <= 'b'")

        testExpression( false, "\"B\" <= \"A\"")
        testExpression( true, "\"B\" <= \"B\"")
        testExpression( true, "\"A\" <= \"B\"")

    }


    fun testGe() {
        testExpression(true, "2 >= 1")
        testExpression(true, "2.0 >= 1.0")
        testExpression(true, "2.0f >= 1.0f")
        testExpression(true, "2.0 >= 1") // Auto cast
        testExpression(true, "2 >= 1.0") // Auto cast
        testExpression(true, "'b' >= 'a'")

        testExpression(true, "2 >= 2")
        testExpression(true, "2.0 >= 2.0")
        testExpression(true, "2.0f >= 2.0f")
        testExpression(true, "2.0 >= 2") // Auto cast
        testExpression(true, "2 >= 2.0") // Auto cast
        testExpression(true, "'b' >= 'b'")

        testExpression(false, "1 >= 2")
        testExpression(false, "1.0 >= 2.0")
        testExpression(false, "1.0f >= 2.0f")
        testExpression(false, "1.0 >= 2") // Auto cast
        testExpression(false, "1 >= 2.0") // Auto cast
        testExpression(false, "'a' >= 'b'")

        testExpression( true, "\"B\" >= \"A\"")
        testExpression( true, "\"B\" >= \"B\"")
        testExpression( false, "\"A\" >= \"B\"")
    }

    fun testLt() {
        testExpression(false, "2 < 1")
        testExpression(false, "2.0 < 1.0")
        testExpression(false, "2.0f < 1.0f")
        testExpression(false, "2.0 < 1") // Auto cast
        testExpression(false, "2 < 1.0") // Auto cast
        testExpression(false, "'b' < 'a'")

        testExpression(false, "2 < 2")
        testExpression(false, "2.0 < 2.0")
        testExpression(false, "2.0f < 2.0f")
        testExpression(false, "2.0 < 2") // Auto cast
        testExpression(false, "2 < 2.0") // Auto cast
        testExpression(false, "'b' < 'b'")

        testExpression(true, "1 < 2")
        testExpression(true, "1.0 < 2.0")
        testExpression(true, "1.0f < 2.0f")
        testExpression(true, "1.0 < 2") // Auto cast
        testExpression(true, "1 < 2.0") // Auto cast
        testExpression(true, "'a' < 'b'")

        testExpression( false, "\"B\" < \"A\"")
        testExpression( false, "\"B\" < \"B\"")
        testExpression( true, "\"A\" < \"B\"")
    }

}
