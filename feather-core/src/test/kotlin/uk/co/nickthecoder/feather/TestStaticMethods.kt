package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectValue

class TestStaticMethods : TestCase() {

    fun testNormal() {
        expectValue(
            1, """
            class Foo() {
                static fun foo() = 1
                fun test() : int {
                    return foo()
                }
            }
            """.trimIndent()
        )
        expectValue(
            'H', """
            class Foo() {
                static fun first( str : String ) = str[0]
                 
                fun test() : char {
                    return first( "Hello" )
                }
            }
            """.trimIndent()
        )
    }

    /**
     * This is the easy case, that always worked.
     * [testImportBefore] is the tricky version!
     */
    fun testImportAfter() {
        expectValue(
            1,
            """
            class Bar() {
                static fun foo() = 1
            }
            """.trimIndent(),
            """
            import static Bar.*
            class Foo() {
                fun test() : int {
                    return foo()
                }
            }
            """.trimIndent()
        )
    }

    /**
     * The same as [testImportAfter], but the order of the scripts is reversed.
     */
    fun testImportBefore() {
        expectValue(
            1,
            """
            import static Bar.*
            class Foo() {
                fun test() : int {
                    return foo()
                }
            }
            """.trimIndent(),
            """
            class Bar() {
                static fun foo() = 1
            }
            """.trimIndent()
        )
    }

    fun testSimpleStaticCall() {
         expectValue(
            "Hi",
            """
            class Lumber() {
                static var showCutouts = true
                static fun debugCutout( shape : String ) : String = if (showCutouts) shape else ""
            }
            """.trimIndent(),
            """
            class Foo() {
                fun test() : String {
                    return Lumber.debugCutout("Hi")
                }
            }
            """.trimIndent()
        )
    }
    fun testSimpleStaticCallBefore() {
         expectValue(
            2,
            """
            class Foo() {
                fun test() : int {
                    return Bar.foo("Hi")
                }
            }
            """.trimIndent(),
            """
            class Bar() {
                static fun foo(text : String) : int = text.length()
            }
            """.trimIndent()

        )
    }
}
