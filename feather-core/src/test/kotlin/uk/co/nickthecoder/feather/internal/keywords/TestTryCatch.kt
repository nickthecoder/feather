package uk.co.nickthecoder.feather.internal.keywords

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectErrorAt
import uk.co.nickthecoder.feather.helper.expectValue
import uk.co.nickthecoder.feather.helper.testExpression

class TestTryCatch : TestCase() {

    fun testTryWithoutCatchOrFinally() {
        expectErrorAt(2, 8, "try must have a catch or finally", """
            class Foo() {
                fun test() : int {
                    try {
                        return 2
                    }
                }
            }
        """.trimIndent())
    }

    /**
     * I had a bug, which caused this to work, but [testCatchWithoutE] to fail.
     */
    fun testCatchWithE() {
        testExpression(true, """
            try {
                "Hello".substring( 1 ) // No exception
                true
            } catch (e : Exception) {
                e
                false
            }
        """.trimIndent())
    }

    /**
     * Tests that ignoring the "e" variable is ok.
     * This used to fail because I was using mv.visitIntInsn instead of mv.visitVarInsn
     */
    fun testCatchWithoutE() {
        testExpression(true, """
            try {
                "Hello".substring( 1 ) // No exception
                true
            } catch (e : Exception) {
                false
            }
        """.trimIndent())
    }

    fun testTryCatch() {

        testExpression(true, """
            try {
                "Hello".substring( 1 ) // No exception
                true
            } catch (e : Exception) {
                e
                false
            }
        """.trimIndent())

        testExpression(false, """
            try {
                "Hello".substring( 100 ) // Throws!
                true
            } catch (e : Exception) {
                false
            }
        """.trimIndent())
    }

    fun testTryFinally() {
        expectValue(2, """
            class Foo() {
                fun test() : int {
                    try {
                        "Hello".substring( 1 ) // No exception
                        return 2
                    } catch (e : Exception) {
                        return 3
                    }
                }
            }
        """.trimIndent())

        expectValue(3, """
            class Foo() {
                fun test() : int {
                    try {
                        "Hello".substring( 100 ) // Throws
                        return 2
                    } catch (e : Exception) {
                        return 3
                    }
                }
            }
        """.trimIndent())


        expectValue(2, """
            class Foo() {
                fun test() : int {
                    return try {
                        "Hello".substring( 1 )
                        2
                    } catch (e : Exception) {
                        3
                    }
                }
            }
        """.trimIndent())

        expectValue(3, """
            class Foo() {
                fun test() : int {
                    return try {
                        "Hello".substring( 100 )
                        2
                    } catch (e : Exception) {
                        3
                    }
                }
            }
        """.trimIndent())
    }

    fun testTryCatchFinally() {
        // Perhaps these should fail to compile, because the finally won't run???
        expectValue(2, """
            class Foo() {
                fun test() : int {
                    try {
                        "Hello".substring( 1 ) // No exception
                        return 2
                    } catch (e : Exception) {
                        return 3
                    } finally {
                        return 4
                    }
                }
            }
        """.trimIndent())

        expectValue(3, """
            class Foo() {
                fun test() : int {
                    try {
                        "Hello".substring( 100 ) // Throws
                        return 2
                    } catch (e : Exception) {
                        return 3
                    } finally {
                        return 4
                    }
                }
            }
        """.trimIndent())


        expectValue(4, """
            class Foo() {
                fun test() : int {
                    return try {
                        "Hello".substring( 1 )
                        2
                    } catch (e : Exception) {
                        3
                    } finally {
                        return 4
                    }
                }
            }
        """.trimIndent())

        expectValue(4, """
            class Foo() {
                fun test() : int {
                    return try {
                        "Hello".substring( 100 )
                        2
                    } catch (e : Exception) {
                        3
                    } finally {
                        return 4
                    }
                }
            }
        """.trimIndent()
        )
    }

    /**
     * The try block returns a String, but the catch block returns nothing.
     */
    fun testTryStringCatchNothing() {
        expectValue(
            2, """
            class Foo() {
                fun test() : int {
                    try {
                        "Hello"
                    } catch( e : Exception ) {
                    }
                    return 2
                }
            }
        """.trimIndent()
        )
    }

    fun testTryIntCatchDouble() {
        expectValue(
            2.0, """
            class Foo() {
                fun test() : double {
                    return try {
                        2
                    } catch( e : Exception ) {
                        3.1
                    }
                }
            }
        """.trimIndent()
        )
    }

    fun testTryDoubleCatchInt() {
        expectValue(
            2.1, """
            class Foo() {
                fun test() : double {
                    return try {
                        2.1
                    } catch( e : Exception ) {
                        3
                    }
                }
            }
        """.trimIndent()
        )
    }

}
