package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectValue
import uk.co.nickthecoder.feather.runtime.Extensions

class TestArrayList : TestCase() {

    fun testListAccess() {

        expectValue(
            "[null, hi, null, null]", """
            class Foo() {
                fun test() : String {
                    var list = arrayList<String>(4)
                    list[1] = "hi"
                    return list.toString()
                }
            }
            """.trimIndent()
        )
    }
    fun testListAccess2() {
        val dollar = "$"

        expectValue(
            "[null, hi, null, null]", """
            class Foo() {
                fun test() : String {
                    var list = arrayList<String>(4)
                    for ( i in 0..2 ) {
                        list[1] = "hi"
                    }
                    return list.toString()
                }
            }
            """.trimIndent()
        )
    }

    fun test1DArrayList() {
        val list = Extensions.arrayList<Int>(4)
        for (i in 0..2) {
            list[i] = i * 2
        }
        assertEquals("[0, 2, 4, null]", list.toString())

        val dollar = "$"
        expectValue(
            "[0, 2, 4, null]", """
            class Foo() {
                fun test() : String {
                    var list = arrayList<String>(4)
                    for (i in 0..2) {
                        list.set(i, "$dollar{i * 2}" )
                    //    list[i] = "$dollar{i * 2}"
                    }
                    return list.toString()
                }
            }
            """.trimIndent()
        )

        // When I first wrote this test array.set(n,value) worked, but using array style setter array[n] = value did not.
        // i.e. the above worked, but below didn't.
        expectValue(
            "[0, 2, 4, null]", """
            class Foo() {
                fun test() : String {
                    var list = arrayList<String>(4)
                    for (i in 0..2) {
                        list[i] = "$dollar{i * 2}"
                    }
                    return list.toString()
                }
            }
            """.trimIndent()
        )
    }

    fun test2dArrayList() {
        val grid = Extensions.arrayList<Int>(3, 4)
        for (x in 0..2) {
            for (y in 0..2) { // Note, we are NOT initialising the last row
                grid[x][y] = x * y;
            }
        }
        assertEquals("[[0, 0, 0, null], [0, 1, 2, null], [0, 2, 4, null]]", grid.toString())
    }

    fun test2dArrayListGet() {
        expectValue(
            "Hello",
            """
            class Foo() {
                fun test() : String {
                    val list = arrayList<String>(3, 3)
                    list.get(0).set(0, "Hello")
                    return list.get(0).get(0)
                }
            }
            """.trimIndent()
        )

    }
}
