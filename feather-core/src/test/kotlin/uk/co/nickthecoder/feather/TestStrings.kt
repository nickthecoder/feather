package uk.co.nickthecoder.feather

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.expectValue

/**
 * Tests various smart strings. There are different code paths for smart strings containing a single item
 * verses multiple items, so each type of test has two versions.
 */
class TestStrings : TestCase() {
    val dollar = "$"

    fun testString() {
        expectValue("Hello", """
            class Foo() {
                val hello = "Hello"
                fun test() : String {
                    return "${dollar}hello"
                }
            }
            """.trimIndent()
        )
    }

    fun testString2() {
        expectValue("HelloWorld", """
            
            class Foo() {
                fun test() : String {
                    val hello = "Hello"
                    val world = "World"
                    return "${dollar}hello${dollar}world"
                }
            }
            """.trimIndent()
        )
    }

    fun testInt1() {
        expectValue("1", """
            class Foo() {
                fun test() : String {
                    val a1 = 1
                    return "${dollar}a1"
                }
            }
            """.trimIndent()
        )
    }

    fun testInt2() {
        expectValue("12", """
            class Foo() {
                fun test() : String {
                    val a1 = 1
                    val a2 = 2
                    return "${dollar}a1${dollar}a2"
                }
            }
            """.trimIndent()
        )
    }

    // This tests that
    fun testIntRange1() {
        expectValue("1..2", """
            class Foo() {
                fun test() : String {
                    val range1 = 1..2
                    return "${dollar}range1"
                }
            }
            """.trimIndent()
        )
    }

    // This tests that any Object can be used within a smart string.
    fun testIntRange2() {
        expectValue("1..24..7", """
            class Foo() {
                fun test() : String {
                    val range1 = 1..2
                    val range2 = 4..7
                    return "${dollar}range1${dollar}range2"
                }
            }
            """.trimIndent()
        )
    }


    // This tests that any interface can be used within a smart string.
    fun testInterface1() {
        expectValue("Bar", """
            class Bar() : Cloneable {
                override fun toString() = "Bar"
            }
            
            class Foo() {
                fun test() : String {
                    val a : Cloneable = Bar()
                    return "${dollar}a"
                }
            }
            """.trimIndent()
        )
    }

    // I thought this would fail, because StringBuilderStackEntry would have passed Cloneable as the
    // argument type to StringBuilder.append, but it seems to work.
    // However, I changed StringBuilderStackEntry so that it passes Object as the argument type,
    // as that seems more appropriate (as there IS a method with that signature!). That works too ;-)
    fun testInterface2() {
        expectValue("BarBar", """
            class Bar() : Cloneable {
                override fun toString() = "Bar"
            }
            
            class Foo() {
                fun test() : String {
                    val a : Cloneable = Bar()
                    return "${dollar}a${dollar}a"
                }
            }
            """.trimIndent()
        )
    }

    fun testTripleWithQuote() {
        val str = "\"\"\"Hello\"World\"\"\""
        expectValue( "Hello\"World", """
            class Foo() {
                fun test() = $str
            }
        """.trimIndent()
        )
    }


    fun testTripleEndsWithQuote() {
        val str = "\"\"\"Hello\"World\"\"\"\""
        expectValue( "Hello\"World\"", """
            class Foo() {
                fun test() = $str
            }
        """.trimIndent()
        )
    }
}
