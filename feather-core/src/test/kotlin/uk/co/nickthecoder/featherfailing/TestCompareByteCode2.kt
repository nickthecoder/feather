package uk.co.nickthecoder.featherfailing

import junit.framework.TestCase
import uk.co.nickthecoder.feather.helper.compareByteCode

class TestCompareByteCode2 : TestCase() {

    fun testInterfaceVarArg() {
        compareByteCode("VarArg")
    }

    // The mechanism is quite different, and the Java version calls IntRange.endInclusive each time round the loop.
    // The Feather version is longer though.
    // For constant values for start and end, Java will be marginally faster.
    // For local variable values of start and end, the result will be fairly similar.
    // For a calculated value for the end condition Feather will be faster, but is NOT comparable,
    // as the calculation is only done once, which may not be what you want.
    // Java's for is really a while loop (where the condition is calculated every iteration),
    // whereas Feather's really is a for loop over an int range calculated at the beginning of the loop.
    // IMHO, feather is better, because it has a while loop and a for loop, whereas Java only has two types
    // of while loops.
    fun testFor() {
        compareByteCode("For")
    }

    // There is no way to write an elvis which is as efficient as the Feather equivalent,
    // So I can't create a pass that will work (without making Feather inefficient ;-)
    fun testElvis() {
        compareByteCode("Elvis")
    }

    fun testTry() {
        compareByteCode("Try")
    }


    fun testLambda() {
        compareByteCode("Lambda")
    }
}

