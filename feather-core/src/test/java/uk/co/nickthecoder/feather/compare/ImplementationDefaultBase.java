package uk.co.nickthecoder.feather.compare;

interface ImplementationDefaultBase {

    default int foo(String s) {
        return s.length();
    }

    default int lengthPlus(String s, int a) {
        return s.length() + a;
    }
}
