package uk.co.nickthecoder.feather.compare;

public class JavaIs {

    public boolean isString( Object obj ) {
        return obj instanceof String;
    }

    public void helloString( Object obj ) {
        if ( obj instanceof String ) {
            System.out.println( "Hello" );
        }
    }

}
