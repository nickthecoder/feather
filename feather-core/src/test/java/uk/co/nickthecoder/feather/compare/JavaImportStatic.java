package uk.co.nickthecoder.feather.compare;

import static uk.co.nickthecoder.feather.runtime.Print.println;

public class JavaImportStatic {
    public void hello() {
        println("Hello");
    }
}
