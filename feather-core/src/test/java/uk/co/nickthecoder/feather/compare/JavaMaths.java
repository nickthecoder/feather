package uk.co.nickthecoder.feather.compare;

public class JavaMaths {

    public double add(double a, int b) {
        return a + b;
    }

    public float add(float a, int b) {
        return a + b;
    }

    public long add(long a, int b) {
        return a + b;
    }

    public int add(int a, int b) {
        return a + b;
    }

    public int add(short a, int b) {
        return a + b;
    }

    public char add(char a, int b) {
        return (char) (a + b); // NOTE. In feather Char + Int -> Char (so no cast was needed).
    }

    public int add(byte a, int b) {
        return a + b;
    }


    public double sub(double a, int b) {
        return a - b;
    }

    public float sub(float a, int b) {
        return a - b;
    }

    public long sub(long a, int b) {
        return a - b;
    }

    public int sub(int a, int b) {
        return a - b;
    }

    public int sub(short a, int b) {
        return a - b;
    }

    public char sub(char a, int b) {
        return (char) (a - b); // NOTE. In feather Char + Int -> Char (so no cast was needed).
    }

    public int sub(byte a, int b) {
        return a - b;
    }


    public double mul(double a, int b) {
        return a * b;
    }

    public float mul(float a, int b) {
        return a * b;
    }

    public long mul(long a, int b) {
        return a * b;
    }

    public int mul(int a, int b) {
        return a * b;
    }

    public int mul(short a, int b) {
        return a * b;
    }

    public int mul(byte a, int b) {
        return a * b;
    }


    public double div(double a, int b) {
        return a / b;
    }

    public float div(float a, int b) {
        return a / b;
    }

    public long div(long a, int b) {
        return a / b;
    }

    public int div(int a, int b) {
        return a / b;
    }

    public int div(short a, int b) {
        return a / b;
    }

    public int div(byte a, int b) {
        return a / b;
    }


    public int rem(int a, int b) { return a % b; }
    public long rem(long a, long b) { return a % b; }
    public float rem(float a, float b) { return a % b; }
    public double rem(double a, double b) { return a % b; }


    public int unaryMinus(int a) {
        return -a;
    }

    public int unaryPlus(int a) {
        return +a;
    }

    public long unaryMinus(long a) {
        return -a;
    }

    public long unaryPlus(long a) {
        return +a;
    }

    public float unaryMinus(float a) {
        return -a;
    }

    public float unaryPlus(float a) {
        return +a;
    }

    public double unaryMinus(double a) {
        return -a;
    }

    public double unaryPlus(double a) {
        return +a;
    }

    public boolean unaryNot(boolean a) {
        return !a;
    }
}
