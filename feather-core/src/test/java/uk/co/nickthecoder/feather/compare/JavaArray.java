package uk.co.nickthecoder.feather.compare;

public class JavaArray {
    public String[] oneDimension = new String[3];

    public String[][] twoDimensions = new String[2][3];

    public String[][][] threeDimensions = new String[2][3][4];

    public int[] ints1 = new int[3];

    public int[][] ints2 = new int[2][3];

    public int[][][] ints3 = new int[2][3][4];

    public double[] doubles1 = new double[3];


    public String firstString1() {
        return oneDimension[0];
    }

    public String firstString2() {
        return twoDimensions[0][0];
    }

    public String[] firstStringRow() {
        return twoDimensions[0];
    }


    public int firstInt1() {
        return ints1[0];
    }

    public int firstInt2() {
        return ints2[0][0];
    }

    public int[] firstIntRow() {
        return ints2[0];
    }


    public void setString1(String value) {
        oneDimension[0] = value;
    }

    public void setString2(String value) {
        twoDimensions[0][0] = value;
    }

    public void setInt1(int value) {
        ints1[0] = value;
    }

    public void setInt1FromDouble(double value) {
        ints1[0] = (int) value;
    }

    public void setInts2(int value) {
        ints2[0][0] = value;
    }

    /* Java's implementation is better
    public void addToInt1(int amount) {
        ints1[0] += amount;
    }
    */
}
