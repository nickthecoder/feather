package uk.co.nickthecoder.feather.compare;

public class JavaStrings {

    // Since the major rework to support ImplicitTypes, concatenating multiple strings
    // no longer uses a StringBuffer when using the "+" operator.
    // So for now, I've forced the Java code to behave like the Feather code
    public String concat1(String a, int b) {
        //return a + b;
        return a.concat(Integer.valueOf(b).toString());
    }

    public String concat2(String a, String b) {
        //return a + b;
        return a.concat(b);
    }

    /*
    public String concat3(String a, int b, int c) {
        //return a + b + c;
        return a.concat(Integer.toString(b)).concat(Integer.toString(c));
    }
    */

    public String concat4(String a, String b, String c) {
        //return a + b + c;
        return a.concat(b).concat(c);
    }

    public String build(String a, String b, String c) {
        return a + b + c;
    }

    public String build2(String a, String b, String c) {
        return a + b + c;
    }

    public String buildExpression(String a, int b, int c) {
        return a + (b * c) + " :-)";
    }
}
