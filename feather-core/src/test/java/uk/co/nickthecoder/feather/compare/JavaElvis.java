package uk.co.nickthecoder.feather.compare;

import java.io.PrintStream;

public class JavaElvis {

    public PrintStream errOrOut() {
        return System.err == null ? System.out : System.err;
    }

}
