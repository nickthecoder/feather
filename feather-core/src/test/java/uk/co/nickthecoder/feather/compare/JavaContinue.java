package uk.co.nickthecoder.feather.compare;

public class JavaContinue {

    public void hello(int n) {
        int a = 1;
        while (a < n) {
            a = a * 2;
            if (a == 2) continue;
            System.out.println("Hello");
        }
    }

    public void world(int n) {
        int a = 1;
        do {
            a = a * 2;
            if (a == 2) continue;
            System.out.println("World");
        } while (a < n);
    }
}
