package uk.co.nickthecoder.feather.compare;

public enum JavaEnum2 {
    ORDERED(5),
    READY(2),
    DELIVERED(0);

    public final int timeToDelivery;

    JavaEnum2(int ttd) {
        timeToDelivery = ttd;
    }
}
