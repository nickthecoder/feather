package uk.co.nickthecoder.feather.compare;

public class JavaVarArg {

    // using a long winded version, so that the feather and java versions will compile the same way.
    public void hello(String... names) {
        System.out.println("Hello");
        System.out.println(" ");
        System.out.println(names[0]);
    }

    public void sayHello() {
        hello( "Nick" );
        hello( "Nick", "Nalin" );
    }


    public void foo(String normal, String... names) {
        System.out.println(normal);
        System.out.println(names[0]);
    }

    public void callFoo() {
        foo(  "Hello", "Nick" );
        foo( "Hello", "Nick", "Nalin" );
    }

}
