package uk.co.nickthecoder.feather.compare;

public class JavaExtends extends Exception implements Runnable {

    public JavaExtends(String message) {
        super(message);
    }

    public void run() {
        System.out.println("Run!");
    }

    @Override
    public String toString() {
        return super.toString().substring(1);
    }
}
