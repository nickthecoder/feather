package uk.co.nickthecoder.feather.compare;

public class JavaComparisons {

    public boolean compareInts(int a, int b) {
        return a == b;
    }

    public boolean compareLongs(long a, long b) {
        return a == b;
    }

    public boolean compareFloats(float a, float b) {
        return a == b;
    }

    public boolean compareObjects(Object a, Object b) {
        return a == b;
    }

}
