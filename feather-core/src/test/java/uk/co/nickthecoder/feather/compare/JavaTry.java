package uk.co.nickthecoder.feather.compare;

public class JavaTry {

    public void tryCatch() {
        try {
            String a = "Hello".substring(5, 10);
            System.out.println("Ok");

        } catch (IndexOutOfBoundsException e) {
            System.out.println("Caught IndexOutOfBoundsException");
        } catch (Exception e ) {
            System.out.println("Caught Exception");
        }
    }

    public void tryFinally() {
        try {
            String a = "Hello".substring(5, 10);
            System.out.println("Ok");
        } finally {
            System.out.println("Finally");
        }
    }

    public void tryCatchFinally() {
        try {
            String a ="Hello".substring(5, 10);
            System.out.println("Ok");

        } catch (IndexOutOfBoundsException e) {
            System.out.println("Caught IndexOutOfBoundsException");
        } catch (Exception e ) {
            System.out.println("Caught Exception");
        } finally {
            System.out.println("Finally");
        }
    }

}
