package uk.co.nickthecoder.feather.compare;

public class JavaArguments {

    public void intToVoid(int a) {
    }

    public int voidToInt() {
        return 1;
    }

    public void intAndLong(int a, long b) {
    }
}
