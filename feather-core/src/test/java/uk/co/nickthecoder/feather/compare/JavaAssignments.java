package uk.co.nickthecoder.feather.compare;

public class JavaAssignments {

    public void add(int amount) {
        int a = 1;
        a += amount;
    }

    public void subtract(int amount) {
        int a = 1;
        a -= amount;
    }

    public void multiple(int amount) {
        int a = 1;
        a *= amount;
    }

    public void divide(int amount) {
        int a = 1;
        a /= amount;
    }
}
