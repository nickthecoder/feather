package uk.co.nickthecoder.feather.compare;

import uk.co.nickthecoder.feather.runtime.CharRange;
import uk.co.nickthecoder.feather.runtime.Extensions;
import uk.co.nickthecoder.feather.runtime.IntRange;
import uk.co.nickthecoder.feather.runtime.LongRange;

public class JavaRanges {

    public IntRange intRange(int from, int to) {
        return Extensions.rangeTo(from, to);
    }

    public IntRange intRangeExclusive(int from, int to) {
        return Extensions.until(from, to);
    }


    public CharRange charRange(char from, char to) {
        return Extensions.rangeTo(from, to);
    }

    public CharRange charRangeExclusive(char from, char to) {
        return Extensions.until(from, to);
    }


    public LongRange longRange(long from, long to) {
        return Extensions.rangeTo(from, to);
    }

    public LongRange longRangeExclusive(long from, long to) {
        return Extensions.until(from, to);
    }
}
