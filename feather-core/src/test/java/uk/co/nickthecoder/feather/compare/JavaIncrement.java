package uk.co.nickthecoder.feather.compare;

public class JavaIncrement {

    public static int staticInt = 0;
    public static long staticLong = 0;
    public static float staticFloat = 0;
    public static double staticDouble = 0;

    public int nonStaticInt = 0;
    public long nonStaticLong = 0;
    public float nonStaticFloat = 0.0f;
    public double nonStaticDouble = 0.0;


    public int nextStaticInt() {
        return staticInt ++;
    }
    public long nextStaticLong() {
        return staticLong ++;
    }
    public float nextStaticFloat() {
        return staticFloat ++;
    }
    public double nextStaticDouble() {
        return staticDouble ++;
    }

    public int prevStaticInt() {
        return staticInt --;
    }
    public long prevStaticLong() {
        return staticLong --;
    }
    public float prevStaticFloat() {
        return staticFloat --;
    }
    public double prevStaticDouble() {
        return staticDouble --;
    }



    public int nextNonStaticInt() {
        return nonStaticInt ++;
    }
    public long nextNonStaticLong() {
        return nonStaticLong ++;
    }
    public float nextNonStaticFloat() {
        return nonStaticFloat ++;
    }
    public double nextNonStaticDouble() {
        return nonStaticDouble ++;
    }

    public int prevNonStaticInt() {
        return nonStaticInt --;
    }
    public long prevNonStaticLong() {
        return nonStaticLong --;
    }
    public float prevNonStaticFloat() {
        return nonStaticFloat --;
    }
    public double prevNonStaticDouble() {
        return nonStaticDouble --;
    }

    public void nextLocals() {
        int localInt = 0;
        long localLong  = 0;
        float localFloat= 0;
        double localDouble = 0;

        System.out.println( localLong ++ );
        System.out.println( localFloat ++ );
        System.out.println( localDouble ++ );

        System.out.println(localLong--);
        System.out.println(localFloat--);
        System.out.println(localDouble--);

        System.out.println(localInt);
        System.out.println(localLong);
        System.out.println(localFloat);
        System.out.println(localDouble);
    }


    public void testField()  {
        nonStaticInt ++;
        nonStaticInt += nonStaticInt ++;
        nonStaticInt += ++nonStaticInt;
    }
    public void testStaticField()  {
        staticInt ++;
        staticInt += staticInt ++;
        staticInt += ++staticInt;
    }
    /*
    Java's implementation is better! Feather does not use the IINC opcode.
    If Feather is optimised, remove the comments!

    public void testLocal()  {
        int local = 3;
        local ++;
        local += local ++;
        local += ++local;
    }
    */

}
