package uk.co.nickthecoder.feather.compare;

public class JavaDoWhile {

    public void hello(int n) {
        int a = 1;
        do {
            System.out.println("Hello");
            a = a * 2;
        } while (a < n);
    }

}
