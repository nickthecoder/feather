package uk.co.nickthecoder.feather.compare;

import java.io.FilterOutputStream;
import java.io.Flushable;
import java.io.PrintStream;

public class JavaAs {

    public FilterOutputStream out = System.out;

    public PrintStream printStream() {
        return (PrintStream) out;
    }

    public Flushable flushable() {
        return (Flushable) out;
    }

    /*
    I Cannot write the equivalent code in Java, so commented it out.
    Note that the java version needs to get the value of out twice. Wasteful! ;-)

    public ObjectInput objectInputMaybe() {
        if (out instanceof ObjectInput) {
            return (ObjectInput) out;
        } else {
            return null;
        }
    }
    */
}
