package uk.co.nickthecoder.feather.compare;

public class JavaAnnotation {
    @MyAnnotation( foo="Fred", bar=42 )
    public int foo = 2;
}
