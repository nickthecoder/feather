package uk.co.nickthecoder.feather.compare;

import uk.co.nickthecoder.feather.helper.ExampleInterface;

public class JavaStaticInterface {

    public String hello() {
        return ExampleInterface.getHello();
    }
}
