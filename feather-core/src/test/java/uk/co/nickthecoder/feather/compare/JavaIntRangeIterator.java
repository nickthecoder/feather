package uk.co.nickthecoder.feather.compare;

import uk.co.nickthecoder.feather.runtime.IntRange;

import java.util.Iterator;

public class JavaIntRangeIterator {

    public int foo() {
        IntRange range = new IntRange(2, 5);
        Iterator<Integer> it = range.iterator();
        if (it.hasNext()) {
            return it.next() + 1;
        }
        return 0;
    }
}
