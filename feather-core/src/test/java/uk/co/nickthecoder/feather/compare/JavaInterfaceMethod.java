package uk.co.nickthecoder.feather.compare;

public class JavaInterfaceMethod {

    public int foo(CharSequence cs) {
        return cs.length();
    }

}
