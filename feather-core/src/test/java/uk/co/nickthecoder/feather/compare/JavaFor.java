package uk.co.nickthecoder.feather.compare;

import uk.co.nickthecoder.feather.runtime.IntRange;

/**
 * Loops over a range of integers, in the same way that Feather does it, so that we can
 * compare the byte codes.
 */
public class JavaFor {

    public int oneToTen() {
        int counter = 0;
        IntRange range = new IntRange(1,10);
        for( int i = range.start; i <= range.endInclusive; i ++) {
            counter = counter + 2;
        }
        return counter;
    }
}
