package uk.co.nickthecoder.feather.compare

interface KotlinDefault {
    fun foo(s: String) = s.length

    fun lengthPlus(s: String, a: Int) = s.length + a
}
