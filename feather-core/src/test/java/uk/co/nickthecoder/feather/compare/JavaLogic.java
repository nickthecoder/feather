package uk.co.nickthecoder.feather.compare;

public class JavaLogic {

    public boolean and(boolean a, boolean b) {
        return a && b;
    }

    public boolean or(boolean a, boolean b) {
        return a || b;
    }

}
