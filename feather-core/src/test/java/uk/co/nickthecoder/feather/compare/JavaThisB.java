package uk.co.nickthecoder.feather.compare;

public class JavaThisB {

    public int a = 42;

    public int b = 2;

    public int foo() {
        return a + b;
    }

    public int baz() {
        return foo();
    }
}
