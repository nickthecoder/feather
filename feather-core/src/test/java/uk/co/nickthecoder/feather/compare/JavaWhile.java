package uk.co.nickthecoder.feather.compare;

public class JavaWhile {

    public void hello(int n) {
        int a = 1;
        while (a < n) {
            System.out.println("Hello");
            a = a * 2;
        }
    }

}
