package uk.co.nickthecoder.feather.compare;

import java.util.List;

/**
 * I created this class to have a peek at how lambdas are implemented in Java byte code,
 * with the goal of implementing lambdas in Feather.
 */
public class JavaLambda {
    public void loop(int foo, List<String> list) {
        list.forEach(name -> {
            System.out.println(foo);
        });
    }
}
