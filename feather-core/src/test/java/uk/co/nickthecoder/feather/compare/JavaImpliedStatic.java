package uk.co.nickthecoder.feather.compare;

public class JavaImpliedStatic {
    private JavaImpliedStatic() {}

    public static int add(int a, int b) {
        return a + b;
    }
}
