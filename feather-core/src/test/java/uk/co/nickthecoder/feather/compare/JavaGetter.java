package uk.co.nickthecoder.feather.compare;

import java.io.File;

public class JavaGetter {

    // Would be private in the real world, but Feather don't have private fields yet!
    public long _count = 0;
    public long getCount() {
        return _count;
    }
    public void setCount( long c ) {
        _count = c;
    }

    // Would be private in the real world, but Feather don't have private fields yet!
    public static long _staticCount = 0;
    public long getStaticCount() {
        return _staticCount;
    }
    public void setStaticCount( long c ) {
        _staticCount = c;
    }

    // Has only a getter
    public String fileName(File file) {
        return file.getName();
    }

    // Has a getter and a setter
    public int threadPriority() {
        return Thread.currentThread().getPriority();
    }

    public void threadPriority(int p) {
        Thread.currentThread().setPriority(p);
    }

    // Feather's implementation differs, as Java cannot perform ++ on get/set methods.
    // and therefore Java has to call currentThread() twice (or store it in a local variable - yuck!)
    //public void incrementPriority() {
    //    Thread.currentThread().setPriority(Thread.currentThread().getPriority() + 1);
    //}

    // TODO Add tests for static java get/set methods

}
