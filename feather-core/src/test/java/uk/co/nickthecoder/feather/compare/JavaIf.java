package uk.co.nickthecoder.feather.compare;

public class JavaIf {

    public void justIf(int a) {
        if (a == 2) {
            System.out.println("Hello");
        }
    }

    public void ifElse(int a) {
        if (a == 2) {
            System.out.println("Two");
        } else {
            System.out.println("Not two");
        }
    }

    public boolean not(boolean a ) {
        return !a;
    }

    public int returnInIfAndElse(int a) {
        if (a > 3) {
            return 2;
        } else {
            return 4;
        }
    }


    public int returnBothBranches( Object obj  ) {
        if (!(obj instanceof String)) {
            return 2;
        } else {
            return 4;
        }
    }

    // The same as the above (but the Feather versions are different!)
    public int returnBothBranchesExpression( Object obj  ) {
        if (!(obj instanceof String)) {
            return 2;
        } else {
            return 4;
        }
    }

    public int ifYesExpression( String str ) {
        int result = str.equals("yes") ? 111 : 112;
        return result;
    }

}
