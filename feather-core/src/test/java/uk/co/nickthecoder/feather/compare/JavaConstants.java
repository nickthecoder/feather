package uk.co.nickthecoder.feather.compare;

public class JavaConstants {

    public final boolean yes = true;
    public boolean no = false;

    public final char c1 = '1';

    public final int a = 1;
    public final int b = 60;
    public final int c = 600;
    public final int d = 23456789;

    public final long l1 = 100;
    public final long l2 = 23456;

    public final float f1 = 0.0f;
    public final float f2 = 0.1f;

    public final double d1 = 0.0;
    public final double d2 = 0.2;

    public static final boolean YES = true;
    public static final int LIFE = 42;

}
