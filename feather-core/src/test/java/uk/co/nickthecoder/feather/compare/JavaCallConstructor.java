package uk.co.nickthecoder.feather.compare;

import java.io.File;
import java.util.Date;

public class JavaCallConstructor {

    public File foo() {
        return new File(".");
    }

    public Date bar(double x) {
        return new Date((long) x);
    }
}
