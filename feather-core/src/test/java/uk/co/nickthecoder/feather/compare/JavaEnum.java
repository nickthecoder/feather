package uk.co.nickthecoder.feather.compare;

public enum JavaEnum {
    ORDERED,
    READY,
    DELIVERED;
}
