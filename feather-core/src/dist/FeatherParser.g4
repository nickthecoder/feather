/*
 [The "BSD licence"]
 Copyright (c) 2013 Terence Parr, Sam Harwell
 Copyright (c) 2017 Ivan Kochurkin (upgrade to Java 8)
 All rights reserved.
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/*
    Inspired by the Kotlin Parser :
    https://github.com/antlr/grammars-v4/blob/master/kotlin/kotlin/KotlinParser.g4
*/
parser grammar FeatherParser;

options { tokenVocab=FeatherLexer; }

featherFile
    : hashBangLine? NL* anysemi* (preamble) NL* anysemi* (topLevelObject (anysemi+ topLevelObject?)*)? EOF
    ;

// Preamble

hashBangLine
    : HashBangLine
    ;

preamble
    : packageHeader libraryIncludeImportList
    ;

packageHeader
    : (PACKAGE identifier semi?)?
    ;

libraryIncludeImportList
    : (libraryHeader|includeHeader|importHeader)*
    ;

libraryHeader
    : LIBRARY Path semi?
    ;

importHeader
    : IMPORT STATIC? identifier (DOT MULT | importAlias)? semi?
    ;

includeHeader
    : INCLUDE Path semi?
    ;

    
importAlias
    : AS simpleIdentifier
    ;

// Top Level

topLevelObject
    : classDeclaration | fieldDeclaration | methodDeclaration
    ;

classDeclaration
    : classDetails NL* classBody
    ;

classDetails
    : classModifiersAndName NL* (primaryConstructorParameters)? NL* (COLON NL* extendsList)?
    ;

classModifiersAndName
    : (( (ABSTRACT | ENUM)? CLASS) | INTERFACE) simpleIdentifier
    ;

primaryConstructorParameters
    : LPAREN (constructorParameter (COMMA constructorParameter)*)? RPAREN
    ;

constructorParameter
    : valOrVar=(VAL | VAR)? NL* simpleIdentifier COLON type
    ;

extendsList
    : extendsSpecifier (NL* COMMA NL* extendsSpecifier)*
    ;

extendsSpecifier
    : constructorInvocation
    | simpleIdentifier
    ;

constructorInvocation
    : simpleIdentifier NL* callSuffix
    ;

classBody
    : LCURL NL* enumValues? NL* classMemberDeclarations* NL* RCURL
    ;

enumValues
    : enumValue (NL* COMMA NL* enumValue)* NL* SEMICOLON?
    ;

enumValue
    : simpleIdentifier enumArguments
    ;

enumArguments
    : ( LPAREN (valueArgument (COMMA valueArgument)*)? RPAREN )?
    ;

// Class Level

classMemberDeclarations
    : (fieldDeclaration
    | methodDeclaration
    | initBlock) anysemi*
    ;

fieldDeclaration
    : annotations fieldDeclarationDescription (NL* ASSIGNMENT NL* expression)?
    ;

fieldDeclarationDescription
    : (STATIC)? def=( VAR | VAL ) (NL* variableDeclaration)
    ;

annotations
    : annotation*
    ;

annotation
    : LabelReference NL* annotationValues?
    ;

annotationValues
    : LPAREN NL* annotationValue (NL* COMMA NL* annotationValue)* NL* RPAREN NL*
    ;

annotationValue
    : simpleIdentifier NL* ASSIGNMENT NL* literalConstant
    ;

initBlock
    : INIT NL* block
    ;

variableDeclaration
    : simpleIdentifier (COLON type)?
    ;

// Method Declaration

methodDeclaration
    : methodDescription
    (NL* methodBody)?
    ;

methodDescription
    : annotations (STATIC)? (ABSTRACT)? (OVERRIDE)? FUN NL* (ext=simpleIdentifier DOT NL*)? name=simpleIdentifier
    NL* methodValueParameters
    (NL* COLON NL* type)?
    ;

methodValueParameters
    : LPAREN (methodParameter (COMMA methodParameter)*)? RPAREN
    ;

methodParameter
    : simpleIdentifier COLON type ELIPSIS?
    ;

methodBody
    : methodBlock
    | ASSIGNMENT NL* expression
    ;

methodBlock
    : block
    ;

// Statements

block
    : LCURL statements RCURL
    ;

controlStructureBody
    : block
    | expression
    ;

statements
    : anysemi* (statement (anysemi+ statement?)*)?
    ;

statement
    : localVariableDeclaration
    | expression
    ;

localVariableDeclaration
    : def=( VAR | VAL ) NL* variableDeclaration (NL* ASSIGNMENT NL* expression)?
    ;

throwStatement
    : THROW NL* expression
    ;

returnStatement
    : RETURN ( expression )?
    ;

continueStatement
    : CONTINUE
    ;

breakStatement
    : BREAK
    ;

// Expression


expression
    : disjunction (bop=(ASSIGNMENT | ADD_ASSIGNMENT | SUB_ASSIGNMENT | MULT_ASSIGNMENT | DIVIDE_ASSIGNMENT | FLOOR_DIV_ASSIGNMENT | REM_ASSIGNMENT) disjunction)*
    ;

disjunction
    : disjunction NL* bop=DISJ NL* disjunction
    | conjunction
    ;

conjunction
    : conjunction NL* bop=CONJ NL* conjunction
    | equalityComparison
    ;

equalityComparison
    : equalityComparison bop=( EXCL_EQ | EXCL_EQEQ | EQEQ | EQEQEQ ) NL* equalityComparison
    | comparison
    ;

comparison
    : namedInfix ( bop=( LANGLE | RANGLE | LE | GE ) NL* namedInfix )?
    ;

namedInfix
    : elvisExpression ( bop=( IS | NOT_IS ) NL* ident )?
    ;

elvisExpression
    : elvisExpression bop=ELVIS NL* elvisExpression
    | infixFunctionCall
    ;

infixFunctionCall
    : infixFunctionCall simpleIdentifier NL* infixFunctionCall
    | mapToExpression
    | rangeExpression
    | additiveExpression
    ;

mapToExpression
    : additiveExpression bop=MAP_TO NL* additiveExpression
    ;

rangeExpression
    : additiveExpression bop=RANGE NL* additiveExpression
    ;

additiveExpression
    : additiveExpression bop=( ADD | SUB ) NL* additiveExpression
    | multiplicativeExpression
    ;

multiplicativeExpression
    : multiplicativeExpression bop=( MULT | FLOOR_DIV | DIVIDE | REM )  NL* multiplicativeExpression
    | powerExpression
    ;

powerExpression
    : powerExpression bop=POWER NL* powerExpression
    | typeRHS
    ;

typeRHS
    : prefixUnaryExpression (NL* bop=(AS | AS_SAFE) ident)?
    ;

prefixUnaryExpression
    : uop=( ADD | SUB | EXCL | DECR | INCR ) NL* prefixUnaryExpression
    | invokableExpression
    ;

invokableExpression
    : postfixUnaryExpression callSuffix?
    ;

postfixUnaryExpression
    : postfixUnaryExpression NL* ( uop=( INCR | DECR ) | arrayAccess | receiverAction | nonStaticMethodCall | fieldAccess | curriedFunction )
    | with
    | methodCall
    | atomicExpression
    | function
    ;

function
    : (className=simpleIdentifier)? COLONCOLON methodName=simpleIdentifier ( LANGLE ( type ( COMMA type )* )? RANGLE )?
    ;

curriedFunction
    : CURRIED methodName=simpleIdentifier ( LANGLE ( type ( COMMA type )* )? RANGLE )?
    ;

nonStaticMethodCall
    : DOT simpleIdentifier callSuffix
    ;

methodCall
    : simpleIdentifier callSuffix
    ;

// Feather can't implement the "apply", nor "with" without direct help from the compiler :-(
// FYI, I haven't called this "apply", because I expect to add similar features, such as "let", "run"...
receiverAction
    : receiverDescription NL* block
    ;

receiverDescription
    : DOT action=APPLY
    ;

with
    : withDescription NL* block
    ;

withDescription
    : WITH LPAREN expression RPAREN
    ;

fieldAccess
    : DOT simpleIdentifier
    ;

arrayAccess
    : LSQUARE expression RSQUARE
    ;

callSuffix
    : typeArguments? valueArguments
    ;

valueArguments
    : LPAREN (valueArgument (COMMA valueArgument)*)? RPAREN
    ;

valueArgument
    : NL* expression
    ;

typeArguments
    : LANGLE NL* typeProjection (NL* COMMA typeProjection)* NL* RANGLE
    ;

typeProjection
    : type | MULT
    ;

atomicExpression
    : parenthesizedExpression
    | returnStatement
    | continueStatement
    | breakStatement
    | throwStatement
    | literalConstant
    | thisExpression
    | ident
    | ifExpression
    | whileExpression
    | forExpression
    | doWhileExpression
    | tryExpression
    | superExpression
    ;

thisExpression
    : THIS
    ;

superExpression
    : SUPER
    ;

ident
    : simpleIdentifier
    ;

parenthesizedExpression
    : LPAREN expression RPAREN
    ;

ifExpression
    // IF NL* LPAREN expression RPAREN NL* controlStructureBody? SEMICOLON?
    : IF NL* expression NL* controlStructureBody? SEMICOLON?
    (NL* ELSE NL* controlStructureBody)?
    ;

forExpression
    : forSetup forBody
    ;

forSetup
    : FOR                                          loopVar=simpleIdentifier NL* IN NL* expression
    | FOR LPAREN                                   loopVar=simpleIdentifier NL* IN NL* expression RPAREN
    | FOR        counterVar=simpleIdentifier COMMA loopVar=simpleIdentifier NL* IN NL* expression
    | FOR LPAREN counterVar=simpleIdentifier COMMA loopVar=simpleIdentifier NL* IN NL* expression RPAREN
    ;

forBody
    : controlStructureBody?
    ;

whileExpression
    : WHILE NL* LPAREN expression RPAREN NL* whileBody
    ;

doWhileExpression
    : DO NL* whileBody NL* WHILE NL* LPAREN expression RPAREN
    ;

whileBody
    : controlStructureBody?
    ;

tryExpression
    : TRY NL* tryBlock (NL* catchBlock)* (NL* finallyBlock)?
    ;

tryBlock
    : block
    ;

catchBlock
    : CATCH NL* LPAREN name=simpleIdentifier COLON throwType=simpleIdentifier RPAREN NL* block
    ;

finallyBlock
    : FINALLY NL* block
;

// Literals


literalConstant
    : nullLiteral
    | booleanLiteral
    | characterLiteral
    | byteLiteral
    | shortLiteral
    | intLiteral
    | longLiteral
    | floatLiteral
    | doubleLiteral
    | realLiteral
    | stringLiteral
    | commandLiteral
    ;

nullLiteral : NullLiteral ;
booleanLiteral : BooleanLiteral ;
characterLiteral : CharacterLiteral ;
byteLiteral : ByteLiteral ;
shortLiteral : ShortLiteral ;
intLiteral : (DecLiteral | HexLiteral | BinLiteral);
longLiteral : LongLiteral ;
floatLiteral : FloatLiteral ;
doubleLiteral : DoubleLiteral ;
realLiteral : RealLiteral ;

stringLiteral
    : lineStringLiteral
    | multiLineStringLiteral
    ;

lineStringLiteral
    : QUOTE_OPEN (lineStringContent | lineStringExpression)* QUOTE_CLOSE
    ;

multiLineStringLiteral
    : TRIPLE_QUOTE_OPEN (multiLineStringContent | multiLineStringExpression | multiLineStringQuote)* TRIPLE_QUOTE_CLOSE
    ;

lineStringContent
    : (LineStrText | LineStrEscapedChar)+
    | ref=LineStrRef
    ;

lineStringExpression
    : LineStrExprStart expression RCURL
    ;

multiLineStringContent
    : (MultiLineStrText | MultiLineStrEscapedChar)+
    | ref=MultiLineStrRef
    ;

multiLineStringExpression
    : MultiLineStrExprStart expression RCURL
    ;

multiLineStringQuote
    : MultiLineStringQuote
    ;

commandLiteral
    : COMMAND_OPEN ( commandContent | commandExpression )* COMMAND_CLOSE
    ;

commandContent
    : (CommandText | CommandEscapedChar)+
    | ref=CommandRef
    ;

commandExpression
    : CommandExprStart expression RCURL
    ;

// Medium

type
    : nonFunctionType
    | functionType
    ;

nonFunctionType
    : simpleUserType (genericSpec)?
    ;

functionType
    :LPAREN ( type (COMMA type)* )? RPAREN functionTypeReturn
    ;

functionTypeReturn
    : (ARROW returnType=type)?
    ;

genericSpec
    : LANGLE simpleUserType (COMMA simpleUserType)* RANGLE
    ;

simpleUserType
    : simpleIdentifier // Allow fully qualified names???
    ;

// Basic

identifier
    : simpleIdentifier (NL* DOT simpleIdentifier)*
    ;

simpleIdentifier
    : Identifier | IN | AS  // Some keywords may also be used as simple identifiers!
    ;

semi: NL+ | NL* SEMICOLON NL*;

anysemi: NL | SEMICOLON;
