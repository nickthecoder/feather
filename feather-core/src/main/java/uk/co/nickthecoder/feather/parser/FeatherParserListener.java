// Generated from /home/nick/projects/feather/feather-core/src/dist/FeatherParser.g4 by ANTLR 4.9.1
package uk.co.nickthecoder.feather.parser;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link FeatherParser}.
 */
public interface FeatherParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link FeatherParser#featherFile}.
	 * @param ctx the parse tree
	 */
	void enterFeatherFile(FeatherParser.FeatherFileContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#featherFile}.
	 * @param ctx the parse tree
	 */
	void exitFeatherFile(FeatherParser.FeatherFileContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#hashBangLine}.
	 * @param ctx the parse tree
	 */
	void enterHashBangLine(FeatherParser.HashBangLineContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#hashBangLine}.
	 * @param ctx the parse tree
	 */
	void exitHashBangLine(FeatherParser.HashBangLineContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#preamble}.
	 * @param ctx the parse tree
	 */
	void enterPreamble(FeatherParser.PreambleContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#preamble}.
	 * @param ctx the parse tree
	 */
	void exitPreamble(FeatherParser.PreambleContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#packageHeader}.
	 * @param ctx the parse tree
	 */
	void enterPackageHeader(FeatherParser.PackageHeaderContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#packageHeader}.
	 * @param ctx the parse tree
	 */
	void exitPackageHeader(FeatherParser.PackageHeaderContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#libraryIncludeImportList}.
	 * @param ctx the parse tree
	 */
	void enterLibraryIncludeImportList(FeatherParser.LibraryIncludeImportListContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#libraryIncludeImportList}.
	 * @param ctx the parse tree
	 */
	void exitLibraryIncludeImportList(FeatherParser.LibraryIncludeImportListContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#libraryHeader}.
	 * @param ctx the parse tree
	 */
	void enterLibraryHeader(FeatherParser.LibraryHeaderContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#libraryHeader}.
	 * @param ctx the parse tree
	 */
	void exitLibraryHeader(FeatherParser.LibraryHeaderContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#importHeader}.
	 * @param ctx the parse tree
	 */
	void enterImportHeader(FeatherParser.ImportHeaderContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#importHeader}.
	 * @param ctx the parse tree
	 */
	void exitImportHeader(FeatherParser.ImportHeaderContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#includeHeader}.
	 * @param ctx the parse tree
	 */
	void enterIncludeHeader(FeatherParser.IncludeHeaderContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#includeHeader}.
	 * @param ctx the parse tree
	 */
	void exitIncludeHeader(FeatherParser.IncludeHeaderContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#importAlias}.
	 * @param ctx the parse tree
	 */
	void enterImportAlias(FeatherParser.ImportAliasContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#importAlias}.
	 * @param ctx the parse tree
	 */
	void exitImportAlias(FeatherParser.ImportAliasContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#topLevelObject}.
	 * @param ctx the parse tree
	 */
	void enterTopLevelObject(FeatherParser.TopLevelObjectContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#topLevelObject}.
	 * @param ctx the parse tree
	 */
	void exitTopLevelObject(FeatherParser.TopLevelObjectContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#classDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterClassDeclaration(FeatherParser.ClassDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#classDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitClassDeclaration(FeatherParser.ClassDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#classDetails}.
	 * @param ctx the parse tree
	 */
	void enterClassDetails(FeatherParser.ClassDetailsContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#classDetails}.
	 * @param ctx the parse tree
	 */
	void exitClassDetails(FeatherParser.ClassDetailsContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#classModifiersAndName}.
	 * @param ctx the parse tree
	 */
	void enterClassModifiersAndName(FeatherParser.ClassModifiersAndNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#classModifiersAndName}.
	 * @param ctx the parse tree
	 */
	void exitClassModifiersAndName(FeatherParser.ClassModifiersAndNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#primaryConstructorParameters}.
	 * @param ctx the parse tree
	 */
	void enterPrimaryConstructorParameters(FeatherParser.PrimaryConstructorParametersContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#primaryConstructorParameters}.
	 * @param ctx the parse tree
	 */
	void exitPrimaryConstructorParameters(FeatherParser.PrimaryConstructorParametersContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#constructorParameter}.
	 * @param ctx the parse tree
	 */
	void enterConstructorParameter(FeatherParser.ConstructorParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#constructorParameter}.
	 * @param ctx the parse tree
	 */
	void exitConstructorParameter(FeatherParser.ConstructorParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#extendsList}.
	 * @param ctx the parse tree
	 */
	void enterExtendsList(FeatherParser.ExtendsListContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#extendsList}.
	 * @param ctx the parse tree
	 */
	void exitExtendsList(FeatherParser.ExtendsListContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#extendsSpecifier}.
	 * @param ctx the parse tree
	 */
	void enterExtendsSpecifier(FeatherParser.ExtendsSpecifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#extendsSpecifier}.
	 * @param ctx the parse tree
	 */
	void exitExtendsSpecifier(FeatherParser.ExtendsSpecifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#constructorInvocation}.
	 * @param ctx the parse tree
	 */
	void enterConstructorInvocation(FeatherParser.ConstructorInvocationContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#constructorInvocation}.
	 * @param ctx the parse tree
	 */
	void exitConstructorInvocation(FeatherParser.ConstructorInvocationContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#classBody}.
	 * @param ctx the parse tree
	 */
	void enterClassBody(FeatherParser.ClassBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#classBody}.
	 * @param ctx the parse tree
	 */
	void exitClassBody(FeatherParser.ClassBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#enumValues}.
	 * @param ctx the parse tree
	 */
	void enterEnumValues(FeatherParser.EnumValuesContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#enumValues}.
	 * @param ctx the parse tree
	 */
	void exitEnumValues(FeatherParser.EnumValuesContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#enumValue}.
	 * @param ctx the parse tree
	 */
	void enterEnumValue(FeatherParser.EnumValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#enumValue}.
	 * @param ctx the parse tree
	 */
	void exitEnumValue(FeatherParser.EnumValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#enumArguments}.
	 * @param ctx the parse tree
	 */
	void enterEnumArguments(FeatherParser.EnumArgumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#enumArguments}.
	 * @param ctx the parse tree
	 */
	void exitEnumArguments(FeatherParser.EnumArgumentsContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#classMemberDeclarations}.
	 * @param ctx the parse tree
	 */
	void enterClassMemberDeclarations(FeatherParser.ClassMemberDeclarationsContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#classMemberDeclarations}.
	 * @param ctx the parse tree
	 */
	void exitClassMemberDeclarations(FeatherParser.ClassMemberDeclarationsContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#fieldDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterFieldDeclaration(FeatherParser.FieldDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#fieldDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitFieldDeclaration(FeatherParser.FieldDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#fieldDeclarationDescription}.
	 * @param ctx the parse tree
	 */
	void enterFieldDeclarationDescription(FeatherParser.FieldDeclarationDescriptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#fieldDeclarationDescription}.
	 * @param ctx the parse tree
	 */
	void exitFieldDeclarationDescription(FeatherParser.FieldDeclarationDescriptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#annotations}.
	 * @param ctx the parse tree
	 */
	void enterAnnotations(FeatherParser.AnnotationsContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#annotations}.
	 * @param ctx the parse tree
	 */
	void exitAnnotations(FeatherParser.AnnotationsContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#annotation}.
	 * @param ctx the parse tree
	 */
	void enterAnnotation(FeatherParser.AnnotationContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#annotation}.
	 * @param ctx the parse tree
	 */
	void exitAnnotation(FeatherParser.AnnotationContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#annotationValues}.
	 * @param ctx the parse tree
	 */
	void enterAnnotationValues(FeatherParser.AnnotationValuesContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#annotationValues}.
	 * @param ctx the parse tree
	 */
	void exitAnnotationValues(FeatherParser.AnnotationValuesContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#annotationValue}.
	 * @param ctx the parse tree
	 */
	void enterAnnotationValue(FeatherParser.AnnotationValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#annotationValue}.
	 * @param ctx the parse tree
	 */
	void exitAnnotationValue(FeatherParser.AnnotationValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#initBlock}.
	 * @param ctx the parse tree
	 */
	void enterInitBlock(FeatherParser.InitBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#initBlock}.
	 * @param ctx the parse tree
	 */
	void exitInitBlock(FeatherParser.InitBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#variableDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterVariableDeclaration(FeatherParser.VariableDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#variableDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitVariableDeclaration(FeatherParser.VariableDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#methodDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterMethodDeclaration(FeatherParser.MethodDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#methodDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitMethodDeclaration(FeatherParser.MethodDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#methodDescription}.
	 * @param ctx the parse tree
	 */
	void enterMethodDescription(FeatherParser.MethodDescriptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#methodDescription}.
	 * @param ctx the parse tree
	 */
	void exitMethodDescription(FeatherParser.MethodDescriptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#methodValueParameters}.
	 * @param ctx the parse tree
	 */
	void enterMethodValueParameters(FeatherParser.MethodValueParametersContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#methodValueParameters}.
	 * @param ctx the parse tree
	 */
	void exitMethodValueParameters(FeatherParser.MethodValueParametersContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#methodParameter}.
	 * @param ctx the parse tree
	 */
	void enterMethodParameter(FeatherParser.MethodParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#methodParameter}.
	 * @param ctx the parse tree
	 */
	void exitMethodParameter(FeatherParser.MethodParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#methodBody}.
	 * @param ctx the parse tree
	 */
	void enterMethodBody(FeatherParser.MethodBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#methodBody}.
	 * @param ctx the parse tree
	 */
	void exitMethodBody(FeatherParser.MethodBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#methodBlock}.
	 * @param ctx the parse tree
	 */
	void enterMethodBlock(FeatherParser.MethodBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#methodBlock}.
	 * @param ctx the parse tree
	 */
	void exitMethodBlock(FeatherParser.MethodBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(FeatherParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(FeatherParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#controlStructureBody}.
	 * @param ctx the parse tree
	 */
	void enterControlStructureBody(FeatherParser.ControlStructureBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#controlStructureBody}.
	 * @param ctx the parse tree
	 */
	void exitControlStructureBody(FeatherParser.ControlStructureBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#statements}.
	 * @param ctx the parse tree
	 */
	void enterStatements(FeatherParser.StatementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#statements}.
	 * @param ctx the parse tree
	 */
	void exitStatements(FeatherParser.StatementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(FeatherParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(FeatherParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#localVariableDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterLocalVariableDeclaration(FeatherParser.LocalVariableDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#localVariableDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitLocalVariableDeclaration(FeatherParser.LocalVariableDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#throwStatement}.
	 * @param ctx the parse tree
	 */
	void enterThrowStatement(FeatherParser.ThrowStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#throwStatement}.
	 * @param ctx the parse tree
	 */
	void exitThrowStatement(FeatherParser.ThrowStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#returnStatement}.
	 * @param ctx the parse tree
	 */
	void enterReturnStatement(FeatherParser.ReturnStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#returnStatement}.
	 * @param ctx the parse tree
	 */
	void exitReturnStatement(FeatherParser.ReturnStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#continueStatement}.
	 * @param ctx the parse tree
	 */
	void enterContinueStatement(FeatherParser.ContinueStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#continueStatement}.
	 * @param ctx the parse tree
	 */
	void exitContinueStatement(FeatherParser.ContinueStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#breakStatement}.
	 * @param ctx the parse tree
	 */
	void enterBreakStatement(FeatherParser.BreakStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#breakStatement}.
	 * @param ctx the parse tree
	 */
	void exitBreakStatement(FeatherParser.BreakStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(FeatherParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(FeatherParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#disjunction}.
	 * @param ctx the parse tree
	 */
	void enterDisjunction(FeatherParser.DisjunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#disjunction}.
	 * @param ctx the parse tree
	 */
	void exitDisjunction(FeatherParser.DisjunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#conjunction}.
	 * @param ctx the parse tree
	 */
	void enterConjunction(FeatherParser.ConjunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#conjunction}.
	 * @param ctx the parse tree
	 */
	void exitConjunction(FeatherParser.ConjunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#equalityComparison}.
	 * @param ctx the parse tree
	 */
	void enterEqualityComparison(FeatherParser.EqualityComparisonContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#equalityComparison}.
	 * @param ctx the parse tree
	 */
	void exitEqualityComparison(FeatherParser.EqualityComparisonContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#comparison}.
	 * @param ctx the parse tree
	 */
	void enterComparison(FeatherParser.ComparisonContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#comparison}.
	 * @param ctx the parse tree
	 */
	void exitComparison(FeatherParser.ComparisonContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#namedInfix}.
	 * @param ctx the parse tree
	 */
	void enterNamedInfix(FeatherParser.NamedInfixContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#namedInfix}.
	 * @param ctx the parse tree
	 */
	void exitNamedInfix(FeatherParser.NamedInfixContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#elvisExpression}.
	 * @param ctx the parse tree
	 */
	void enterElvisExpression(FeatherParser.ElvisExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#elvisExpression}.
	 * @param ctx the parse tree
	 */
	void exitElvisExpression(FeatherParser.ElvisExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#infixFunctionCall}.
	 * @param ctx the parse tree
	 */
	void enterInfixFunctionCall(FeatherParser.InfixFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#infixFunctionCall}.
	 * @param ctx the parse tree
	 */
	void exitInfixFunctionCall(FeatherParser.InfixFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#mapToExpression}.
	 * @param ctx the parse tree
	 */
	void enterMapToExpression(FeatherParser.MapToExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#mapToExpression}.
	 * @param ctx the parse tree
	 */
	void exitMapToExpression(FeatherParser.MapToExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#rangeExpression}.
	 * @param ctx the parse tree
	 */
	void enterRangeExpression(FeatherParser.RangeExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#rangeExpression}.
	 * @param ctx the parse tree
	 */
	void exitRangeExpression(FeatherParser.RangeExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#additiveExpression}.
	 * @param ctx the parse tree
	 */
	void enterAdditiveExpression(FeatherParser.AdditiveExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#additiveExpression}.
	 * @param ctx the parse tree
	 */
	void exitAdditiveExpression(FeatherParser.AdditiveExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 */
	void enterMultiplicativeExpression(FeatherParser.MultiplicativeExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 */
	void exitMultiplicativeExpression(FeatherParser.MultiplicativeExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#powerExpression}.
	 * @param ctx the parse tree
	 */
	void enterPowerExpression(FeatherParser.PowerExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#powerExpression}.
	 * @param ctx the parse tree
	 */
	void exitPowerExpression(FeatherParser.PowerExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#typeRHS}.
	 * @param ctx the parse tree
	 */
	void enterTypeRHS(FeatherParser.TypeRHSContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#typeRHS}.
	 * @param ctx the parse tree
	 */
	void exitTypeRHS(FeatherParser.TypeRHSContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#prefixUnaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterPrefixUnaryExpression(FeatherParser.PrefixUnaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#prefixUnaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitPrefixUnaryExpression(FeatherParser.PrefixUnaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#invokableExpression}.
	 * @param ctx the parse tree
	 */
	void enterInvokableExpression(FeatherParser.InvokableExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#invokableExpression}.
	 * @param ctx the parse tree
	 */
	void exitInvokableExpression(FeatherParser.InvokableExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#postfixUnaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterPostfixUnaryExpression(FeatherParser.PostfixUnaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#postfixUnaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitPostfixUnaryExpression(FeatherParser.PostfixUnaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#function}.
	 * @param ctx the parse tree
	 */
	void enterFunction(FeatherParser.FunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#function}.
	 * @param ctx the parse tree
	 */
	void exitFunction(FeatherParser.FunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#curriedFunction}.
	 * @param ctx the parse tree
	 */
	void enterCurriedFunction(FeatherParser.CurriedFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#curriedFunction}.
	 * @param ctx the parse tree
	 */
	void exitCurriedFunction(FeatherParser.CurriedFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#nonStaticMethodCall}.
	 * @param ctx the parse tree
	 */
	void enterNonStaticMethodCall(FeatherParser.NonStaticMethodCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#nonStaticMethodCall}.
	 * @param ctx the parse tree
	 */
	void exitNonStaticMethodCall(FeatherParser.NonStaticMethodCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#methodCall}.
	 * @param ctx the parse tree
	 */
	void enterMethodCall(FeatherParser.MethodCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#methodCall}.
	 * @param ctx the parse tree
	 */
	void exitMethodCall(FeatherParser.MethodCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#receiverAction}.
	 * @param ctx the parse tree
	 */
	void enterReceiverAction(FeatherParser.ReceiverActionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#receiverAction}.
	 * @param ctx the parse tree
	 */
	void exitReceiverAction(FeatherParser.ReceiverActionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#receiverDescription}.
	 * @param ctx the parse tree
	 */
	void enterReceiverDescription(FeatherParser.ReceiverDescriptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#receiverDescription}.
	 * @param ctx the parse tree
	 */
	void exitReceiverDescription(FeatherParser.ReceiverDescriptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#with}.
	 * @param ctx the parse tree
	 */
	void enterWith(FeatherParser.WithContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#with}.
	 * @param ctx the parse tree
	 */
	void exitWith(FeatherParser.WithContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#withDescription}.
	 * @param ctx the parse tree
	 */
	void enterWithDescription(FeatherParser.WithDescriptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#withDescription}.
	 * @param ctx the parse tree
	 */
	void exitWithDescription(FeatherParser.WithDescriptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#fieldAccess}.
	 * @param ctx the parse tree
	 */
	void enterFieldAccess(FeatherParser.FieldAccessContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#fieldAccess}.
	 * @param ctx the parse tree
	 */
	void exitFieldAccess(FeatherParser.FieldAccessContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#arrayAccess}.
	 * @param ctx the parse tree
	 */
	void enterArrayAccess(FeatherParser.ArrayAccessContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#arrayAccess}.
	 * @param ctx the parse tree
	 */
	void exitArrayAccess(FeatherParser.ArrayAccessContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#callSuffix}.
	 * @param ctx the parse tree
	 */
	void enterCallSuffix(FeatherParser.CallSuffixContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#callSuffix}.
	 * @param ctx the parse tree
	 */
	void exitCallSuffix(FeatherParser.CallSuffixContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#valueArguments}.
	 * @param ctx the parse tree
	 */
	void enterValueArguments(FeatherParser.ValueArgumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#valueArguments}.
	 * @param ctx the parse tree
	 */
	void exitValueArguments(FeatherParser.ValueArgumentsContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#valueArgument}.
	 * @param ctx the parse tree
	 */
	void enterValueArgument(FeatherParser.ValueArgumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#valueArgument}.
	 * @param ctx the parse tree
	 */
	void exitValueArgument(FeatherParser.ValueArgumentContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#typeArguments}.
	 * @param ctx the parse tree
	 */
	void enterTypeArguments(FeatherParser.TypeArgumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#typeArguments}.
	 * @param ctx the parse tree
	 */
	void exitTypeArguments(FeatherParser.TypeArgumentsContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#typeProjection}.
	 * @param ctx the parse tree
	 */
	void enterTypeProjection(FeatherParser.TypeProjectionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#typeProjection}.
	 * @param ctx the parse tree
	 */
	void exitTypeProjection(FeatherParser.TypeProjectionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#atomicExpression}.
	 * @param ctx the parse tree
	 */
	void enterAtomicExpression(FeatherParser.AtomicExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#atomicExpression}.
	 * @param ctx the parse tree
	 */
	void exitAtomicExpression(FeatherParser.AtomicExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#thisExpression}.
	 * @param ctx the parse tree
	 */
	void enterThisExpression(FeatherParser.ThisExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#thisExpression}.
	 * @param ctx the parse tree
	 */
	void exitThisExpression(FeatherParser.ThisExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#superExpression}.
	 * @param ctx the parse tree
	 */
	void enterSuperExpression(FeatherParser.SuperExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#superExpression}.
	 * @param ctx the parse tree
	 */
	void exitSuperExpression(FeatherParser.SuperExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#ident}.
	 * @param ctx the parse tree
	 */
	void enterIdent(FeatherParser.IdentContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#ident}.
	 * @param ctx the parse tree
	 */
	void exitIdent(FeatherParser.IdentContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#parenthesizedExpression}.
	 * @param ctx the parse tree
	 */
	void enterParenthesizedExpression(FeatherParser.ParenthesizedExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#parenthesizedExpression}.
	 * @param ctx the parse tree
	 */
	void exitParenthesizedExpression(FeatherParser.ParenthesizedExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#ifExpression}.
	 * @param ctx the parse tree
	 */
	void enterIfExpression(FeatherParser.IfExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#ifExpression}.
	 * @param ctx the parse tree
	 */
	void exitIfExpression(FeatherParser.IfExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#forExpression}.
	 * @param ctx the parse tree
	 */
	void enterForExpression(FeatherParser.ForExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#forExpression}.
	 * @param ctx the parse tree
	 */
	void exitForExpression(FeatherParser.ForExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#forSetup}.
	 * @param ctx the parse tree
	 */
	void enterForSetup(FeatherParser.ForSetupContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#forSetup}.
	 * @param ctx the parse tree
	 */
	void exitForSetup(FeatherParser.ForSetupContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#forBody}.
	 * @param ctx the parse tree
	 */
	void enterForBody(FeatherParser.ForBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#forBody}.
	 * @param ctx the parse tree
	 */
	void exitForBody(FeatherParser.ForBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#whileExpression}.
	 * @param ctx the parse tree
	 */
	void enterWhileExpression(FeatherParser.WhileExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#whileExpression}.
	 * @param ctx the parse tree
	 */
	void exitWhileExpression(FeatherParser.WhileExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#doWhileExpression}.
	 * @param ctx the parse tree
	 */
	void enterDoWhileExpression(FeatherParser.DoWhileExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#doWhileExpression}.
	 * @param ctx the parse tree
	 */
	void exitDoWhileExpression(FeatherParser.DoWhileExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#whileBody}.
	 * @param ctx the parse tree
	 */
	void enterWhileBody(FeatherParser.WhileBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#whileBody}.
	 * @param ctx the parse tree
	 */
	void exitWhileBody(FeatherParser.WhileBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#tryExpression}.
	 * @param ctx the parse tree
	 */
	void enterTryExpression(FeatherParser.TryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#tryExpression}.
	 * @param ctx the parse tree
	 */
	void exitTryExpression(FeatherParser.TryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#tryBlock}.
	 * @param ctx the parse tree
	 */
	void enterTryBlock(FeatherParser.TryBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#tryBlock}.
	 * @param ctx the parse tree
	 */
	void exitTryBlock(FeatherParser.TryBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#catchBlock}.
	 * @param ctx the parse tree
	 */
	void enterCatchBlock(FeatherParser.CatchBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#catchBlock}.
	 * @param ctx the parse tree
	 */
	void exitCatchBlock(FeatherParser.CatchBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#finallyBlock}.
	 * @param ctx the parse tree
	 */
	void enterFinallyBlock(FeatherParser.FinallyBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#finallyBlock}.
	 * @param ctx the parse tree
	 */
	void exitFinallyBlock(FeatherParser.FinallyBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#literalConstant}.
	 * @param ctx the parse tree
	 */
	void enterLiteralConstant(FeatherParser.LiteralConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#literalConstant}.
	 * @param ctx the parse tree
	 */
	void exitLiteralConstant(FeatherParser.LiteralConstantContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#nullLiteral}.
	 * @param ctx the parse tree
	 */
	void enterNullLiteral(FeatherParser.NullLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#nullLiteral}.
	 * @param ctx the parse tree
	 */
	void exitNullLiteral(FeatherParser.NullLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#booleanLiteral}.
	 * @param ctx the parse tree
	 */
	void enterBooleanLiteral(FeatherParser.BooleanLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#booleanLiteral}.
	 * @param ctx the parse tree
	 */
	void exitBooleanLiteral(FeatherParser.BooleanLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#characterLiteral}.
	 * @param ctx the parse tree
	 */
	void enterCharacterLiteral(FeatherParser.CharacterLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#characterLiteral}.
	 * @param ctx the parse tree
	 */
	void exitCharacterLiteral(FeatherParser.CharacterLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#byteLiteral}.
	 * @param ctx the parse tree
	 */
	void enterByteLiteral(FeatherParser.ByteLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#byteLiteral}.
	 * @param ctx the parse tree
	 */
	void exitByteLiteral(FeatherParser.ByteLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#shortLiteral}.
	 * @param ctx the parse tree
	 */
	void enterShortLiteral(FeatherParser.ShortLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#shortLiteral}.
	 * @param ctx the parse tree
	 */
	void exitShortLiteral(FeatherParser.ShortLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#intLiteral}.
	 * @param ctx the parse tree
	 */
	void enterIntLiteral(FeatherParser.IntLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#intLiteral}.
	 * @param ctx the parse tree
	 */
	void exitIntLiteral(FeatherParser.IntLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#longLiteral}.
	 * @param ctx the parse tree
	 */
	void enterLongLiteral(FeatherParser.LongLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#longLiteral}.
	 * @param ctx the parse tree
	 */
	void exitLongLiteral(FeatherParser.LongLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#floatLiteral}.
	 * @param ctx the parse tree
	 */
	void enterFloatLiteral(FeatherParser.FloatLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#floatLiteral}.
	 * @param ctx the parse tree
	 */
	void exitFloatLiteral(FeatherParser.FloatLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#doubleLiteral}.
	 * @param ctx the parse tree
	 */
	void enterDoubleLiteral(FeatherParser.DoubleLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#doubleLiteral}.
	 * @param ctx the parse tree
	 */
	void exitDoubleLiteral(FeatherParser.DoubleLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#realLiteral}.
	 * @param ctx the parse tree
	 */
	void enterRealLiteral(FeatherParser.RealLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#realLiteral}.
	 * @param ctx the parse tree
	 */
	void exitRealLiteral(FeatherParser.RealLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#stringLiteral}.
	 * @param ctx the parse tree
	 */
	void enterStringLiteral(FeatherParser.StringLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#stringLiteral}.
	 * @param ctx the parse tree
	 */
	void exitStringLiteral(FeatherParser.StringLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#lineStringLiteral}.
	 * @param ctx the parse tree
	 */
	void enterLineStringLiteral(FeatherParser.LineStringLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#lineStringLiteral}.
	 * @param ctx the parse tree
	 */
	void exitLineStringLiteral(FeatherParser.LineStringLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#multiLineStringLiteral}.
	 * @param ctx the parse tree
	 */
	void enterMultiLineStringLiteral(FeatherParser.MultiLineStringLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#multiLineStringLiteral}.
	 * @param ctx the parse tree
	 */
	void exitMultiLineStringLiteral(FeatherParser.MultiLineStringLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#lineStringContent}.
	 * @param ctx the parse tree
	 */
	void enterLineStringContent(FeatherParser.LineStringContentContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#lineStringContent}.
	 * @param ctx the parse tree
	 */
	void exitLineStringContent(FeatherParser.LineStringContentContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#lineStringExpression}.
	 * @param ctx the parse tree
	 */
	void enterLineStringExpression(FeatherParser.LineStringExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#lineStringExpression}.
	 * @param ctx the parse tree
	 */
	void exitLineStringExpression(FeatherParser.LineStringExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#multiLineStringContent}.
	 * @param ctx the parse tree
	 */
	void enterMultiLineStringContent(FeatherParser.MultiLineStringContentContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#multiLineStringContent}.
	 * @param ctx the parse tree
	 */
	void exitMultiLineStringContent(FeatherParser.MultiLineStringContentContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#multiLineStringExpression}.
	 * @param ctx the parse tree
	 */
	void enterMultiLineStringExpression(FeatherParser.MultiLineStringExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#multiLineStringExpression}.
	 * @param ctx the parse tree
	 */
	void exitMultiLineStringExpression(FeatherParser.MultiLineStringExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#multiLineStringQuote}.
	 * @param ctx the parse tree
	 */
	void enterMultiLineStringQuote(FeatherParser.MultiLineStringQuoteContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#multiLineStringQuote}.
	 * @param ctx the parse tree
	 */
	void exitMultiLineStringQuote(FeatherParser.MultiLineStringQuoteContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#commandLiteral}.
	 * @param ctx the parse tree
	 */
	void enterCommandLiteral(FeatherParser.CommandLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#commandLiteral}.
	 * @param ctx the parse tree
	 */
	void exitCommandLiteral(FeatherParser.CommandLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#commandContent}.
	 * @param ctx the parse tree
	 */
	void enterCommandContent(FeatherParser.CommandContentContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#commandContent}.
	 * @param ctx the parse tree
	 */
	void exitCommandContent(FeatherParser.CommandContentContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#commandExpression}.
	 * @param ctx the parse tree
	 */
	void enterCommandExpression(FeatherParser.CommandExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#commandExpression}.
	 * @param ctx the parse tree
	 */
	void exitCommandExpression(FeatherParser.CommandExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(FeatherParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(FeatherParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#nonFunctionType}.
	 * @param ctx the parse tree
	 */
	void enterNonFunctionType(FeatherParser.NonFunctionTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#nonFunctionType}.
	 * @param ctx the parse tree
	 */
	void exitNonFunctionType(FeatherParser.NonFunctionTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#functionType}.
	 * @param ctx the parse tree
	 */
	void enterFunctionType(FeatherParser.FunctionTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#functionType}.
	 * @param ctx the parse tree
	 */
	void exitFunctionType(FeatherParser.FunctionTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#functionTypeReturn}.
	 * @param ctx the parse tree
	 */
	void enterFunctionTypeReturn(FeatherParser.FunctionTypeReturnContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#functionTypeReturn}.
	 * @param ctx the parse tree
	 */
	void exitFunctionTypeReturn(FeatherParser.FunctionTypeReturnContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#genericSpec}.
	 * @param ctx the parse tree
	 */
	void enterGenericSpec(FeatherParser.GenericSpecContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#genericSpec}.
	 * @param ctx the parse tree
	 */
	void exitGenericSpec(FeatherParser.GenericSpecContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#simpleUserType}.
	 * @param ctx the parse tree
	 */
	void enterSimpleUserType(FeatherParser.SimpleUserTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#simpleUserType}.
	 * @param ctx the parse tree
	 */
	void exitSimpleUserType(FeatherParser.SimpleUserTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#identifier}.
	 * @param ctx the parse tree
	 */
	void enterIdentifier(FeatherParser.IdentifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#identifier}.
	 * @param ctx the parse tree
	 */
	void exitIdentifier(FeatherParser.IdentifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#simpleIdentifier}.
	 * @param ctx the parse tree
	 */
	void enterSimpleIdentifier(FeatherParser.SimpleIdentifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#simpleIdentifier}.
	 * @param ctx the parse tree
	 */
	void exitSimpleIdentifier(FeatherParser.SimpleIdentifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#semi}.
	 * @param ctx the parse tree
	 */
	void enterSemi(FeatherParser.SemiContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#semi}.
	 * @param ctx the parse tree
	 */
	void exitSemi(FeatherParser.SemiContext ctx);
	/**
	 * Enter a parse tree produced by {@link FeatherParser#anysemi}.
	 * @param ctx the parse tree
	 */
	void enterAnysemi(FeatherParser.AnysemiContext ctx);
	/**
	 * Exit a parse tree produced by {@link FeatherParser#anysemi}.
	 * @param ctx the parse tree
	 */
	void exitAnysemi(FeatherParser.AnysemiContext ctx);
}