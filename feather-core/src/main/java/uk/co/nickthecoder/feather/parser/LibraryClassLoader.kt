package uk.co.nickthecoder.feather.parser

import org.apache.xbean.classloader.JarFileClassLoader
import java.io.File
import java.net.URL
import java.net.URLClassLoader
import java.util.jar.JarFile

class LibraryClassLoader(parent: ClassLoader) : ClassLoader(parent) {
    constructor() : this(getSystemClassLoader())

    private val jars = mutableListOf<URL>()

    /**
     * Lazily evaluated to UrlClassLoader, but set back to null each time a new jar file is added.
     */
    private var actualClassLoader: ClassLoader? = null

    fun addJar(file: File) {
        val url = file.toURI().toURL()
        jars.add(url)
        actualClassLoader = null
    }

    override fun findClass(name: String): Class<*>? {
        if (actualClassLoader == null) {
            actualClassLoader = JarFileClassLoader("library", jars.toTypedArray(), parent)
        }
        return try {
            actualClassLoader?.loadClass(name)
        } catch (e: Exception) {
            null
        }

    }

}
