package uk.co.nickthecoder.feather.internal

import uk.co.nickthecoder.feather.FeatherPosition
import java.lang.reflect.Type


/**
 * An item in [MetaDataType.constructors]
 */
internal class MetaDataConstructor(
    val owner: MetaDataType,
    val parameterTypes: List<Type>,
    val isPrimary: Boolean,
    val access: MetaDataAccess,
    val position : FeatherPosition
) {
    var isVarArgs : Boolean = false

    override fun toString() = "${owner.name}( ${parameterTypes.joinToString(separator = ", ")} )"
}
