package uk.co.nickthecoder.feather.internal.operators

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.Source
import uk.co.nickthecoder.feather.internal.autoCast
import uk.co.nickthecoder.feather.internal.doublePrimitive
import uk.co.nickthecoder.feather.internal.numberPrimitives


/**
 * Used by the ^ operator. Raises a number to the power of another number.
 * The resulting type is the "larger" of the two input types. e.g. int ^ short -> int
 */
internal class Power(
    a: StackEntry,
    b: StackEntry,
    source: Source,
    position: FeatherPosition
) : BinaryMathsOperator(a, b, "^", "power", source, position) {

    override fun standardCompile(mv: MethodVisitor) {

        val type = resolvedType()
        if (type in numberPrimitives) {
            a.compile(mv)
            autoCast(mv, a.resolvedType(), doublePrimitive)
            b.compile(mv)
            autoCast(mv, b.resolvedType(), doublePrimitive)
            mv.visitMethodInsn(Opcodes.INVOKESTATIC, Math::class.java.asmName(), "pow", "(DD)D", false)
            autoCast(mv, doublePrimitive, type)

        } else {
            throw unexpectedTypes()
        }
    }

    override fun compileApply(mv: MethodVisitor) {
    }
}