package uk.co.nickthecoder.feather.internal.operators

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import org.objectweb.asm.Opcodes.*
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.Source
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.bytePrimitive
import uk.co.nickthecoder.feather.internal.charPrimitive
import uk.co.nickthecoder.feather.internal.doublePrimitive
import uk.co.nickthecoder.feather.internal.floatPrimitive
import uk.co.nickthecoder.feather.internal.intPrimitive
import uk.co.nickthecoder.feather.internal.longPrimitive
import uk.co.nickthecoder.feather.internal.shortPrimitive
import uk.co.nickthecoder.feather.internal.stringClass
import java.lang.reflect.Type


internal class Plus(
    a: StackEntry,
    b: StackEntry,
    source: Source,
    position: FeatherPosition
) : BinaryMathsOperator(a, b, "+", "plus", source, position) {

    override fun standardType(): Type {
        val aPrim = a.type().primitive()
        val bPrim = b.type().primitive()

        return if (aPrim === charPrimitive) {
            if (bPrim in listOf(bytePrimitive, shortPrimitive, intPrimitive)) {
                charPrimitive
            } else if (bPrim === charPrimitive) {
                stringClass
            } else {
                super.standardType()
            }
        } else if (aPrim === stringClass) {
            stringClass
        } else {
            super.standardType()
        }
    }


    override fun standardCompile(mv: MethodVisitor) {

        val aPrim = a.resolvedType().primitive()
        val bPrim = b.resolvedType().primitive()

        if (aPrim === stringClass) {
            // Must be concatenating strings
            a.compile(mv)
            b.compile(mv)
            primitiveToObject(mv, b.resolvedType())
            if (b.resolvedType() != stringClass) {
                mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, b.resolvedType().nonPrimitive().asmName(), "toString", "()" + stringClass.asmDescriptor(), false)
            }
            val desc = "(${stringClass.asmDescriptor()})${stringClass.asmDescriptor()}"
            mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, stringClass.asmName(), "concat", desc, false)
            return
        }

        if (aPrim == charPrimitive && bPrim == charPrimitive) {
            // First part of creating a new string
            mv.visitTypeInsn(NEW, stringClass.asmName())
            mv.visitInsn(DUP)
            // Create an array size 2 of char primitives
            mv.visitInsn(ICONST_2)
            mv.visitIntInsn(NEWARRAY, T_CHAR)
            mv.visitInsn(DUP)
            // Set the 1st element
            mv.visitInsn(ICONST_0)
            a.compileAutoCast(mv, charPrimitive)
            mv.visitInsn(CASTORE)
            mv.visitInsn(DUP)
            // Set the 2nd element
            mv.visitInsn(ICONST_1)
            b.compileAutoCast(mv, charPrimitive)
            mv.visitInsn(CASTORE)
            // Call the String's constructor.
            mv.visitMethodInsn(INVOKESPECIAL, stringClass.asmName(), "<init>", "([C)V", false)

            return
        }

        if ((aPrim == charPrimitive && bPrim.isNumber()) || (bPrim == charPrimitive && aPrim.isNumber())) {
            a.compileAutoCast(mv, intPrimitive)
            b.compileAutoCast(mv, intPrimitive)
            mv.visitInsn(IADD)
            autoCast(mv, intPrimitive, charPrimitive)
            return
        }

        super.standardCompile(mv)
    }


    override fun compileApply(mv: MethodVisitor) {
        when (resolvedType()) {
            bytePrimitive, shortPrimitive, intPrimitive -> mv.visitInsn(IADD)
            longPrimitive -> mv.visitInsn(LADD)
            floatPrimitive -> mv.visitInsn(FADD)
            doublePrimitive -> mv.visitInsn(DADD)
            else -> throw unexpectedTypes()
        }
    }
}
