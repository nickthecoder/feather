package uk.co.nickthecoder.feather.internal.keywords

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes.DUP
import uk.co.nickthecoder.feather.internal.Block
import uk.co.nickthecoder.feather.internal.ReceiverStackEntry
import uk.co.nickthecoder.feather.internal.StackEntry
import uk.co.nickthecoder.feather.internal.UnresolvedTypeFromStackEntry

/**
 * We create a "hidden" local variable, which holds a reference to [a]. Then make a [Receiver] for that local variable,
 *
 */
internal class Apply(
    val a: StackEntry,
    val block: Block

) : ReceiverStackEntry {

    override val receiver = LocalVariableReceiver(
        block.addVariable(
            "this@apply",
            UnresolvedTypeFromStackEntry(a),
            true
        )
    )

    override fun type() = a.type()

    override fun isTerminating() = block.isTerminating()

    override fun compile(mv: MethodVisitor) {
        block.initialiseLocalVariableIndices()
        a.compile(mv)
        mv.visitInsn(DUP) // The return value of the apply statement.
        receiver.localVariable.store(mv)

        block.stackEntry(true).compile(mv)
    }
}
