package uk.co.nickthecoder.feather.internal.operators

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.Source
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.bytePrimitive
import uk.co.nickthecoder.feather.internal.doublePrimitive
import uk.co.nickthecoder.feather.internal.intPrimitive
import uk.co.nickthecoder.feather.internal.longPrimitive
import uk.co.nickthecoder.feather.internal.shortPrimitive


internal class FloorDivide(
    a: StackEntry,
    b: StackEntry,
    source: Source,
    position: FeatherPosition
) : BinaryMathsOperator(a, b, "~/", "floorDiv", source, position) {

    override fun standardCompile(mv: MethodVisitor) {
        if (resolvedType().primitive() === floatClass) {
            a.compile(mv)
            autoCast(mv, a.resolvedType(), doublePrimitive)
            b.compile(mv)
            autoCast(mv, b.resolvedType(), doublePrimitive)
            mv.visitInsn(Opcodes.DDIV)
            mv.visitMethodInsn(Opcodes.INVOKESTATIC, Math::class.java.asmName(), "floor", null, false)
            mv.visitInsn(Opcodes.D2F)
        } else {
            super.standardCompile(mv)
        }
    }

    override fun compileApply(mv: MethodVisitor) {
        when (resolvedType().primitive()) {
            bytePrimitive, shortPrimitive, intPrimitive -> mv.visitInsn(Opcodes.IDIV)
            longPrimitive -> mv.visitInsn(Opcodes.LDIV)
            doublePrimitive -> {
                mv.visitInsn(Opcodes.DDIV)
                mv.visitMethodInsn(Opcodes.INVOKESTATIC, Math::class.java.asmName(), "floor", "(D)D", false)
            }
            else -> throw unexpectedTypes()
        }
    }
}
