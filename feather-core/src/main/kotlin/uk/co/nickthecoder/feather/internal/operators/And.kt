package uk.co.nickthecoder.feather.internal.operators

import org.objectweb.asm.Label
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.Source
import uk.co.nickthecoder.feather.internal.autoCast
import uk.co.nickthecoder.feather.internal.booleanPrimitive
import uk.co.nickthecoder.feather.internal.primitive


internal class And(
    a: StackEntry,
    b: StackEntry,
    source : Source,
    position: FeatherPosition
) : BinaryOperator(a, b, "&&", "and", source, position) {

    override fun standardType() = booleanPrimitive

    override fun standardCompile(mv: MethodVisitor) {
        val aType = a.resolvedType().primitive()
        val bType = b.resolvedType().primitive()

        if (aType != booleanPrimitive || bType != booleanPrimitive) throw unexpectedTypes()

        val falseLabel = Label()
        val endLabel = Label()

        a.compile(mv)
        autoCast(mv, aType, booleanPrimitive)
        mv.visitJumpInsn(Opcodes.IFEQ, falseLabel) // If a is false goto false

        b.compile(mv)
        autoCast(mv, bType, booleanPrimitive)
        mv.visitJumpInsn(Opcodes.IFEQ, falseLabel) // If a is false goto false

        mv.visitInsn(Opcodes.ICONST_1)  // Result is true
        mv.visitJumpInsn(Opcodes.GOTO, endLabel) // Skip

        mv.visitLabel(falseLabel) // falseLabel

        mv.visitInsn(Opcodes.ICONST_0) // Result is false
        mv.visitLabel(endLabel)
    }
}
