package uk.co.nickthecoder.feather.internal.keywords

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.FeatherException
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.booleanPrimitive
import uk.co.nickthecoder.feather.internal.bytePrimitive
import uk.co.nickthecoder.feather.internal.canAutoCast
import uk.co.nickthecoder.feather.internal.charPrimitive
import uk.co.nickthecoder.feather.internal.doublePrimitive
import uk.co.nickthecoder.feather.internal.floatPrimitive
import uk.co.nickthecoder.feather.internal.intPrimitive
import uk.co.nickthecoder.feather.internal.longPrimitive
import uk.co.nickthecoder.feather.internal.resolve
import uk.co.nickthecoder.feather.internal.shortPrimitive
import uk.co.nickthecoder.feather.internal.voidPrimitive
import java.lang.reflect.Type


internal class Return(
    val method: MetaDataMethod,
    val value: StackEntry?,
    val position: FeatherPosition?
) : StackEntry {

    override fun type() = voidPrimitive

    override fun isTerminating() = true

    fun resolvedMethodReturnType() = method.returnType()

    override fun compile(mv: MethodVisitor) {
        mv.addLineNumber(position)
        val methodReturnType = resolvedMethodReturnType()

        // Check the return type
        if (methodReturnType == voidPrimitive) {
            if (value != null) {
                throw FeatherException.returningAValueFromAVoidMethod(position ?: method.position)
            }
        } else {
            if (value == null) {
                throw FeatherException.returningVoidFromNonVoidMethod(position ?: method.position, methodReturnType)
            } else if (!canAutoCast(value.resolvedType(), resolvedMethodReturnType().resolve())) {
                throw FeatherException.incompatibleType(
                    position ?: method.position,
                    resolvedMethodReturnType().resolve(),
                    value.resolvedType()
                )
            }
        }

        value?.compileAutoCast(mv, methodReturnType)
        compileReturn(mv, methodReturnType)
    }

    companion object {
        fun compileReturn(mv: MethodVisitor, type: Type) {
            when (type) {
                voidPrimitive -> mv.visitInsn(Opcodes.RETURN)
                booleanPrimitive -> mv.visitInsn(Opcodes.IRETURN)
                bytePrimitive -> mv.visitInsn(Opcodes.IRETURN)
                charPrimitive -> mv.visitInsn(Opcodes.IRETURN)
                shortPrimitive -> mv.visitInsn(Opcodes.IRETURN)

                intPrimitive -> mv.visitInsn(Opcodes.IRETURN)
                longPrimitive -> mv.visitInsn(Opcodes.LRETURN)
                floatPrimitive -> mv.visitInsn(Opcodes.FRETURN)
                doublePrimitive -> mv.visitInsn(Opcodes.DRETURN)
                else -> mv.visitInsn(Opcodes.ARETURN)
            }
        }
    }
}
