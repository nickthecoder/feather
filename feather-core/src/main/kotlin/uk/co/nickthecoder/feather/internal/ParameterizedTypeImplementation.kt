package uk.co.nickthecoder.feather.internal

import java.lang.reflect.MalformedParameterizedTypeException
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.util.*


/**
 * This is a near identical copy of the implementation in sun.reflect.generics.reflectiveObjects.
 * Needed to re-implement it because the sun package is not a public API.
 */
class ParameterizedTypeImplementation(
    private val rawType: Class<*>,
    private val actualTypeArguments: kotlin.Array<out Type>,
    ownerType: Type? = null
) : ParameterizedType {

    private val ownerType: Type? = ownerType ?: rawType.declaringClass

    init {
        if (actualTypeArguments.size != rawType.typeParameters.size) {
            throw MalformedParameterizedTypeException()
        }
    }

    override fun getActualTypeArguments() = actualTypeArguments

    override fun getRawType() = rawType

    override fun getOwnerType(): Type? = ownerType

    /**
     * Look at the Java implementation, and see how HUGE it is!
     */
    override fun equals(other: Any?) =
            this === other || (other is ParameterizedType &&
                    ownerType == other.ownerType &&
                    rawType == other.rawType &&
                    Arrays.equals(actualTypeArguments, other.actualTypeArguments)
                    )

    override fun hashCode() =
            Arrays.hashCode(actualTypeArguments) xor
                    Objects.hashCode(ownerType) xor
                    Objects.hashCode(rawType)


    override fun toString() =
        if (isFunctionType()) {
            // Special handling for the Function type
            // Output examples: (tnt,String)->char
            // (int) // No return type
            // () // No return type, nor parameters
            val returnType = actualTypeArguments[9].primitive()
            val returnTypeString = if (returnType == voidPrimitive) "" else " -> $returnType"
            var firstVoid = actualTypeArguments.indexOf(voidClass)
            if (firstVoid < 0) firstVoid = 9
            val typeParams = actualTypeArguments.slice(0 until firstVoid).map { it.primitive() }
            typeParams.joinToString(prefix = "(", separator = ", ", postfix = ")$returnTypeString")
        } else {
            (rawType as? Class<*>)?.name + actualTypeArguments
                .joinToString(prefix = "<", separator = ",", postfix = ">") {
                    if (it is Class<*>) it.name else it.toString()
                }
        }
}
