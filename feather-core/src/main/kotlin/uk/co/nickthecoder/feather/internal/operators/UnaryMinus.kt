package uk.co.nickthecoder.feather.internal.operators

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.Source
import uk.co.nickthecoder.feather.internal.StackEntry
import uk.co.nickthecoder.feather.internal.bytePrimitive
import uk.co.nickthecoder.feather.internal.doublePrimitive
import uk.co.nickthecoder.feather.internal.floatPrimitive
import uk.co.nickthecoder.feather.internal.intPrimitive
import uk.co.nickthecoder.feather.internal.longPrimitive
import uk.co.nickthecoder.feather.internal.shortPrimitive


internal class UnaryMinus(
    a: StackEntry,
    source : Source,
    position: FeatherPosition
) : UnaryMathOperator(a, "-", "unaryMinus", source, position) {

    override fun compileApply(mv: MethodVisitor) {
        when (resolvedType()) {
            bytePrimitive, shortPrimitive, intPrimitive -> mv.visitInsn(Opcodes.INEG)
            longPrimitive -> mv.visitInsn(Opcodes.LNEG)
            floatPrimitive -> mv.visitInsn(Opcodes.FNEG)
            doublePrimitive -> mv.visitInsn(Opcodes.DNEG)
            else -> throw unexpectedType()
        }
    }
}
