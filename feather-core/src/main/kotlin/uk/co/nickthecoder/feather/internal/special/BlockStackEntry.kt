package uk.co.nickthecoder.feather.internal.special

import org.objectweb.asm.MethodVisitor
import uk.co.nickthecoder.feather.internal.*
import java.lang.reflect.Type


internal class BlockStackEntry(
    val block: Block,
    val popFinalValue: Boolean = false

) : StackEntry {

    fun isEmpty(): Boolean = block.stack.isEmpty()

    override fun type(): Type {
        val lastEntry = block.stack.lastOrNull()
        return if (popFinalValue || lastEntry == null) {
            voidPrimitive
        } else {
            lastEntry.type()
        }
    }

    override fun isTerminating(): Boolean {
        return block.stack.lastOrNull()?.isTerminating() == true
    }

    /**
     * Iterate over each [StackEntry] in [Block.stack], compiling each in turn.
     * If a [StackEntry] has an (unused) value left of the stack, then we need to pop it.
     * For example, if we call List.remove(0), we often do not use the return result,
     * so it must be removed.
     */
    override fun compile(mv: MethodVisitor) {
        block.initialiseLocalVariableIndices()
        val lastEntry = block.stack.lastOrNull()
        for (entry in block.stack) {
            val popThisValue = (entry !== lastEntry || popFinalValue)
            if (popThisValue && entry is StackEntryWithOptionalResult) {
                entry.compile(mv, false)
            } else {
                entry.compile(mv)
                if (entry !== lastEntry || popFinalValue) {
                    if (!entry.isTerminating()) {
                        popUnusedValue(mv, entry.resolvedType())
                    }
                }
            }
        }
    }

    override fun toString(): String {
        return "BlockStackEntry ${hashCode()} Block ${block.name} : ${block.hashCode()} : [ ${block.stack.joinToString()} ] "
    }
}
