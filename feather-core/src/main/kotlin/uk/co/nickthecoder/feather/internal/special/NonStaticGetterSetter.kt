package uk.co.nickthecoder.feather.internal.special

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.keywords.This
import java.lang.reflect.TypeVariable

/**
 * Allows get/set methods to be seen as a field in feather.
 */
internal class NonStaticGetterSetter(
    val subject: StackEntry,
    val getter: MethodWrapper,
    val setter: MethodWrapper
) : CachedTypeStackEntry(), AssignableStackEntry {

    override fun isFinal() = false

    override fun actualType() =
        resolveGenericType(subject.type(), getter.resolvedReturnType ?: getter.genericReturnType())

    override fun compile(mv: MethodVisitor) {
        val subjectClass = subject.resolvedType()
        val returnType = getter.returnType()

        subject.compile(mv)
        primitiveToObject(mv, subjectClass)

        mv.visitMethodInsn(
            if (getter.isFromInterface()) Opcodes.INVOKEINTERFACE else Opcodes.INVOKEVIRTUAL,
            subjectClass.asmName(),
            getter.name(),
            asmMethodDescription(emptyList(), returnType),
            getter.isFromInterface()
        )
        val grt = getter.genericReturnType()
        if (grt is TypeVariable<*>) {
            mv.visitTypeInsn(Opcodes.CHECKCAST, resolvedType().asmName())
        }
    }

    override fun compileSet(value: StackEntry, mv: MethodVisitor) {
        val subjectClass = subject.resolvedType()

        subject.compile(mv)
        primitiveToObject(mv, subjectClass)
        value.compileAutoCast(mv, getter.returnType())

        mv.visitMethodInsn(
            if (setter.isFromInterface()) Opcodes.INVOKEINTERFACE else Opcodes.INVOKEVIRTUAL,
            subjectClass.asmName(),
            setter.name(),
            asmMethodDescription(listOf(getter.returnType()), voidPrimitive),
            setter.isFromInterface()
        )
    }

    override fun adjust(
        mv: MethodVisitor,
        includeOldValue: Boolean,
        includeNewValue: Boolean,
        adjustment: (MethodVisitor) -> Unit
    ) {
        val subjectClass = subject.resolvedType()
        val itemType = resolvedType()

        subject.compile(mv)
        primitiveToObject(mv, subjectClass)
        if (subject is This) {
            mv.visitVarInsn(Opcodes.ALOAD, 0)
        } else {
            mv.visitInsn(Opcodes.DUP) // The duplicate will be used when calling the setter.
        }

        // Stack : subject, subject

        mv.visitMethodInsn(
            if (getter.isFromInterface()) Opcodes.INVOKEINTERFACE else Opcodes.INVOKEVIRTUAL,
            subjectClass.asmName(),
            getter.name(),
            asmMethodDescription(emptyList(), getter.returnType()),
            getter.isFromInterface()
        )
        // Stack : subject, oldValue

        if (includeOldValue) {
            mv.dupX1(itemType) // The duplicate will be left on the stack (as the result of this StackEntry)
        }

        // Stack : oldValue, subject oldValue

        adjustment(mv) // Stack is now oldValue, subject, newValue

        if (includeNewValue) {
            mv.dup(itemType)
        }

        mv.visitMethodInsn(
            if (setter.isFromInterface()) Opcodes.INVOKEINTERFACE else Opcodes.INVOKEVIRTUAL,
            subjectClass.asmName(),
            setter.name(),
            asmMethodDescription(listOf(getter.returnType()), voidPrimitive),
            setter.isFromInterface()
        )
        // Stack is now oldValue
    }
}