package uk.co.nickthecoder.feather.internal.operators

import org.objectweb.asm.MethodVisitor
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.Source
import uk.co.nickthecoder.feather.internal.StackEntry
import uk.co.nickthecoder.feather.internal.autoCast
import uk.co.nickthecoder.feather.internal.compatiblePrimitiveType
import uk.co.nickthecoder.feather.internal.primitive
import java.lang.reflect.Type


internal abstract class BinaryMathsOperator(
    a: StackEntry,
    b: StackEntry,
    operator: String,
    operatorMethodName: String,
    source: Source,
    position: FeatherPosition
) : BinaryOperator(a, b, operator, operatorMethodName, source, position) {

    override fun standardType(): Type = compatiblePrimitiveType(a.resolvedType(), b.resolvedType())?.primitive()
        ?: throw unexpectedTypes()

    override fun standardCompile(mv: MethodVisitor) {
        val common = resolvedType()

        a.compile(mv)
        autoCast(mv, a.resolvedType(), common.primitive())
        b.compile(mv)
        autoCast(mv, b.resolvedType(), common.primitive())
        compileApply(mv)
    }

    abstract fun compileApply(mv: MethodVisitor)
}
