package uk.co.nickthecoder.feather.internal.operators.comparisons

import org.objectweb.asm.Label
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.internal.isPrimitive
import java.lang.reflect.Type


internal class NotSameInstance : Comparison {
    override fun compile(mv: MethodVisitor, commonType: Type?, aType: Type, bType: Type, notEqualLabel: Label): Boolean {
        return if (commonType != null || aType.isPrimitive() || bType.isPrimitive()) {
            false
        } else {
            mv.visitJumpInsn(Opcodes.IF_ACMPEQ, notEqualLabel)
            true
        }
    }
}
