package uk.co.nickthecoder.feather

import org.antlr.v4.runtime.ParserRuleContext
import uk.co.nickthecoder.feather.internal.*
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

interface SandBox : Cloneable {
    fun isAllowed(klass: Class<*>): Boolean

    public override fun clone(): SandBox
}

/**
 * A Sandbox which denys access to all classes.
 * Feather is intended to be "safe", so the default sandbox is DenyAll, and therefore if the [FeatherCompiler]
 * is incorrectly initialised with an appropriate Sandbox, then safety wins (and the script will fail).
 *
 * This is the default Whitelist, and should be changed to something useful, such as [AllowList].
 */
class DenyAll : SandBox {
    override fun isAllowed(klass: Class<*>) = false

    override fun clone() = DenyAll()
}

class AllowList : SandBox {

    val allowedPackages = mutableSetOf<String>()
    val allowedClasses = mutableSetOf<String>()

    fun allowClassesInPackage(packageName: String, classNames: List<String>) {
        allowedClasses.addAll(classNames.map { "$packageName.$it" })
    }

    /**
     * Do NOT allow everything in java.reflect, because this include Process, System and other dangerous classes!
     * Allows interfaces : Appendable, CharSequence, Comparable, Iterable, Readable, Runnable.
     * Allows classes : Character.Subset, Character.UnicodeBlock, Enum, Math, Number, Object, StrictMath, String, StringBuilder, StringBuffer, Throwable
     * Allows all exceptions in java.util.
     *
     * Also adds the package containing [uk.co.nickthecoder.feather.reflect.System].
     * Consider adding feather's reflect package to [FeatherCompiler.impliedImportPackages] at index 0 (ahead of java.reflect),
     * to hide java.reflect.System and replace it with the safe feather version.
     */
    fun allowSomeOfJavaLang() {
        // Interfaces
        allowClassesInPackage("java.lang", listOf(
                "Appendable",
                "CharSequence", "Comparable",
                "Iterable",
                "Readable", "Runnable"
        ))
        allowedPackages.add("uk.co.nickthecoder.feather.reflect")

        // Classes
        allowClassesInPackage("java.lang", listOf(
                "Character.Subset", "Character.UnicodeBlock",
                "Enum",
                "Math",
                "Number",
                "Object",
                "StrictMath", "String", "StringBuffer", "StringBuilder",
                "Throwable"
        ))
        // Exceptions
        allowClassesInPackage("java.lang", listOf(
                "ArithmeticException",
                "ClassCastException", "ClassNotFoundException", "CloneNotSupportedException",
                "EnumConstantNotPresentException", "Exception",
                "IllegalAccessException", "IllegalArgumentException", "IllegalMonitorStateException",
                "IllegalStateException", "IllegalThreadStateException", "IndexOutOfBoundsException",
                "InstantiationException", "InterruptedException",
                "NegativeArraySizeException", "NoSuchFieldException", "NoSuchMethodException", "NumberFormatException",
                "ReflectiveOperationException", "RuntimeException",
                "SecurityException", "StackTraceElement", "StringIndexOutOfBoundsException",
                "TypeNotPresentException",
                "UnsupportedOperationException"
        ))
    }

    fun allowJavaUtil() {
        allowedPackages.add("java.util")
    }

    fun allowJavaUtilRegex() {
        allowClassesInPackage("java.util.regex", listOf(
                "MatchResult", "Matcher", "Pattern", "PatternSyntaxException"
        ))
    }

    fun allowSomeOfJavaText() {
        allowClassesInPackage("java.text", listOf(
                "DateFormat", "DecimalFormat", "Format", "NumberFormat", "SimpleDateFormat", "ParseException"
        ))
    }

    fun allowJavaMath() {
        allowedPackages.add("java.math")
    }

    /**
     * Allows commonly used safe classes from the standard Java runtime.
     *
     * Calls [allowSomeOfJavaLang], [allowJavaUtil], [allowJavaUtilRegex], [allowSomeOfJavaText], [allowJavaMath].
     */
    fun allowStandardJavaClasses() {
        allowSomeOfJavaLang()
        allowJavaUtil()
        allowJavaUtilRegex()
        allowSomeOfJavaText()
        allowJavaMath()
    }

    fun allowFeatherRuntime() {
        allowedPackages.add("uk.co.nickthecoder.feather.runtime")
    }

    override fun isAllowed(klass: Class<*>): Boolean {
        return allowedClasses.contains(klass.name) ||
                (klass.`package` != null && allowedPackages.contains(klass.`package`.name))
    }

    override fun clone(): AllowList {
        val result = AllowList()

        result.allowedPackages.clear()
        result.allowedPackages.addAll(allowedPackages)

        result.allowedClasses.clear()
        result.allowedClasses.addAll(allowedClasses)

        return result
    }
}

// TODO Use FeatherPosition rather than source and ctx???
internal fun SandBox.checkSandbox(type: Type, source: Source, ctx: ParserRuleContext) {
    if (type.primitive().isPrimitive()) return
    if (type is ImpliedType) return
    if (type is UnresolvedType) return
    if (type is MetaDataType) return
    if (type.isArrayType()) return

    if (type is Class<*>) {
        if (!isAllowed(type)) {
            throw FeatherException.cannotUseType(source, ctx, type)
        }
    } else if (type is ParameterizedType) {
        checkSandbox(type.rawType, source, ctx)
        for (t in type.actualTypeArguments) {
            checkSandbox(t, source, ctx)
        }
    } else {
        throw FeatherException.cannotUseType(source, ctx, type)
    }
}
