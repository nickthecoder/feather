package uk.co.nickthecoder.feather.internal

import uk.co.nickthecoder.feather.NullType
import java.lang.reflect.Type
import java.util.*

// https://stackoverflow.com/questions/9797212/finding-the-nearest-common-superclass-or-superinterface-of-a-collection-of-cla
/*
fun findCommonSuperClasses(vararg classes: Class<*>): List<Class<*>> {

    fun superClasses(clazz: Class<*>): Set<Class<*>> {
        val result: MutableSet<Class<*>> = LinkedHashSet()
        val queue = ArrayDeque<Class<*>>()
        queue.add(clazz)
        if (clazz.isInterface) {
            queue.add(Any::class.java) // optional
        }
        while (!queue.isEmpty()) {
            val c = queue.remove()
            if (result.add(c)) {
                val sup = c.superclass
                if (sup != null) queue.add(sup)
                queue.addAll(c.interfaces)
            }
        }
        return result
    }

    val rollingIntersect = LinkedHashSet(superClasses(classes[0]))
    // intersect with next
    for (i in 1 until classes.size) {
        rollingIntersect.retainAll(superClasses(classes[i]))
    }
    return LinkedList(rollingIntersect)
}

fun findCommonSuperClass(classes: List<Class<*>>) = findCommonSuperClass(classes.toSet())

fun findCommonSuperClass(classes: Set<Class<*>>): Class<*> {
    val allNumbers = classes.firstOrNull { !it.isNumber() } == null

    if (allNumbers) {
        var largestType: Class<*> = bytePrimitive
        for (t in classes) {
            largestType = bestCommonNumberType(largestType, t)!!
        }
        return largestType
    }
    return findCommonSuperClasses(* classes.toTypedArray()).firstOrNull() ?: objectClass
}

fun findCommonSuperClass(a: Class<*>, b: Class<*>): Class<*> {
    if (a == voidPrimitive || b == voidPrimitive) return voidPrimitive

    if (a === b) return a

    if (a.isPrimitive() && b.isPrimitive()) {
        bestCommonNumberType(a, b)?.let {
            return it
        }
    }

    val classA = a.nonPrimitive() as Class<*>
    val classB = b.nonPrimitive() as Class<*>

    if (classA === classB) return classA
    if (classA.isAssignableFrom(classB)) {
        return a
    }
    if (classB.isAssignableFrom(classA)) {
        return classB
    }
    println("End")
    return findCommonSuperClasses(classA, classB).firstOrNull() ?: objectClass
}
*/

// The following are Type versions of the above functions.

fun findCommonSuperTypes(vararg types: Type): List<Type> {

    fun superClasses(type: Type): Set<Type> {
        val result: MutableSet<Type> = LinkedHashSet()
        val queue = ArrayDeque<Type>()
        queue.add(type)
        while (!queue.isEmpty()) {
            val c = queue.remove()
            if (result.add(c)) {
                val sup = c.superType()
                if ( sup != objectClass && sup != null) queue.add(sup)
                queue.addAll(c.interfaces())
            }
        }
        queue.add(Any::class.java) // optional
        return result
    }

    val rollingIntersect = LinkedHashSet(superClasses(types[0]))
    // intersect with next
    for (i in 1 until types.size) {
        rollingIntersect.retainAll(superClasses(types[i]))
    }
    return LinkedList(rollingIntersect)
}

fun findCommonSuperType(types: List<Type>) = findCommonSuperType(types.toSet())

fun findCommonSuperType(types: Set<Type>): Type {
    val allNumbers = types.firstOrNull { !it.isNumber() } == null

    if (allNumbers) {
        var largestType: Class<*> = bytePrimitive
        for (t in types) {
            largestType = bestCommonNumberType(largestType, t)!!
        }
        return largestType
    }
    return findCommonSuperTypes(* types.toTypedArray()).firstOrNull() ?: objectClass

}

fun findCommonSuperType(a: Type, b: Type): Type {

    if (a === b) return a

    if (a == voidPrimitive) return voidPrimitive
    if (b == voidPrimitive) return voidPrimitive

    if (a == NullType.instance) return b.nonPrimitive()
    if (b == NullType.instance) return a.nonPrimitive()

    if (a.isPrimitive() && b.isPrimitive()) {
        bestCommonNumberType(a, b)?.let { return it }
    }

    val classA = a.nonPrimitive()
    val classB = b.nonPrimitive()

    if (classA is Class<*> && classB is Class<*>) {
        if (classA === classB) return classA
        if (classA.isAssignableFrom(classB)) {
            return classA
        }
        if (classB.isAssignableFrom(classA)) {
            return classB
        }
    }

    return findCommonSuperTypes(classA, classB).firstOrNull() ?: objectClass
}
