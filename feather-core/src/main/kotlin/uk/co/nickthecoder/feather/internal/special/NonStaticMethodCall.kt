package uk.co.nickthecoder.feather.internal.special

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.keywords.Super
import java.lang.reflect.Type
import java.lang.reflect.TypeVariable

internal class NonStaticMethodCall(
    val subject: StackEntry,
    val args: List<StackEntry>,
    val typeArgs: List<Type>,
    val method: MethodWrapper
) : CachedTypeStackEntry() {

    override fun actualType(): Type {
        // We need to resolve generic types in two ways...
        // Using the T arguments of the class
        val part1 = resolveGenericType(subject.type(), method.resolvedReturnType ?: method.genericReturnType())
        // And also using the type arguments of the method call
        return resolveGenericType(method, typeArgs, part1)
    }

    override fun compile(mv: MethodVisitor) {
        val subjectType = subject.resolvedType()
        val paramTypes = method.parameterTypes()
        val genericParamTypes = method.genericParameterTypes(subjectType)
        val returnType = method.returnType()

        subject.compile(mv)
        primitiveToObject(mv, subjectType)

        pushArgs(mv, args, genericParamTypes, method.isVarArgs())

        val op = if (subject is Super) {
            Opcodes.INVOKESPECIAL
        } else if (subjectType.isInterface()) { //(method.isFromInterface()) {
            Opcodes.INVOKEINTERFACE
        } else {
            Opcodes.INVOKEVIRTUAL
        }

        val owner = if (subject is Super) {
            subjectType.extends()?.asmName() ?: subjectType.asmName()
        } else {
            subjectType.asmName()
        }
        mv.visitMethodInsn(
            op,
            owner,
            method.name(),
            asmMethodDescription(paramTypes, returnType),
            subjectType.isInterface()//method.isFromInterface()
        )

        val grt = method.genericReturnType()

        if (grt is TypeVariable<*>) {
            mv.visitTypeInsn(Opcodes.CHECKCAST, resolvedType().asmName())
        }
    }
}
