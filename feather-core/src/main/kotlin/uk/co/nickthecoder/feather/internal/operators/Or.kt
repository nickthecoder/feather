package uk.co.nickthecoder.feather.internal.operators

import org.objectweb.asm.Label
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.Source
import uk.co.nickthecoder.feather.internal.autoCast
import uk.co.nickthecoder.feather.internal.booleanPrimitive
import uk.co.nickthecoder.feather.internal.primitive


internal class Or(
    a: StackEntry,
    b: StackEntry,
    source : Source,
    position: FeatherPosition
) : BinaryOperator(a, b, "||", "or", source, position) {

    override fun standardType() = booleanPrimitive

    override fun standardCompile(mv: MethodVisitor) {
        val aType = a.resolvedType().primitive()
        val bType = b.resolvedType().primitive()

        if (aType != booleanPrimitive || bType != booleanPrimitive) throw unexpectedTypes()

        val aIsTrue = Label() //L0
        val bIsFalse = Label()
        val endLabel = Label()

        a.compile(mv)
        autoCast(mv, aType, booleanPrimitive)
        mv.visitJumpInsn(Opcodes.IFNE, aIsTrue) // if a is true

        b.compile(mv)
        autoCast(mv, bType, booleanPrimitive)
        mv.visitJumpInsn(Opcodes.IFEQ, bIsFalse) // if b is false goto false

        mv.visitLabel(aIsTrue)

        mv.visitInsn(Opcodes.ICONST_1)  // result is true
        mv.visitJumpInsn(Opcodes.GOTO, endLabel) // skip

        mv.visitLabel(bIsFalse)

        mv.visitInsn(Opcodes.ICONST_0) // result is false
        mv.visitLabel(endLabel)
    }
}
