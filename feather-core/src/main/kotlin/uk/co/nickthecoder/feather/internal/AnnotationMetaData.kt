package uk.co.nickthecoder.feather.internal

import uk.co.nickthecoder.feather.ConstantStackEntry
import uk.co.nickthecoder.feather.FeatherPosition
import java.lang.annotation.RetentionPolicy
import java.lang.reflect.Type

internal class AnnotationMetaData {

    lateinit var position: FeatherPosition
    lateinit var type: Type
    val valueStackEntries = mutableMapOf<String, ConstantStackEntry>()

    fun isVisibleAtRuntime(): Boolean {
        type.klass().annotations.forEach { an ->
            if (an is java.lang.annotation.Retention) {
                return an.value == RetentionPolicy.RUNTIME
            }
        }
        return false
    }

    override fun toString(): String {
        return "Annotation ${type.typeName} : ${valueStackEntries}"
    }
}
