package uk.co.nickthecoder.feather.internal.operators

import org.objectweb.asm.Label
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.Source
import uk.co.nickthecoder.feather.internal.StackEntry
import uk.co.nickthecoder.feather.internal.autoCast
import uk.co.nickthecoder.feather.internal.booleanPrimitive


internal class Not(
    a: StackEntry,
    source: Source,
    position: FeatherPosition
) : UnaryOperator(a, "!", "not", source, position) {

    override fun standardType() = booleanPrimitive

    override fun standardCompile(mv: MethodVisitor) {
        val type = a.resolvedType()
        a.compile(mv)
        autoCast(mv, a.resolvedType(), booleanPrimitive)

        if (type == booleanPrimitive) {
            val notEqualLabel = Label()
            val endLabel = Label()

            mv.visitJumpInsn(Opcodes.IFNE, notEqualLabel)
            mv.visitInsn(Opcodes.ICONST_1)
            mv.visitJumpInsn(Opcodes.GOTO, endLabel)
            mv.visitLabel(notEqualLabel)
            mv.visitInsn(Opcodes.ICONST_0)
            mv.visitLabel(endLabel)
        } else {
            throw unexpectedType()
        }

    }
}
