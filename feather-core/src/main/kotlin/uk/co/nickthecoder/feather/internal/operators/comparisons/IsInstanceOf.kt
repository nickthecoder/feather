package uk.co.nickthecoder.feather.internal.operators.comparisons

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.ClassConstant
import uk.co.nickthecoder.feather.FeatherException
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.internal.StackEntry
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.booleanPrimitive
import uk.co.nickthecoder.feather.internal.special.Identifier


internal class IsInstanceOf(
    val a: StackEntry,
    val b: StackEntry,
    val position: FeatherPosition

) : StackEntry {

    override fun type() = booleanPrimitive

    override fun compile(mv: MethodVisitor) {

        val aType = a.resolvedType()
        val actualB = if (b is Identifier) b.actual() else b

        if (aType.isPrimitive()) throw FeatherException.expectedAnObject(position, a.resolvedType())
        if (actualB !is ClassConstant) throw FeatherException.expectedClass(position, actualB.resolvedType())

        a.compile(mv)

        mv.visitTypeInsn(Opcodes.INSTANCEOF, actualB.constantValue.asmName())
    }
}
