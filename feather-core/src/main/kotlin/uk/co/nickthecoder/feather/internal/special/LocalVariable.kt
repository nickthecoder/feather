package uk.co.nickthecoder.feather

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.internal.*
import java.lang.reflect.Type


internal data class LocalVariable(
    val name: String,
    private var type: Type,
    val isFinal: Boolean
) {
    var index: Int = 0 // Set to the correct value when the BlockStackEntry is compiled.

    internal var initialised = false

    fun type(): Type =
        if (type is ImpliedType) {
            (type as ImpliedType).calculateType()
        } else {
            type
        }

    /**
     * This was created to help the For loop class update the type of the loop variable iterator.
     * This is DANGEROUS. Call [Block.initialiseLocalVariableIndices] afterwards.
     * Only call this BEFORE compiling the block containing this local variable.
     */
    // For loops used to change the type of the loop variable during the compile phase.
    // But this meant that the loop variable would sometimes be Object, so we would have to cast it manually.
    //fun changeType(type: Type) {
    //    this.type = type
    //}

    fun store(mv: MethodVisitor) {
        val opcode = when (type()) {
            booleanPrimitive, bytePrimitive, charPrimitive, shortPrimitive, intPrimitive -> Opcodes.ISTORE
            longPrimitive -> Opcodes.LSTORE
            floatPrimitive -> Opcodes.FSTORE
            doublePrimitive -> Opcodes.DSTORE
            else -> Opcodes.ASTORE
        }
        mv.visitVarInsn(opcode, index)
    }

    fun load(mv: MethodVisitor) {
        val opcode = when (type()) {
            booleanPrimitive, bytePrimitive, charPrimitive, shortPrimitive, intPrimitive -> Opcodes.ILOAD
            longPrimitive -> Opcodes.LLOAD
            floatPrimitive -> Opcodes.FLOAD
            doublePrimitive -> Opcodes.DLOAD
            else -> Opcodes.ALOAD
        }
        mv.visitVarInsn(opcode, index)
    }
}


internal class LocalVariableStackEntry(
    val localVar: LocalVariable,
    val position: FeatherPosition

) : StackEntry, AssignableStackEntry {

    override fun isFinal() = localVar.isFinal

    override fun type() = localVar.type()

    override fun compile(mv: MethodVisitor) {
        localVar.load(mv)
    }

    override fun adjust(
        mv: MethodVisitor,
        includeOldValue: Boolean,
        includeNewValue: Boolean,
        adjustment: (MethodVisitor) -> Unit
    ) {
        val itemType = resolvedType()
        localVar.load(mv)
        if (includeOldValue) {
            mv.dup(itemType)
        }
        adjustment(mv)
        if (includeNewValue) {
            mv.dup(itemType)
        }
        localVar.store(mv)
    }

    override fun compileSet(value: StackEntry, mv: MethodVisitor) {
        localVar.initialised = true
        val opcode = when (localVar.type()) {
            booleanPrimitive, bytePrimitive, charPrimitive, shortPrimitive, intPrimitive -> Opcodes.ISTORE
            longPrimitive -> Opcodes.LSTORE
            floatPrimitive -> Opcodes.FSTORE
            doublePrimitive -> Opcodes.DSTORE
            else -> Opcodes.ASTORE
        }
        value.compileAutoCast(mv, localVar.type())
        mv.visitVarInsn(opcode, localVar.index)
    }

    override fun toString() = "LocalVariableStackEntry : $localVar"
}
