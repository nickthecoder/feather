package uk.co.nickthecoder.feather.internal.special

import org.objectweb.asm.MethodVisitor
import uk.co.nickthecoder.feather.*
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.keywords.Receiver
import java.lang.reflect.Type

/**
 * When we encounter expressions such as : methodName<typeArg0,typeArg1,...>(arg0, arg1, ...)
 *
 * This could refer to a static method, a static method of the current class,
 * a non-static method from any of the [receivers].
 *
 * We do not know enough information during the first phase parsing of the Feather script,
 * so [actual] is only calculated during the 2nd phase.
 *
 * See also [LazyNonStaticMethodCall] when there is an explicit receiver before the method name.
 *
 * The expression <code>this.foo()</code> will use [LazyNonStaticMethodCall], whereas <code>foo()</code>
 * will use this class, but should result in the same [actual].
 */
internal class LazyMethodCall(
    private val currentClass: MetaDataType,
    private val receivers: List<Receiver>,
    private val block: Block?,
    private val name: String,
    private val args: List<StackEntry>,
    private val unresolvedTypeArgs: List<Type>,
    private val source: Source,
    private val position: FeatherPosition

) : CachedTypeStackEntry() {

    private var actual: StackEntry? = null

    /**
     * The order that foo(...) is considered :
     *
     * * A method of the receiver(s)
     * * An extension function of the receiver(s) (Hmm, not implemented)
     * * A static method of the current class
     * * An extension function of the current class (Hmm, not implemented) (assuming the receiver isn't This)
     * * A constructor call
     * * A static function
     */
    private fun actual(): StackEntry {

        val resolvedTypeArgs = unresolvedTypeArgs.map{ it.resolve() }

        actual?.let { return it }

        // Look for a local variable of type Function.
        block?.findLocalVariable(name)?.let { lv ->
            if (lv.initialised && lv.type().resolve().isFunctionType()) {
                actual = InvokeFunction(LocalVariableStackEntry(lv, position), args, position)
                return actual !!
            }
        }

        // Look for non-static methods in all receivers, as well as fields of type Function
        for (receiver in receivers) {

            findFieldAccess(receiver, name, source, position)?.let { field ->
                if (field.resolvedType().isFunctionType()) {
                    actual = InvokeFunction(field, args, position)
                    return actual!!
                }
            }

            createNonStaticMethodCall(receiver, name, args, resolvedTypeArgs, true, source, position)?.let {
                actual = it
                return it
            }

        }
        // Static methods of the current class
        createStaticMethodCall(currentClass, name, args, resolvedTypeArgs, source, position)?.let {
            actual = it
            return it
        }


        // Look for constructor class (object instantiation).
        val klass = source.worker.resolveClass(source, name)
        if (klass != null) {
            if (klass === arrayClass) {
                if (resolvedTypeArgs.size != 1) {
                    throw FeatherException.expectedOneTypeArgument(position, resolvedTypeArgs)
                } else {
                    return NewArray(resolvedTypeArgs[0], args, position)
                }
            } else {
                createConstructorCall(klass, args, resolvedTypeArgs, position)?.let {
                    actual = it
                    return it
                }
            }
            createConstructorCall(klass, args, resolvedTypeArgs, position)?.let {
                actual = it
                return it
            }
        }
        // Now look for imported static methods.
        source.findStaticAliases(name).forEach { (klass, realName) ->
            createStaticMethodCall(klass, realName, args, resolvedTypeArgs, source, position)?.let {
                actual = it
                return it
            }
        }

        if (name == "arrayOf" && resolvedTypeArgs.size == 1 && !resolvedTypeArgs[0].isPrimitive()) {
            val elementType = resolvedTypeArgs[0]
            var typesMatch = true
            for (arg in args) {
                val argType = arg.type()
                if (!canAutoCast(argType, elementType)) typesMatch = false
            }
            if (typesMatch) {
                return ArrayOf(elementType, args, position)
            }
        }

        throw FeatherException.noSuchMethod(position, currentClass, name, args.map { it.type() })
    }

    override fun actualType() = actual().resolvedType()

    override fun compile(mv: MethodVisitor) {
        mv.addLineNumber(position)
        actual().compile(mv)
    }

    override fun toString() = "LazyMethodCall ${currentClass.name}.$name"
}
