package uk.co.nickthecoder.feather.internal.operators.comparisons

import org.objectweb.asm.Label
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.internal.bytePrimitive
import uk.co.nickthecoder.feather.internal.charPrimitive
import uk.co.nickthecoder.feather.internal.doublePrimitive
import uk.co.nickthecoder.feather.internal.floatPrimitive
import uk.co.nickthecoder.feather.internal.intPrimitive
import uk.co.nickthecoder.feather.internal.longPrimitive
import uk.co.nickthecoder.feather.internal.shortPrimitive
import java.lang.reflect.Type


internal class LessThanOrEqual : Comparison {
    override fun compile(mv: MethodVisitor, commonType: Type?, aType: Type, bType: Type, notEqualLabel: Label): Boolean {

        return when (commonType) {
            bytePrimitive, charPrimitive, shortPrimitive, intPrimitive -> {
                mv.visitJumpInsn(Opcodes.IF_ICMPGT, notEqualLabel)
                true
            }
            longPrimitive -> {
                mv.visitInsn(Opcodes.LCMP)
                mv.visitJumpInsn(Opcodes.IFGT, notEqualLabel)
                true
            }
            floatPrimitive -> {
                mv.visitInsn(Opcodes.FCMPL)
                mv.visitJumpInsn(Opcodes.IFGT, notEqualLabel)
                true
            }
            doublePrimitive -> {
                mv.visitInsn(Opcodes.DCMPL)
                mv.visitJumpInsn(Opcodes.IFGT, notEqualLabel)
                true
            }
            else -> {
                compareToComparison(mv, Opcodes.IFGT, aType, bType, notEqualLabel)
            }
        }
    }
}
