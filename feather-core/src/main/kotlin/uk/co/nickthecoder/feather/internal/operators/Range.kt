package uk.co.nickthecoder.feather.internal.operators

import org.objectweb.asm.MethodVisitor
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.Source
import uk.co.nickthecoder.feather.internal.StackEntry

internal class Range(
    a: StackEntry,
    b: StackEntry,
    source: Source,
    position: FeatherPosition

) : BinaryOperator(a, b, "..", "rangeTo", source, position) {

    override fun standardType() = throw unexpectedTypes()

    override fun standardCompile(mv: MethodVisitor) {
        throw unexpectedTypes()
    }

}
