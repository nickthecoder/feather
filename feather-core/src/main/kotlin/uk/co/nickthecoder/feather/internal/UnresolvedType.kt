package uk.co.nickthecoder.feather.internal

import uk.co.nickthecoder.feather.Source
import java.lang.reflect.Type

internal interface UnresolvedType : Type {

    fun resolve(): Type
}


/**
 * Used when an unqualified class name has been specified, but we cannot find it, possibly because
 * the class/interface hasn't been parsed yet (i.e. it is further down the file,
 * or in another file, which hasn't been parsed yet).
 *
 * This not the same as an [ImpliedType] (which is a type of a field/local variable,
 * whose type wasn't specified).
 *
 * [source] is used to resolve this type (using [Source]'s info about import statements).
 */
internal class UnqualifiedUnresolvedType(
    val simpleName: String,
    val source: Source
) : UnresolvedType {

    override fun getTypeName() = simpleName

    override fun resolve() = source.worker.resolveClass(source, simpleName)
        ?: throw UnresolvedClassException(simpleName)

    override fun toString() = "UnqualifiedUnresolvedType : $simpleName"
}

/**
 * Used when an qualified class name has been specified, but we cannot find it, possibly because
 * the class/interface hasn't been parsed yet (i.e. it is further down the file,
 * or in another file, which hasn't been parsed yet).
 *
 * This not the same as an [ImpliedType] (which is a type of a field/local variable,
 * whose type wasn't specified).
 *
 * [source] is used to resolve this type (using [Source]'s info about import statements).
 */
internal class QualifiedUnresolvedType(
    val name: String,
    val source: Source
) : UnresolvedType {

    override fun getTypeName() = name

    override fun resolve() = source.worker.metaDataMap[name]
        ?: throw UnresolvedClassException(name)

    override fun toString() = "QualifiedUnresolvedType : $name"

}

/**
 * A Type, which isn't yet known, but in the second Phase [stackEntry] will have enough information,
 * and we can the resolve the actual type.
 */
internal class UnresolvedTypeFromStackEntry( val stackEntry : StackEntry ) : UnresolvedType {
    override fun resolve() = stackEntry.resolvedType()
}

class UnresolvedClassException(className: String) : Exception("Could not resolve class $className")