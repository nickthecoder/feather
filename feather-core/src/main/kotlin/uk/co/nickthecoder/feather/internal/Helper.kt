package uk.co.nickthecoder.feather.internal

import uk.co.nickthecoder.feather.internal.keywords.Super
import uk.co.nickthecoder.feather.internal.keywords.This
import uk.co.nickthecoder.feather.internal.special.ConstructorWrapper
import uk.co.nickthecoder.feather.internal.special.FieldWrapper
import uk.co.nickthecoder.feather.internal.special.MethodWrapper
import java.lang.reflect.*
import java.util.*

internal fun <T> Stack<T>.safePeek(): T? = if (empty()) null else peek()


/**
 * Returns 0 for a perfect fit, [Int.MAX_VALUE] for incompatible.
 */
internal fun argCompatibilityScore(callingTypes: List<Type>, methodTypes: List<Type>, isVarArgs: Boolean): Int {
    if (isVarArgs) {

        if (callingTypes.size < methodTypes.size - 1) return Int.MAX_VALUE
        var score = 1

        // Score the "regular" parameters
        for (i in 0 until methodTypes.size - 1) {
            if (callingTypes[i] != methodTypes[i]) {
                if (canAutoCast(callingTypes[i], methodTypes[i])) {
                    score += 1
                } else {
                    return Int.MAX_VALUE
                }
            }
        }
        // Now score the vararg parameters
        val varArgType = methodTypes.last().shallowestElementTypeOfArray()
        for (i in methodTypes.size - 1 until callingTypes.size) {
            if (callingTypes[i] != varArgType) {
                if (canAutoCast(callingTypes[i], varArgType)) {
                    score += 1
                } else {
                    return Int.MAX_VALUE
                }
            }
        }
        return score

    } else {

        if (callingTypes.size != methodTypes.size) return Int.MAX_VALUE
        var score = 0

        for (i in 0 until callingTypes.size) {
            if (callingTypes[i] != methodTypes[i]) {
                val a = callingTypes[i]
                val b = methodTypes[i]
                if (canAutoCast(a, b)) {
                    score += 1
                } else {
                    return Int.MAX_VALUE
                }
            }
        }
        return score
    }
}


/**
 * Unescapes the insides of string literals.
 */
internal fun unescapeString(str: String): String {
    return if (str.contains("\\")) {
        val builder = StringBuilder(str.length)
        var escape = false
        for (c in str) {
            if (escape) {
                builder.append(
                    when (c) {
                        'n' -> '\n'
                        't' -> '\t'
                        else -> c
                    }
                )
                escape = false
            } else {
                if (c == '\\') {
                    escape = true
                } else {
                    builder.append(c)
                }
            }
        }
        builder.toString()
    } else {
        str
    }
}


internal fun Type.findField(name: String): FieldWrapper? {
    return when (this) {
        is MetaDataType -> findField(name)
        is UnresolvedType -> resolve().findField(name)
        else ->
            try {
                this.klass().getField(name).let { FieldWrapper(it) }
            } catch (e: Exception) {
                null
            }
    }
}


/**
 * Checks if the method/field is accessible.
 * Currently, we can call private, package and protected fields/methods from the
 * owner class AND ALSO from sub-classes. // TODO This is too permissive.
 * We shouldn't be able to call private methods from sub-classes.
 * NOTE, to implement this correctly, we will also need the "owner" of the field/method.
 *
 * Calling private/protected OR package methods from a 3rd party class is not allowed.
 * NOTE, feather has no concept of package level protection, so even though it may seem
 * that this is too restrictive, it really isn't.
 */
internal fun scopeOkFrom(subject: StackEntry, modifiers: Int): Boolean {
    if (subject is This) return true
    if (subject is Super) return true

    return (Modifier.isPublic(modifiers))
}

internal fun Type.allMethods(): List<MethodWrapper> {
    val result = mutableListOf<MethodWrapper>()

    allMethods(result)
    return result
}

private fun Type.allMethods(into: MutableList<MethodWrapper>) {
    if (this is MetaDataType) {
        into.addAll(this.methods.map { MethodWrapper(it) })
    } else if (this is Class<*>) {
        into.addAll(this.declaredMethods.map { MethodWrapper(it) })
    }
    for (parent in this.interfaces()) {
        parent.allMethods(into)
    }
    superType()?.allMethods(into)
}

internal fun Type.findMethod(
    name: String,
    unresolvedTypeArgs: List<Type>,
    unresolvedArgTypes: List<Type>
): MethodWrapper? {

    val argTypes = unresolvedArgTypes.map { it.resolve() }
    val resolvedTypeArgs = unresolvedTypeArgs.map{ it.resolve() }

    val best = this.getMethods(name, argTypes.size).map { method ->
        val genericArgTypes = if (resolvedTypeArgs.isEmpty()) {
            argTypes
        } else {
            argTypes.map { resolveGenericType(method, resolvedTypeArgs, it) }
        }
        val genericParameterTypes = if (resolvedTypeArgs.isEmpty()) {
            method.genericParameterTypes().map { resolveGenericType(this, it) }
        } else {
            method.genericParameterTypes().map { resolveGenericType(method, resolvedTypeArgs, it) }
        }
        val score = argCompatibilityScore(genericArgTypes, genericParameterTypes, method.isVarArgs())
        Pair(method, score)

    }.minByOrNull { it.second }

    return if ( best?.second == Int.MAX_VALUE ) {
        null
    } else {
        best?.first
    }
}

internal fun Type.findConstructor(argTypes: List<Type>): ConstructorWrapper? {
    return if (this is MetaDataType) {
        this.findConstructor(argTypes)?.let { ConstructorWrapper(it) }
    } else {
        this.findJavaConstructor(argTypes)?.let { ConstructorWrapper(it) }
    }
}

// TODO Should we be only looking at pubic constructors?
internal fun Type.findJavaConstructor(paramTypes: List<Type>): Constructor<*>? {
    if (this !is Class<*>) return null

    return this.constructors.filter { it.parameterCount == paramTypes.size || it.isVarArgs }.map {
        Pair(it, argCompatibilityScore(paramTypes, it.parameterTypes.map { it }, it.isVarArgs))
    }.filter { it.second != Int.MAX_VALUE }.sortedBy { it.second }.firstOrNull()?.first
}


/**
 * Returns the generic type of Type [toResolve], which may be a TypeVariable, or a ParametrizedType containing
 * TypeVariables.
 *
 * Also, JVM Arrays are converted to ParameterizedTypes of uk.co.nickthecoder.feather.internal.Array.
 * This is required, because it is impossible to create the Type for a JVM array, which has an elementType
 * of type MetaData (i.e. an as-yet un-compiled Feather class).
 * Therefore *all* array types are passed around as Type Array<T>.
 * This has a follow-on problem, that method calls, and constructor calls and field accessors in Java Classes must
 * also convert the parameters' Types to Array<T>, so that the equality of parameter types still work.
 *
 * As varargs are also JVM arrays, special care is needed for these too.
 */
fun resolveGenericType(source: Type, toResolve: Type): Type {
    return when (toResolve) {
        is Class<*> -> if (toResolve.isArray) {
            arrayType(toResolve.componentType)
        } else {
            toResolve
        }

        is MetaDataType -> toResolve
        is ParameterizedType -> {
            // Does the ParameterizedType only contain Class types?
            if (toResolve.actualTypeArguments.firstOrNull { it !is Class<*> } == null) {
                toResolve
            } else {

                // We need to resolve the non-class types (as they may contain TypeVariables.)
                ParameterizedTypeImplementation(
                    toResolve.rawType as Class<*>,
                    toResolve.actualTypeArguments.map { resolveGenericType(source, it) }.toTypedArray()
                )

            }
        }
        is TypeVariable<*> -> {
            if (source is ParameterizedType) {
                // I'm not sure if this is correct, but it works for the limited number of tests so far.
                val sourceClass = source.rawType as Class<*>
                val index = sourceClass.typeParameters.indexOfFirst { it.name == toResolve.name }
                if (source.actualTypeArguments.size > index && index >= 0) {
                    source.actualTypeArguments[index]
                } else {
                    toResolve
                }
            } else {
                toResolve
            }
        }
        else -> toResolve
    }
}

/**
 * Returns the generic type of Type [toResolve], which may be a TypeVariable, or a ParametrizedType containing
 * TypeVariables.
 *
 * Also, JVM Arrays are converted to ParameterizedTypes of uk.co.nickthecoder.feather.internal.Array.
 * This is required, because it is impossible to create the Type for a JVM array, which has an elementType
 * of type MetaData (i.e. an as-yet un-compiled Feather class).
 * Therefore *all* array types are passed around as Type Array<T>.
 * This has a follow-on problem, that method calls, and constructor calls and field accessors in Java Classes must
 * also convert the parameters' Types to Array<T>, so that the equality of parameter types still work.
 *
 * As varargs are also JVM arrays, special care is needed for these too.
 */
internal fun resolveGenericType(method: MethodWrapper, typeArgs: List<Type>, toResolve: Type): Type {
    val result = when (toResolve) {
        is Class<*> -> {
            if (toResolve.isArray) {
                arrayType(toResolve.componentType)
            } else {
                toResolve
            }
        }
        is UnresolvedType -> toResolve.resolve()
        is MetaDataType -> toResolve
        is GenericArrayType -> arrayType(resolveGenericType(method, typeArgs, toResolve.genericComponentType))
        is TypeVariable<*> -> {
            // I'm not sure if this is correct, but it works for the limited number of tests so far.
            val typeName = toResolve.name
            val index = method.typeParameters().indexOfFirst { it.name == typeName }
            if (index >= 0 && typeArgs.size > index) {
                typeArgs[index]
            } else {
                toResolve
            }
        }
        is WildcardType -> {
            val upper = toResolve.upperBounds.firstOrNull() ?: toResolve
            resolveGenericType(method,typeArgs,upper)
        }
        is ParameterizedType -> {
            // Does the ParameterizedType only contain Class types?
            if (toResolve.actualTypeArguments.firstOrNull { it !is Class<*> } == null) {
                toResolve
            } else {

                // We need to resolve the non-class types (as they may contain TypeVariables.)
                ParameterizedTypeImplementation(
                    toResolve.rawType as Class<*>,
                    toResolve.actualTypeArguments.map {
                        resolveGenericType(method, typeArgs, it.resolve())
                    }.toTypedArray()
                )
            }
        }
        else -> toResolve
    }
    // println( "*** Resolved generic type $result : ${result.javaClass}" )
    return result
}


/**
 * TODO Not checking if the permissions are ok (private, protected etc).
 * TODO To do this efficiently I think we need to cache the data, as we need to get all *declared* methods of this class
 * as well as all of the super classes.
 */
internal fun Type.getMethods(name: String, argCount: Int): List<MethodWrapper> {
    val foundJavaMethods = mutableSetOf<Method>()
    val foundFeatherMethods = mutableSetOf<MetaDataMethod>()
    val result = mutableListOf<MethodWrapper>()

    /*
     * Note, it is possible for a method to be found twice, and we want the most specific version,
     * so ignore the super class versions.
     */
    fun alreadyGotIt(m: MethodWrapper): Boolean {
        val myParamTypes = m.parameterTypes()

        for (existing in result) {
            fun same(): Boolean {
                existing.parameterTypes().forEachIndexed { i, type ->
                    if (type !== myParamTypes[i]) return false
                }
                return true
            }
            if (same()) return true
        }
        return false
    }

    fun getFromOne(one: Type) {
        if (one is UnresolvedType) return getFromOne(one.resolve())

        if (one is MetaDataType) {
            one.methods.filter {
                it.name == name &&
                        (it.isVarArgs || it.parameterTypes.size == argCount) &&
                        !foundFeatherMethods.contains(it)
            }.forEach { method ->
                val m = MethodWrapper(method)
                if (!alreadyGotIt(m)) {
                    foundFeatherMethods.add(method)
                    result.add(m)
                }
            }
        } else {
            one.klass().declaredMethods.filter {
                it.name == name &&
                        (it.isVarArgs || it.parameterCount == argCount) &&
                        !foundJavaMethods.contains(it)
            }.forEach { method ->
                val resolvedReturnType = resolveGenericType(one, method.genericReturnType)
                val m = MethodWrapper(method, resolvedReturnType)
                if (!alreadyGotIt(m)) {
                    foundJavaMethods.add(method)
                    result.add(m)
                }
            }
        }
    }

    this.superTypes(true).forEach {
        getFromOne(it)
    }
    return result

}

/**
 * TODO Not checking if the permissions are ok (private, protected etc).
 * TODO To do this efficiently I think we need to cache the data, as we need to get all *declared* methods of this class
 * as well as all of the super classes.
 */
internal fun Type.getMethods(name: String): List<MethodWrapper> {
    val foundJavaMethods = mutableSetOf<Method>()
    val foundFeatherMethods = mutableSetOf<MetaDataMethod>()
    val result = mutableListOf<MethodWrapper>()

    /*
     * Note, it is possible for a method to be found twice, and we want the most specific version,
     * so ignore the super class versions.
     */
    fun alreadyGotIt(m: MethodWrapper): Boolean {
        val myParamTypes = m.parameterTypes()

        for (existing in result) {
            fun same(): Boolean {
                existing.parameterTypes().forEachIndexed { i, type ->
                    if (type !== myParamTypes[i]) return false
                }
                return true
            }
            if (same()) return true
        }
        return false
    }

    fun getFromOne(one: Type) {
        if (one is UnresolvedType) return getFromOne(one.resolve())

        if (one is MetaDataType) {
            one.methods.filter {
                it.name == name && !foundFeatherMethods.contains(it)
            }.forEach { method ->
                val m = MethodWrapper(method)
                if (!alreadyGotIt(m)) {
                    foundFeatherMethods.add(method)
                    result.add(m)
                }
            }
        } else {
            one.klass().declaredMethods.filter {
                it.name == name && !foundJavaMethods.contains(it)
            }.forEach { method ->
                val resolvedReturnType = resolveGenericType(one, method.genericReturnType)
                val m = MethodWrapper(method, resolvedReturnType)
                if (!alreadyGotIt(m)) {
                    foundJavaMethods.add(method)
                    result.add(m)
                }
            }
        }
    }

    this.superTypes(true).forEach {
        getFromOne(it)
    }
    return result

}