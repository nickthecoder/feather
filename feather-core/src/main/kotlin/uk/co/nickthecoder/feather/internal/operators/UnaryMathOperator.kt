package uk.co.nickthecoder.feather.internal.operators

import org.objectweb.asm.MethodVisitor
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.Source
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.bytePrimitive
import uk.co.nickthecoder.feather.internal.doublePrimitive
import uk.co.nickthecoder.feather.internal.floatPrimitive
import uk.co.nickthecoder.feather.internal.intPrimitive
import uk.co.nickthecoder.feather.internal.longPrimitive
import uk.co.nickthecoder.feather.internal.shortPrimitive
import java.lang.reflect.Type


internal abstract class UnaryMathOperator(
    a: StackEntry,
    operator: String,
    operatorMethodName : String?,
    source : Source,
    position: FeatherPosition
) : UnaryOperator(a, operator, operatorMethodName, source, position) {

    override fun standardType(): Type {
        val prim = a.type().primitive()
        if (prim in listOf(bytePrimitive, shortPrimitive, intPrimitive, longPrimitive, floatPrimitive, doublePrimitive)) {
            return prim
        } else {
            throw unexpectedType()
        }
    }

    override fun standardCompile(mv: MethodVisitor) {
        val type = resolvedType()
        a.compile(mv)
        autoCast(mv, a.resolvedType(), type)
        compileApply(mv)
    }

    abstract fun compileApply(mv: MethodVisitor)

}
