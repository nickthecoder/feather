package uk.co.nickthecoder.feather.internal.special

import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.internal.MetaDataConstructor
import uk.co.nickthecoder.feather.internal.StackEntry
import uk.co.nickthecoder.feather.internal.findConstructor
import java.lang.reflect.Constructor
import java.lang.reflect.Type

/**
 * Allows us to treat a java.lang.reflect.Constructor, and a MetaDataConstructor as the same Type.
 * This allows code which requires a Constructor to work for both types.
 * (e.g. in ConstructorCall)
 */
internal class ConstructorWrapper private constructor(
    val java: Constructor<*>?,
    val feather: MetaDataConstructor?
) {
    constructor(constructor: Constructor<*>) : this(constructor, null)
    constructor(constructor: MetaDataConstructor) : this(null, constructor)

    fun parameterTypes(): List<Type> = java?.parameterTypes?.toList() ?: feather!!.parameterTypes

    fun isVarArgs() = java?.isVarArgs ?: feather!!.isVarArgs

    fun owner(): Type = if (java == null) feather!!.owner else java.declaringClass

    override fun toString() = java?.toString() ?: feather!!.toString()
}

internal fun createConstructorCall(klass: Type, args: List<StackEntry>, typeArgs: List<Type>, position: FeatherPosition): StackEntry? {
    val argTypes = args.map { it.resolvedType() }
    klass.findConstructor(argTypes)?.let {
        return ConstructorCall(it, args, typeArgs, position)
    }
    return null
}

