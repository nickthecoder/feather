package uk.co.nickthecoder.feather.internal.operators

import org.objectweb.asm.MethodVisitor
import uk.co.nickthecoder.feather.*
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.Source
import uk.co.nickthecoder.feather.internal.StackEntry
import uk.co.nickthecoder.feather.internal.special.createNonStaticMethodCall
import java.lang.reflect.Type


internal abstract class UnaryOperator(
    val a: StackEntry,
    val operator: String,
    val operatorMethodName : String?,
    val source : Source,
    val position: FeatherPosition

) : CachedTypeStackEntry() {

    override fun actualType(): Type {
        return if (overloadedMethodCall == null) {
            standardType()
        } else {
            overloadedMethodCall!!.type()
        }
    }

    abstract fun standardType(): Type


    override fun compile(mv: MethodVisitor) {
        mv.addLineNumber(position)
        overloadedMethodCall?.let {
            it.compile(mv)
            return
        }

        standardCompile(mv)
    }
    abstract fun standardCompile(mv: MethodVisitor)

    protected val overloadedMethodCall by lazy {
        if (operatorMethodName == null) {
            null
        } else {
            try {
                createNonStaticMethodCall(a, operatorMethodName, emptyList(), emptyList(), false, source, position)
            } catch (e: Exception) {
                null
            }
        }
    }

    protected fun unexpectedType() = FeatherException.unexpectedType(position, operator, a)

}
