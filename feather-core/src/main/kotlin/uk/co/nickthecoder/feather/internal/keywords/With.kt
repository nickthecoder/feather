package uk.co.nickthecoder.feather.internal.keywords

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.internal.Block
import uk.co.nickthecoder.feather.internal.ReceiverStackEntry
import uk.co.nickthecoder.feather.internal.StackEntry
import uk.co.nickthecoder.feather.internal.UnresolvedTypeFromStackEntry

/**
 * The implementation of expressions such as :
 *
 *      with( foo.myThing ) {
 *          prop = 1
 *      }
 *
 * The field `prop`may refer to a field of `foo.myThing`.
 *
 * How it works: [a] is the expression in the parentheses.
 * We store that value as a JVM local variable of the inner-[block].
 * CompilerWorker, then puts [receiver] at the top of the receiver stack, so that any field/method reference
 * within the inner-[block] are first checked against [a] (and the through the rest of the receivers in turn)
 */
internal class With(
    val a : StackEntry,
    val block : Block

) : ReceiverStackEntry {

    override val receiver = LocalVariableReceiver(block.addVariable("this@with", UnresolvedTypeFromStackEntry(a), true))

    override fun type() = block.type()

    override fun isTerminating() = block.isTerminating()

    override fun compile(mv: MethodVisitor) {
        block.initialiseLocalVariableIndices()
        a.compile(mv)
        mv.visitInsn(Opcodes.DUP) // The return value of the apply statement.
        receiver.localVariable.store(mv)

        block.stackEntry(false).compile(mv)
    }
}
