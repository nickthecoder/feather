package uk.co.nickthecoder.feather.internal.keywords

import org.objectweb.asm.Label
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes.*
import uk.co.nickthecoder.feather.*
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.Block
import uk.co.nickthecoder.feather.internal.doublePrimitive
import uk.co.nickthecoder.feather.internal.longPrimitive
import uk.co.nickthecoder.feather.internal.special.BlockStackEntry
import uk.co.nickthecoder.feather.internal.voidPrimitive
import java.lang.reflect.Type

internal class CatchSection(
        val variableName: String,
        val exceptionType: Type,
        val blockStackEntry: BlockStackEntry
) {
    var catchStart = Label()
    val catchEnd = Label()
}

internal class TryCatchFinally(
    private val parentBlock: Block,
    private val tryBlock: StackEntry,
    private val catchSections: List<CatchSection>,
    private val finallyBlock: StackEntry?
) : StackEntry {

    override fun type() : Type {
        val tryType = tryBlock.type()
        if (tryType === voidPrimitive || catchSections.firstOrNull { it.blockStackEntry.type() === uk.co.nickthecoder.feather.internal.voidPrimitive } != null) return voidPrimitive
        val types = mutableListOf<Type>()
        types.add(tryType)
        types.addAll(catchSections.map { it.blockStackEntry.type() })
        return findCommonSuperType(types)
    }

    override fun isTerminating(): Boolean {
        if (finallyBlock?.isTerminating() == true) return true
        return tryBlock.isTerminating() &&
                catchSections.firstOrNull { !it.blockStackEntry.isTerminating() } == null
    }

    override fun compile(mv: MethodVisitor) {

        val tryStart = Label()
        val tryEnd = Label()
        val finallyStart = Label()
        val veryEnd = Label()

        val hasFinally = finallyBlock != null

        val returnType = resolvedType()

        // Create all of the TRYCATCHBLOCK for the try block
        for (catchSection in catchSections) {
            mv.visitTryCatchBlock(tryStart, tryEnd, catchSection.catchStart, catchSection.exceptionType.asmName())
        }
        if (hasFinally) {
            // Create TRYCATCHBLOCK for all other exception types not caught which are thrown within the try block
            mv.visitTryCatchBlock(tryStart, tryEnd, finallyStart, null)

            // Create TRYCATCHBLOCK for exceptions thrown within the catch blocks
            for (catchSection in catchSections) {
                mv.visitTryCatchBlock(catchSection.catchStart, catchSection.catchEnd, finallyStart, null)
            }
        }

        var tempResult: LocalVariable? = null
        if (returnType !== voidPrimitive && hasFinally) {
            val tempName = if (returnType === doublePrimitive || returnType == longPrimitive) ".tempVariable2" else ".tempVariable"

            tempResult = parentBlock.findLocalVariable(tempName)
            if (tempResult == null) {
                tempResult = parentBlock.addVariable(tempName, returnType, false)
                parentBlock.initialiseLocalVariableIndices()
            }
        }

        fun endOfSection(label: Label, sectionType: Type) {
            if (returnType === voidPrimitive) {
                popUnusedValue(mv, sectionType)
            } else {
                autoCast(mv, sectionType, returnType)
                if (hasFinally) {
                    tempResult?.store(mv)
                }
            }
            mv.visitLabel(label)
            finallyBlock?.compile(mv)

            if (returnType !== voidPrimitive && hasFinally) {
                tempResult?.load(mv)
            }
        }

        // Now compile the code for each section

        // Try block
        mv.visitLabel(tryStart)
        tryBlock.compile(mv)
        endOfSection(tryEnd, tryBlock.resolvedType())
        mv.visitJumpInsn(GOTO, veryEnd)

        // Ok, now for each catch section...
        for (catchSection in catchSections) {
            // As we haven't compiled the catch block yet, the index of catchBlock.localVariable hasn't been set yet.
            catchSection.blockStackEntry.block.initialiseLocalVariableIndices()
            val exceptionVariable = catchSection.blockStackEntry.block.findLocalVariable(catchSection.variableName)!!

            mv.visitLabel(catchSection.catchStart)
            // The JVMS's error handler pushes the exception onto the stack, which we store into the local variable.
            mv.visitVarInsn(ASTORE, exceptionVariable.index)

            catchSection.blockStackEntry.compile(mv)

            endOfSection(catchSection.catchEnd, catchSection.blockStackEntry.resolvedType())

            // We don't need to jump to the end if this IS the end.
            if (catchSection != catchSections.last() || hasFinally) {
                mv.visitJumpInsn(GOTO, veryEnd)
            }
        }

        // Now create a finally block for any exceptions thrown within the catch blocks.
        mv.visitLabel(finallyStart)
        if (hasFinally) {
            var finalExceptionLocalVariable = parentBlock.findLocalVariable(".final_exception")
            if (finalExceptionLocalVariable == null) {
                finalExceptionLocalVariable = parentBlock.addVariable(".final_exception", Throwable::class.java, true)
                parentBlock.initialiseLocalVariableIndices()
            }

            // Store the exception in a local variable (It will be used to re-throw in a moment)
            mv.visitVarInsn(ASTORE, finalExceptionLocalVariable.index)
            finallyBlock?.compile(mv)
            // Re-throw the exception
            mv.visitVarInsn(ALOAD, finalExceptionLocalVariable.index)
            mv.visitInsn(ATHROW)
        }
        mv.visitLabel(veryEnd)

    }

}
