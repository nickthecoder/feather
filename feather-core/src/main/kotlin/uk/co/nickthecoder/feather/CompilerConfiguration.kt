package uk.co.nickthecoder.feather

import uk.co.nickthecoder.feather.internal.doubleClass
import uk.co.nickthecoder.feather.internal.floatClass

enum class RealType(val klass: Class<*>) {
    DOUBLE(doubleClass),
    FLOAT(floatClass)
}

class CompilerConfiguration : Cloneable {

    /**
     * Decides which classes scripts are allowed to use.
     * The default is [AllowList] with [AllowList.allowStandardJavaClasses] applied.
     */
    var sandBox: SandBox = AllowList().apply {
        allowStandardJavaClasses()
    }

    /**
     * Are operating system commands allowed?
     * Only set this to true in a trusted environment. Never set it to true when Feather is used as
     * a macro/extensions language within an application, where the script writers may be malicious!
     */
    var allowCommands = false

    /**
     * If you want errors to be reported during the compilation (rather than all at once at the end),
     * then set this to an error handler of your choosing.
     */
    var errorHandler: (FeatherPosition, String) -> Unit = { _, _ ->
        //System.err.println("$pos\n    $message")
    }

    /**
     * If set to true, then all loops will automatically check Thread.interrupted, and throw
     * InterruptedException iff true.
     * This is useful when feather is used as a scripting language, and you want the ability to
     * stop the script cleanly.
     * Run the script in a new Thread, then call that thread's interrupt() method to stop the script.
     */
    var throwWhenInterrupted = false

    /**
     * Which packages are automatically imported.
     * The default mimics Java, importing just java.reflect.
     *
     * To save your programmers extra cruft, consider adding more packages specific to your application.
     *
     * The following achieve the same results :
     *
     *      impliedImportPackages.add( "my.package" )
     *
     *      import my.package.*
     */
    val impliedImportPackages = mutableListOf<String>()

    /**
     * Similar to [impliedImportPackages], but for individual classes.
     * The key is the name of the class as it will be referred to in the feather code, and the value
     * is the actual Class.
     *
     * The default is empty.
     *
     * The following achieve the same results :
     *
     *      impliedImportClasses["MyClass"] = MyClass::class.java
     *
     *      import my.package.MyClass
     */
    val impliedImportClasses = mutableMapOf<String, Class<*>>(
        "Appendable" to Appendable::class.java,
        "AutoCloseable" to AutoCloseable::class.java,
        "CharSequence" to CharSequence::class.java,
        "Cloneable" to Cloneable::class.java,
        "Comparable" to Comparable::class.java,
        "Iterable" to Iterable::class.java,
        "Readable" to Readable::class.java,
        "Runnable" to Runnable::class.java,

        "Class" to Class::class.java,
        "Math" to Math::class.java,
        "Object" to Object::class.java,
        "String" to String::class.java,
        "StringBuffer" to StringBuffer::class.java,
        "StringBuilder" to StringBuilder::class.java,

        "Exception" to Exception::class.java,
        "RuntimeException" to RuntimeException::class.java,
        "Throwable" to Throwable::class.java
    )

    /**
     * Names of static fields/methods which are automatically imported.
     * The value is a [Pair] of the class name and the field/method name.
     *
     * The following achieve the same results :
     *
     *      impliedImportStaticNames["foo"] = Pair("my.package.MyClass", "foo")
     *
     *      import static my.package.MyClass.foo
     */
    val impliedStaticImports = mutableMapOf<String, Pair<String, String>>()

    /**
     * The following achieve similar results :
     *
     *      impliedImportStaticClasses.add( "my.package.MyClass" )
     *
     *      import static my.package.MyClass.*
     */
    val impliedImportStaticClasses = mutableListOf<String>()

    /**
     * The java version for the compiled byte code. The default is Java 11, however, the default may change in
     * later versions of Feather. I'm not sure what the lowest possible version number is, but do NOT go below 5,
     * as we do need generics!
     */
    var javaVersion = 11

    /**
     * Include debug symbols
     */
    var debug = true

    /**
     * If set to true, then a static main method with no arguments is
     * converted to one taking an array of String.
     */
    var simplifyMain = false

    /**
     * The ClassLoader used to resolve class names.
     * The default is to use [ClassLoader.getSystemClassLoader].
     * Note, if you use a different ClassLoader than the default one, then you must ensure that the same
     * ClassLoader is used when running the script
     * (otherwise some classes this the script requires may be unavailable at runtime).
     */
    var classLoader: ClassLoader = ClassLoader.getSystemClassLoader()

    /**
     * Is the "include" keyword allowed?
     * This allows extra files to be compiled, and was only designed to be used from the "feather" and
     * "feathers" command line programs; not when Feather is embedded within an application.
     */
    var allowIncludes = false

    /**
     * When true, includes can be absolute file paths.
     * When false, the paths can only be relative, and must not include ".." (parent directory).
     */
    var allowAbsoluteIncludes = false

    /**
     * When we encounter a decimal number, without a "d" or "f" suffix, then which type should we use?
     * Currently, the options are [RealType.DOUBLE] and [RealType.FLOAT], but later I may add
     * support for BigDecimal.
     */
    var defaultRealType: RealType = RealType.DOUBLE

    private var runtimeIncluded = false
    fun isRuntimeIncluded() = runtimeIncluded

    /**
     * Adds "uk.co.nickthecoder.feather.runtime" to [impliedImportPackages].
     * Adds "uk.co.nickthecoder.feather.runtime.Extensions" and
     * "uk.co.nickthecoder.feather.runtime.Print" to [impliedImportClasses]
     *
     * If you turn this on, ensure your project also include the feather-runtime jar
     * in your classpath at compile-time and runtime.
     */
    fun includeFeatherRuntime() {
        runtimeIncluded = true
        (sandBox as? AllowList)?.allowFeatherRuntime()
        impliedImportPackages.add("uk.co.nickthecoder.feather.runtime")
        impliedImportStaticClasses.add("uk.co.nickthecoder.feather.runtime.Print")
        impliedImportStaticClasses.add("uk.co.nickthecoder.feather.runtime.Extensions")
    }

    /**
     * WARNING. This will allow scripts to call `System.exit()`, which stops the entire JVM.
     *
     * This is used by the `feathers` command (which compiles and runs a single feather script)
     */
    fun includeExit() {
        (sandBox as? AllowList)?.allowedClasses?.add("uk.co.nickthecoder.feather.runtime.exit.Exit")
        impliedImportStaticClasses.add("uk.co.nickthecoder.feather.runtime.exit.Exit")
    }

    /**
     * A less severe way to stop a script compared to [includeExit].
     * A script which calls abort, will cause an [AbortFeather] to be thrown
     * (which extends Throwable, not Exception nor RuntimeException).
     *
     * You should catch the exception from where you called the Feather script.
     */
    fun includeAbort() {
        (sandBox as? AllowList)?.allowedClasses?.add("uk.co.nickthecoder.feather.runtime.abort.Abort")
        impliedImportStaticClasses.add("uk.co.nickthecoder.feather.runtime.abort.Abort")
    }

    public override fun clone(): CompilerConfiguration {
        val result = CompilerConfiguration()

        result.sandBox = sandBox.clone()
        result.allowCommands = allowCommands
        result.errorHandler = errorHandler
        result.throwWhenInterrupted = throwWhenInterrupted

        result.impliedImportPackages.clear()
        result.impliedImportPackages.addAll(impliedImportPackages)

        result.impliedImportClasses.clear()
        result.impliedImportClasses.putAll(impliedImportClasses.toMutableMap())

        result.impliedStaticImports.clear()
        result.impliedStaticImports.putAll(impliedStaticImports)

        result.impliedImportClasses.clear()
        result.impliedImportClasses.putAll(impliedImportClasses)

        result.impliedImportStaticClasses.clear()
        result.impliedImportStaticClasses.addAll(impliedImportStaticClasses)

        result.javaVersion = javaVersion
        result.debug = debug
        result.simplifyMain = simplifyMain
        result.classLoader = classLoader
        result.allowIncludes = allowIncludes
        result.allowAbsoluteIncludes = allowAbsoluteIncludes
        
        return result
    }
}
