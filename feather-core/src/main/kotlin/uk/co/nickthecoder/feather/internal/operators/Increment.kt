package uk.co.nickthecoder.feather.internal.operators

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.FeatherException
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.internal.*

/**
 * Implements ++
 * [isPrefix] is true for expressions such as `++a`
 * [isPrefix] is false for expressions such as `a++`
 */
internal class Increment(
    val a: AssignableStackEntry,
    val position: FeatherPosition,
    val isPrefix: Boolean

) : StackEntryWithOptionalResult {

    override fun type() = a.type()

    override fun compile(mv: MethodVisitor) {
        compile(mv, true)
    }

    override fun compile(mv: MethodVisitor, includeResult: Boolean) {

        if (a.isFinal()) {
            throw FeatherException.cannotChange(position)
        }

        val aType = a.resolvedType()
        val aTypePrim = aType.primitive()

        when (aTypePrim) {
            bytePrimitive, shortPrimitive, charPrimitive, intPrimitive -> {
                a.adjust(mv, includeResult && !isPrefix, includeResult && isPrefix) {
                    autoCast(mv, aType, aTypePrim)
                    mv.visitInsn(Opcodes.ICONST_1)
                    mv.visitInsn(Opcodes.IADD)
                    autoCast(mv, aTypePrim, aType)
                }
            }
            longPrimitive -> {
                a.adjust(mv, includeResult && !isPrefix, includeResult && isPrefix) {
                    autoCast(mv, aType, aTypePrim)
                    mv.visitInsn(Opcodes.LCONST_1)
                    mv.visitInsn(Opcodes.LADD)
                    autoCast(mv, aTypePrim, aType)
                }
            }
            floatPrimitive -> {
                a.adjust(mv, includeResult && !isPrefix, includeResult && isPrefix) {
                    autoCast(mv, aType, aTypePrim)
                    mv.visitInsn(Opcodes.FCONST_1)
                    mv.visitInsn(Opcodes.FADD)
                    autoCast(mv, aTypePrim, aType)
                }
            }
            doublePrimitive -> {
                a.adjust(mv, includeResult && !isPrefix, includeResult && isPrefix) {
                    autoCast(mv, aType, aTypePrim)
                    mv.visitInsn(Opcodes.DCONST_1)
                    mv.visitInsn(Opcodes.DADD)
                    autoCast(mv, aTypePrim, aType)
                }
            }
            else -> throw FeatherException.unexpectedType(position, "++", aType)
        }

    }

}
