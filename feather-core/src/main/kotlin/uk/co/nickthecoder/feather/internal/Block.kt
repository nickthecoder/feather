package uk.co.nickthecoder.feather.internal

import org.objectweb.asm.Label
import uk.co.nickthecoder.feather.LocalVariable
import uk.co.nickthecoder.feather.internal.special.BlockStackEntry
import java.lang.reflect.Type


/**
 * Refers a set of feather statements, usually enclosed in curly brackets.
 * For example, and if..else.. statement will result in two Blocks being created (one for the true branch, and
 * one for the else branch.
 *
 * During compilation, the compiler keeps track of the current block, and once the block ends
 * (when the right curly bracket is found), then the parent block will be referenced instead.
 * Therefore, there is not a tree structure of [Block]s, only a chain where the current [Block] can have a reference
 * to its parent, all the way to the top-most block, which has no parent.
 * A parent-less [Block]s are created for each method, field and constructor.
 */
internal class Block(val parent: Block?, val name: String) {

    /**
     * Set in [initialiseLocalVariableIndices] for any child blocks to use
     */
    private var nextVariableIndex: Int = -1

    val localVariables = mutableListOf<LocalVariable>()

    /**
     * This is a stack of [StackEntry], which are only used during compile-time, and
     * has no relation to the stack used by the Java byte code.
     */
    val stack = mutableListOf<StackEntry>()

    /**
     * Used by the continue keyword, to jump to the top of the loop
     */
    var continueLabel: Label? = null

    /**
     * Used by the break keyword, to jump out of the loop
     */
    var breakLabel: Label? = null

    fun top(): Block = parent?.top() ?: this

    fun findContinueLabel(): Label? = if (continueLabel == null) {
        parent?.findContinueLabel()
    } else {
        continueLabel
    }

    fun findBreakLabel(): Label? = if (breakLabel == null) {
        parent?.findBreakLabel()
    } else {
        breakLabel
    }

    fun findLocalVariable(name: String): LocalVariable? {
        return localVariables.firstOrNull { it.name == name } ?: parent?.findLocalVariable(name)
    }

    fun addVariable(name: String, type: Type, isFinal: Boolean): LocalVariable {
        val lv = LocalVariable(name, type, isFinal)
        localVariables.add(lv)
        return lv
    }

    fun addFirstVariable(name: String, type: Type, isFinal: Boolean): LocalVariable {
        val lv = LocalVariable(name, type, isFinal)
        localVariables.add(0, lv)
        return lv
    }

    fun initialiseLocalVariableIndices(): Int {

        var index = if (parent == null) 0 else {
            if (parent.nextVariableIndex < 0) {
                parent.initialiseLocalVariableIndices()
            }
            parent.nextVariableIndex
        }

        for (lv in localVariables) {
            lv.index = index
            val lvType = lv.type()
            if (lvType === longPrimitive || lvType === doublePrimitive) {
                index += 2
            } else {
                index++
            }
        }
        nextVariableIndex = index

        return nextVariableIndex
    }

    fun type(): Type = stack.lastOrNull()?.resolvedType() ?: voidPrimitive

    fun stackEntry(popFinalValue: Boolean = false): BlockStackEntry {
        return BlockStackEntry(this, popFinalValue)
    }

    fun isTerminating(): Boolean = stack.any { it.isTerminating() }
}
