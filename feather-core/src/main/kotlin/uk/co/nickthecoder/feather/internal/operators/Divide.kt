package uk.co.nickthecoder.feather.internal.operators

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.Source
import uk.co.nickthecoder.feather.internal.*
import java.lang.reflect.Type


internal class Divide(
    a: StackEntry,
    b: StackEntry,
    source : Source,
    position: FeatherPosition
) : BinaryMathsOperator(a, b, "/", "div", source, position) {

    override fun standardType(): Type {

        val common = super.standardType().primitive()
        return if (common in listOf(bytePrimitive, shortPrimitive, longPrimitive, intPrimitive, doublePrimitive)) {
            doublePrimitive
        } else if (common === floatPrimitive) {
            floatPrimitive
        } else {
            throw unexpectedTypes()
        }
    }

    override fun compileApply(mv: MethodVisitor) {
        when (resolvedType().primitive()) {
            floatPrimitive -> mv.visitInsn(Opcodes.FDIV)
            doublePrimitive -> mv.visitInsn(Opcodes.DDIV)
            else -> throw unexpectedTypes()
        }
    }
}

/**
 * During the parsing stage `a /= b` needs to use [FloorDivide] if [a] is a whole-number type,
 * and [Divide] otherwise.
 * We cannot tell the type of [a] during the parsing phase if its type is derived from a yet-to-be-parsed
 * expression.
 * Therefore, we must choose `lazily`.
 */
internal class LazyDivideEquals(
    private val a : AssignableStackEntry,
    private val b : StackEntry,
    private val source : Source,
    private val position : FeatherPosition
) : LazyStackEntry() {

    override fun createActual(): StackEntry {
        val nonAssignmentOperator = if (a.resolvedType().primitive().isWholeNumber()) {
            FloorDivide(a, b, source, position)
        } else {
            Divide(a, b, source, position)
        }
        return AdjustAssignment(
            a, b, "/=", "divAssign",
            nonAssignmentOperator,
            source, position
        )

    }

}
