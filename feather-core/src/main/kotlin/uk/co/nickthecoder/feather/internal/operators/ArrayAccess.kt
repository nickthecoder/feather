package uk.co.nickthecoder.feather.internal.operators

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes.*
import uk.co.nickthecoder.feather.FeatherException
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.Source
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.special.MethodWrapper
import uk.co.nickthecoder.feather.internal.special.findExtensionFunction
import java.lang.reflect.Type

internal class ArrayAccess(
    a: StackEntry,
    index: StackEntry,
    source: Source,
    position: FeatherPosition

) : AssignableStackEntry, BinaryOperator(a, index, "[]", "get", source, position) {

    private var setter: MethodWrapper? = null

    override fun standardType(): Type {
        return if (a.resolvedType().isArrayType() && b.resolvedType().isNumber()) {
            a.resolvedType().shallowestElementTypeOfArray()
        } else if (a.resolvedType() == stringClass) {
            charPrimitive
        } else {
            throw unexpectedTypes()
        }
    }


    override fun standardCompile(mv: MethodVisitor) {
        if (a.resolvedType().isArrayType() && b.resolvedType().isNumber()) {

            a.compile(mv)
            b.compileAutoCast(mv, intPrimitive)
            loadArray(mv, resolvedType())
        } else if (a.resolvedType() == stringClass) {
            a.compile(mv)
            b.compileAutoCast(mv, intPrimitive)
            mv.visitMethodInsn(
                INVOKEVIRTUAL,
                stringClass.asmName(),
                "charAt",
                asmMethodDescription(listOf(intPrimitive), charPrimitive),
                false
            )

        } else {
            throw unexpectedTypes()
        }
    }

    override fun isFinal() = false

    /**
     * Expects the jvm stack : (a, index, value)
     * On exit the jvm stack is empty.
     */
    private fun callSet(mv: MethodVisitor,) {
        val setterMethod = setter()
        val setterReturnType = setterMethod.returnType()
        if (setterMethod.isStatic()) {
            mv.visitMethodInsn(
                INVOKESTATIC,
                setterMethod.owner().asmName(),
                setterMethod.name(),
                asmMethodDescription(setterMethod.parameterTypes(), setterReturnType),
                setterMethod.owner().isInterface()
            )

        } else {
            val isInterface = setterMethod.owner().isInterface()
            mv.visitMethodInsn(
                if (isInterface) INVOKEINTERFACE else INVOKEVIRTUAL,
                setterMethod.owner().asmName(),
                setterMethod.name(),
                asmMethodDescription(setterMethod.parameterTypes(), setterReturnType),
                isInterface
            )
        }
        popUnusedValue(mv, setterReturnType)
    }

    override fun compileSet(value: StackEntry, mv: MethodVisitor) {
        if (a.resolvedType().isArrayType()) {
            a.compile(mv)
            b.compileAutoCast(mv, intPrimitive)
            value.compileAutoCast(mv, resolvedType())
            storeArray(mv, resolvedType())

        } else {
            a.compileAutoCast(mv, objectClass)
            b.compileAutoCast(mv, setterIndexType())
            value.compileAutoCast(mv, setterValueType())
            callSet(mv)

        }
    }

    override fun adjust(
        mv: MethodVisitor,
        includeOldValue: Boolean,
        includeNewValue: Boolean,
        adjustment: (MethodVisitor) -> Unit
    ) {
        val itemType = resolvedType()

        // The comments are the stack entries expected, when includeOldValue = true and includeNewValue = false.
        if (a.resolvedType().isArrayType()) {
            a.compile(mv)
            // array
            mv.visitInsn(DUP)
            // array, array
            b.compileAutoCast(mv, intPrimitive)
            // array, array, index
            mv.visitInsn(DUP_X1)
            // array, index, array, index
            loadArray(mv, resolvedType())
            // array, index, oldValue
            if (includeOldValue) {
                mv.dupX2(itemType)  // Keep the old value, as the "result" of this expression.
            }
            // (oldValue), array, index, oldValue
            adjustment(mv)

            // (oldValue), array, index, newValue
            if (includeNewValue) {
                mv.dupX2(itemType)
            }
            // (oldValue), (newValue), array, index newValue
            storeArray(mv, resolvedType())
            // (oldValue) (newValue)

        } else {
            a.compileAutoCast(mv, operatorFirstType())
            mv.visitInsn(DUP)
            // a, a
            b.compileAutoCast(mv, operatorSecondType())
            // a, a, index
            mv.visitInsn(DUP_X1)
            // a, index, a, index
            applyOperator(mv)
            // a, index, [oldValue]

            if (includeOldValue) {
                mv.dupX2(itemType) // Keep the old value, as the "result" of this expression.
            }

            // (oldValue), a, index, oldValue

            adjustment(mv)
            // (oldValue), a, index, newValue

            autoCast(mv, resolvedType(), setterValueType())
            // (oldValue), a, index, newValue(maybeBoxed)

            if (includeNewValue) {
                mv.dupX2(itemType)  // Keep the old value, as the "result" of this expression.
            }
            // (oldValue), (newValue), a, index, newValue

            callSet(mv)
            // (oldValue) (newValue)
        }
    }

    private fun setterIndexType(): Type {

        return with(setter()) {
            if (isStatic()) {
                parameterTypes()[1]
            } else {
                parameterTypes()[0]
            }

        }
    }

    private fun setterValueType(): Type {
        return with(setter()) {
            if (isStatic()) {
                parameterTypes()[2]
            } else {
                parameterTypes()[1]
            }
        }
    }

    private fun setter(): MethodWrapper {
        setter?.let { return it }

        for (name in listOf("set", "put")) {
            // Look for a method a.set( b, value ) where value is the same type as this.type()
            a.resolvedType().findMethod(name, emptyList(), listOf(b.resolvedType(), resolvedType()))?.let {
                setter = it
                return it
            }

            // Look for a "set" extension function
            a.resolvedType().findExtensionFunction(name, listOf(b.resolvedType(), resolvedType()), source)?.let {
                setter = it
                return it
            }
        }

        throw FeatherException.expectedAssignable(position)
    }
}
