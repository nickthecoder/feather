package uk.co.nickthecoder.feather.internal

import org.antlr.v4.runtime.*
import org.antlr.v4.runtime.tree.ParseTreeWalker
import org.antlr.v4.runtime.tree.TerminalNode
import org.objectweb.asm.AnnotationVisitor
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.Label
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.*
import uk.co.nickthecoder.feather.internal.keywords.*
import uk.co.nickthecoder.feather.internal.operators.*
import uk.co.nickthecoder.feather.internal.operators.comparisons.ConditionalStackEntry
import uk.co.nickthecoder.feather.internal.operators.comparisons.IsInstanceOf
import uk.co.nickthecoder.feather.internal.operators.comparisons.IsNotTrue
import uk.co.nickthecoder.feather.internal.operators.comparisons.IsTrue
import uk.co.nickthecoder.feather.internal.special.*
import uk.co.nickthecoder.feather.parser.FeatherLexer
import uk.co.nickthecoder.feather.parser.FeatherParser
import uk.co.nickthecoder.feather.parser.FeatherParserBaseListener
import uk.co.nickthecoder.feather.parser.LibraryClassLoader
import java.io.File
import java.lang.reflect.Method
import java.lang.reflect.Type
import java.util.*

internal class CompilerWorker(
    val configuration: CompilerConfiguration
) {


    /**
     * Data obtained in the 1st pass
     */
    val metaDataMap = mutableMapOf<String, MetaDataType>()

    private val errors = mutableListOf<FeatherException>()

    private val firstPhaseFiles = mutableSetOf<String>()

    /**
     * Parses all of the source code, building up data into [metaDataMap], which the 2nd phase
     * will use to generate the byte code.
     *
     * Catches syntax errors, but not semantic errors.
     */
    fun firstPhase(source: Source, input: CharStream) {
        // Prevent the same file being included twice.
        if (source.file != null) {
            if (firstPhaseFiles.contains(source.file.absolutePath)) {
                return
            } else {
                firstPhaseFiles.add(source.file.absolutePath)
            }
        }

        try {
            configuration.impliedImportClasses.forEach { key, value ->
                source.importClasses[key] = value.name
            }
            source.importPackages.addAll(configuration.impliedImportPackages)
            configuration.impliedStaticImports.forEach { key, value ->
                lookupClassName(value.first)?.let { source.importStaticAlias(it, value.second, key) }
            }
            configuration.impliedImportStaticClasses.forEach { className ->
                source.importStaticClass(className)
            }

            val lexer = FeatherLexer(input)
            val tokens = CommonTokenStream(lexer)
            lexer.removeErrorListeners()

            val parser = FeatherParser(tokens)
            parser.removeErrorListeners()

            val errorListener = object : BaseErrorListener() {
                override fun syntaxError(
                    recognizer: Recognizer<*, *>,
                    offendingSymbol: Any?,
                    line: Int,
                    charPositionInLine: Int,
                    msg: String,
                    e: RecognitionException?
                ) {
                    val pos = FeatherPosition(source, line - 1, charPositionInLine)
                    errors.add(FeatherException(pos, msg))
                    configuration.errorHandler(pos, msg)
                }
            }
            lexer.addErrorListener(errorListener)
            parser.addErrorListener(errorListener)

            val tree = parser.featherFile()

            val firstPassListener = FeatherParserListener(source)
            ParseTreeWalker.DEFAULT.walk(firstPassListener, tree)

            if (firstPassListener.receiverStack.isNotEmpty()) {
                println("WARNING Receiver stack is not empty")
            }

        } catch (e: FeatherException) {
            configuration.errorHandler(e.pos, e.message ?: "Unknown error")
            throw CompilationFailed(listOf(e))
        } catch (e: Exception) {
            val fe = FeatherException(FeatherPosition(source), "Unexpected error : ${e.message}")
            throw CompilationFailed(listOf(fe))
        }
    }

    /**
     * Creates java byte code from the data in [metaDataMap] (produced in the 1st phase).
     *
     * The buggy feather code may get passed the 1st stage without error being detected.
     * This phase will catch semantic errors (the 1st phase caught all of the syntax errors).
     */
    fun secondPhase(): Map<String, ByteArray> {
        /**
         * Byte code generated in the 2nd pass
         */
        val compiledMap = mutableMapOf<String, ByteArray>()
        var className = "none"
        // We can now start to generate byte code.
        try {
            for (mdc in metaDataMap.values) {
                className = mdc.name
                compiledMap[mdc.name] = compileClass(mdc)
            }
        } catch (e: FeatherException) {
            configuration.errorHandler(e.pos, e.message ?: "Unknown error")
            throw CompilationFailed(listOf(e))
        } catch (e: Exception) {
            val fe = FeatherException(FeatherPosition(Source(this, className)), "Unexpected error : ${e.message}")
            throw CompilationFailed(listOf(fe))
        }

        return compiledMap
    }

    inner class FeatherClassWriter(options: Int) : ClassWriter(options) {
        override fun getCommonSuperClass(typeString1: String, typeString2: String): String {
            val name1 = typeString1.replace('/', '.')
            val name2 = typeString2.replace('/', '.')
            val type1 = try {
                metaDataMap[name1] ?: Class.forName(name1, false, classLoader)
            } catch (e: ClassNotFoundException) {
                throw TypeNotPresentException(typeString1, e)
            }
            val type2 = try {
                metaDataMap[name2] ?: Class.forName(name2, false, classLoader)
            } catch (e: ClassNotFoundException) {
                throw TypeNotPresentException(typeString2, e)
            }
            val returnType = findCommonSuperType(type1, type2)
            return returnType.typeName.replace('.', '/');
        }
    }

    private fun compileClass(mdc: MetaDataType): ByteArray {

        // While debugging a broken test case, it is often useful to turn off COMPUTER_FRAMES,
        // as it can hide some of the byte-code, or add unwanted byte-code when invalid byte-code
        // is generated by FeatherCompiler.
        val classWriter = FeatherClassWriter(ClassWriter.COMPUTE_FRAMES)
        //val classWriter = ClassWriter(ClassWriter.COMPUTE_MAXS)

        mdc.classWriter = classWriter

        val position = mdc.position
        var access = if (mdc.isClass) Opcodes.ACC_SUPER else Opcodes.ACC_INTERFACE + Opcodes.ACC_ABSTRACT
        access += Opcodes.ACC_PUBLIC
        if (mdc.isEnum) access += Opcodes.ACC_ENUM
        if (mdc.isFinal) access += Opcodes.ACC_FINAL
        if (mdc.isClass && mdc.isAbstract) access += Opcodes.ACC_ABSTRACT

        var extendsType = mdc.extendsType

        if (mdc.isClass) {
            // If the class declaration didn't call a super constructor, look to see if any implemented types are
            // classes (not interfaces), and add a call to that class's default constructor.
            mdc.implementsTypes.filter { it !== extendsType && !it.isInterface() }.forEach { implementsType ->
                if (extendsType != null) {
                    // Attempting to extend more than one class
                    throw FeatherException.cannotExtendFromTwoClasses(position, extendsType!!, implementsType)
                } else {
                    mdc.initBlock.stack.add(0, SuperConstructorCall(implementsType, emptyList(), position))
                    extendsType = implementsType
                    mdc.extendsType = implementsType
                }
            }
            if (extendsType == null) {
                if (mdc.isEnum) {
                    extendsType = enumClass
                    mdc.extendsType = enumClass
                    mdc.initBlock.stack.add(0, SuperConstructorCall(enumClass, emptyList(), position))
                } else {
                    extendsType = objectClass
                    mdc.extendsType = objectClass
                    mdc.initBlock.stack.add(0, SuperConstructorCall(objectClass, emptyList(), position))
                }
            } else {
                if (extendsType!!.isFinal()) {
                    throw FeatherException.cannotExtendFinalClass(mdc.position, extendsType!!)
                }
            }
        }

        if ((extendsType != null || mdc.implementsTypes.firstOrNull { !it.isInterface() } != null) && mdc.isInterface()) {
            throw FeatherException.interfaceCannotExtendClass(position)
        }

        val signature = if (mdc.isEnum) {
            "Ljava/lang/Enum<L${mdc.asmName()};>;"
        } else {
            null
        }

        classWriter.visit(
            asmVersion(configuration.javaVersion),
            access,
            mdc.asmName(),
            signature,
            (mdc.extendsType ?: objectClass).asmName(),
            mdc.implementsTypes.filter { it.isInterface() }.map { it.asmName() }.toTypedArray()
        )
        classWriter.visitSource(mdc.source.source, null)


        if (mdc.isClass) {

            //if (mdc.isEnum) {
            //    compileEnumFields(classWriter, mdc)
            //}

            for (field in mdc.fields) {
                compileField(classWriter, field)
            }

            if (mdc.isEnum) {
                compileEnumExtras(classWriter, mdc)
            }

            // Constructors
            for (constructor in mdc.constructors) {
                val constructorSignature = if (mdc.isEnum) {
                    val types = constructor.parameterTypes.subList(2, constructor.parameterTypes.size)
                    asmMethodDescription(types, voidPrimitive)
                } else {
                    null
                }
                val mv = classWriter.visitMethod(
                    constructor.access.asmCode,
                    "<init>",
                    asmMethodDescription(constructor.parameterTypes.toList(), voidPrimitive),
                    constructorSignature,
                    null
                )
                mv.visitCode()
                if (constructor.isPrimary) {

                    if (mdc.isEnum) {
                        mv.visitVarInsn(Opcodes.ALOAD, 0)
                        mv.visitVarInsn(Opcodes.ALOAD, 1)
                        mv.visitVarInsn(Opcodes.ILOAD, 2)
                        mv.visitMethodInsn(
                            Opcodes.INVOKESPECIAL,
                            "java/lang/Enum",
                            "<init>",
                            "(Ljava/lang/String;I)V",
                            false
                        )
                    }
                    mdc.initBlock.stackEntry(true).compile(mv)
                }
                mv.visitInsn(Opcodes.RETURN)
                mv.visitMaxs(0, 0) // Values are automatically computed by ASM.
                mv.visitEnd()

            }
        }

        for (method in mdc.methods) {
            if (!method.isAutoGen) {
                compileMethod(classWriter, method)
            }
        }

        checkForUnimplementedMethods(position, classWriter, mdc)

        if (mdc.classInitBlock.stack.isNotEmpty() || mdc.isEnum) {
            val mv = classWriter.visitMethod(Opcodes.ACC_STATIC, "<clinit>", "()V", null, null)
            mv.visitCode()

            // Instantiate each enum value
            val constructor = mdc.primaryConstructor()!!
            if (mdc.isEnum) {
                var index = 0
                for ((enumName, arguments) in mdc.enums) {
                    if (constructor.parameterTypes.size != 2 + arguments.size) {
                        throw FeatherException.constructorNotFound(
                            position,
                            arguments.map { it.resolvedType() }
                        )
                    }
                    mv.visitTypeInsn(Opcodes.NEW, mdc.asmName())
                    mv.visitInsn(Opcodes.DUP)
                    StringConstant(enumName).compile(mv)
                    IntConstant(index).compile(mv)
                    var argCount = 2
                    for (argument in arguments) {
                        if (canAutoCast(argument.resolvedType(), constructor.parameterTypes[argCount].resolve())) {
                            argument.compile(mv)
                            autoCast(mv, argument.resolvedType(), constructor.parameterTypes[argCount].resolve())
                        } else {
                            throw FeatherException.constructorNotFound(
                                position,
                                arguments.map { it.resolvedType() }
                            )
                        }
                        argCount++
                    }
                    mv.visitMethodInsn(
                        Opcodes.INVOKESPECIAL,
                        mdc.asmName(),
                        "<init>",
                        asmMethodDescription(constructor.parameterTypes, voidPrimitive),
                        false
                    )
                    mv.visitFieldInsn(
                        Opcodes.PUTSTATIC,
                        mdc.asmName(),
                        enumName,
                        mdc.asmDescriptor()
                    )
                    index++
                }
                IntConstant(mdc.enums.size).compile(mv)
                newArray(mv, mdc)
                index = 0
                for ((enumName, _) in mdc.enums) {
                    mv.visitInsn(Opcodes.DUP)
                    IntConstant(index).compile(mv)
                    mv.visitFieldInsn(
                        Opcodes.GETSTATIC,
                        mdc.asmName(),
                        enumName,
                        mdc.asmDescriptor()
                    )
                    mv.visitInsn(Opcodes.AASTORE)
                    index++
                }
                mv.visitFieldInsn(
                    Opcodes.PUTSTATIC,
                    mdc.asmName(),
                    "\$VALUES",
                    "[L${mdc.asmName()};"
                )
            }


            if (mdc.classInitBlock.stack.isNotEmpty()) {
                mdc.classInitBlock.stackEntry(true).compile(mv)
            }

            mv.visitInsn(Opcodes.RETURN)
            mv.visitMaxs(0, 0) // Values are automatically computed by ASM.
            mv.visitEnd()
        }


        return classWriter.toByteArray()
    }

    private fun compileEnumExtras(classWriter: ClassWriter, mdc: MetaDataType) {

        // Synthetic "$VALUES" field
        classWriter.visitField(
            Opcodes.ACC_PRIVATE + Opcodes.ACC_FINAL + Opcodes.ACC_STATIC + Opcodes.ACC_SYNTHETIC,
            "\$VALUES",
            "[L${mdc.asmName()};",
            null,
            null
        )

        // values() method
        val mv = classWriter.visitMethod(
            Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC,
            "values",
            "()[L${mdc.asmName()};",
            null,
            null
        )
        mv.visitCode()
        mv.visitFieldInsn(Opcodes.GETSTATIC, mdc.asmName(), "\$VALUES", "[L${mdc.asmName()};")
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "[L${mdc.asmName()};", "clone", "()Ljava/lang/Object;", false)
        mv.visitTypeInsn(Opcodes.CHECKCAST, "[L${mdc.asmName()};")
        mv.visitInsn(Opcodes.ARETURN)
        mv.visitMaxs(0, 0) // Values are automatically computed by ASM.
        mv.visitEnd()

        // valueOf() method
        val mv2 = classWriter.visitMethod(
            Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC,
            "valueOf",
            "(Ljava/lang/String;)L${mdc.asmName()};",
            null,
            null
        )
        ClassConstant(mdc).compile(mv2)
        mv2.visitVarInsn(Opcodes.ALOAD, 0)
        mv2.visitMethodInsn(
            Opcodes.INVOKESTATIC,
            enumClass.asmName(),
            "valueOf",
            "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;",
            false
        )
        mv2.visitTypeInsn(Opcodes.CHECKCAST, mdc.asmName())
        mv2.visitInsn(Opcodes.ARETURN)
        mv2.visitCode()
        mv2.visitMaxs(0, 0) // Values are automatically computed by ASM.
        mv2.visitEnd()

    }

    private fun checkForUnimplementedMethods(position: FeatherPosition, classWriter: ClassWriter, mdc: MetaDataType) {

        // If the types are generic types such as E, then we will ignore it, as Feather doesn't
        // allow functions to use generic types.
        fun isRegular(method: MethodWrapper): Boolean {
            if (method.returnType().simpleTypeOrNull() == null) return false
            for (t in method.parameterTypes()) {
                if (t.simpleTypeOrNull() == null) return false
            }
            return true
        }

        fun isImplemented(entry: Map.Entry<Pair<String, List<Type?>>, List<MethodWrapper>>): Boolean {
            for (m in entry.value) {
                if (!m.isAbstract()) {
                    return true
                }
            }
            return false
        }

        fun findKotlinDefaultImplementation(method: MethodWrapper): Method? {
            val owner = method.owner()
            if (owner.isInterface() && owner is Class<*>) {
                val implClassName = "${owner.name}${"$"}DefaultImpls"
                val implClass = lookupClassName(implClassName)
                if (implClass != null) {

                    // The static method's parameter types must include "this"
                    val pt = mutableListOf<Type>()
                    pt.add(owner)
                    pt.addAll(method.parameterTypes())
                    val implMethod = implClass.declaredMethods.filter {
                        it.name == method.name() && it.parameterCount == pt.size && it.parameterTypes.toList() == pt &&
                                it.isStatic()
                    }.firstOrNull()
                    if (implMethod != null) {
                        return implMethod
                    }
                }
            }
            return null
        }

        fun createCallToDefaultImplementation(
            classWriter: ClassWriter,
            missingMethod: MethodWrapper,
            implMethod: Method
        ) {
            val descriptor = asmMethodDescription(missingMethod.parameterTypes(), missingMethod.returnType())
            val mv =
                classWriter.visitMethod(
                    Opcodes.ACC_PUBLIC,
                    missingMethod.name(),
                    descriptor, null, null
                )

            mv.visitVarInsn(Opcodes.ALOAD, 0)
            var loadCount = 1
            for (paramType in missingMethod.parameterTypes()) {
                val oldLoadCount = loadCount
                val opcode = when (paramType) {
                    booleanPrimitive, bytePrimitive, charPrimitive, shortPrimitive, intPrimitive -> Opcodes.ILOAD
                    longPrimitive -> {
                        loadCount++
                        Opcodes.LLOAD
                    }
                    floatPrimitive -> Opcodes.FLOAD
                    doublePrimitive -> {
                        loadCount++
                        Opcodes.DLOAD
                    }
                    else -> Opcodes.ALOAD
                }
                loadCount++
                mv.visitVarInsn(opcode, oldLoadCount)
            }

            mv.visitMethodInsn(
                Opcodes.INVOKESTATIC,
                implMethod.declaringClass.asmName(),
                implMethod.name,
                asmMethodDescription(implMethod.parameterTypes.toList(), implMethod.returnType),
                implMethod.declaringClass.isInterface
            )
            Return.compileReturn(mv, missingMethod.returnType())

            mv.visitMaxs(0, 0) // Values are automatically computed by ASM.
            mv.visitEnd()
        }

        if (mdc.isClass && !mdc.isAbstract) {
            // Non abstract classes should implement all methods
            val allMethods = mdc.allMethods()
            val filtered = allMethods.filter { isRegular(it) }
            val bySig = filtered.groupBy { Pair(it.name(), it.parameterTypes().map { it.simpleTypeOrNull() }) }
            val onlyUnimplemented = bySig.filter { !isImplemented(it) }
            for (foo in onlyUnimplemented) {
                val methods = foo.value

                // We have an unimplemented method. Either this is an error, or it is implemented in
                // Kotlin interface's implementation class.
                val method = methods.first()
                val implMethod = findKotlinDefaultImplementation(method)
                if (implMethod != null) {
                    createCallToDefaultImplementation(classWriter, method, implMethod)
                } else {
                    throw FeatherException.methodNotImplemented(
                        position,
                        method.name(),
                        method.parameterTypes()
                    )
                }
            }
        }
    }


    private fun compileField(classWriter: ClassWriter, field: MetaDataField) {

        var access = Opcodes.ACC_PUBLIC

        if (field.isFinal) access += Opcodes.ACC_FINAL
        if (field.isStatic) access += Opcodes.ACC_STATIC
        if (field.isEnum) access += Opcodes.ACC_ENUM

        val initialValue = if (field.isFinal) {
            if (field.initialValue is Boolean) {
                if (field.initialValue == true) 1 else 0
            } else {
                field.initialValue
            }
        } else {
            null
        }

        val fv = classWriter.visitField(
            access,
            field.name,
            field.type().asmDescriptor(),
            null,
            initialValue
        )

        for (an in field.annotations) {
            createAnnotation(fv.visitAnnotation(an.type.asmDescriptor(), an.isVisibleAtRuntime()), an)
        }
        fv.visitEnd()
    }

    private fun createAnnotation(av: AnnotationVisitor, amd: AnnotationMetaData) {
        val an = amd.type
        amd.valueStackEntries.forEach { name, valueStackEntry ->
            val method = an.findMethod(name, emptyList(), emptyList())
            if (method == null) {
                println("Unknown annotation property $name") // TODO
            } else {
                val type = method.returnType().klass()
                val castStackEntry = valueStackEntry.castTo(type)
                if (castStackEntry != null) {
                    av.visit(name, castStackEntry.constantValue)
                }
            }
        }
    }

    private fun compileMethod(classWriter: ClassWriter, method: MetaDataMethod) {
        var access = Opcodes.ACC_PUBLIC
        if (method.isAbstract) access += Opcodes.ACC_ABSTRACT
        if (method.isVarArgs) access += Opcodes.ACC_VARARGS
        if (method.isStatic) access += Opcodes.ACC_STATIC

        val returnType = method.returnType()
        val descriptor = asmMethodDescription(method.parameterTypes, returnType)
        val mv = classWriter.visitMethod(access, method.name, descriptor, null, null)

        if (! method.isAbstract) {
            mv.visitCode()

            if (method.isExpression) {
                val blockReturnType = method.block.type()

                if (! canAutoCast(blockReturnType, returnType)) {
                    throw FeatherException.incompatibleType(method.position, returnType, blockReturnType)
                }

                method.block.initialiseLocalVariableIndices()
                val body = method.block.stack.first()
                if (body is MultipleBranches) {
                    body.returnFromEveryBranch(method).compile(mv)
                } else {
                    Return(method, body, method.position).compile(mv)
                }
            } else {
                try {
                    method.block.stackEntry(true).compile(mv)
                } catch (e: FeatherException) {
                    throw e
                } catch (e: UnresolvedClassException) {
                    throw FeatherException( method.position, e.message ?: "Unresolved Class" )
                } catch (e: Exception) {
                    e.printStackTrace() // TODO
                    throw FeatherException( method.position, "Unexpected error : $e" )
                }
            }

            mv.visitMaxs(0, 0) // Values are automatically computed by ASM.
            mv.visitEnd()


            // If this method overrides a method from base/class/interface, and it narrows the return type,
            // then we need to create a bridging method which has the same signature (including return type)
            // as in the super-class/interface, which invokes THIS method.
            // FYI, In the JVM a method signature includes the return type. Neither the Java language,
            // nor feather uses that feature, but it does mean that extra bridging methods are needed.
            // that feature,
            var overridesMethod = false
            // ClassName strings
            val doneReturnTypes = mutableListOf<String>()
            var supers = method.owner.implementsTypes
            // TODO Not needed? The docs say that implementsType includes extendsType.
            if (method.owner.extendsType != null) supers =
                supers.toMutableList().apply { add(method.owner.extendsType!!) }

            for (unresolvedSuperType in supers) {
                val superType = unresolvedSuperType.resolve()
                superType.findMethod(method.name, emptyList(), method.parameterTypes)?.let { superMethod2 ->
                    if (superMethod2.isFinal()) {
                        throw FeatherException.cannotOverrideIsFinal(method.position, method.name)
                    }

                    overridesMethod = true
                    if (superMethod2.returnType().className() != method.returnType().className() &&
                        (!doneReturnTypes.contains(superMethod2.returnType().className()))
                    ) {
                        if (method.returnType() != voidPrimitive || superMethod2.returnType().isPrimitive()) {
                            if (!isAssignable(method.returnType(), superMethod2.returnType())) {
                                throw FeatherException.overrideReturnTypeMismatch(
                                    method.position,
                                    superMethod2.returnType(),
                                    method.returnType()
                                )
                            }
                        }

                        createBridgeMethod(classWriter, method, superMethod2)
                        doneReturnTypes.add(superMethod2.returnType().className())
                    }
                }
            }
            if (overridesMethod && !method.isOverride) {
                throw FeatherException.overrideExpected(method, method.position)
            }
            if (!overridesMethod && method.isOverride) {
                throw FeatherException.overrideUnexpected(method, method.position)
            }
        }

        for (an in method.annotations) {
            createAnnotation(mv.visitAnnotation(an.type.asmDescriptor(), an.isVisibleAtRuntime()), an)
        }

    }

    /**
     * When a sub-class declares a method with the same signature as the super class or interface with a narrower
     * return type, then a bridging method is needed.
     * This is because the JVM includes the return type as part of the method signature when looking up
     * methods. In the JVM it is possible to have two methods with the same name and argument types, but
     * with different return types.
     */
    private fun createBridgeMethod(classWriter: ClassWriter, method: MetaDataMethod, superMethod: MethodWrapper) {

        var access = Opcodes.ACC_PUBLIC
        if (method.isVarArgs) access += Opcodes.ACC_VARARGS
        access += Opcodes.ACC_BRIDGE + Opcodes.ACC_SYNTHETIC

        val subDescriptor = asmMethodDescription(method.parameterTypes, method.returnType())
        val superDescriptor = asmMethodDescription(superMethod.parameterTypes(), superMethod.returnType())

        val mv = classWriter.visitMethod(access, superMethod.name(), superDescriptor, null, null)

        mv.visitCode()

        mv.visitVarInsn(Opcodes.ALOAD, 0)
        var loadCount = 1
        method.parameterTypes.forEach { paramType ->
            val oldCount = loadCount
            val opcode = when (paramType) {
                booleanPrimitive, bytePrimitive, charPrimitive, shortPrimitive, intPrimitive -> Opcodes.ILOAD
                longPrimitive -> {
                    loadCount++
                    Opcodes.LLOAD
                }
                floatPrimitive -> Opcodes.FLOAD
                doublePrimitive -> {
                    loadCount++
                    Opcodes.DLOAD
                }
                else -> Opcodes.ALOAD
            }
            loadCount++
            mv.visitVarInsn(opcode, oldCount)
        }

        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, method.owner.asmName(), method.name, subDescriptor, false)

        if (method.returnType() == voidPrimitive && !superMethod.returnType().isPrimitive()) {
            NullLiteral.instance.compile(mv)
        } else {
            autoCast(mv, method.returnType(), superMethod.returnType())
        }
        Return.compileReturn(mv, superMethod.returnType())

        mv.visitMaxs(0, 0) // Values are automatically computed by ASM.
        mv.visitEnd()
    }


    fun lookupType(name: String): Type? {
        lookupClassName(name)?.let { return it }
        return metaDataMap[name]
    }

    fun lookupClassName(name: String): Class<*>? {
        return try {
            configuration.classLoader.loadClass(name)
        } catch (e: Exception) {
            null
        }
    }


    private fun parseGenericSpec(
        source: Source,
        genericSpec: FeatherParser.GenericSpecContext,
        allowPrimitives: Boolean
    ): kotlin.Array<Type> {
        val gspecTypes = mutableListOf<Type>()
        for (gt in genericSpec.simpleUserType()) {
            val gspecTypeName = gt.simpleIdentifier().text
            val gspecType = resolveClassOrCreate(source, gspecTypeName)
            if (gspecType.isPrimitive() && !allowPrimitives) {
                gspecTypes.add(gspecType.nonPrimitive())
            } else {
                gspecTypes.add(gspecType)
            }
        }
        return gspecTypes.toTypedArray()
    }

    private fun parseTypeArguments(
        source: Source,
        ctx: FeatherParser.TypeArgumentsContext?,
        allowPrimitives: Boolean
    ): List<Type> {
        if (ctx == null) return emptyList()

        val types = mutableListOf<Type>()
        for (tp in ctx.typeProjection()) {
            if (tp.MULT() != null) {
                types.add(StarProjection::class.java)
            }

            tp.type()?.let { typeContext ->
                val type = if (allowPrimitives) {
                    resolveTypeOrCreate(source, typeContext)
                } else {
                    resolveTypeOrCreate(source, typeContext).nonPrimitive()
                }
                val genericSpec = typeContext.nonFunctionType().genericSpec()
                if (genericSpec == null) {
                    types.add(type)
                } else {
                    types.add(
                        ParameterizedTypeImplementation(
                            type.klass(),
                            parseGenericSpec(source, genericSpec, allowPrimitives)
                        )
                    )
                }

            }
        }
        return types
    }

    @Deprecated(message = "Cannot handle function types")
    fun resolveClassOrCreate(source: Source, str: String): Type {
        return resolveClass(source, str) ?: if (str.contains('.')) {
            QualifiedUnresolvedType(str, source)
        } else {
            UnqualifiedUnresolvedType(str, source)
        }
    }

    fun resolveClass(source: Source, name: String): Type? {

        fun qualifiedName(pack: String, name: String) = "$pack.$name"

        return when (name) {
            "void" -> voidPrimitive
            "bool" -> booleanPrimitive // Oops, I meant to copy Java's primitives, should have been boolean
            "boolean" -> booleanPrimitive // So let's allow both for now! Why did java shorted int and char but not bool?
            "byte" -> bytePrimitive
            "char" -> charPrimitive
            "short" -> shortPrimitive
            "int" -> intPrimitive
            "long" -> longPrimitive
            "float" -> floatPrimitive
            "double" -> doublePrimitive
            "Array" -> arrayClass
            else -> {
                // A class without a package name (rare!)

                metaDataMap[name]?.let { return it }
                lookupClassName(name)?.let { return it }

                // An imported class
                source.importClasses[name]?.let { qualifiedName ->
                    metaDataMap[qualifiedName]?.let { return it }
                    lookupClassName(qualifiedName)?.let { return it }
                }
                // Classes in the same package
                lookupClassName(name)?.let { return it }
                source.packageName?.let { pack ->
                    metaDataMap[qualifiedName(pack, name)]?.let { return it }
                    lookupClassName(qualifiedName(pack, name))?.let { return it }
                }
                // Classes in an import .* package
                for (pack in source.importPackages) {
                    metaDataMap[qualifiedName(pack, name)]?.let { return it }
                    lookupClassName(qualifiedName(pack, name))?.let { return it }
                }

                null
            }
        }

    }

    fun resolveTypeOrCreate(source: Source, typeCtx: FeatherParser.TypeContext): Type {
        return if (typeCtx.nonFunctionType() == null) {
            // Must be a FUNCTION type e.g. (String, int)->char

            val fCtx = typeCtx.functionType()
            val argTypes = mutableListOf<Type>()
            for (argCtx in fCtx?.type() ?: emptyList()) {
                argTypes.add(resolveTypeOrCreate(source, argCtx))
            }
            val returnType = if (fCtx?.functionTypeReturn()?.returnType == null) {
                voidClass
            } else {
                resolveTypeOrCreate(source, fCtx.functionTypeReturn().returnType)
            }
            functionType(argTypes, returnType)

        } else {

            val gspec = typeCtx.nonFunctionType().genericSpec()

            return if (gspec == null) {
                val str = typeCtx.nonFunctionType().text
                resolveClass(source, str) ?: if (str.contains('.')) {
                    QualifiedUnresolvedType(str, source)
                } else {
                    UnqualifiedUnresolvedType(str, source)
                }
            } else {
                val simpleType = resolveClass(source, typeCtx.nonFunctionType().simpleUserType().text)
                if (simpleType == null) {
                    throw FeatherException.unknownType(toPosition(source, typeCtx), typeCtx.nonFunctionType().text)
                }
                val allowPrim = simpleType == arrayClass
                ParameterizedTypeImplementation(simpleType.klass(), parseGenericSpec(source, gspec, allowPrim))
            }
        }
    }

    // **** FeatherParserListener ****

    /**
     * Gathers basic information about each class/interface including :
     *
     * * field names and their type,
     * * method names, their parameter types and their return types.
     *
     * The second pass can use this information to get type information about fields and methods
     * which have yet to be compiled.
     */
    private inner class FeatherParserListener(val source: Source) : FeatherParserBaseListener() {

        // If the file has fields or methods declared outside of a class,
        // then they are added to a "dummy" class with the same name as the file.
        var impliedStaticClass: MetaDataType? = null

        var currentClass: MetaDataType? = null

        /**
         * When inside a non-static method, we push This(...) onto the receiverStack (and pop it when we exit the method).
         * Likewise for fields.
         *
         * Also, when "apply" is implemented, a different receiver is push onto the stack, so that functions can fields
         * will then refer to the latest receiver.
         */
        val receiverStack = Stack<Receiver>()

        var currentMethod: MetaDataMethod? = null

        /**
         * The block for the current function/method/constructor.
         */
        var block: Block = Block(null, "top")

        fun toPosition(token: Token?) = toPosition(source, token)
        fun toPosition(ctx: ParserRuleContext?) = toPosition(ctx?.start)
        fun toPosition(node: TerminalNode?) = toPosition(node?.symbol)


        @Deprecated(message = "Cannot handle function types")
        fun resolveClassOrCreate(str: String): Type {
            return resolveClassOrCreate(source, str)
        }

        fun resolveClass(name: String) = resolveClass(source, name)

        fun resolveTypeOrCreate(typeCtx: FeatherParser.TypeContext) = resolveTypeOrCreate(source, typeCtx)

        fun checkSandbox(type: Type, ctx: ParserRuleContext) {
            // Function types are allowed, and are not checked against the sand box.
            // This is because the type include the Class type, which the sandbox will disallow
            // (all reflection based classes are disallowed)
            if ( ! type.isFunctionType() ) {
                configuration.sandBox.checkSandbox(type, source, ctx)
            }
        }

        // Preamble

        override fun exitPackageHeader(ctx: FeatherParser.PackageHeaderContext) {
            val packageName = ctx.identifier() ?: return
            source.packageName = packageName.text
            // TODO If source (excluding the file name) doesn't match the package name, then warn.
        }

        override fun exitImportHeader(ctx: FeatherParser.ImportHeaderContext) {

            val identifier = ctx.identifier() ?: return

            val fullName = identifier.text
            val isStatic = ctx.STATIC() != null
            val aliasName = ctx.importAlias()?.simpleIdentifier()?.text

            if (ctx.DOT() == null) {
                // Not a ".*" import

                val lastDot = fullName.lastIndexOf(".")
                val lastPart = if (lastDot >= 0) fullName.substring(lastDot + 1) else null
                val mostPart = if (lastDot >= 0) fullName.substring(0, lastDot) else null

                if (isStatic) {

                    val type = lookupType(mostPart!!)

                    if (type == null) {
                        throw FeatherException.unknownType(toPosition(identifier), mostPart)
                    } else {
                        checkSandbox(type, identifier)
                        source.importStaticAlias(type, lastPart!!, aliasName ?: lastPart)
                    }

                } else {
                    // fullName is the qualified name of a Class to import
                    val type = lookupType(fullName)
                    if (type != null) {
                        // Issue an early error if attempted to import a class not in the allowed list of classes.
                        checkSandbox(type, identifier)
                    }
                    val className = aliasName ?: lastPart
                    className?.let { source.importClasses[it] = fullName }

                }

            } else {

                // Is a ".*" import

                if (isStatic) {
                    // fullName is the name of the class
                    val type = lookupType(fullName)
                    if (type != null) {
                        // Issue an early error if attempted to import a class not in the allowed list of classes.
                        checkSandbox(type, identifier)
                    }
                    source.importStaticClass(fullName)
                } else {
                    source.importPackages.add(fullName)
                }
            }
        }

        override fun exitIncludeHeader(ctx: FeatherParser.IncludeHeaderContext) {
            val filename = (ctx.Path() ?: return).text.trim()
            val position = toPosition(ctx)

            if (! configuration.allowIncludes) {
                throw FeatherException.includeNotSupported(position)
            }
            val file = File(filename)
            if (! configuration.allowAbsoluteIncludes) {
                if (file.isAbsolute || filename.contains("..")) {
                    throw FeatherException.includeCannotBeAbsolute(position)
                }
            }

            val sourceFile = source.file?.absoluteFile ?: throw FeatherException.includeNoScriptFile(position)

            val parent = sourceFile.parentFile
            val includeFile = if (parent == null) File(filename) else File(parent, filename)
            if (includeFile.exists()) {
                firstPhase(Source(this@CompilerWorker, includeFile), CharStreams.fromFileName(includeFile.path))
                return
            }

            throw FeatherException.includeNotFound(position)
        }

        override fun exitLibraryHeader(ctx: FeatherParser.LibraryHeaderContext) {
            val filename = (ctx.Path() ?: return).text.trim()
            var file = File(filename)
            if (!file.isAbsolute) {
                file = File(source.file?.absoluteFile?.parentFile ?: File(".").absoluteFile, filename)
                //println(file)
            }
            val classLoader = configuration.classLoader
            if (classLoader is LibraryClassLoader) {

                if (file.name == "*.jar") {
                    val dir = file.parentFile
                    dir.listFiles()?.forEach { child ->
                        if (child.extension == "jar") {
                            classLoader.addJar(child)
                        }
                    }
                } else {
                    if (file.exists()) {
                        classLoader.addJar(file)
                    } else {
                        throw FeatherException.libraryNotFound(toPosition(ctx), file)
                    }
                }
            } else {
                throw FeatherException.libraryUnexpected(toPosition(ctx))
            }
        }

        // Class definition

        override fun exitClassDeclaration(ctx: FeatherParser.ClassDeclarationContext?) {
            val currentClass = currentClass!!
            source.classes.add(currentClass)
            metaDataMap[currentClass.name] = currentClass

            this.currentClass = null
        }

        override fun enterClassDetails(ctx: FeatherParser.ClassDetailsContext) {
            block = Block(null, "class")
            // Save a space in the variables list for "this".
            block.addVariable("", objectClass, true)
        }

        override fun exitClassModifiersAndName(ctx: FeatherParser.ClassModifiersAndNameContext) {
            val position = toPosition(ctx)

            val identifier = ctx.simpleIdentifier() ?: return

            val isEnum = ctx.ENUM() != null
            val mdc = MetaDataType(
                source,
                source.qualifiedName(identifier.text),
                isClass = ctx.CLASS() != null,
                isAbstract = ctx.ABSTRACT() != null || ctx.CLASS() == null,
                isEnum,
                isFinal = isEnum,
                toPosition(ctx)
            )
            if (isEnum) {
                mdc.extendsType = enumClass
                mdc.implementsTypes
                mdc.methods.add(
                    MetaDataMethod(
                        mdc,
                        "values",
                        emptyList(),
                        arrayType(mdc),
                        isStatic = true,
                        isAutoGen = true,
                        position = position
                    )
                )
                mdc.methods.add(
                    MetaDataMethod(
                        mdc,
                        "valueOf",
                        listOf(stringClass),
                        mdc,
                        isStatic = true,
                        isAutoGen = true,
                        position = position
                    )
                )
            }
            currentClass = mdc
            metaDataMap[currentClass!!.name] = currentClass!!
            mdc.initBlock = block

        }

        override fun exitEnumValue(ctx: FeatherParser.EnumValueContext) {
            val cc = currentClass
            if (cc?.isEnum == true) {
                val enumName = ctx.simpleIdentifier().text

                // Collected the arguments (if there are any) to the constructor call.
                val argValues = mutableListOf<StackEntry>()
                ctx.enumArguments()?.let { enumArguments ->
                    val args = enumArguments.valueArgument()?.size ?: 0
                    for (i in 0 until args) {
                        pop()?.let { argValues.add(it) }
                    }
                }
                cc.enums.add(Pair(enumName, argValues))

                // Just so that we create *identical* bytecode to javac,
                // I want to insert the enum fields BEFORE the fields
                // declared in the constructor (if there are any)
                val index = cc.fields.count{ it.isEnum }
                cc.fields.add(
                    index,
                    MetaDataField(
                        cc,
                        enumName,
                        cc,
                        isStatic = true,
                        isFinal = true,
                        isEnum = true,
                        toPosition(ctx)
                    )
                )
            } else {
                reportError(FeatherException.notEnum(toPosition(ctx)))
            }
        }

        override fun enterInitBlock(ctx: FeatherParser.InitBlockContext) {
            receiverStack.push(This(currentClass!!))
        }

        override fun exitInitBlock(ctx: FeatherParser.InitBlockContext) {
            receiverStack.pop()
        }

        override fun exitPrimaryConstructorParameters(ctx: FeatherParser.PrimaryConstructorParametersContext) {
            val cc = currentClass!!

            if (cc.isInterface()) {
                throw FeatherException.interfaceCannotHaveConstructor(toPosition(ctx))
            }

            // Enum types have implied parameters : name : String, index : int
            if (cc.isEnum) {
                cc.initBlock.addVariable("<name>", stringClass, true)
                cc.initBlock.addVariable("<index>", intPrimitive, true)
            }

            val argTypes = mutableListOf<Type>()
            ctx.constructorParameter()?.forEach { parameterCtx ->

                val parameterName = parameterCtx.simpleIdentifier()?.text ?: return
                val parameterType = resolveTypeOrCreate(parameterCtx.type())
                val valOrVar = parameterCtx.valOrVar?.text
                checkSandbox(parameterType, ctx)
                argTypes.add(parameterType)
                val localVariable = cc.initBlock.addVariable(parameterName, parameterType, false)
                if (valOrVar != null) {
                    val field =
                        MetaDataField(
                            cc,
                            parameterName,
                            parameterType,
                            false,
                            valOrVar == "val",
                            isEnum = false,
                            toPosition(ctx)
                        )
                    cc.fields.add(field)
                    val position = toPosition(parameterCtx)
                    cc.initBlock.stack.add(
                        Assignment(
                            NonStaticFieldAccess(This(cc), FieldWrapper(field)),
                            LocalVariableStackEntry(localVariable, position),
                            true, position
                        )
                    )
                }
            }

            // Enum types have implied parameters : name : String, index : int
            if (cc.isEnum) {
                argTypes.add(0, stringClass)
                argTypes.add(1, intPrimitive)
            }

            cc.constructors.add(
                MetaDataConstructor(
                    cc,
                    argTypes,
                    true,
                    if (cc.isEnum) MetaDataAccess.PRIVATE else MetaDataAccess.PUBLIC,
                    toPosition(ctx)
                )
            )

        }

        override fun exitClassDetails(ctx: FeatherParser.ClassDetailsContext) {

            val cc = currentClass!!

            if (!cc.isInterface() && ctx.primaryConstructorParameters() == null) {
                if (cc.isEnum) {
                    cc.constructors.add(
                        MetaDataConstructor(
                            cc,
                            listOf(stringClass, intPrimitive),
                            true,
                            MetaDataAccess.PRIVATE,
                            toPosition(ctx)
                        )
                    )
                } else {
                    cc.constructors.add(
                        MetaDataConstructor(
                            cc,
                            emptyList(),
                            true,
                            MetaDataAccess.PUBLIC,
                            toPosition(ctx)
                        )
                    )
                }
            }

            ctx.extendsList()?.extendsSpecifier()?.forEach { extendsCtx ->
                val extendsIdentifierCtx = extendsCtx.simpleIdentifier()
                    ?: extendsCtx.constructorInvocation().simpleIdentifier()
                val extendsType = resolveClassOrCreate(extendsIdentifierCtx.text)
                checkSandbox(extendsType, extendsIdentifierCtx)

                if (extendsCtx.constructorInvocation() == null) {
                    // No brackets. If it is extending a class which we haven't compiled yet, we
                    // won't know if it is a class or an interface, but we put it in the class
                    // section anyway!
                    cc.implementsTypes.add(extendsType)
                } else {
                    // Has brackets, so this must be extending a class (and calling super's constructor), or an error
                    if (!cc.isClass) {
                        throw FeatherException.interfaceCannotExtendClass(toPosition(extendsIdentifierCtx))
                    }
                    if (cc.extendsType != null) {
                        throw FeatherException.cannotExtendFromTwoClasses(toPosition(extendsIdentifierCtx))
                    }
                    cc.extendsType = extendsType
                    cc.implementsTypes.add(extendsType)

                    val constructorInvocationCtx = extendsCtx.constructorInvocation()!!

                    val superArgCount = constructorInvocationCtx.callSuffix()?.valueArguments()?.valueArgument()?.size
                        ?: 0
                    val superArgs = mutableListOf<StackEntry>()
                    for (i in 1..superArgCount) {
                        superArgs.add(0, pop() ?: return)
                    }
                    push(SuperConstructorCall(extendsType, superArgs, toPosition(constructorInvocationCtx)))
                }
            }
        }

        private var annotations = mutableListOf<AnnotationMetaData>()

        private var annotation: AnnotationMetaData? = null

        override fun enterAnnotations(ctx: FeatherParser.AnnotationsContext) {
            annotations.clear()
        }

        override fun enterAnnotation(ctx: FeatherParser.AnnotationContext) {
            annotation = AnnotationMetaData()
        }

        override fun exitAnnotation(ctx: FeatherParser.AnnotationContext) {
            val an = annotation ?: return
            an.position = toPosition(ctx)
            an.type = resolveClassOrCreate(ctx.LabelReference().text.substring(1))
            annotations.add(an)
            annotation = null
        }

        override fun exitAnnotationValue(ctx: FeatherParser.AnnotationValueContext) {
            val name = ctx.simpleIdentifier()?.text ?: return
            val value = pop() ?: return
            if (value is ConstantStackEntry) {
                annotation?.valueStackEntries?.put(name, value)
                return
            } else if (value is StringBuilderStackEntry) {
                if (value.toConcat.size == 1) {
                    val first = value.toConcat.first()
                    if (first is StringConstant) {
                        annotation?.valueStackEntries?.put(name, first)
                        return
                    }
                }
            }
            // TODO Report an error
        }

        /**
         * When a field/method is declared outside of a class, then create a dummy
         * type to contain the static fields/methods.
         */
        private fun beginImpliedStaticClass(position: FeatherPosition) {
            if (impliedStaticClass == null) {

                val className = source.qualifiedName(source.name)
                if (metaDataMap.containsKey(className)) {
                    throw FeatherException.duplicateClassName(position, className)
                }
                val newClass = MetaDataType(
                    source,
                    className,
                    isClass = true,
                    isAbstract = false,
                    isEnum = false,
                    isFinal = false,
                    FeatherPosition(source, 0, 0)
                )
                newClass.constructors.add(
                    MetaDataConstructor(newClass, emptyList(), true, MetaDataAccess.PRIVATE, position)
                )

                metaDataMap[className] = newClass
                impliedStaticClass = newClass
            }
            currentClass = impliedStaticClass
        }

        /**
         * Called at the end of a field/method. If the field/method was declared outside
         * of a class, then resets currentClass to null.
         */
        private fun endImpliedStaticClass() {
            if (currentClass === impliedStaticClass) {
                currentClass = null
            }
        }

        private fun isImpliedStaticClass() = currentClass === impliedStaticClass

        override fun enterFieldDeclaration(ctx: FeatherParser.FieldDeclarationContext) {
            annotations.clear()
            if (currentClass == null) {
                beginImpliedStaticClass(toPosition(ctx))
            }
        }

        override fun exitFieldDeclaration(ctx: FeatherParser.FieldDeclarationContext) {

            val fdd = ctx.fieldDeclarationDescription() ?: return
            val vd = fdd.variableDeclaration() ?: return

            if (currentClass?.isClass == false) {
                throw FeatherException.interfaceCannotHaveFields(toPosition(ctx))
            }

            val isFinal = fdd.def.type == FeatherParser.VAL
            val position = toPosition(ctx)
            val name = vd.simpleIdentifier().text
            val isStatic = isImpliedStaticClass() || fdd.STATIC() != null
            val typeCtx = vd.type()

            val initialExpression = if (ctx.ASSIGNMENT() == null) {
                if (isFinal) {
                    throw FeatherException.valMustBeInitialised(toPosition(ctx), name)
                }
                null
            } else {
                pop()
            }

            val assignmentPosition = if (ctx.ASSIGNMENT() == null) toPosition(ctx) else toPosition(ctx.ASSIGNMENT())

            val type = if (typeCtx == null) {
                SimpleImpliedType(initialExpression ?: throw FeatherException.unknownType(position, name), position)
            } else {
                resolveTypeOrCreate(typeCtx)
            }

            val field = MetaDataField(currentClass!!, name, type, isStatic, isFinal, isEnum = false, toPosition(ctx))
            field.annotations = annotations.toList()

            if (initialExpression != null) {
                field.initialExpression = initialExpression

                if (initialExpression is ConstantStackEntry) {
                    field.initialValue = initialExpression.constantValue
                    if (typeCtx == null) {
                        field.type = initialExpression.constantValue?.javaClass?.primitive()
                            ?: voidPrimitive
                    }
                }

                if (isStatic) {
                    if (initialExpression !is ConstantStackEntry || !isFinal) {
                        val f = StaticFieldAccess(currentClass!!, FieldWrapper(field))
                        currentClass!!.classInitBlock.stack.add(
                            Assignment(
                                f,
                                initialExpression,
                                true,
                                assignmentPosition
                            )
                        )
                    }
                } else {
                    val f = NonStaticFieldAccess(This(currentClass!!), FieldWrapper(field))
                    push(Assignment(f, initialExpression, true, assignmentPosition))
                }
            }

            currentClass!!.fields.add(field)
            if (!isStatic) {
                receiverStack.pop()
            }
            endImpliedStaticClass()
        }

        override fun exitFieldDeclarationDescription(ctx: FeatherParser.FieldDeclarationDescriptionContext) {
            if (ctx.STATIC() == null) {
                receiverStack.push(This(currentClass!!))
            }
        }

        override fun enterMethodDeclaration(ctx: FeatherParser.MethodDeclarationContext?) {
            annotations.clear()
            if (currentClass == null) {
                beginImpliedStaticClass(toPosition(ctx))
            }
        }

        override fun enterMethodDescription(ctx: FeatherParser.MethodDescriptionContext) {
            block = Block(null, "method")
        }

        override fun exitMethodDescription(ctx: FeatherParser.MethodDescriptionContext) {
            val position = toPosition(ctx)

            val name = ctx.name?.text ?: ""
            val ext = ctx.ext?.text // Class name if this is an extension function
            val isStatic = isImpliedStaticClass() || ctx.STATIC() != null
            val isAbstract = ctx.ABSTRACT() != null || !currentClass!!.isClass
            val isOverride = ctx.OVERRIDE() != null

            if (isAbstract && !currentClass!!.isAbstract) {
                throw FeatherException.cannotHaveAbstractMethod(position)
            }

            val typeCtx = ctx.type()
            val returnType = if (typeCtx == null) {
                if (isAbstract) {
                    voidPrimitive
                } else {
                    // NOTE. It could also be voidPrimitive if there is a block.
                    // See enterMethodBlock for how this is tidied up.
                    SimpleImpliedType(block.stackEntry(), position)
                }
            } else {
                val type = resolveTypeOrCreate(typeCtx)
                checkSandbox(type, ctx)
                type
            }

            var isVarArg = false
            val mps = ctx.methodValueParameters()?.methodParameter() ?: emptyList()
            mps.forEachIndexed { index, mp ->
                if (mp.ELIPSIS() != null) {
                    if (index != mps.size - 1) {
                        throw FeatherException.varargMustBeLast(source, mp)
                    }
                    isVarArg = true
                }
            }

            if (!isStatic) {
                block.addVariable("this@${currentClass!!.name}", currentClass!!, true)
            }

            val argTypes = mutableListOf<Type>()
            (ctx.methodValueParameters()?.methodParameter() ?: emptyList()).forEachIndexed { i, parameterCtx ->
                val parameterName = parameterCtx.simpleIdentifier().text
                val parameterType = resolveTypeOrCreate(parameterCtx.type())
                argTypes.add(parameterType)
                checkSandbox(parameterType, parameterCtx)
                if (isVarArg && i == ctx.methodValueParameters().methodParameter().size - 1) {
                    argTypes[i] = ParameterizedTypeImplementation(arrayClass, arrayOf(parameterType))
                }
                block.addVariable(parameterName, argTypes[i], false).apply { initialised = true }
            }
            // An extension function? If so add the extension function class to the parameters list.
            if (ext != null) {
                val extType = resolveClassOrCreate(ext)
                argTypes.add(extType)
                checkSandbox(extType, ctx.ext)
                block.addFirstVariable(ext, extType, true)
            }

            // Allow a static main method to be declared without any arguments, and a standard
            // argv parameter will be automatically added (a vararg of type String).
            if (configuration.simplifyMain && ext == null && name == "main" && isStatic && argTypes.isEmpty()) {
                isVarArg = true
                argTypes.add(ParameterizedTypeImplementation(arrayClass, arrayOf(stringClass)))
                block.addVariable("argv", stringClass, false)
            }

            val method =
                MetaDataMethod(
                    currentClass!!,
                    name,
                    argTypes,
                    returnType,
                    isStatic,
                    isAbstract,
                    isVarArg,
                    isOverride,
                    position = position
                )

            if (ext != null) {
                method.isExtensionFunction = true
            }

            fun existingMethodWithSameSignature(): Boolean {
                currentClass!!.methods.forEach { existingMethod ->
                    if (existingMethod.name == method.name && existingMethod.parameterTypes.size == method.parameterTypes.size) {
                        var i = 0
                        for (existingType in existingMethod.parameterTypes) {
                            if (existingType != method.parameterTypes[i]) {
                                return false
                            }
                            i++
                        }
                        return true
                    }
                }
                return false
            }
            if (existingMethodWithSameSignature()) {
                throw FeatherException.duplicateMethod(position, method.name)
            }

            method.annotations = annotations.toList()

            currentClass?.methods?.add(method)
            currentMethod = method
            if (!isStatic) {
                receiverStack.push(This(currentClass!!))
            }
            method.block = block
        }

        override fun exitMethodBlock(ctx: FeatherParser.MethodBlockContext) {
            val method = currentMethod ?: return
            if (method.type is ImpliedType) {
                method.type = voidPrimitive
            }
            if (currentClass?.isInterface() == true) {
                throw FeatherException.interfaceCannotDefineFunctions(toPosition(ctx))
            }
        }

        override fun exitMethodDeclaration(ctx: FeatherParser.MethodDeclarationContext) {

            val localCurrentClass = currentClass ?: return
            val method = currentMethod ?: return

            if (ctx.methodBody()?.ASSIGNMENT() != null) {
                method.isExpression = true
                if (localCurrentClass.isInterface()) {
                    throw FeatherException.interfaceCannotDefineFunctions(toPosition(ctx.methodBody()))
                }
            }
            val lastEntry = method.block.stack.lastOrNull()

            if (lastEntry?.isTerminating() == true) {
                // Could add a warning if there's a return at the end of a void method.
            } else {
                if (!method.isAbstract && !method.isExpression) {
                    if (method.type == voidPrimitive) {
                        method.block.stack.add(Return(method, null, null))
                    } else {
                        reportError(
                            FeatherException.missingReturnStatement(
                                toPosition(
                                    ctx.methodBody()?.methodBlock()?.block()?.RCURL()
                                )
                            )
                        )
                    }
                }
            }

            block = localCurrentClass.initBlock
            if (!currentMethod!!.isStatic) {
                receiverStack.pop()
            }
            currentMethod = null
            endImpliedStaticClass()
        }


        // Literals

        override fun exitNullLiteral(ctx: FeatherParser.NullLiteralContext) {
            push(NullLiteral())
        }

        override fun exitBooleanLiteral(ctx: FeatherParser.BooleanLiteralContext) {
            val value = ctx.text == "true"
            push(BooleanConstant(value))
        }

        override fun exitCharacterLiteral(ctx: FeatherParser.CharacterLiteralContext) {
            val text = ctx.text
            val raw = text.substring(1, text.length - 1) // strip quote marks
            val value = if (raw.length == 2) {
                when (raw[1]) {
                    't' -> '\t'
                    'b' -> '\b'
                    'r' -> '\r'
                    'n' -> '\n'
                    else -> raw[1]
                }
            } else {
                raw[0]
            }
            push(CharConstant(value))
        }

        override fun exitByteLiteral(ctx: FeatherParser.ByteLiteralContext) {
            val fullText = ctx.text.replace("_", "")
            val text = fullText.substring(0, fullText.length - 1) // chop off the trailing "B"

            val value = if (text.startsWith("0x")) {
                // We need to go through an int because Byte.toByte(str,radix) only works for the range -127..128
                // Note that Java bytes are SIGNED, and therefore 0xff is actually -1. Eek!
                text.substring(2).toInt(16).toByte()
            } else if (text.startsWith("0b")) {
                text.substring(2).toInt(2).toByte()
            } else {
                text.toByte()
            }

            push(ByteConstant(value))
        }

        override fun exitShortLiteral(ctx: FeatherParser.ShortLiteralContext) {
            val fullText = ctx.text.replace("_", "").replace("_", "")
            val text = fullText.substring(
                0,
                fullText.length - 1
            ) // chop off the trailing "L"                val value = ctx.text.substring(0, text.length - 1).toShort() // chop off the trailing "S"
            val value = if (text.startsWith("0x")) {
                text.substring(2).toShort(16)
            } else if (text.startsWith("0b")) {
                text.substring(2).toShort(2)
            } else {
                text.toShort()
            }
            push(ShortConstant(value))
        }

        override fun exitIntLiteral(ctx: FeatherParser.IntLiteralContext) {
            val text = ctx.text
            val value = if (text.startsWith("0x")) {
                text.substring(2).toInt(16)
            } else if (text.startsWith("0b")) {
                text.substring(2).toInt(2)
            } else {
                text.toInt()
            }
            push(IntConstant(value))
        }

        override fun exitLongLiteral(ctx: FeatherParser.LongLiteralContext) {
            val fullText = ctx.text.replace("_", "")
            val text = fullText.substring(0, fullText.length - 1) // chop off the trailing "L"
            val value = if (text.startsWith("0x")) {
                text.substring(2).toLong(16)
            } else if (text.startsWith("0b")) {
                text.substring(2).toLong(2)
            } else {
                text.toLong()
            }

            push(LongConstant(value))
        }

        override fun exitFloatLiteral(ctx: FeatherParser.FloatLiteralContext) {
            val text = ctx.text.replace("_", "")
            val value = text.substring(0, text.length - 1).toFloat() // chop the trailing "F" or "f"

            push(FloatConstant(value))
        }

        override fun exitDoubleLiteral(ctx: FeatherParser.DoubleLiteralContext) {
            val text = ctx.text
            val value = text.toDouble()

            push(DoubleConstant(value))
        }

        override fun exitRealLiteral(ctx: FeatherParser.RealLiteralContext) {
            val text = ctx.text

            when (configuration.defaultRealType) {
                RealType.DOUBLE -> {
                    val value = text.toDouble()
                    push(DoubleConstant(value))
                }
                RealType.FLOAT -> {
                    val value = text.toFloat()
                    push(FloatConstant(value))
                }
            }
        }

        // ***** Expressions *****

        override fun exitIdent(ctx: FeatherParser.IdentContext) {
            val identifier = ctx.simpleIdentifier() ?: return

            val name = identifier.text
            if (name == "this") {
                // I don't know why I need this, why isn't exitThisExpression being called?
                // instead of exitIdent???
                // return this // Uses THIS
                // return bar(this) // Uses Ident ???
                val thisSE = receiverStack.safePeek()
                if (thisSE == null) {
                    reportError(FeatherException.cannotUseThisInStaticContext(toPosition(ctx)))
                } else {
                    push(thisSE)
                }
            } else {

                resolveClass(name)?.let { type ->
                    push(ClassConstant(type))
                    return
                }
                block.findLocalVariable(name)?.let { lv ->
                    push(LocalVariableStackEntry(lv, toPosition(ctx)))
                    return
                }
                push(Identifier(name, receiversAsList(), currentClass, block, source, toPosition(ctx)))
            }
        }

        override fun exitThisExpression(ctx: FeatherParser.ThisExpressionContext) {
            val thisSE = receiverStack.safePeek()
            if (thisSE == null) {
                reportError(FeatherException.cannotUseThisInStaticContext(toPosition(ctx)))
            } else {
                push(thisSE)
            }
        }

        override fun exitSuperExpression(ctx: FeatherParser.SuperExpressionContext) {
            val thisSE = receiverStack.lastOrNull { it is This }
            if (thisSE is This) {
                push(Super(thisSE))
            } else {
                reportError(FeatherException.cannotUseThisInStaticContext(toPosition(ctx)))
            }
        }

        override fun exitEqualityComparison(ctx: FeatherParser.EqualityComparisonContext) {
            val position = toPosition(ctx)
            val op = ctx.bop ?: return

            val b = pop() ?: return
            val a = pop() ?: return

            when (op.type) {
                FeatherParser.EQEQ -> push(ConditionalStackEntry.isEqual(a, b, position))
                FeatherParser.EQEQEQ -> push(ConditionalStackEntry.isSameObject(a, b, position))
                FeatherParser.EXCL_EQ -> push(ConditionalStackEntry.isNotEqual(a, b, position))
                FeatherParser.EXCL_EQEQ -> push(ConditionalStackEntry.isNotSameObject(a, b, position))
                else -> reportError(FeatherException.unexpectedOperator(position, op.text))
            }
        }

        override fun exitComparison(ctx: FeatherParser.ComparisonContext) {
            val position = toPosition(ctx)
            val op = ctx.bop ?: return

            val b = pop() ?: return
            val a = pop() ?: return

            when (op.type) {
                FeatherParser.LANGLE -> push(ConditionalStackEntry.isLessThan(a, b, position))
                FeatherParser.RANGLE -> push(ConditionalStackEntry.isGreaterThan(a, b, position))
                FeatherParser.LE -> push(ConditionalStackEntry.isLessThanOrEqual(a, b, position))
                FeatherParser.GE -> push(ConditionalStackEntry.isGreaterThanOrEqual(a, b, position))
                else -> reportError(FeatherException.unexpectedOperator(position, op.text))
            }
        }

        // ***** Methods/Functions/Fields *****

        /**
         * When we encounter the likes of: bar.foo(1, "Hello")
         *
         * See [exitMethodCall] for when we encounter: foo(1, "Hello" ) without an explicit receiver
         */
        override fun exitNonStaticMethodCall(ctx: FeatherParser.NonStaticMethodCallContext) {
            val position = toPosition(ctx)
            val name = (ctx.simpleIdentifier() ?: return).text

            val argCount = ctx.callSuffix().valueArguments()?.valueArgument()?.size ?: 0
            val args = mutableListOf<StackEntry>()

            val typeArgs = parseTypeArguments(source, ctx.callSuffix()?.typeArguments(), false)
            for (i in 1..argCount) {
                args.add(0, pop() ?: return)
            }

            val subject = pop() ?: return

            push(LazyNonStaticMethodCall(subject, name, args, typeArgs, source, position))
        }

        /**
         * When we encounter the likes of: foo(1, "Hello")
         *
         * See [exitNonStaticMethodCall] for when we encounter: bar.foo(1, "Hello")
         */
        override fun exitMethodCall(ctx: FeatherParser.MethodCallContext) {
            val position = toPosition(ctx)
            val name = (ctx.simpleIdentifier() ?: return).text

            val argCount = ctx.callSuffix().valueArguments()?.valueArgument()?.size ?: 0
            val args = mutableListOf<StackEntry>()
            val allowPrimitives = name == "Array"
            val typeArgs = parseTypeArguments(source, ctx.callSuffix()?.typeArguments(), allowPrimitives)
            for (i in 1..argCount) {
                args.add(0, pop() ?: return)
            }

            push(LazyMethodCall(currentClass!!, receiversAsList(), block, name, args, typeArgs, source, position))
        }

        /**
         * When we encounter SOME_CODE( ) or SOME_CODE( args ),
         * and SOME_CODE does not look like a method call, then the top of the stack will be a
         * StackEntry, which is (hopefully) of type Function, we should invoke that function.
         */
        override fun exitInvokableExpression(ctx: FeatherParser.InvokableExpressionContext) {
            // We have to make sure that this *IS* an invocation of a Function
            // There must be a callSuffix (i.e. parentheses with optional arguments)
            if (ctx.callSuffix() != null ) {
                val argCount = ctx.callSuffix().valueArguments()?.valueArgument()?.size ?: 0
                val args = mutableListOf<StackEntry>()
                for (i in 1..argCount) {
                    args.add(0, pop() ?: return)
                }
                val position = toPosition(ctx.callSuffix())
                val func = pop() ?: return
                push(InvokeFunction(func, args, position))
            }
        }

        /**
         * Either
         *
         *  * ::methodName(paramType0,paramType1...) or
         *  * className::methodName(paramType0,paramType2...)
         *  * ::methodName
         *  * className::methodName
         *
         * The return type is calculated, when we find the method being referenced.
         */
        override fun exitFunction(ctx: FeatherParser.FunctionContext) {
            val className = ctx.className?.text
            val ownerType = if (className == null) null else resolveClassOrCreate(className)
            val methodName = ctx.methodName?.text ?: return
            val parameterTypes = if (ctx.LANGLE() == null) {
                null
            } else {
                ctx.type()?.map { resolveTypeOrCreate(it) }
            }

            val functionSE =
                LazyFunctionStackEntry(
                    ownerType,
                    currentClass,
                    methodName,
                    parameterTypes,
                    source,
                    toPosition(ctx)
                )
            push(functionSE)
        }

        /**
         * A curried function such as :
         *
         *      this:>foo(int)
         *
         * Syntactic sugar for :
         *
         *      FooClass::foo(int).curry(this)
         *
         * The function is the non-static method `foo`, and it has been curried with the value `this`.
         *
         * The return type is calculated, when we find the method being referenced.
         */
        override fun exitCurriedFunction(ctx: FeatherParser.CurriedFunctionContext) {
            val position = toPosition(ctx.CURRIED())
            val subject = pop() ?: return
            val methodName = ctx.methodName?.text ?: return
            val parameterTypes = if (ctx.LANGLE() == null) {
                null
            } else {
                ctx.type()?.map { resolveTypeOrCreate(it) }
            }

            val functionSE =
                LazyFunctionStackEntry(
                    subject, methodName, parameterTypes, source, toPosition(ctx)
                )

            val curryCall = LazyNonStaticMethodCall(functionSE, "curry", listOf(subject), emptyList(), source, position)
            push(curryCall)
        }

        override fun exitInfixFunctionCall(ctx: FeatherParser.InfixFunctionCallContext) {
            val name = ctx.simpleIdentifier()?.text ?: return
            val b = pop() ?: return
            val a = pop() ?: return
            push(LazyNonStaticMethodCall(a, name, listOf(b), emptyList(), source, toPosition(ctx.simpleIdentifier())))
        }

        override fun exitFieldAccess(ctx: FeatherParser.FieldAccessContext) {

            val position = toPosition(ctx)
            val name = (ctx.simpleIdentifier() ?: return).text

            val subject = pop() ?: return
            push(LazyFieldStackEntry(subject, name, source, position))
        }


        // ***** Control Statements *****

        override fun exitLocalVariableDeclaration(ctx: FeatherParser.LocalVariableDeclarationContext) {
            ctx.variableDeclaration() ?: return

            val isFinal = ctx.def.type == FeatherParser.VAL
            val position = toPosition(ctx)
            val name = ctx.variableDeclaration()?.simpleIdentifier()?.text ?: return
            val typeCtx = ctx.variableDeclaration().type()

            val initialExpression = if (ctx.ASSIGNMENT() == null) {
                if (isFinal) {
                    reportError(FeatherException.valMustBeInitialised(position, name))
                }
                null
            } else {
                pop()
            }

            val type = if (typeCtx == null) {
                if (initialExpression == null) {
                    reportError(FeatherException.unknownType(position, name))
                    objectClass
                } else {
                    SimpleImpliedType(initialExpression, position)
                }
            } else {
                resolveTypeOrCreate(typeCtx)
            }

            if (block.findLocalVariable(name) == null) {
                val localVar = block.addVariable(name, type, isFinal)

                if (initialExpression != null) {
                    val assignmentPosition =
                        if (ctx.ASSIGNMENT() == null) toPosition(ctx) else toPosition(ctx.ASSIGNMENT())
                    push(
                        Assignment(
                            LocalVariableStackEntry(localVar, position),
                            initialExpression,
                            true,
                            assignmentPosition
                        )
                    )
                }
            } else {
                throw FeatherException.duplicateLocalVariable(toPosition(ctx.variableDeclaration()), name)
            }
        }

        override fun enterBlock(ctx: FeatherParser.BlockContext?) {
            block = Block(block, "simple block")
        }

        override fun exitBlock(ctx: FeatherParser.BlockContext?) {
            val oldBlock = block
            block = block.parent!!
            push(oldBlock.stackEntry())
        }

        override fun exitIfExpression(ctx: FeatherParser.IfExpressionContext) {
            val position = toPosition(ctx)
            val elseBranch = if (ctx.ELSE() == null) null else pop()
            val ifBranch = pop() ?: return
            var condition = pop() ?: return

            if (condition !is ConditionalStackEntry) {
                // Optimization when the if is a Not
                if (condition is Not) {
                    condition = ConditionalStackEntry(
                        condition.a,
                        DoNothingStackEntry(),
                        "if",
                        IsNotTrue(),
                        IsTrue(),
                        position
                    )
                } else {
                    condition = ConditionalStackEntry(
                        condition,
                        DoNothingStackEntry(),
                        "if",
                        IsTrue(),
                        IsNotTrue(),
                        position
                    )
                }
            }
            push(If(condition, ifBranch, elseBranch))
        }

        override fun exitForExpression(ctx: FeatherParser.ForExpressionContext) {

            val body = pop() ?: return

            val oldBlock = block
            // Undo the extra block that was created in enterForExpression.
            block = block.parent!!

            val position = toPosition(ctx)
            val loopVarName = ctx.forSetup()?.loopVar?.text ?: return
            val counterVarName = ctx.forSetup()?.counterVar?.Identifier()?.text

            val iterableSE = pop() ?: return

            push(
                For(
                    oldBlock, body, loopVarName, counterVarName, iterableSE,
                    configuration.throwWhenInterrupted, source, position
                )
            )
        }

        override fun exitForSetup(ctx: FeatherParser.ForSetupContext) {
            val loopVarName = ctx.loopVar?.text ?: return
            val counterVarName = ctx.counterVar?.Identifier()?.text


            val loopExpression = peek()
            // TODO This looks suspicious. We are looking for type information in the first pass.
            val loopVarType = For.firstPassLoopVariableType(source, toPosition(ctx), loopExpression)

            block = Block(block, "for")
            block.continueLabel = Label()
            block.breakLabel = Label()

            block.addVariable(loopVarName, loopVarType, false)
            counterVarName?.let { block.addVariable(counterVarName, intPrimitive, false) }
        }

        override fun enterWhileBody(ctx: FeatherParser.WhileBodyContext?) {
            block = Block(block, "while")
            block.continueLabel = Label()
            block.breakLabel = Label()
        }


        override fun exitWhileExpression(ctx: FeatherParser.WhileExpressionContext) {

            val whileBody = pop() ?: return

            val continueLabel = block.continueLabel ?: return
            val breakLabel = block.breakLabel ?: return

            // Undo the extra block that was created in enterWhileBody
            block = block.parent!!

            val condition = pop() ?: return
            val position = toPosition(ctx)
            push(
                LazyWhile(
                    condition,
                    whileBody,
                    continueLabel,
                    breakLabel,
                    configuration.throwWhenInterrupted,
                    position
                )
            )
        }

        override fun exitDoWhileExpression(ctx: FeatherParser.DoWhileExpressionContext) {

            val condition = pop() ?: return
            val whileBody = pop() ?: return

            val breakLabel = block.breakLabel ?: return
            val continueLabel = block.continueLabel ?: return

            // Exit into the parent block from enterWhileBody.
            block = block.parent!!

            val position = toPosition(ctx)
            push(
                LazyDoWhile(
                    condition,
                    whileBody,
                    continueLabel,
                    breakLabel,
                    configuration.throwWhenInterrupted,
                    position
                )
            )
        }

        override fun exitReturnStatement(ctx: FeatherParser.ReturnStatementContext) {
            val value = if (ctx.expression() == null) null else pop()
            if (value is MultipleBranches) {
                push(value.returnFromEveryBranch(currentMethod!!))
            } else {
                push(Return(currentMethod!!, value, toPosition(ctx)))
            }
        }

        override fun exitBreakStatement(ctx: FeatherParser.BreakStatementContext) {

            val breakLabel = block.findBreakLabel()
            if (breakLabel == null) {
                reportError(FeatherException.breakWithoutLoop(source, ctx))
            } else {
                push(SimpleStackEntry(voidPrimitive) { mv ->
                    mv.visitJumpInsn(Opcodes.GOTO, breakLabel)
                })
            }
        }

        override fun exitContinueStatement(ctx: FeatherParser.ContinueStatementContext) {
            val continueLabel = block.findContinueLabel()
            if (continueLabel == null) {
                reportError(FeatherException.continueWithoutLoop(source, ctx))
            } else {
                push(SimpleStackEntry(voidPrimitive) { mv ->
                    mv.visitJumpInsn(Opcodes.GOTO, continueLabel)
                })
            }
        }

        // Try Catch Finally

        override fun exitTryExpression(ctx: FeatherParser.TryExpressionContext) {

            val finallyBlock = if (ctx.finallyBlock() != null) pop() else null
            val catchBlocks = mutableListOf<CatchSection>()
            ctx.catchBlock().reversed().forEach {
                val name = it.name.text
                val expType = resolveClassOrCreate(it.throwType.text)
                catchBlocks.add(
                    0, CatchSection(
                        name,
                        expType,
                        pop() as BlockStackEntry
                    )
                )
            }
            val tryBlock = pop() ?: return

            if (finallyBlock == null && catchBlocks.isEmpty()) {
                reportError(FeatherException.tryWithoutCatchOrFinally(toPosition(ctx)))
            }
            push(TryCatchFinally(block, tryBlock, catchBlocks, finallyBlock))
        }

        override fun enterCatchBlock(ctx: FeatherParser.CatchBlockContext) {
            val name = ctx.name?.text ?: return
            val expType = resolveClassOrCreate(ctx.throwType.text)
            // Create a new block, so we can add the exception variable into it.
            block = Block(block, "catch")
            block.addVariable(name, expType, true)
        }

        override fun exitCatchBlock(ctx: FeatherParser.CatchBlockContext) {
            val oldBlock = block
            block = block.parent!!
            push(oldBlock.stackEntry())
        }

        override fun exitThrowStatement(ctx: FeatherParser.ThrowStatementContext) {
            push(Throw(pop() ?: return, toPosition(ctx)))
        }

        // ***** Assignment *****

        override fun exitExpression(ctx: FeatherParser.ExpressionContext) {
            val op = ctx.bop ?: return
            val position = toPosition(op)

            val b = pop() ?: return
            val a = pop() ?: return

            if (a !is AssignableStackEntry) {
                reportError(FeatherException.expectedAssignable(position))
                push(a)
                return
            }

            fun pushAdjustAssign(symbol: String, methodName: String, operator: StackEntry) {
                push(
                    AdjustAssignment(a, b, symbol, methodName, operator, source, position)
                )
            }

            when (op.type) {
                FeatherParser.ASSIGNMENT -> push(Assignment(a, b, false, position))
                FeatherParser.ADD_ASSIGNMENT -> pushAdjustAssign( "+=", "plusAssign", Plus(a, b, source, position))
                FeatherParser.SUB_ASSIGNMENT -> pushAdjustAssign( "-=", "minusAssign", Minus(a, b, source, position))
                FeatherParser.MULT_ASSIGNMENT -> pushAdjustAssign( "*=", "timesAssign", Times(a, b, source, position))
                FeatherParser.REM_ASSIGNMENT -> pushAdjustAssign( "%=", "remAssign", Remainder(a, b, source, position))
                FeatherParser.DIVIDE_ASSIGNMENT -> push(LazyDivideEquals(a, b, source, position))
                FeatherParser.FLOOR_DIV_ASSIGNMENT ->pushAdjustAssign( "~/=", "floorDivAssign", FloorDivide(a, b, source, position))

                else -> {
                    reportError(FeatherException.unexpectedOperator(position, op.text))
                    push(a)
                }
            }
        }

        override fun exitPostfixUnaryExpression(ctx: FeatherParser.PostfixUnaryExpressionContext) {

            val op = ctx.uop ?: return
            val position = toPosition(ctx.uop)

            val a = pop() ?: return

            if (a !is AssignableStackEntry) {
                reportError(FeatherException.expectedAssignable(position))
                push(a)
                return
            }

            when (op.type) {
                FeatherParser.INCR -> push(Increment(a, position, isPrefix = false))
                FeatherParser.DECR -> push(Decrement(a, position, isPrefix = false))

                else -> {
                    reportError(FeatherException.unexpectedOperator(position, op.text))
                    push(a)
                }
            }
        }

        // ***** Numeric operators *****

        override fun exitPrefixUnaryExpression(ctx: FeatherParser.PrefixUnaryExpressionContext) {
            val op = ctx.uop ?: return
            val position = toPosition(op)

            val a = pop() ?: return

            when (op.type) {
                FeatherParser.SUB -> push(UnaryMinus(a, source, position))
                FeatherParser.ADD -> push(UnaryPlus(a, source, position))
                FeatherParser.EXCL -> push(Not(a, source, position))
                FeatherParser.INCR -> {
                    if (a !is AssignableStackEntry) {
                        throw FeatherException.expectedAssignable(position)
                    }
                    push(Increment(a, position, isPrefix = true))
                }
                FeatherParser.DECR -> {
                    if (a !is AssignableStackEntry) {
                        throw FeatherException.expectedAssignable(position)
                    }
                    push(Decrement(a, position, isPrefix = true))
                }

                else -> {
                    reportError(FeatherException.unexpectedOperator(position, op.text))
                    push(a)
                }
            }
        }

        override fun exitAdditiveExpression(ctx: FeatherParser.AdditiveExpressionContext) {
            val op = ctx.bop ?: return
            val position = toPosition(op)

            val b = pop() ?: return
            val a = pop() ?: return

            when (op.type) {
                // TODO Use StringBuilderStackEntry if we are concatenating strings
                FeatherParser.ADD -> push(Plus(a, b, source, position))
                FeatherParser.SUB -> push(Minus(a, b, source, position))
                else -> {
                    reportError(FeatherException.unexpectedOperator(position, op.text))
                    push(a)
                }
            }
        }

        override fun exitMultiplicativeExpression(ctx: FeatherParser.MultiplicativeExpressionContext) {
            val op = ctx.bop ?: return
            val position = toPosition(op)

            val b = pop() ?: return
            val a = pop() ?: return

            when (op.type) {
                FeatherParser.MULT -> push(Times(a, b, source, position))
                FeatherParser.DIVIDE -> push(Divide(a, b, source, position))
                FeatherParser.FLOOR_DIV -> push(FloorDivide(a, b, source, position))
                FeatherParser.REM -> push(Remainder(a, b, source, position))
                else -> {
                    reportError(FeatherException.unexpectedOperator(position, op.text))
                    push(a)
                }
            }
        }

        override fun exitPowerExpression(ctx: FeatherParser.PowerExpressionContext) {
            val op = ctx.bop ?: return
            val position = toPosition(op)

            val b = pop() ?: return
            val a = pop() ?: return

            when (op.type) {
                FeatherParser.POWER -> push(Power(a, b, source, position))

                else -> {
                    reportError(FeatherException.unexpectedOperator(position, op.text))
                    push(a)
                }
            }
        }

        // ***** Boolean Operators *****

        override fun exitDisjunction(ctx: FeatherParser.DisjunctionContext) {

            val op = ctx.bop ?: return
            val position = toPosition(op)

            val b = pop() ?: return
            val a = pop() ?: return

            when (op.type) {
                FeatherParser.DISJ -> push(Or(a, b, source, position))
                else -> {
                    reportError(FeatherException.unexpectedOperator(position, op.text))
                    push(a)
                }
            }
        }

        override fun exitConjunction(ctx: FeatherParser.ConjunctionContext) {

            val op = ctx.bop ?: return
            val position = toPosition(op)

            val b = pop() ?: return
            val a = pop() ?: return

            when (op.type) {
                FeatherParser.CONJ -> push(And(a, b, source, position))
                else -> {
                    reportError(FeatherException.unexpectedOperator(position, op.text))
                    push(a)
                }
            }
        }

        // ***** Other operators : Range, Elvis, is !is, as, as? *****

        override fun exitNamedInfix(ctx: FeatherParser.NamedInfixContext) {

            val op = ctx.bop ?: return
            val position = toPosition(op)

            val b = pop() ?: return
            val a = pop() ?: return

            when (op.type) {
                FeatherParser.IS -> push(IsInstanceOf(a, b, position))
                FeatherParser.NOT_IS -> push(Not(IsInstanceOf(a, b, position), source, position))
                else -> {
                    reportError(FeatherException.unexpectedOperator(position, op.text))
                    push(a)
                }
            }
        }

        override fun exitRangeExpression(ctx: FeatherParser.RangeExpressionContext) {
            val position = toPosition(ctx)

            val b = pop() ?: return
            val a = pop() ?: return

            push(Range(a, b, source, position))
        }

        override fun exitMapToExpression(ctx: FeatherParser.MapToExpressionContext) {
            val b = pop() ?: return
            val a = pop() ?: return

            push(MapTo(a, b))
        }

        override fun exitTypeRHS(ctx: FeatherParser.TypeRHSContext) {
            val op = ctx.bop ?: return
            val position = toPosition(op)

            val b = pop() ?: return
            val a = pop() ?: return

            when (op.type) {
                FeatherParser.AS -> push(As(a, b, position))
                FeatherParser.AS_SAFE -> push(AsSafe(a, b, position))

                else -> {
                    reportError(FeatherException.unexpectedOperator(position, op.text))
                    push(a)
                }
            }
        }

        override fun exitElvisExpression(ctx: FeatherParser.ElvisExpressionContext) {
            val op = ctx.bop ?: return
            val position = toPosition(op)

            val b = pop() ?: return
            val a = pop() ?: return

            when (op.type) {
                FeatherParser.ELVIS -> push(Elvis(a, b, position))

                else -> {
                    reportError(FeatherException.unexpectedOperator(position, op.text))
                    push(a)
                }
            }
        }

        override fun exitArrayAccess(ctx: FeatherParser.ArrayAccessContext) {
            val index = pop() ?: return
            val a = pop() ?: return
            push(ArrayAccess(a, index, source, toPosition(ctx)))
        }

        // Special constructs : with, apply

        override fun exitReceiverDescription(ctx: FeatherParser.ReceiverDescriptionContext) {

            val subject = pop() ?: return
            val extraBlock = Block(block, "receiver")
            val rse = when (ctx.action?.text) {
                "apply" -> Apply(subject, extraBlock)
                else -> null
            }
            if (rse == null) {
                reportError(FeatherException.unexpectedOperator(toPosition(ctx), ctx.action?.text ?: ""))
                receiverStack.push(This(currentClass!!)) // A dummy, so that pop() won't cause a problem.
            } else {
                push(rse)
                receiverStack.push(rse.receiver)
            }
            block = extraBlock
        }

        override fun exitReceiverAction(ctx: FeatherParser.ReceiverActionContext) {
            block = block.parent!!
            receiverStack.pop()
        }

        override fun exitWithDescription(ctx: FeatherParser.WithDescriptionContext) {
            val subject = pop() ?: return
            val extraBlock = Block(block, "with")
            val w = With(subject, extraBlock)
            push(w)
            receiverStack.push(w.receiver)

            block = extraBlock
        }

        override fun exitWith(ctx: FeatherParser.WithContext?) {
            block = block.parent!!
            receiverStack.pop()
        }

        // ***** String Literals *****

        private fun exitStringPart(stackEntry: StackEntry) {
            (peek() as StringBuilderStackEntry).append(stackEntry)
        }

        /**
         * If ref==null, then this is plain text, otherwise it is in the form $identifier
         */
        private fun exitStringContent(text: String, ref: Token?, position: FeatherPosition) {
            if (ref == null) {
                exitStringPart(StringConstant(unescapeString(text)))
            } else {
                val id = ref.text.substring(1)
                val lv = block.findLocalVariable(id)
                if (lv == null) {
                    push(Identifier(id, receiversAsList(), currentClass, block, source, position))
                } else {
                    push(LocalVariableStackEntry(lv, position))
                }
                exitStringPart(pop() ?: return)
            }
        }

        override fun enterLineStringLiteral(ctx: FeatherParser.LineStringLiteralContext) {
            push(StringBuilderStackEntry())
        }

        override fun enterMultiLineStringLiteral(ctx: FeatherParser.MultiLineStringLiteralContext) {
            push(StringBuilderStackEntry())
        }

        override fun exitLineStringContent(ctx: FeatherParser.LineStringContentContext) {
            exitStringContent(ctx.text, ctx.ref, toPosition(ctx))
        }

        override fun exitLineStringExpression(ctx: FeatherParser.LineStringExpressionContext) {
            exitStringPart(pop() ?: return)
        }

        override fun exitMultiLineStringContent(ctx: FeatherParser.MultiLineStringContentContext) {
            exitStringContent(ctx.text, ctx.ref, toPosition(ctx))
        }

        override fun exitMultiLineStringExpression(ctx: FeatherParser.MultiLineStringExpressionContext) {
            exitStringPart(pop() ?: return)
        }

        override fun exitMultiLineStringQuote(ctx: FeatherParser.MultiLineStringQuoteContext) {
            // Due to the way double quotes are handled in the parser, double quotes are parsed as
            // separate symbols, so we need to add them here.
            exitStringContent(ctx.text, null, toPosition(ctx))
        }

        override fun exitMultiLineStringLiteral(ctx: FeatherParser.MultiLineStringLiteralContext) {
            // Due to the way double quotes are handled in the parser, the close may actually have extra
            // double quotes, which need to be added to the text.
            val extraQuotes = ctx.TRIPLE_QUOTE_CLOSE().text.length - 3
            if (extraQuotes > 0) {
                exitStringContent("\"".repeat(extraQuotes), null, toPosition(ctx))
            }
        }

        // ***** Commands *****

        override fun enterCommandLiteral(ctx: FeatherParser.CommandLiteralContext) {
            push(StringBuilderStackEntry())
        }

        override fun exitCommandLiteral(ctx: FeatherParser.CommandLiteralContext) {
            val commandLine = pop() as StringBuilderStackEntry
            val position = toPosition(ctx.COMMAND_OPEN())
            if (!configuration.allowCommands) {
                reportError(FeatherException.commandsAreDisabled(position))
                push(NullLiteral())
            } else {
                push(CommandStackEntry(commandLine, true, position))
            }
        }

        override fun exitCommandExpression(ctx: FeatherParser.CommandExpressionContext) {
            exitStringPart(pop() ?: return)
        }

        override fun exitCommandContent(ctx: FeatherParser.CommandContentContext) {
            exitStringContent(ctx.text, ctx.ref, toPosition(ctx))
        }

        // ***** Helper methods *****

        /**
         * Pushes an entry onto the current [Block]'s stack.
         * NOTE. This is a compile-time stack, and has nothing to do with the Java runtime's stack.
         */
        fun push(entry: StackEntry) {
            block.stack.add(entry)
        }

        /**
         * Pops the last entry off of the current [Block]'s stack.
         * NOTE. This is a compile-time stack, and has nothing to do with the Java runtime's stack.
         */
        fun pop(): StackEntry? {
            if (block.stack.isEmpty()) return null
            return block.stack.removeAt(block.stack.size - 1)
        }

        fun peek(): StackEntry = block.stack.last()

        fun receiversAsList(): List<Receiver> {
            return if (receiverStack.empty()) {
                emptyList()
            } else if (receiverStack.size == 1) {
                listOf(receiverStack.peek())
            } else {
                receiverStack.toList().reversed()
            }
        }
    }

    /**
     * Rather than throwing an error, use this method to report the error.
     * This allows the compiler to spit out LOTS of errors if there are many.
     *  If we are using this from a command line, we can use [CompilerConfiguration.errorHandler]
     *  to print the errors as they occur.
     */
    fun reportError(exception: FeatherException) {
        errors.add(exception)
        configuration.errorHandler(exception.pos, exception.message ?: "")
    }

    /**
     * If there are errors, from [reportError], throw an exception.
     * NOTE. at a later date, we can introduce WARNINGS, so we need to filter out warnings.
     */
    fun throwAnyErrors() {
        if (errors.isNotEmpty()) {
            throw CompilationFailed(errors)
        }
    }

    internal fun toPosition(source: Source, token: Token?): FeatherPosition {
        return if (token == null) {
            FeatherPosition(source)
        } else {
            FeatherPosition(source.source, token.line - 1, token.charPositionInLine, configuration.debug)
        }
    }

    internal fun toPosition(source: Source, ctx: ParserRuleContext?) = toPosition(source, ctx?.start)
    internal fun toPosition(source: Source, node: TerminalNode?) = toPosition(source, node?.symbol)

}

