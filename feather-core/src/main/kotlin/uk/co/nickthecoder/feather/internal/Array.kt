package uk.co.nickthecoder.feather.internal

import java.lang.reflect.Type

/**
 * This interface is used as part of Feather's Type meta-data. No classes implement this interface.
 *
 * Java primitive types are tricky to deal with, because it is impossible to create a Type for
 * an array of a Feather (MetaDataClass). Array types can only be created for compiled classes,
 * but we need to have type information *while* the code is being compiles.
 *
 * So instead, internally, array types use Array<T>. For example, an array of Strings :
 *
 *  * In java code : String[]
 *  * In Feather code : Array&lt;String>
 *  * Internal Type representation : ParameterisedTypeImplementation( arrayClass, listof(String::class.java) )
 *
 * Multiple dimensional arrays, use nesting :
 *
 *  * Array&lt;Array&lt;String>>
 *
 * Normally, parameterised types cannot use primitives. For example, a list of integers, could not use the int
 * primitive, and instead would have to use the "boxed" version Integer (which is a class, not a primitive).
 * This is NOT the case for Array though, we CAN use primitive types for &ltT>, so that we can represent
 * arrays of primitives.
 *
 * * In Java code : int[]
 * * In Feather code : Array<int>
 * * Internal Type representation : ParameterisedTypeImplementation( arrayClass, listof(intPrimitive) )
 *
 * During compilation, array types use this representation of array types, and not the "normal" one.
 * This is true, even for method calls and fields of Java classes imported into a feather script.
 *
 * Feather arrays are nicer to use than in Java, because they feel like normal classes.
 * For example, to get the size of an array, we use the size() method. Arrays and Lists feel similar in feather.
 * Java arrays are really janky, because performing operations on them is like no other class,
 * there are no "normal" methods, and instead you have to use static methods in java.util.Arrays.
 * Feather isn't so janky, the following methods are available, as if arrays were normal classes :
 * [sort], [hashCode], [toString]
 * Each of these use the static methods in java.util.Arrays behind the scenes.
 *
 * Note. the declaration of these methods is NOT required to be here, they are merely documentation.
 * (remember, there will never be an instance of this type - it is only used as meta-data during the
 * compilation process).
 *
 * Note, there are extension functions for Array in the Feather runtime.
 */
internal interface Array<T> {

    // Implemented using JVM's length property of the array
    fun size(): Int

    // The following are implemented using java.util.Arrays static methods of the same name
    fun sort()

    /**
     * A list of fixed size, backed by the original array.
     * This is not supported for arrays with primitive element types.
     *
     * NOTE, there is also an extension toList() in the Feather runtime, which returns a COPY
     * (not backed by the original array), whose size can also change.
     */
    fun asList(): List<T>

    override fun hashCode(): Int

    override fun toString(): String
}

internal fun arrayType(elementType: Type): Type {
    return ParameterizedTypeImplementation(Array::class.java, arrayOf(elementType))
}
