package uk.co.nickthecoder.feather.internal.special

import uk.co.nickthecoder.feather.FeatherException
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.Source
import uk.co.nickthecoder.feather.internal.*
import java.lang.reflect.Type

/**
 * During the parsing phase, we may not have enough information to create the [FunctionStackEntry],
 * because it may refer to functions in scripts which have not been parsed yet.
 * So we keep enough information here, and look for the actual function in the code-generation phase.
 *
 * [owner] is either null, or the current class being compiled, when using the syntax :
 *
 *      ::funcName(...)
 *
 * and is the Type for className, when using :
 *
 *      className::funcName(...)
 *
 */
internal class LazyFunctionStackEntry private constructor(
    private val ownerType: Type?,
    private val ownerStackEntry: StackEntry?,
    private val currentClass: Type?,
    private val functionName: String,
    private val types: List<Type>?, // If null, then we look for methods with that name with ANY signature
    private val source: Source,
    private val position: FeatherPosition

) : LazyStackEntry() {

    constructor(
        ownerStackEntry: StackEntry,
        functionName: String,
        types: List<Type>?,
        source: Source,
        position: FeatherPosition
    ) :
            this(null, ownerStackEntry, null, functionName, types, source, position)

    constructor(
        ownerType: Type?,
        currentClass: Type?,
        functionName: String,
        types: List<Type>?,
        source: Source,
        position: FeatherPosition
    ) :
            this(ownerType, null, currentClass, functionName, types, source, position)

    override fun createActual(): FunctionStackEntry {

        val owner = ownerType?.resolve() ?: ownerStackEntry?.resolvedType()
        val currentClass = currentClass?.resolve()

        if (owner != null) {
            // The owning class was explicit i.e. className::functionName
            createFunction(owner, functionName, types, position)?.let {
                return it
            }

        }

        if (owner == null) {

            // Is the function part of the class where we found ::functionName?
            if (currentClass!= null) {
                createFunction(currentClass, functionName, types, position)?.let {
                    return it
                }
            }

            // Now look for imported static methods.
            source.findStaticAliases(functionName).forEach { (klass, _) ->
                createFunction(klass, functionName, types, position)?.let {
                    return it
                }
            }

        }

        throw FeatherException.noSuchMethod(position, owner, functionName, types ?: emptyList())
    }

}


internal fun createFunction(
    subjectType: Type,
    name: String,
    unresolvedParameterTypes: List<Type>?,  // If null, then we need to look for ANY methods of the given name
    position: FeatherPosition
): FunctionStackEntry? {

    if (unresolvedParameterTypes == null) {

        val methods = subjectType.getMethods(name)
        return when (methods.size) {
            0 -> {
                null
            }
            1 -> {
                val method = methods.first()
                FunctionStackEntry(method)
            }
            else -> {
                throw FeatherException.ambiguousMethod(position, subjectType, name)
            }
        }

    } else {

        subjectType.findMethod(name, emptyList(), unresolvedParameterTypes)?.let { method ->
            if (method.typeParameters().isNotEmpty()) {
                throw FeatherException.mismatchedTypeParameters(position, name, method.typeParameters().size, 0)
            }
            return FunctionStackEntry(method)
        }
    }

    return null
}
