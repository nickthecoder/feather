package uk.co.nickthecoder.feather.internal.special

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.internal.StackEntry
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.objectClass
import java.lang.reflect.Type
import java.util.*

/**
 * "fake" non-static method for java arrays, using the java.reflect.Arrays static methods.
 *
 * This allows code such as : myArray.toString() and myArray.hashCode() etc.
 * Instead of the Janky Java equivalent Arrays.toString(myArray) etc.
 */
internal class ArraySpecialMethod(
    private val array: StackEntry,
    /**
     * Must be one of zero-argument method names of java.util.Arrays
     */
    private val methodName: String,
    /**
     * Must be the same as the return type from Arrays.<methodName>()
     */
    private val returnType: Type,
    private val position: FeatherPosition
) : StackEntry {

    override fun type(): Type {
        return returnType
    }

    override fun compile(mv: MethodVisitor) {
        mv.addLineNumber(position)

        val elementType = array.resolvedType().shallowestElementTypeOfArray()
        val generalElementType = if (elementType.isPrimitive()) elementType else objectClass

        val asmMethodDescription = "([${generalElementType.asmDescriptor()})${returnType.asmDescriptor()}"

        array.compile(mv)
        mv.visitMethodInsn(
            Opcodes.INVOKESTATIC,
            Arrays::class.java.asmName(),
            methodName,
            asmMethodDescription,
            false
        )

    }
}
