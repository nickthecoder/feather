package uk.co.nickthecoder.feather.internal.special

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes.*
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.IntConstant
import uk.co.nickthecoder.feather.internal.StackEntry
import uk.co.nickthecoder.feather.StringConstant
import uk.co.nickthecoder.feather.internal.asmMethodDescription
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.booleanPrimitive
import uk.co.nickthecoder.feather.internal.stringClass
import uk.co.nickthecoder.feather.internal.voidPrimitive
import uk.co.nickthecoder.feather.runtime.command.Command
import java.lang.reflect.Type

internal class CommandStackEntry(
    val commandLine: StringBuilderStackEntry,
    val inheritIO: Boolean,
    val position: FeatherPosition
) : StackEntry {

    override fun type(): Type = Command::class.java

    override fun compile(mv: MethodVisitor) {

        mv.visitTypeInsn(NEW, Command::class.java.asmName())
        mv.visitInsn(DUP)
        IntConstant(commandLine.toConcat.size).compile(mv)
        mv.visitTypeInsn(ANEWARRAY, Command.CommandPart::class.java.asmName())

        commandLine.toConcat.forEachIndexed { index, part ->
            mv.visitInsn(DUP)
            IntConstant(index).compile(mv)
            mv.visitTypeInsn(NEW, Command.CommandPart::class.java.asmName())
            mv.visitInsn(DUP)
            part.compile(mv)
            val partType = part.resolvedType()
            if (partType != stringClass) {
                // Call toString on the object.
                if (partType.isPrimitive()) {
                    mv.visitMethodInsn(
                        INVOKESTATIC,
                        partType.nonPrimitive().asmName(),
                        "toString",
                        "(${partType.asmDescriptor()})${stringClass.asmDescriptor()}",
                        false
                    )
                } else {
                    mv.visitMethodInsn(
                        INVOKEVIRTUAL,
                        Object::class.java.asmName(),
                        "toString",
                        asmMethodDescription(emptyList(), stringClass),
                        false
                    )
                }
            }
            if (part is StringConstant) {
                mv.visitInsn(ICONST_1)
            } else {
                mv.visitInsn(ICONST_0)
            }
            mv.visitMethodInsn(
                INVOKESPECIAL,
                Command.CommandPart::class.java.asmName(),
                "<init>",
                asmMethodDescription(listOf(stringClass, booleanPrimitive), voidPrimitive),
                false
            )
            mv.visitInsn(AASTORE)

        }
        mv.visitMethodInsn(
            INVOKESPECIAL,
            Command::class.java.asmName(),
            "<init>",
            "([L" + Command.CommandPart::class.java.name.replace('.', '/') + ";)V",
            false
        )
    }

}
