package uk.co.nickthecoder.feather.internal

import java.lang.reflect.Type

/**
 */
internal abstract class CachedTypeStackEntry : StackEntry {

    private var type: Type? = null

    private var resolving = false

    final override fun type() = type ?: actualType().resolve().apply { type = this }

    abstract fun actualType(): Type

}
