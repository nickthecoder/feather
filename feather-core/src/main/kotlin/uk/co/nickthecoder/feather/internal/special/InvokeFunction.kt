package uk.co.nickthecoder.feather.internal.special

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.FeatherException
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.keywords.As
import uk.co.nickthecoder.feather.reflect.Function
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

/**
 * Takes a Function, and calls its invoke method.
 * This isn't optimal, because each argument has to be placed in an array, and that array passed as
 * a vararg.
 *
 * It would be more efficient if we interrogated the Function object, put the curried arguments on the stack,
 * then placed each of [args] on the stack, and called the method directly without calling [Function.invoke].
 *
 * This would also negate the need to box each primitive argument.
 * NOTE, if I do choose to do it that way, then we would also need Function's type parameters to use primitives.
 */
internal class InvokeFunction(
    private val functionSE: StackEntry,
    private val args: List<StackEntry>,
    private val position: FeatherPosition
) : CachedTypeStackEntry() {

    override fun actualType() : Type {
        val functionType = functionSE.resolvedType()
        if (functionType.isFunctionType() && functionType is ParameterizedType) {
            return functionType.actualTypeArguments.last()
        } else {
            throw FeatherException.expectedAFunction(position, functionType)
        }
    }
    override fun compile(mv: MethodVisitor) {
        val returnType = resolvedType()

        functionSE.compile(mv)
        val argsAsArray = ArrayOf(objectClass, args, position)
        argsAsArray.compile(mv)

        mv.visitMethodInsn(
            Opcodes.INVOKEVIRTUAL,
            functionClass.asmName(),
            "invoke",
            asmMethodDescription(listOf(argsAsArray.resolvedType()), objectClass),
            false
        )

        As.cast( mv, objectClass, returnType.nonPrimitive(), position )
        if ( returnType.isPrimitive() ) {
            As.cast( mv, returnType.nonPrimitive(), returnType, position )
        }

    }
}
