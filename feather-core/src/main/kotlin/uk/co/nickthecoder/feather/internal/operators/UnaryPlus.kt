package uk.co.nickthecoder.feather.internal.operators

import org.objectweb.asm.MethodVisitor
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.Source
import uk.co.nickthecoder.feather.internal.StackEntry


internal class UnaryPlus(
    a: StackEntry,
    source : Source,
    position: FeatherPosition
) : UnaryMathOperator(a, "-", "unaryPlus", source, position) {

    override fun compileApply(mv: MethodVisitor) {
        // Do nothing
    }
}
