package uk.co.nickthecoder.feather.internal.keywords

import org.objectweb.asm.Label
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.FeatherException
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.operators.Not
import uk.co.nickthecoder.feather.internal.operators.comparisons.ConditionalStackEntry
import uk.co.nickthecoder.feather.internal.operators.comparisons.IsNotTrue
import uk.co.nickthecoder.feather.internal.operators.comparisons.IsTrue

internal class LazyWhile(
    private val condition: StackEntry,
    private val whileBody: StackEntry,
    private val continueLabel: Label,
    private val breakLabel: Label,
    private val throwWhenInterrupted: Boolean,
    private val position: FeatherPosition
) : LazyStackEntry() {

    override fun createActual(): StackEntry {

        val cse = createConditionalStackEntry(condition, "while", position)

        if (condition.resolvedType() !== booleanPrimitive) {
            throw FeatherException.expectedBoolean(position, condition.resolvedType())
        }

        return While(cse, whileBody, continueLabel, breakLabel, throwWhenInterrupted)

    }

}

internal fun createConditionalStackEntry(
    expression: StackEntry,
    operatorName: String,
    position: FeatherPosition
): ConditionalStackEntry {
    return if (expression is ConditionalStackEntry) {
        expression
    } else {
        if (expression is Not) {
            ConditionalStackEntry(
                expression.a, DoNothingStackEntry(), operatorName, IsNotTrue(), IsTrue(), position
            )
        } else {
            ConditionalStackEntry(
                expression, DoNothingStackEntry(), operatorName, IsTrue(), IsNotTrue(), position
            )
        }
    }
}

internal class While(
    val condition: ConditionalStackEntry,
    val whileBody: StackEntry,
    val continueLabel: Label,
    val breakLabel: Label,
    val throwWhenInterrupted: Boolean

) : StackEntry {

    override fun type() = voidPrimitive

    override fun compile(mv: MethodVisitor) {
        mv.visitLabel(continueLabel)

        // If we want to automatically throw an InterruptedException when Thread.interrupted(),
        // then insert that code before the condition is tested, so that it is checked each time
        // through the loop.
        if (throwWhenInterrupted) {
            compileCheckInterrupted(mv)
        }

        condition.compileComparison(mv)

        whileBody.compile(mv)
        popUnusedValue(mv, whileBody.resolvedType())

        mv.visitJumpInsn(Opcodes.GOTO, continueLabel)
        mv.visitLabel(condition.notEqualLabel)
        mv.visitLabel(breakLabel)
    }

    companion object {
        fun compileCheckInterrupted(mv: MethodVisitor) {

            // Thread.interrupted()
            mv.visitMethodInsn(
                    Opcodes.INVOKESTATIC,
                    Thread::class.java.asmName(),
                    "interrupted",
                    asmMethodDescription(emptyList(), booleanPrimitive),
                    false
            )

            // if ( ! interrupted ) skip to the end
            // This label is added at the end, so that "normal" flow jumps over the throw InterruptedException()
            val notInterruptedLabel = Label()
            mv.visitJumpInsn(Opcodes.IFEQ, notInterruptedLabel)

            // new InterruptedException()
            mv.visitTypeInsn(Opcodes.NEW, InterruptedException::class.java.asmName())
            mv.visitInsn(Opcodes.DUP)
            mv.visitMethodInsn(
                    Opcodes.INVOKESPECIAL,
                    InterruptedException::class.java.asmName(),
                    "<init>",
                    asmMethodDescription(emptyList(), voidPrimitive),
                    false
            )

            // throw(it)
            mv.visitInsn(Opcodes.ATHROW)

            // End. Will not get here if Thread.interrupted was true
            mv.visitLabel(notInterruptedLabel)
        }
    }
}
