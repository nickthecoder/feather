package uk.co.nickthecoder.feather.internal

import org.objectweb.asm.MethodVisitor
import uk.co.nickthecoder.feather.internal.keywords.Receiver
import uk.co.nickthecoder.feather.internal.operators.Decrement
import uk.co.nickthecoder.feather.internal.operators.Increment
import java.lang.reflect.Type
import java.util.*


internal interface StackEntry {

    /**
     * Used to determine if a RETURN should be automatically appended to the end of a void method.
     * Return and Throw are both terminating. Multi-branched instructions, such as "If" are terminating
     * if all branches are terminating.
     */
    fun isTerminating() = false

    fun resolvedType() = type().resolve()

    fun type(): Type


    fun compile(mv: MethodVisitor)

    fun compileAutoCast(mv: MethodVisitor, to: Type) {
        compile(mv)
        autoCast(mv, resolvedType(), to)
    }

}

/**
 * StackEntries, which usually leave a result on the JVM stack, but which are not always needed.
 * Normally, a StackEntry will add its result to the stack, and then [BlockStackEntry] will
 * immediately pop that value off, if it isn't used.
 *
 * To optimise this, a StackEntry can implement this interface, and if [compile]'s ignoreResult == true,
 * then it does NOT add its result to the stack, and [BlockStackEntry] will not need to include the "pop" either.
 */
internal interface StackEntryWithOptionalResult : StackEntry {

    fun compile(mv: MethodVisitor, includeResult: Boolean)
}


/**
 * It seems that we cannot put values onto the Java stack, then branch, and then return.
 * Therefore statements such as return "return if ( foo ) 1 else 2" are troublesome.
 * Therefore we cannot create a Return StackEntry whose value is an If (or a when etc).
 * So instead, we put a return at the end of each branch.
 * Note, this must be done recursively (i.e. a nested set of ifs must have a return at the end
 * of each one.
 */
internal interface MultipleBranches {
    // TODO Do we really need this? Perhaps the problem that I was facing was due incorrectly
    // adding a RETURN at the end of a method.
    fun returnFromEveryBranch(method: MetaDataMethod): StackEntry
}

class DoNothingStackEntry : StackEntry {
    override fun type() = voidPrimitive
    override fun compile(mv: MethodVisitor) {
    }
}

/**
 * For example, if we have a variable foo of type List (which is an Interface), and we call toString on it,
 * List does NOT have a toString method, so we must wrap foo's stack entry with this, so that the [resolvedType]
 * will return Object, and not List (so that toString will work).
 *
 * Only use this AFTER we have checked the interface's methods.
 */
internal class ToObjectStackEntry(val wrapped: StackEntry) : StackEntry {
    override fun compile(mv: MethodVisitor) {
        wrapped.compile(mv)
    }

    override fun type() = objectClass
}

internal class SimpleStackEntry(val type: Type, val asm: (mv: MethodVisitor) -> Unit)
    : StackEntry {

    override fun type() = type

    override fun compile(mv: MethodVisitor) {
        asm(mv)
    }

}

/**
 * Used for fields, local variables, arrays and lists.
 * [compile] is used when using the value of the variable/field
 * [compileSet] is used when assigning a value to the variable/field.
 * [adjust] is used when using the likes of ++ and --
 *
 * [isFinal] is true if the this is immutable (and therefore, we cannot call [compileSet] or [adjust])
 *
 * Adjustments are only made on primitives, but we also support Lists, where the values will be boxed
 * (as Integer objects etc).
 */
internal interface AssignableStackEntry : StackEntry {

    fun compileSet(value: StackEntry, mv: MethodVisitor)

    /**
     * Used by [Increment], [Decrement] etc.
     * Before this call, assume the stack is empty, get the value of the field/local/array etc,
     * make the adjustment.
     * If [includeNewValue] is true, the stack should end up with the new value as the only stack entry.
     * If [includeOldValue] is true, the stack should end up with the old value.
     * If neither, then the stack should be empty. We NEVER call it with both set.
     *
     * I chose to use two booleans rather the one, because at some point in the future,
     * I may call with both false, so that the "spare", unused value does not need to be popped.
     * Expressions such as :
     *
     *      count ++
     *
     * currently returns the result, and then throws it away (because it isn't being used).
     */
    fun adjust(mv: MethodVisitor, includeOldValue : Boolean, includeNewValue : Boolean, adjustment: (MethodVisitor) -> Unit)

    fun isFinal(): Boolean
}

internal interface ReceiverStackEntry : StackEntry {
    val receiver: Receiver
}
