package uk.co.nickthecoder.feather.internal

import org.objectweb.asm.ClassWriter
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.Source
import uk.co.nickthecoder.feather.internal.special.FieldWrapper
import java.lang.reflect.Type


/**
 * Created during the first pass when a class/interface is defined.
 */
internal class MetaDataType(

    val source: Source, // TODO Replace this with a FeatherPosition (so that exceptions can be thrown from compileClass)
    /**
     * The fully qualified name of the class/interface
     */
    val name: String,
    val isClass: Boolean,
    val isAbstract: Boolean,
    val isEnum: Boolean,
    val isFinal: Boolean,
    val position: FeatherPosition

) : Type {

    /**
     * During the parsing phase, this is set to the first type in the "extends/implements" list,
     * with a constructor call (i.e. with brackets).
     */
    var extendsType: Type? = null

    /**
     * If the extends/implements list did not include a constructor call, then maybe the first
     * in the implements list is a class, with a default constructor (feather allows the default constructor call
     * to be omitted).
     */
    val maybeExtends: Type?
        get() = extendsType ?: implementsTypes.firstOrNull()

    /**
     * A list of all interfaces AND possibly a class that this class implements/extends.
     * So if you are only interested in the interfaces, use :
     *
     * implementsType.filter{ it.isInterface() }
     */
    val implementsTypes = mutableListOf<Type>()


    val fields = mutableListOf<MetaDataField>()

    val constructors = mutableListOf<MetaDataConstructor>()

    val methods = mutableListOf<MetaDataMethod>()


    var initBlock: Block = Block(null, "init")

    val classInitBlock = Block(null, "classInit")


    var classWriter: ClassWriter? = null

    val enums = mutableListOf<Pair<String, List<StackEntry>>>()

    fun findField(name: String): FieldWrapper? {
        val resolvedExtends = maybeExtends?.resolve()
        fields.firstOrNull { it.name == name }?.let { return FieldWrapper(it) }
        if (resolvedExtends != null) {
            resolvedExtends.findField(name)?.let { return it }
        }
        return null
    }

    fun findConstructor(argTypes: List<Type>): MetaDataConstructor? {
        return constructors.filter { it.parameterTypes.size == argTypes.size }.map {
            Pair(it, argCompatibilityScore(argTypes, it.parameterTypes, it.isVarArgs))
        }.filter { it.second != Int.MAX_VALUE }.minByOrNull { it.second }?.first
    }

    fun primaryConstructor(): MetaDataConstructor? = constructors.firstOrNull { it.isPrimary }

    override fun getTypeName() = name

    override fun toString() = name

}
