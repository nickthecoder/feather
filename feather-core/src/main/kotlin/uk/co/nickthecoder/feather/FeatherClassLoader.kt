package uk.co.nickthecoder.feather

/**
 * The simplest way to take the results of FeatherCompile, and wrap it in a ClassLoader.
 */
class FeatherClassLoader(val classes: Map<String, ByteArray>, parent: ClassLoader) : ClassLoader() {

    constructor(classes: Map<String, ByteArray>) : this(classes, getSystemClassLoader())

    override fun findClass(name: String): Class<*> {
        val bytes = classes[name] ?: return super.findClass(name)
        return defineClass(name, bytes, 0, bytes.size)
    }
}
