package uk.co.nickthecoder.feather.internal.special

import org.objectweb.asm.MethodVisitor
import uk.co.nickthecoder.feather.ClassConstant
import uk.co.nickthecoder.feather.FeatherException
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.Source
import uk.co.nickthecoder.feather.internal.*
import java.lang.reflect.Type

/**
 * When we encounter code such as : subject.methodName<typeArg0,typeArg1,...>(arg0,arg1,...)
 *
 * See also [LazyMethodCall] when there is no explicit receiver before the method name.
 *
 * The expression <code>this.foo()</code> will use this class, whereas <code>foo()</code>
 * will use [LazyMethodCall], but should result in the same [actual].
 */
internal class LazyNonStaticMethodCall(
    private val subject: StackEntry,
    private val methodName: String,
    private val args: List<StackEntry>,
    private val unresolvedTypeArgs: List<Type>, // e.g. "Bar" in this expression : subject.myFoo<Bar>(...)
    private val source: Source,
    private val position: FeatherPosition

) : CachedTypeStackEntry() {

    private var actual: StackEntry? = null

    private fun actual(): StackEntry {
        if (actual == null) {

            val resolvedTypeArgs = unresolvedTypeArgs.map { it.resolve() }
            // Look for "fake" methods of an array. size() and toString()
            if (subject.resolvedType().isArrayType() ) {
                if (args.isEmpty()) {
                    if (methodName == "size") {
                        // This is a "fake" method call, which actually uses the Java array "length" property.
                        actual = ArraySize(subject, position)
                        return actual!!
                    }
                    if (methodName == "toString") {
                        val elementType = subject.resolvedType().shallowestElementTypeOfArray()
                        if (elementType.isPrimitive()) {
                            // Calls the static method Arrays.toString(array)
                            actual = ArraySpecialMethod(subject, "toString", stringClass, position)
                        } else {
                            // Calls the static method Arrays.toString(array)
                            actual = ArraySpecialMethod(subject, "deepToString", stringClass, position)
                        }
                        return actual!!
                    }
                    if (methodName == "sort") {
                        // Calls the static method Arrays.sort(array)
                        actual = ArraySpecialMethod(subject, "sort", voidPrimitive, position)
                        return actual!!
                    }
                    if (methodName == "hashCode") {
                        val elementType = subject.resolvedType().shallowestElementTypeOfArray()
                        if (elementType.isPrimitive()) {
                            // Calls the static method Arrays.hashCode(array)
                            actual = ArraySpecialMethod(subject, "hashCode", intPrimitive, position)
                        } else {
                            // Calls the static method Arrays.hashCode(array)
                            actual = ArraySpecialMethod(subject, "deepHashCode", intPrimitive, position)
                        }
                        return actual!!
                    }
                    if (methodName == "deepHashCode" && !subject.resolvedType().shallowestElementTypeOfArray().isPrimitive()) {
                        // Calls the static method Arrays.hashCode(array)
                        actual = ArraySpecialMethod(subject, "deepHashCode", intPrimitive, position)
                        return actual!!
                    }
                    if (methodName == "asList" && !subject.resolvedType().shallowestElementTypeOfArray().isPrimitive()) {
                        actual = ArraySpecialMethod(
                            subject,
                            "asList",
                            ParameterizedTypeImplementation(
                                List::class.java, arrayOf(subject.resolvedType().shallowestElementTypeOfArray())
                            ),
                            position
                        )
                        return actual!!
                    }
                }
            }

            // Look for methods of the receiver's class
            if (subject is Identifier) {
                val inner = subject.actual()
                if (inner is ClassConstant) {
                    actual = createStaticMethodCall(inner.constantValue, methodName, args, resolvedTypeArgs, source, position)
                } else {
                    actual = createNonStaticMethodCall(inner, methodName, args, resolvedTypeArgs, false, source, position)
                }
            } else if (subject is ClassConstant) {
                actual = createStaticMethodCall(subject.constantValue, methodName, args, resolvedTypeArgs, source, position)
            } else {
                actual = createNonStaticMethodCall(subject, methodName, args, resolvedTypeArgs, false, source, position)
            }
        }

        return actual ?: throw FeatherException.noSuchMethod(position, subject.resolvedType(), methodName, args.map { it.type() })
    }

    override fun actualType() = actual().type()

    override fun compile(mv: MethodVisitor) {
        mv.addLineNumber(position)
        actual().compile(mv)
    }
}
