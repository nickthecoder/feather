package uk.co.nickthecoder.feather.internal.special

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.internal.*
import java.lang.reflect.Type

internal class StaticMethodCall(
    val subjectType: Type,
    val args: List<StackEntry>,
    val typeArgs: List<Type>,
    val method: MethodWrapper

) : CachedTypeStackEntry() {

    override fun actualType() = resolveGenericType(method, typeArgs, method.genericReturnType())

    override fun compile(mv: MethodVisitor) {

        // NOTE, we do NOT use resolveGenericType() to find the generic type, because we need the signature of the
        // method for runtime. i.e. the Type-Erasure version, such as Object.
        val paramTypes = method.parameterTypes()
        val returnType = method.returnType()

        pushArgs(mv, args, paramTypes, method.isVarArgs())

        mv.visitMethodInsn(
            Opcodes.INVOKESTATIC,
            subjectType.asmName(),
            method.name(),
            asmMethodDescription(paramTypes, returnType),
            subjectType.isInterface()
        )
        val genericReturnType = resolveGenericType(method, typeArgs,method.genericReturnType())
        if (genericReturnType != returnType) {
            mv.visitTypeInsn(Opcodes.CHECKCAST, genericReturnType.asmName())
        }
    }

    override fun toString() = "StaticMethodCall : $method"

}
