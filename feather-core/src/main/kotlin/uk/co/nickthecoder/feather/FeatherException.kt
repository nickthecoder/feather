package uk.co.nickthecoder.feather

import org.antlr.v4.runtime.ParserRuleContext
import org.antlr.v4.runtime.Token
import uk.co.nickthecoder.feather.internal.*
import java.io.File
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

class FeatherException(
    val pos: FeatherPosition,
    message: String
) : Exception(message) {

    override fun toString() = "$pos : $message"

    companion object {

        internal fun unexpectedTypes(position: FeatherPosition, operation: String, a: StackEntry, b: StackEntry) =
            FeatherException(
                position,
                "Cannot perform operation '$operation' on types : ${a.resolvedType().typeName} and ${b.resolvedType().typeName}"
            )

        internal fun unexpectedTypes(source: Source, token: Token, a: Type, b: Type) =
            FeatherException(
                toPosition2(source, token),
                "Cannot perform operation  ${token.text} on types : ${a.niceString()} and ${b.niceString()}"
            )

        internal fun unexpectedType(position: FeatherPosition, operation: String, a: StackEntry) =
            FeatherException(position, "Cannot perform operation $operation on type : ${a.resolvedType().niceString()}")

        internal fun unexpectedType(position: FeatherPosition, operation: String, a: Type) =
            FeatherException(position, "Cannot perform operation $operation on type : ${a.niceString()}")

        internal fun unexpectedOperator(position: FeatherPosition, op: String) =
            FeatherException(position, "Unexpected operator : $op")

        internal fun noSuchField(position: FeatherPosition, name: String, type: Type) =
            FeatherException(position, "No such field '$name' for class : ${type.niceString()}")

        internal fun unknownType(position: FeatherPosition, itemName: String) =
            FeatherException(position, "Cannot determine the type for $itemName")

        internal fun unknownIdentifier(position: FeatherPosition, name: String) =
            FeatherException(position, "Unknown identifier : $name")

        internal fun duplicateLocalVariable(position: FeatherPosition, name: String) =
            FeatherException(position, "Duplicate local variable : $name")

        internal fun ambiguousStaticImport(position: FeatherPosition, name: String, classes: List<Type>) =
            FeatherException(position, "Ambiguous Static Import: $name defined in ${classes}")


        internal fun ambiguousMethod(position: FeatherPosition, subjectType: Type?, name: String) =
            FeatherException(position, "Ambiguous function $subjectType}.name(...)")

        internal fun noSuchMethod(
            position: FeatherPosition,
            subjectType: Type?,
            name: String,
            paramTypes: Collection<Type>
        ) =
            if (subjectType != null && subjectType.isFunctionType() && name == "curry") {
                // Function has 10 type arguments, and most of them will not be relevant to the user's code,
                // so the default message is dreadful, so let's make it better.
                // The expected argument type is given in its primitive form, such as "char", not the boxed version
                // (java.lang.Character). While technically, curry takes an Object, the user will probably be using
                // primitive types, and the conversion will be done for them.
                FeatherException(
                    position,
                    "Wrong curry argument. Expected ${(subjectType as ParameterizedType).actualTypeArguments.firstOrNull()?.primitive()}"
                )
            } else if (subjectType == null ) {
                FeatherException(
                    position,
                    "No such function $name(${paramTypes.joinToString(separator = " , ") { it.niceString() }})"
                )
            } else {
                FeatherException(
                    position,
                    "No such method ${subjectType.niceString()}.$name(${paramTypes.joinToString(separator = " , ") { it.niceString() }})"
                )
            }

        internal fun noSuchConstructor(position: FeatherPosition, subjectType: Type, paramTypes: Collection<Type>) =
            FeatherException(
                position,
                "No constructor ${subjectType.niceString()}(${paramTypes.joinToString(separator = " , ") { it.niceString() }})"
            )

        internal fun expectedStaticMethod(
            position: FeatherPosition,
            subjectType: Type,
            name: String,
            paramTypes: Collection<Type>
        ) =
            FeatherException(
                position,
                "Expected a static method ${subjectType.niceString()}.$name(${paramTypes.joinToString(separator = " , ") { it.niceString() }})"
            )

        internal fun mismatchedTypeParameters(position: FeatherPosition, name: String, actual: Int, required: Int) =
            FeatherException(position, "Function $name has $actual type parameters, but $required were given.")

        internal fun unexpectedStaticMethod(
            position: FeatherPosition,
            subjectType: Type,
            name: String,
            paramTypes: Collection<Type>
        ) =
            FeatherException(
                position,
                "Unexpected static method ${subjectType.niceString()}.$name(${paramTypes.joinToString(separator = " , ") { it.niceString() }})"
            )

        internal fun inaccessibleMethod(
            position: FeatherPosition,
            subjectType: Type,
            name: String,
            paramTypes: Collection<Type>
        ) =
            FeatherException(
                position,
                "Inaccessible method : ${subjectType.niceString()}.$name(${paramTypes.joinToString(separator = " , ") { it.niceString() }})"
            )

        internal fun cannotAccessFromStaticContext(position: FeatherPosition, name: String) =
            FeatherException(position, "Cannot access a non-static field from a static context : $name")

        internal fun cannotAccessFromStaticContext(position: FeatherPosition, subjectType: Type, name: String) =
            FeatherException(
                position,
                "Cannot access a non-static field from a static context : ${subjectType.niceString()}.$name"
            )

        internal fun unexpectedStaticField(position: FeatherPosition, subjectType: Type, name: String) =
            FeatherException(position, "Unexpected static field ${subjectType.niceString()}.$name")

        internal fun missingReturnStatement(position: FeatherPosition) =
            FeatherException(position, "Missing return statement")

        internal fun interfaceCannotExtendClass(position: FeatherPosition) =
            FeatherException(position, "An interface cannot extend a class")

        internal fun cannotExtendFromTwoClasses(position: FeatherPosition) =
            FeatherException(position, "Cannot extend from more than one class")

        internal fun cannotExtendFromTwoClasses(position: FeatherPosition, a: Type, b: Type) =
            FeatherException(
                position,
                "Cannot extend from more than one class : ${a.niceString()} and ${b.niceString()}"
            )

        internal fun interfaceCannotHaveConstructor(position: FeatherPosition) =
            FeatherException(position, "An interface cannot have a constructor")

        internal fun interfaceCannotHaveFields(position: FeatherPosition) =
            FeatherException(position, "An interface cannot contain a field")

        internal fun interfaceCannotDefineFunctions(position: FeatherPosition) =
            FeatherException(position, "An interface cannot contain a function body")

        internal fun cannotHaveAbstractMethod(position : FeatherPosition) =
            FeatherException(position, "Abstract methods are only allowed in abstract classes")

        internal fun expectedAssignable(op: String, position: FeatherPosition) =
            FeatherException(position, "Expected an assignable value on the left of '$op'")

        internal fun expectedAssignable(position: FeatherPosition) =
            FeatherException(position, "Expected an assignable value")

        internal fun expectedBoolean(position: FeatherPosition, foundType: Type) =
            FeatherException(position, "Expected a boolean value, but found : ${foundType.niceString()}")

        internal fun breakWithoutLoop(source: Source, ctx: ParserRuleContext) =
            FeatherException(toPosition2(source, ctx), "break can only occur inside a for,while or do..while loop")

        internal fun continueWithoutLoop(source: Source, ctx: ParserRuleContext) =
            FeatherException(toPosition2(source, ctx), "continue can only occur inside a for,while or do..while loop")

        internal fun varargMustBeLast(source: Source, ctx: ParserRuleContext) =
            FeatherException(toPosition2(source, ctx), "vararg parameter must be the last parameter")

        internal fun cannotUseType(source: Source, ctx: ParserRuleContext, type: Type) =
            FeatherException(toPosition2(source, ctx), "The sandbox prevents the use of type : ${type.niceString()}")

        internal fun cannotUseThisInStaticContext(position: FeatherPosition) =
            FeatherException(position, "Cannot use 'this' in a static context")

        internal fun expectedIterable(position: FeatherPosition, type: Type) =
            FeatherException(position, "Expected Iterable, but found ${type.niceString()}")

        internal fun expectedClass(position: FeatherPosition, type: Type) =
            FeatherException(position, "Expected a class name but found ${type.niceString()}")

        internal fun expectedAnObject(position: FeatherPosition, type: Type) =
            FeatherException(position, "Expected an Object but found ${type.niceString()}")

        internal fun tryWithoutCatchOrFinally(position: FeatherPosition) =
            FeatherException(position, "try must have a catch or finally")

        internal fun expectedTypeThrowable(position: FeatherPosition, type: Type) =
            FeatherException(position, "Expected type Throwable, but found ${type.niceString()}")

        internal fun cannotChange(position: FeatherPosition) =
            FeatherException(position, "The value cannot be changed")

        internal fun valMustBeInitialised(position: FeatherPosition, variableName: String) =
            FeatherException(position, "val ($variableName) must be initialised")

        internal fun returningAValueFromAVoidMethod(position: FeatherPosition) =
            FeatherException(position, "Method does not have a return value")

        internal fun returningVoidFromNonVoidMethod(position: FeatherPosition, expectedType: Type) =
            FeatherException(position, "Expected a return value of type ${expectedType.niceString()}")

        internal fun commandsAreDisabled(position: FeatherPosition) =
            FeatherException(position, "Commands are disabled")

        internal fun cannotExtendFinalClass(position: FeatherPosition, extends: Type) =
            FeatherException(position, "Cannot extend final class ${extends.niceString()}")

        internal fun wrongNumberOfTypeParameters(position: FeatherPosition, expected: Int, found: Int) =
            FeatherException(position, "Wrong number of type parameters. Expected $expected, found $found")

        internal fun expectedOneTypeArgument(position: FeatherPosition, found: List<Type>) =
            FeatherException(position, "Expected one type argument, but found " +
                    if (found.isEmpty()) "None" else found.joinToString(separator = ", ") { it.niceString() }
            )

        internal fun duplicateClassName(position: FeatherPosition, name: String) =
            FeatherException(position, "Duplicate class name : $name")

        internal fun duplicateMethod(position: FeatherPosition, methodName: String) =
            FeatherException(position, "Duplicate method name $methodName with identical parameter types")

        internal fun arraySizeMissing(position: FeatherPosition) =
            FeatherException(position, "Array size missing")

        internal fun recursiveTypeDefinition(position: FeatherPosition) =
            FeatherException(position, "Recursive type definition")

        internal fun includeNotSupported(position: FeatherPosition) =
            FeatherException(position, "include not supported")

        internal fun includeCannotBeAbsolute(position: FeatherPosition) =
            FeatherException(position, "Include paths cannot be absolute paths, nor paths containing \"..\"")

        internal fun includeNotFound(position: FeatherPosition) = FeatherException(position, "Include not found")

        internal fun includeNoScriptFile(position: FeatherPosition) =
            FeatherException(position, "Include could not determine the source code directory")

        internal fun overrideExpected(method: MetaDataMethod, position: FeatherPosition) =
            FeatherException(position, "'override' expected on method ${method.name}")

        internal fun overrideUnexpected(method: MetaDataMethod, position: FeatherPosition) =
            FeatherException(position, "'override' not expected on method ${method.name}")

        internal fun libraryUnexpected(position: FeatherPosition) =
            FeatherException(position, "library not supported")

        internal fun libraryNotFound(position: FeatherPosition, file: File) =
            FeatherException(position, "Library not found : $file")

        internal fun overrideReturnTypeMismatch(position: FeatherPosition, expected: Type, found: Type) =
            FeatherException(position, "Override return type mismatch. Expected ${expected.typeName} but found ${found.typeName}")

        internal fun incompatibleType(position: FeatherPosition, expected: Type, found: Type) =
            FeatherException(position, "Expected type ${expected.typeName} but found ${found.typeName}")

        internal fun comparisonFailed(position: FeatherPosition) =
            FeatherException(position, "Comparison failed")

        internal fun methodNotImplemented(position: FeatherPosition, methodName: String, paramTypes: List<Type>) =
            FeatherException(
                position,
                "Method not implemented : $methodName(${paramTypes.joinToString(separator = ", ")})"
            )

        internal fun expectedAFunction(position: FeatherPosition, foundType: Type) =
            FeatherException(position, "Expected a function, but found : $foundType")

        internal fun notEnum(position: FeatherPosition) =
            FeatherException(position, "Not an enum")

        internal fun constructorNotFound(position: FeatherPosition, types: List<Type>) =
            FeatherException(position, "Constructor not found. Types : ${types.joinToString(separator = ", ")}")

        internal fun cannotOverrideIsFinal(position: FeatherPosition, name: String) =
            FeatherException(position, "Cannot override $name, because it is `final`")
    }
}

private fun toPosition2(source: Source, token: Token?) =
    if (token == null) FeatherPosition(source) else FeatherPosition(source, token.line - 1, token.charPositionInLine)

private fun toPosition2(source: Source, ctx: ParserRuleContext?) = toPosition2(source, ctx?.start)
