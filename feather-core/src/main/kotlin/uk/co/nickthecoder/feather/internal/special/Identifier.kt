package uk.co.nickthecoder.feather.internal.special

import org.objectweb.asm.MethodVisitor
import uk.co.nickthecoder.feather.*
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.keywords.Receiver


internal class Identifier(
    val name: String,
    val receivers: List<Receiver>,
    val classContext: MetaDataType?,
    val block: Block,
    val source: Source,
    val position: FeatherPosition
) : StackEntry, AssignableStackEntry {

    private var actual: StackEntry? = null

    override fun isFinal() = (actual() as? AssignableStackEntry)?.isFinal() ?: true

    fun actual(): StackEntry {
        actual?.let { return it }

        // Look for a non-static field on the receivers
        for (receiver in receivers) {
            findFieldAccess(receiver, name, source, position)?.let {
                actual = it
                return it
            }
        }

        // Look for a Class name
        source.worker.resolveClass(source, name)?.let { type ->
            actual = ClassConstant(type)
            return actual!!
        }

        // Look for a static field of the current class
        classContext?.findField(name)?.let { field ->
            if (field.isStatic()) {
                actual = StaticFieldAccess(classContext, field)
                return actual!!
            }

            if (!field.isStatic()) {
                throw FeatherException.cannotAccessFromStaticContext(position, name)
            }
        }

        val staticImportedFields = source.findStaticAliases(name)

        if (staticImportedFields.size > 1) {
            throw FeatherException.ambiguousStaticImport(position, name, staticImportedFields.map { it.first })
        }
        if (staticImportedFields.size == 1) {
            val klass = staticImportedFields.first().first
            val fieldName = staticImportedFields.first().second
            klass.findField(fieldName)?.let { field ->
                actual = StaticFieldAccess(klass, field)
                return actual!!
            }
        }

        // Look for a static getter
        // e.g. the code :  window.close()
        // getWindow() may be a static import
        for ((subjectType, _) in source.findStaticAliases(getterName(name))) {
            subjectType.findMethod(getterName(name), emptyList(), emptyList())?.let { getter ->
                if (getter.isStatic()) {

                    val setter = subjectType.findMethod(setterName(name), emptyList(), listOf(getter.returnType()))
                    actual = if (setter == null || setter.isStatic()) {
                        StaticMethodCall(subjectType, emptyList(), emptyList(), getter)
                    } else {
                        StaticGetterSetter(subjectType, getter, setter)
                    }
                    return actual!!
                }
            }
        }

        throw FeatherException.unknownIdentifier(position, name)
    }

    override fun compileSet(value: StackEntry, mv: MethodVisitor) {
        val a = actual() as? AssignableStackEntry ?: throw FeatherException.expectedAssignable(position)
        a.compileSet(value, mv)
    }

    override fun adjust(
        mv: MethodVisitor,
        includeOldValue: Boolean,
        includeNewValue: Boolean,
        adjustment: (MethodVisitor) -> Unit
    ) {
        val a = actual() as? AssignableStackEntry ?: throw FeatherException.expectedAssignable(position)
        a.adjust(mv, includeOldValue, includeNewValue, adjustment)
    }

    override fun type() = actual().type()

    override fun compile(mv: MethodVisitor) {
        actual().compile(mv)
    }
}
