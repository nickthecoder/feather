package uk.co.nickthecoder.feather.internal.keywords

import org.objectweb.asm.Label
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes.*
import uk.co.nickthecoder.feather.*
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.autoCast
import uk.co.nickthecoder.feather.internal.canAutoCast
import uk.co.nickthecoder.feather.internal.isPrimitive
import uk.co.nickthecoder.feather.internal.klass
import uk.co.nickthecoder.feather.internal.special.Identifier
import java.lang.reflect.Type


internal class As(
    val a: StackEntry,
    val b: StackEntry,
    val position: FeatherPosition

) : StackEntry {

    override fun type() : Type {
        return when (b) {
            is Identifier -> b.source.worker.resolveClassOrCreate(b.source, b.name)
            is ClassConstant -> b.constantValue
            else -> throw FeatherException.expectedClass(position, b.type())
        }
    }

    override fun compile(mv: MethodVisitor) {

        val aType = a.resolvedType()
        val requiredType = resolvedType()

        a.compile(mv)

        cast(mv, aType, requiredType, position)
    }

    companion object {

        fun cast(mv: MethodVisitor, fromType: Type, toType: Type, position: FeatherPosition) {

            if (toType.isPrimitive()) {
                if (canAutoCast(fromType, toType)) {
                    autoCast(mv, fromType, toType)
                } else {
                    throw FeatherException.incompatibleType(position, toType, fromType)
                }
            } else {
                if (fromType.isPrimitive()) {
                    autoCast(mv, fromType, fromType)
                }

                mv.visitTypeInsn(CHECKCAST, toType.asmName())
            }
        }
    }
}


internal class AsSafe(
    val a: StackEntry,
    val b: StackEntry,
    val position: FeatherPosition

) : StackEntry {

    override fun type(): Type {
        return when (b) {
            is Identifier -> b.source.worker.resolveClassOrCreate(b.source, b.name)
            is ClassConstant -> b.constantValue
            else -> throw FeatherException.expectedClass(position, b.type())
        }
    }

    override fun compile(mv: MethodVisitor) {

        val aType = a.resolvedType()
        val requiredType = resolvedType()

        a.compile(mv)
        if (!requiredType.klass().isAssignableFrom(aType.klass())) {
            val skip = Label()
            mv.visitInsn(DUP)
            mv.visitTypeInsn(INSTANCEOF, requiredType.asmName())

            mv.visitJumpInsn(IFNE, skip)

            mv.visitInsn(POP)
            mv.visitInsn(ACONST_NULL)

            mv.visitLabel(skip)

            // Is this needed? Kotlin adds it, but surely it should always pass.
            // mv.visitTypeInsn(CHECKCAST, requiredType.asmName())
        }

    }
}
