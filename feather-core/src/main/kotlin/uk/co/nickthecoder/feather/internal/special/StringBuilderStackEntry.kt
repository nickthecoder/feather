package uk.co.nickthecoder.feather.internal.special

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes.*
import uk.co.nickthecoder.feather.internal.StackEntry
import uk.co.nickthecoder.feather.StringConstant
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.isPrimitive
import uk.co.nickthecoder.feather.internal.objectClass
import uk.co.nickthecoder.feather.internal.stringClass
import java.lang.reflect.Type

/**
 * A special StackEntry to help optimise concatenation of strings.
 */
internal class StringBuilderStackEntry()
    : StackEntry {

    internal val toConcat = mutableListOf<StackEntry>()

    override fun type() = stringClass

    override fun compile(mv: MethodVisitor) {

        if (toConcat.isEmpty()) {
            StringConstant("").compile(mv)

        } else if (toConcat.size == 1) {
            val item = toConcat.first()
            val itemType = item.resolvedType()

            if (itemType == stringClass) {
                // A single String is simple - needs no extra processing.
                item.compile(mv)

            } else {
                // A single item, which is not a string
                item.compile(mv)
                if (itemType.isPrimitive()) {
                    mv.visitMethodInsn(
                        INVOKESTATIC,
                        itemType.nonPrimitive().asmName(),
                        "toString",
                        "(${itemType.asmDescriptor()})${stringClass.asmDescriptor()}",
                        false
                    )
                } else {
                    mv.visitMethodInsn(
                        INVOKEVIRTUAL,
                        objectClass.asmName(),
                        "toString",
                        "()${stringClass.asmDescriptor()}",
                        false
                    )
                }
            }

        } else {

            // More than one item, use a StringBuilder.
            // TODO If there are two Objects in toConcat is it better to use Object.toString() and then String.concat rather than use a StringBuilder?
            // I think it must be, because the existing algorithm will create an extra object (the StringBuilder).
            // But AFAIK Java doesn't do it, so I think I should check before implementing this optimisation.

            mv.visitTypeInsn(NEW, StringBuilder::class.java.asmName())
            mv.visitInsn(DUP)
            mv.visitMethodInsn(INVOKESPECIAL, StringBuilder::class.java.asmName(), "<init>", "()V", false)
            for (item in toConcat) {
                item.compile(mv)
                mv.visitMethodInsn(
                    INVOKEVIRTUAL,
                    StringBuilder::class.java.asmName(),
                    "append",
                    appendDesc(item.resolvedType()),
                    false
                )
            }
            mv.visitMethodInsn(
                INVOKEVIRTUAL,
                StringBuilder::class.java.asmName(),
                "toString", "()${stringClass.asmDescriptor()}",
                false
            )
        }
    }

    fun append(item: StackEntry) {
        toConcat.add(item)
    }

    private fun appendDesc(type: Type): String {
        val argType = if (type.isPrimitive()) {
            type
        } else if (type === stringClass) {
            stringClass
        } else {
            objectClass
        }
        return "(${argType.asmDescriptor()})${StringBuilder::class.java.asmDescriptor()}"
    }

}
