package uk.co.nickthecoder.feather.internal.operators

import org.objectweb.asm.MethodVisitor
import uk.co.nickthecoder.feather.FeatherException
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.Source
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.special.NonStaticMethodCall
import uk.co.nickthecoder.feather.internal.special.createStaticMethodCall

/**
 * Operators += , -= , /= , *= , %=
 *
 * If there is a method with the appropriate signature (such as plusAssign( b.type ) : void ),
 * then call that method.
 * Otherwise, if [a] is an [AssignableStackEntry], then perform the operator without the assignment,
 * (using [nonAssignmentOperator], which is for example [Plus]).
 * and then assign the value to [a].
 *
 * Note, [nonAssignmentOperator] is unused if [methodName] is found.
 */
internal class AdjustAssignment(
    /**
     * This should be an [AssignableStackEntry] if [methodName] is not a method of
     */
    val a: StackEntry,
    val b: StackEntry,
    /**
     * e.g. += , -= etc. Only used for error messages!
     */
    val operatorSymbol: String,
    /**
     *   Such as plusAssign for +=
     */
    val methodName: String,
    /**
     * Such as [Plus]. Used if there is no method of a called [methodName] (with the correct signature).
     */
    val nonAssignmentOperator: StackEntry,
    val source: Source,
    val position: FeatherPosition

) : StackEntry {

    override fun type() = voidPrimitive

    override fun compile(mv: MethodVisitor) {
        val aType = a.resolvedType()
        val bType = b.resolvedType()

        // First let's check for a method on [a] named [methodName] with the correct signature
        val method = aType.findMethod(methodName, emptyList(), listOf(bType))
        if (method != null && method.returnType() == voidPrimitive && !method.isStatic()) {
            NonStaticMethodCall(a, listOf(b), emptyList(), method).compile(mv)
            return
        }

        // Now look for imported static method with [a] as the first parameter
        source.findStaticAliases(methodName).forEach { (klass, realName) ->
            createStaticMethodCall(klass, realName, listOf(a, b), emptyList(), source, position)?.let { smc ->
                smc.compile(mv)
                return
            }
        }

        if (a is AssignableStackEntry) {
            if (a.isFinal()) {
                throw FeatherException.cannotChange(position)
            }
            if (!canAutoCast(b.resolvedType(), a.resolvedType())) {
                throw FeatherException.unexpectedTypes(position, operatorSymbol, a, b)
            }
            a.compileSet(nonAssignmentOperator, mv)
        } else {
            throw FeatherException.unexpectedTypes(position, operatorSymbol, a, b)
        }
    }
}
