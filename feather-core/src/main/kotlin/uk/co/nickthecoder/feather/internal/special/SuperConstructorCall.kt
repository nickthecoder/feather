package uk.co.nickthecoder.feather.internal.special

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.FeatherException
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.StackEntry
import uk.co.nickthecoder.feather.internal.asmMethodDescription
import uk.co.nickthecoder.feather.internal.pushArgs
import uk.co.nickthecoder.feather.internal.voidPrimitive
import java.lang.reflect.Type

/**
 * Generates Bytecode to call the super class's <init> block.
 *
 * This is added to the sub-class's <init> block, before the block is compiled.
 */
internal class SuperConstructorCall(
    private val superClass: Type,
    private val args: List<StackEntry>,
    private val position: FeatherPosition

) : StackEntry {

    override fun type() = voidPrimitive

    override fun compile(mv: MethodVisitor) {
        val argTypes = args.map { it.resolvedType() }
        val resolvedSuperClass = superClass.resolve()
        val constructor = resolvedSuperClass.findConstructor(argTypes)
                ?: throw FeatherException.noSuchConstructor(position, resolvedSuperClass, argTypes)
        val paramTypes = constructor.parameterTypes()

        mv.visitVarInsn(Opcodes.ALOAD, 0) // THIS

        pushArgs(mv, args, paramTypes, constructor.isVarArgs())

        mv.visitMethodInsn(
            Opcodes.INVOKESPECIAL,
                constructor.owner().asmName(),
                "<init>",
            asmMethodDescription(paramTypes, voidPrimitive),
                false
        )
    }
}