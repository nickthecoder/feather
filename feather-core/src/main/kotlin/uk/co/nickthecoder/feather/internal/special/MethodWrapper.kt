package uk.co.nickthecoder.feather.internal.special

import uk.co.nickthecoder.feather.internal.MetaDataMethod
import uk.co.nickthecoder.feather.internal.isStatic
import uk.co.nickthecoder.feather.internal.resolveGenericType
import java.lang.reflect.Method
import java.lang.reflect.Modifier
import java.lang.reflect.Type
import java.lang.reflect.TypeVariable
import java.util.*

internal class MethodWrapper private constructor(
    val java: Method?,
    val feather: MetaDataMethod?,
    val resolvedReturnType: Type?
) {
    constructor(method: Method) : this(method, null, null)
    constructor(method: Method, resolvedReturnType: Type) : this(method, null, resolvedReturnType)
    constructor(method: MetaDataMethod) : this(null, method, null)

    fun name() = if (java == null) feather!!.name else java.name

    fun className() = if (java == null) feather!!.owner.name else java.declaringClass.name

    fun isStatic() = if (java == null) feather!!.isStatic else java.isStatic()

    fun isFinal() = if (java == null) false else Modifier.isFinal(java.modifiers)

    fun parameterTypes(): List<Type> = if (java == null) feather!!.parameterTypes else java.parameterTypes.toList()

    fun genericParameterTypes(): List<Type> =
        if (java == null) feather!!.parameterTypes else java.genericParameterTypes.toList()

    fun genericParameterTypes(subject: Type): List<Type> =
        if (java == null) feather!!.parameterTypes else java.genericParameterTypes.map {
            resolveGenericType(
                subject,
                it
            )
        }

    val modifiers
        get() = java?.modifiers ?: Modifier.PUBLIC // All feather methods are public

    fun typeParameters(): List<TypeVariable<*>> = if (java == null) emptyList() else java.typeParameters.toList()

    fun returnType() = if (java == null) feather!!.returnType() else java.returnType

    fun genericReturnType() = (if (java == null) feather!!.returnType() else java.genericReturnType)

    fun declaringClass(): Type = if (java == null) feather!!.owner else java.declaringClass

    fun isFromInterface() = if (java == null) !feather!!.owner.isClass else java.declaringClass.isInterface

    fun isAbstract() = if (java == null) feather!!.isAbstract else Modifier.isAbstract(java.modifiers)

    fun owner() = if (java == null) feather!!.owner else java.declaringClass

    fun isVarArgs() = if (java == null) feather!!.isVarArgs else java.isVarArgs

    fun parameterCount() = if (java == null) feather!!.parameterTypes.size else java.parameterCount

    override fun toString() = if (java == null) feather!!.toString() else java.toString()

    override fun equals(other: Any?): Boolean {
        if (other !is MethodWrapper) return false
        return other.java == this.java && other.feather == this.feather && other.resolvedReturnType == this.resolvedReturnType
    }

    override fun hashCode(): Int {
        return Objects.hash(java, feather, resolvedReturnType)
    }
}
