package uk.co.nickthecoder.feather.internal.keywords

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes.ALOAD
import uk.co.nickthecoder.feather.LocalVariable
import uk.co.nickthecoder.feather.internal.StackEntry
import java.lang.reflect.Type

internal interface Receiver : StackEntry {

}

internal class This(val type: Type)
    : Receiver {

    override fun type() = type

    override fun compile(mv: MethodVisitor) {
        mv.visitVarInsn(ALOAD, 0)
    }
}

internal class LocalVariableReceiver(
        val localVariable: LocalVariable
) : Receiver {
    override fun type() = localVariable.type()

    override fun compile(mv: MethodVisitor) {
        mv.visitVarInsn(ALOAD, localVariable.index)
    }
}

internal class Super(val from: This) : Receiver {

    override fun type() = from.type()

    override fun compile(mv: MethodVisitor) {
        from.compile(mv)
    }
}