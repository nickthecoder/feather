package uk.co.nickthecoder.feather.internal.special

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes.ARRAYLENGTH
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.internal.StackEntry
import uk.co.nickthecoder.feather.internal.addLineNumber
import uk.co.nickthecoder.feather.internal.intPrimitive
import java.lang.reflect.Type

/**
 * In Feather, the length/size of an array is found by calling the size() method.
 * This makes it look similar to List's size method, rather than the clunky Java .length property.
 *
 * Compiled Methods.kt specifically looks for a method call with signature array.size(), and returns this
 * stack entry, and not an actual method call.
 */
internal class ArraySize(
    private val array: StackEntry,
    private val position: FeatherPosition
) : StackEntry {

    override fun type(): Type {
        return intPrimitive
    }

    override fun compile(mv: MethodVisitor) {
        mv.addLineNumber(position)

        array.compile(mv)
        mv.visitInsn(ARRAYLENGTH)
    }

}
