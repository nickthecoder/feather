package uk.co.nickthecoder.feather.internal.special

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.isArrayType
import java.lang.reflect.Type

/**
 * Creates a java array or typed Objects, such as an array of String.
 *
 * [elementType] is the Type of objects in this array.
 * These may be arrays themselves, in which case, will need to make a multi-dimensional array.
 */
internal class ArrayOf(

    val elementType: Type,
    val args: List<StackEntry>,
    val position: FeatherPosition

) : CachedTypeStackEntry() {

    override fun actualType(): Type {
        return arrayType(elementType)
    }

    override fun compile(mv: MethodVisitor) {

        if (elementType.isArrayType()) {
            val dimensions = 1 + elementType.dimensionsOfArray()
            intConstant(mv, args.size)
            // Make all but the first dimension zero in size.
            // We will add the data later.
            for (i in 1 until dimensions) {
                intConstant(mv, 0 )
            }
            mv.visitMultiANewArrayInsn(resolvedType().asmDescriptor(), dimensions)
        } else {
            intConstant(mv, args.size)
            newArray(mv, elementType)
        }

        // arrayref


        for (i in args.indices) {
            // While iterating over each of the arguments, have two copies or the array reference
            mv.visitInsn(Opcodes.DUP)
            // arrayref, arrayref

            val arg = args[i]
            intConstant(mv, i)
            arg.compileAutoCast(mv, elementType)
            // arrayref, arrayref, index, value
            mv.visitInsn(Opcodes.AASTORE)
            // arrayref

        }
        // The array reference is now at the top of the stack.

    }
}
