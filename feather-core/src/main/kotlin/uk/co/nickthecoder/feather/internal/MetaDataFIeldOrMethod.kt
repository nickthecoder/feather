package uk.co.nickthecoder.feather.internal

import java.lang.reflect.Type


internal abstract class MetaDataFieldOrMethod(
    val owner: MetaDataType,
    val name: String,
    var type: Type, // Maybe UnresolvedType,
    val isStatic: Boolean
) {
    /**
     * The stack entry which can represents this field's initializer, or this method's body (excluding an implied RETURN)
     */
    var stackEntry: StackEntry? = null

}
