package uk.co.nickthecoder.feather.internal

import org.objectweb.asm.Label
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import org.objectweb.asm.Opcodes.*
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.StringConstant
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

/**
 * Note, this cannot handle versions prior to Java 2 (aka 1.2)
 */
internal fun asmVersion(version: Int) = 44 + version

internal fun Type.asmName(): String =
    if (this.isArrayType()) {
        "[".repeat(this.dimensionsOfArray()) + this.deepestElementTypeOfArray().asmName()
    } else if (this is Class<*>) {
        if (isPrimitive()) {
            when (this) {
                booleanPrimitive -> "Z"
                voidPrimitive -> "V"
                bytePrimitive -> "B"
                charPrimitive -> "C"
                shortPrimitive -> "S"
                intPrimitive -> "I"
                longPrimitive -> "J"
                floatPrimitive -> "F"
                doublePrimitive -> "D"
                else -> "?unknown_type?"
            }
        } else {
            this.typeName.replace('.', '/')
        }
    } else if (this is MetaDataType) {
        this.name.replace('.', '/')
    } else if (this is ParameterizedType && this.rawType == arrayClass) {
        "[" + this.actualTypeArguments[0].asmName()

    } else if (this is UnresolvedType) {
        this.resolve().asmName()
    } else {
        this.klass().asmName()
    }

internal fun Type.compileForName( mv : MethodVisitor ) {

    if (isPrimitive()) {
        val nonPrimitive = nonPrimitive()
        mv.visitFieldInsn(
            Opcodes.GETSTATIC,
            nonPrimitive.asmName(),
            "TYPE",
            classClass.asmDescriptor()
        )

    } else {
        StringConstant(className()).compile(mv)
        // ... Class[] Class[] index className

        mv.visitMethodInsn( // Class.forName( className : String ) : Class
            Opcodes.INVOKESTATIC,
            classClass.asmName(),
            "forName",
            asmMethodDescription(listOf(stringClass), classClass),
            false
        )
    }
}

internal fun Type.asmDescriptor(): String =
    if (this is Class<*>) {
        if (isArray) {
            "[${this.componentType.asmDescriptor()}"
        } else if (isPrimitive()) {
            when (this) {
                booleanPrimitive -> "Z"
                voidPrimitive -> "V"
                bytePrimitive -> "B"
                charPrimitive -> "C"
                shortPrimitive -> "S"
                intPrimitive -> "I"
                longPrimitive -> "J"
                floatPrimitive -> "F"
                doublePrimitive -> "D"
                else -> "?unknown_type?"
            }
        } else {
            "L" + this.typeName.replace('.', '/') + ";"
        }
    } else if (this is MetaDataType) {
        "L" + this.name.replace('.', '/') + ";"
    } else if (this is ParameterizedType && this.rawType == arrayClass) {
        "[" + this.actualTypeArguments[0].asmDescriptor()
    } else if (this is UnresolvedType) {
        this.resolve().asmDescriptor()
    } else {
        this.klass().asmDescriptor()
    }

/**
 * Unboxes an object into its primitive cousin.
 * If [fromType] isn't Integer, Long etc then this is a no-op.
 */
internal fun objectToPrimitive(mv: MethodVisitor, fromType: Type) {
    when (fromType) {
        booleanClass -> mv.visitMethodInsn(INVOKEVIRTUAL, booleanClass.asmName(), "booleanValue", "()Z", false)
        byteClass -> mv.visitMethodInsn(INVOKEVIRTUAL, byteClass.asmName(), "byteValue", "()B", false)
        characterClass -> mv.visitMethodInsn(INVOKEVIRTUAL, characterClass.asmName(), "charValue", "()C", false)
        shortClass -> mv.visitMethodInsn(INVOKEVIRTUAL, shortClass.asmName(), "shortValue", "()S", false)
        integerClass -> mv.visitMethodInsn(INVOKEVIRTUAL, integerClass.asmName(), "intValue", "()I", false)
        longClass -> mv.visitMethodInsn(INVOKEVIRTUAL, longClass.asmName(), "longValue", "()J", false)
        floatClass -> mv.visitMethodInsn(INVOKEVIRTUAL, floatClass.asmName(), "floatValue", "()F", false)
        doubleClass -> mv.visitMethodInsn(INVOKEVIRTUAL, doubleClass.asmName(), "doubleValue", "()D", false)
    }
}

/**
 * Boxes a primitive into its object cousin.
 */
internal fun primitiveToObject(mv: MethodVisitor, fromType: Type) {
    when (fromType) {
        booleanPrimitive -> mv.visitMethodInsn(INVOKESTATIC, booleanClass.asmName(), "valueOf", "(Z)${booleanClass.asmDescriptor()}", false)
        bytePrimitive -> mv.visitMethodInsn(INVOKESTATIC, byteClass.asmName(), "valueOf", "(B)${byteClass.asmDescriptor()}", false)
        charPrimitive -> mv.visitMethodInsn(INVOKESTATIC, characterClass.asmName(), "valueOf", "(C)${characterClass.asmDescriptor()}", false)
        shortPrimitive -> mv.visitMethodInsn(INVOKESTATIC, shortClass.asmName(), "valueOf", "(S)${shortClass.asmDescriptor()}", false)
        intPrimitive -> mv.visitMethodInsn(INVOKESTATIC, integerClass.asmName(), "valueOf", "(I)${integerClass.asmDescriptor()}", false)
        longPrimitive -> mv.visitMethodInsn(INVOKESTATIC, longClass.asmName(), "valueOf", "(J)${longClass.asmDescriptor()}", false)
        floatPrimitive -> mv.visitMethodInsn(INVOKESTATIC, floatClass.asmName(), "valueOf", "(F)${floatClass.asmDescriptor()}", false)
        doublePrimitive -> mv.visitMethodInsn(INVOKESTATIC, doubleClass.asmName(), "valueOf", "(D)${doubleClass.asmDescriptor()}", false)
    }
}

/**
 * Boxes/unboxes primitives as well as auto-casting between number types.
 * Also casts numbers to charPrimitive
 * See [compatiblePrimitiveType] and [canAutoCast] for conversions that must be handled.
 */
internal fun autoCast(mv: MethodVisitor, from: Type, to: Type) {

    var fromIntermediate = from
    var toIntermediate = to

    if (!from.isPrimitive() && to.isPrimitive()) {
        // Unbox first
        objectToPrimitive(mv, from)
        fromIntermediate = from.primitive()
    } else if (from.isPrimitive() && !to.isPrimitive()) {
        // We will box after any conversions...
        toIntermediate = to.primitive()
    }

    if (fromIntermediate.isPrimitive() && toIntermediate.isPrimitive()) {
        when (fromIntermediate) {

            bytePrimitive -> {
                when (toIntermediate) {
                    charPrimitive -> mv.visitInsn(I2C)
                    longPrimitive -> mv.visitInsn(I2L)
                    floatPrimitive -> mv.visitInsn(I2F)
                    doublePrimitive -> mv.visitInsn(I2D)
                }
            }
            charPrimitive -> {
                when (toIntermediate) {
                    bytePrimitive -> mv.visitInsn(I2B)
                    longPrimitive -> mv.visitInsn(I2L)
                    floatPrimitive -> mv.visitInsn(I2F)
                    doublePrimitive -> mv.visitInsn(I2D)
                }
            }
            shortPrimitive -> {
                when (toIntermediate) {
                    bytePrimitive -> mv.visitInsn(I2B)
                    charPrimitive -> mv.visitInsn(I2C)
                    longPrimitive -> mv.visitInsn(I2L)
                    floatPrimitive -> mv.visitInsn(I2F)
                    doublePrimitive -> mv.visitInsn(I2D)
                }
            }

            intPrimitive -> {
                when (toIntermediate) {
                    bytePrimitive -> mv.visitInsn(I2B)
                    charPrimitive -> mv.visitInsn(I2C)
                    shortPrimitive -> mv.visitInsn(I2S)
                    longPrimitive -> mv.visitInsn(I2L)
                    floatPrimitive -> mv.visitInsn(I2F)
                    doublePrimitive -> mv.visitInsn(I2D)
                }
            }

            longPrimitive -> {
                when (toIntermediate) {
                    bytePrimitive -> {
                        mv.visitInsn(L2I)
                        mv.visitInsn(I2B)
                    }
                    charPrimitive -> {
                        mv.visitInsn(L2I)
                        mv.visitInsn(I2C)
                    }
                    shortPrimitive -> {
                        mv.visitInsn(L2I)
                        mv.visitInsn(I2S)
                    }
                    intPrimitive -> mv.visitInsn(L2I)
                    floatPrimitive -> mv.visitInsn(L2F)
                    doublePrimitive -> mv.visitInsn(L2D)
                }
            }

            floatPrimitive -> {
                when (toIntermediate) {
                    bytePrimitive -> {
                        mv.visitInsn(F2I)
                        mv.visitInsn(I2B)
                    }
                    charPrimitive -> {
                        mv.visitInsn(F2I)
                        mv.visitInsn(I2C)
                    }
                    shortPrimitive -> {
                        mv.visitInsn(F2I);
                        mv.visitInsn(I2S)
                    }
                    intPrimitive -> mv.visitInsn(F2I)
                    longPrimitive -> mv.visitInsn(F2L)
                    doublePrimitive -> mv.visitInsn(F2D)
                }
            }
            doublePrimitive -> {
                when (toIntermediate) {
                    bytePrimitive -> {
                        mv.visitInsn(D2I)
                        mv.visitInsn(I2B)
                    }
                    charPrimitive -> {
                        mv.visitInsn(D2I)
                        mv.visitInsn(I2C)
                    }
                    shortPrimitive -> {
                        mv.visitInsn(D2I)
                        mv.visitInsn(I2S)
                    }
                    intPrimitive -> mv.visitInsn(D2I)
                    longPrimitive -> mv.visitInsn(D2L)
                    floatPrimitive -> mv.visitInsn(D2F)
                }
            }
        }
    }

    if (from.isPrimitive() && !to.isPrimitive()) {
        primitiveToObject(mv, if (toIntermediate.isPrimitive()) toIntermediate else from)
    }

}

internal fun popUnusedValue(mv: MethodVisitor, t: Type) {
    when (t) {
        voidPrimitive -> {}
        longPrimitive, doublePrimitive -> mv.visitInsn(POP2)
        else -> mv.visitInsn(POP)
    }
}

internal fun MethodVisitor.dup(type: Type) {
    if (type == doublePrimitive || type == longPrimitive) {
        visitInsn(DUP2)
    } else {
        visitInsn(DUP)
    }
}

internal fun MethodVisitor.dupX1(type: Type) {
    if (type == doublePrimitive || type == longPrimitive) {
        visitInsn(DUP2_X1)
    } else {
        visitInsn(DUP_X1)
    }
}

internal fun MethodVisitor.dupX2(type: Type) {
    if (type == doublePrimitive || type == longPrimitive) {
        visitInsn(DUP2_X2)
    } else {
        visitInsn(DUP_X2)
    }
}


internal fun asmMethodDescription(paramTypes: List<Type>, returnType: Type): String {
    return "(" + paramTypes.joinToString(separator = "") {
        it.asmDescriptor()
    } + ")" + returnType.asmDescriptor()
}

internal fun asmMethodDescription(paramTypes: kotlin.Array<Class<*>>, returnType: Type): String {
    return "(" + paramTypes.joinToString(separator = "") {
        it.asmDescriptor()
    } + ")" + returnType.asmDescriptor()
}

internal fun pushArgs(mv: MethodVisitor, args: List<StackEntry>, paramTypes: List<Type>, isVarArgs: Boolean) {

    if (isVarArgs) {
        // Put the "normal" args onto the stack
        val varArgType = paramTypes.last().shallowestElementTypeOfArray()
        for (i in 0..paramTypes.size - 2) {
            args[i].compileAutoCast(mv, paramTypes[i])
        }

        // Put the vararg args onto the stack as an array.
        val arraySize = args.size - paramTypes.size + 1
        val offset = paramTypes.size - 1
        newArray(mv, arraySize, varArgType)
        for (i in 0 until arraySize) {
            mv.visitInsn(DUP) // Keep the array on the stack.
            intConstant(mv, i)
            args[offset + i].compileAutoCast(mv, varArgType)
            storeArray(mv, varArgType)
        }

    } else {
        args.forEachIndexed { i, arg ->
            arg.compileAutoCast(mv, paramTypes[i])
        }
    }
}

fun newArray(mv: MethodVisitor, size: Int, type: Type) {
    intConstant(mv, size)
    newArray(mv, type)
}


/**
 * Assumes the top of the stack contains the size of the array
 */
fun newArray(mv: MethodVisitor, type: Type) {
    if (type.isPrimitive()) {
        val arrayType = when (type) {
            booleanPrimitive -> T_BOOLEAN
            charPrimitive -> T_CHAR
            bytePrimitive -> T_BYTE
            shortPrimitive -> T_SHORT
            intPrimitive -> T_INT
            longPrimitive -> T_LONG
            floatPrimitive -> T_FLOAT
            doublePrimitive -> T_DOUBLE
            else -> -1
        }
        mv.visitIntInsn(NEWARRAY, arrayType)

    } else {
        mv.visitTypeInsn(ANEWARRAY, type.asmName())
    }
}

/**
 * Assumes the top of the stack is array, index, and is replaced with the value.
 */
fun loadArray(mv: MethodVisitor, type: Type) {
    val opcode = when (type) {
        booleanPrimitive -> BALOAD
        charPrimitive -> CALOAD
        bytePrimitive -> BALOAD
        shortPrimitive -> SALOAD
        intPrimitive -> IALOAD
        longPrimitive -> LALOAD
        floatPrimitive -> FALOAD
        doublePrimitive -> DALOAD
        else -> AALOAD
    }
    mv.visitInsn(opcode)
}

/**
 * Assumes the top of the stack is array, index, value, and is replaced with <empty>
 */
fun storeArray(mv: MethodVisitor, type: Type) {
    val opcode = when (type) {
        booleanPrimitive -> BASTORE
        charPrimitive -> CASTORE
        bytePrimitive -> BASTORE
        shortPrimitive -> SASTORE
        intPrimitive -> IASTORE
        longPrimitive -> LASTORE
        floatPrimitive -> FASTORE
        doublePrimitive -> DASTORE
        else -> AASTORE
    }
    mv.visitInsn(opcode)
}

fun intConstant(mv: MethodVisitor, value: Int) {
    when (value) {
        0 -> mv.visitInsn(ICONST_0)
        1 -> mv.visitInsn(ICONST_1)
        2 -> mv.visitInsn(ICONST_2)
        3 -> mv.visitInsn(ICONST_3)
        4 -> mv.visitInsn(ICONST_4)
        5 -> mv.visitInsn(ICONST_5)
        -1 -> mv.visitInsn(ICONST_M1)
        else -> {
            if (value >= Byte.MIN_VALUE && value <= Byte.MAX_VALUE) {
                mv.visitIntInsn(BIPUSH, value)
            } else if (value >= Short.MIN_VALUE && value <= Short.MAX_VALUE) {
                mv.visitIntInsn(SIPUSH, value)
            } else {
                mv.visitLdcInsn(value)
            }
        }
    }
}

fun MethodVisitor.addLineNumber(position: FeatherPosition?) {
    position ?: return
    if (position.debug) {
        // println( "Adding debug info @ $position" )
        val label = Label()
        visitLabel(label)
        visitLineNumber(position.line + 1, label)
    }
}
