package uk.co.nickthecoder.feather.internal.keywords

import org.objectweb.asm.Label
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.operators.comparisons.ConditionalStackEntry
import uk.co.nickthecoder.feather.internal.special.BlockStackEntry


internal class If(
    val condition: ConditionalStackEntry,
    val ifBranch: StackEntry,
    elseBranch: StackEntry?

) : StackEntry, MultipleBranches {

    val ifBranchIsEmpty = ifBranch is BlockStackEntry && ifBranch.isEmpty()
    val elseBranch = if (elseBranch is BlockStackEntry && elseBranch.isEmpty()) null else elseBranch

    override fun type() =
        if (elseBranch == null) {
            voidPrimitive
        } else if (ifBranchIsEmpty) {
            voidPrimitive
        } else {
            findCommonSuperType(ifBranch.type(), elseBranch.type())
        }

    override fun isTerminating(): Boolean {
        return ifBranch.isTerminating() && elseBranch != null && elseBranch.isTerminating()
    }

    override fun compile(mv: MethodVisitor) {
        val type = resolvedType()
        val ifType = ifBranch.resolvedType()

        condition.compileComparison(mv)

        if (! ifBranchIsEmpty) {
            ifBranch.compile(mv)
            if (type == voidPrimitive) {
                popUnusedValue(mv, ifType)
            } else {
                autoCast(mv, ifType, type)
            }
        }

        if (elseBranch != null) {
            val elseType = elseBranch.resolvedType()

            // We don't need to jump over the "else" branch when the "if" branch will RETURN.
            if (ifBranch.isTerminating()) {
                mv.visitLabel(condition.notEqualLabel)
                elseBranch.compile(mv)
                if (type == voidPrimitive) {
                    popUnusedValue(mv, elseType)
                }
            } else {
                val end = Label()
                mv.visitJumpInsn(Opcodes.GOTO, end)

                mv.visitLabel(condition.notEqualLabel)

                elseBranch.compile(mv)
                if (type == voidPrimitive) {
                    popUnusedValue(mv, elseType)
                } else {
                    autoCast(mv, elseType, type)
                }
                mv.visitLabel(end)
            }

        } else {
            mv.visitLabel(condition.notEqualLabel)
        }
    }

    override fun returnFromEveryBranch(method: MetaDataMethod): StackEntry {
        val ifBranch = if (ifBranch is MultipleBranches) {
            ifBranch.returnFromEveryBranch(method)
        } else {
            Return(method, ifBranch, null)
        }
        val elseBranch = if (elseBranch is MultipleBranches) {
            elseBranch.returnFromEveryBranch(method)
        } else {
            Return(method, elseBranch, null)
        }
        return If(condition, ifBranch, elseBranch)
    }
}
