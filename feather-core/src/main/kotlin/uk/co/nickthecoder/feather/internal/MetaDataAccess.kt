package uk.co.nickthecoder.feather.internal

import org.objectweb.asm.Opcodes

/**
 * Feather doesn't support different access levels (yet?), but we do need
 * to make a constructor private. So I've added the minimum required.
 *
 * Later, fields, methods and constructors may all have access levels.
 */
enum class MetaDataAccess(val asmCode: Int) {
    PUBLIC(Opcodes.ACC_PUBLIC),
    PRIVATE(Opcodes.ACC_PRIVATE)
}
