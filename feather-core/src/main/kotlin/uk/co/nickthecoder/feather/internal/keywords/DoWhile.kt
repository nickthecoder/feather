package uk.co.nickthecoder.feather.internal.keywords

import org.objectweb.asm.Label
import org.objectweb.asm.MethodVisitor
import uk.co.nickthecoder.feather.FeatherException
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.LazyStackEntry
import uk.co.nickthecoder.feather.internal.StackEntry
import uk.co.nickthecoder.feather.internal.operators.comparisons.ConditionalStackEntry
import uk.co.nickthecoder.feather.internal.popUnusedValue
import uk.co.nickthecoder.feather.internal.voidPrimitive


internal class LazyDoWhile(
    private val condition: StackEntry,
    private val whileBody: StackEntry,
    private val continueLabel: Label,
    private val breakLabel: Label,
    private val throwWhenInterrupted: Boolean,
    private val position: FeatherPosition
) : LazyStackEntry() {

    override fun createActual(): StackEntry {

        if (condition.resolvedType() !== booleanPrimitive) {
            FeatherException.expectedBoolean(position, condition.resolvedType())
        }

        val cse = createConditionalStackEntry(condition, "do...while", position)
        return DoWhile(whileBody, cse, continueLabel, breakLabel, throwWhenInterrupted)

    }
}

internal class DoWhile(
    val whileBody: StackEntry,
    val condition: ConditionalStackEntry,
    val continueLabel: Label,
    val breakLabel: Label,
    val throwWhenInterrupted: Boolean

) : StackEntry {

    override fun type() = voidPrimitive

    override fun compile(mv: MethodVisitor) {
        mv.visitLabel(condition.notEqualLabel)

        // If we want to automatically throw an InterruptedException when Thread.interrupted(),
        // then insert that code before the condition is tested, so that it is checked each time
        // through the loop.
        if (throwWhenInterrupted) {
            While.compileCheckInterrupted(mv)
        }

        whileBody.compile(mv)
        popUnusedValue(mv, whileBody.resolvedType())

        mv.visitLabel(continueLabel)

        condition.compileOppositeComparison(mv)

        mv.visitLabel(breakLabel)
    }
}

