package uk.co.nickthecoder.feather

class CompilationFailed(val errors: List<FeatherException>)
    : Exception(errors.firstOrNull()?.message ?: "Compilation Failed"
) {

    override fun toString(): String {
        return super.toString() + errors.joinToString(prefix = "\n    ", separator = "    \n ")
    }
}
