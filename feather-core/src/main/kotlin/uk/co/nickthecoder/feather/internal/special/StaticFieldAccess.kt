package uk.co.nickthecoder.feather.internal.special

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.internal.*
import java.lang.reflect.Type

internal class StaticFieldAccess(
    val subjectClass: Type,
    val field: FieldWrapper
) : CachedTypeStackEntry(), AssignableStackEntry {

    override fun isFinal() = field.isFinal()

    override fun actualType() = field.genericReturnType()

    override fun compile(mv: MethodVisitor) {
        mv.visitFieldInsn(Opcodes.GETSTATIC, subjectClass.asmName(), field.name(), field.returnType().asmDescriptor())
    }

    override fun compileSet(value: StackEntry, mv: MethodVisitor) {
        value.compileAutoCast(mv, field.returnType())
        mv.visitFieldInsn(Opcodes.PUTSTATIC, subjectClass.asmName(), field.name(), field.returnType().asmDescriptor())
    }

    override fun adjust(
        mv: MethodVisitor,
        includeOldValue: Boolean,
        includeNewValue: Boolean,
        adjustment: (MethodVisitor) -> Unit
    ) {
        mv.visitFieldInsn(Opcodes.GETSTATIC, subjectClass.asmName(), field.name(), field.returnType().asmDescriptor())

        if (includeOldValue) {
            mv.dup(resolvedType())
        }

        adjustment(mv)

        if (includeNewValue) {
            mv.dup(resolvedType())
        }

        mv.visitFieldInsn(Opcodes.PUTSTATIC, subjectClass.asmName(), field.name(), field.returnType().asmDescriptor())
    }
}