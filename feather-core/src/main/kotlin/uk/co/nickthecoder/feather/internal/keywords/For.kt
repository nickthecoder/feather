package uk.co.nickthecoder.feather.internal.keywords

import org.objectweb.asm.Label
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes.*
import uk.co.nickthecoder.feather.FeatherException
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.Source
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.special.StaticMethodCall
import uk.co.nickthecoder.feather.internal.special.createStaticMethodCall
import uk.co.nickthecoder.feather.runtime.IntRange
import java.lang.reflect.Type

/**
 * Loops over any of the following types :
 *
 * * Array
 * * [Iterable] (including [IntRange])
 * * Any object which has an extensions function `iterator()` (mimicking an [Iterable]).
 * * [Iterator] - Warning! Most languages don't let you do this, probably for good reasons!
 *   You can have weird side effects, because other code could have access to the iterator, and could advance
 *   it, so the for would "skip" values. Hmm.
 *
 */
internal class For(
    private val block: Block,
    private val body: StackEntry,
    private val loopVarName: String,
    private val counterVarName: String?,
    private val loopExpression: StackEntry,
    private val throwWhenInterrupted: Boolean,
    private val source: Source,
    private val position: FeatherPosition
) : StackEntry {

    override fun type() = voidPrimitive

    override fun compile(mv: MethodVisitor) {
        mv.addLineNumber(position)
        val loopExpressionType = loopExpression.resolvedType()

        val itemType = loopVariableType(source, position, loopExpression)

        // There are different implementations depending on what the for is looping over.
        // The possibilities are :
        //      An IntRange (a special optimised case). Uses compileIntRange
        //      An array. (another special, optimised case). Uses compileArray
        //      An object of type Iterator<T>
        //      An object of type Iterable<T>
        //      An object which is not of type Iterable, but there is an extension function,
        //          called "iterator" which returns an Iterator<T>
        // The last 3 use the same implementation compileIterator.

        if (loopExpressionType == IntRange::class.java) {
            // A special, optimised case for IntRange, which doesn't use Iterators.
            compileIntRange(mv)
        } else if (loopExpressionType.isArrayType()) {
            // A special case for primitive arrays.
            compileArray(mv)

        } else if (isAssignable(loopExpressionType, Iterable::class.java)) {

            // iterableSE is iterable, so do the "normal" thing.

            compileIterator(mv, itemType) {
                loopExpression.compile(mv)
                val isInterface = loopExpressionType.isInterface()
                mv.visitMethodInsn(
                    if (isInterface) INVOKEINTERFACE else INVOKEVIRTUAL,
                    loopExpressionType.asmName(),
                    "iterator",
                    asmMethodDescription(emptyList(), Iterator::class.java),
                    isInterface
                )
            }

        } else if (isAssignable(loopExpressionType, Iterator::class.java)) {
            // iterableSE is an Iterator<T>
            compileIterator(mv, itemType) {
                loopExpression.compile(mv)
            }
        } else {

            // Ok, so iterableSE is not an Iterable, but maybe there is an extension function
            // called "iterator" instead.
            // NOTE. runtime.Extensions has such a method for CharSequence, so that we can iterate
            // over Strings and other CharSequences.
            val typeAndCall = findIteratorMethod()
            if (typeAndCall == null) {
                throw FeatherException.expectedIterable(position, loopExpressionType)
            } else {
                compileIterator(mv, typeAndCall.first) {
                    typeAndCall.second.compile(mv)
                }
            }

        }
    }

    /**
     * Used when the loop expression is an Iterator, Iterable or is any objects with an extension function
     * called "iterator" that returns an Iterator (mimicking an Iterable).
     * [iteratorMaker] is a lambda, which compiles byte code, whose final result is an extra
     * [Iterator] on the stack.
     */
    private fun compileIterator(mv: MethodVisitor, itemType: Type, iteratorMaker: () -> Unit) {

        val bodyType = body.resolvedType()

        val iteratorLocalVariable = block.addVariable("_FOR_ITERATOR", Iterator::class.java, false)

        val loopVarLocalVariable = block.findLocalVariable(loopVarName)!!

        val counterLocalVariable = if (counterVarName == null) {
            null
        } else {
            block.findLocalVariable(counterVarName)!!
        }

        block.initialiseLocalVariableIndices()

        // Initialise the counter to 0
        counterLocalVariable?.let {
            mv.visitInsn(ICONST_0)
            mv.visitVarInsn(ISTORE, it.index)
        }

        // Create the iterator.
        // This will call the "iterator()" method in the normal case, but for a CharSequence,
        // it will create a CharSequenceIterator (in the runtime module).
        iteratorMaker()

        mv.visitVarInsn(ASTORE, iteratorLocalVariable.index)

        // Used by the "GOTO" at the end of the loop
        val topLabel = Label()
        mv.visitLabel(topLabel)

        // If we want to automatically throw an InterruptedException when Thread.interrupted(),
        // then insert that code before the condition is tested, so that it is checked each time
        // through the loop.
        if (throwWhenInterrupted) {
            While.compileCheckInterrupted(mv)
        }

        // hasNext()
        mv.visitVarInsn(ALOAD, iteratorLocalVariable.index)
        mv.visitMethodInsn(
            INVOKEINTERFACE,
            Iterator::class.java.asmName(),
            "hasNext",
            asmMethodDescription(emptyList(), booleanPrimitive),
            true
        )
        mv.visitJumpInsn(IFEQ, block.breakLabel)

        // next()
        mv.visitVarInsn(ALOAD, iteratorLocalVariable.index)
        mv.visitMethodInsn(
            INVOKEINTERFACE,
            Iterator::class.java.asmName(),
            "next",
            asmMethodDescription(emptyList(), objectClass),
            true
        )
        mv.visitTypeInsn(CHECKCAST, itemType.asmName())
        mv.visitVarInsn(ASTORE, loopVarLocalVariable.index)

        // The main body of the loop
        body.compile(mv)
        popUnusedValue(mv, bodyType) // Throw away the result of the body expression if it is not void.

        // If the loop contains a "continue" statement, it will jump here.
        mv.visitLabel(block.continueLabel)

        // Increment the counter
        counterLocalVariable?.let {
            mv.visitIincInsn(it.index, 1)
        }

        // Iterate - jump back to "if hasNext()"
        mv.visitJumpInsn(GOTO, topLabel)

        // This label is used by the "if iterable.hasNext()" and also by any "break" statements inside the loop body
        mv.visitLabel(block.breakLabel)

    }

    /**
     * An optimised version of [compileIterator] for [IntRange].
     */
    private fun compileIntRange(mv: MethodVisitor) {
        val loopVarLocalVariable = block.findLocalVariable(loopVarName)!!

        val counterLocalVariable = if (counterVarName == null) {
            null
        } else {
            block.findLocalVariable(counterVarName)!!
        }

        block.initialiseLocalVariableIndices()

        val topOfLoopLabel = Label()
        val endLabel = block.breakLabel

        // Here's the pseudo code...
        // The stack is indicated by the brackets.
        // NOTE that during the loop we have the "endInclusive" on the stack, so that we don't need to
        // retrieve the field each time round the loop.
        //
        // A Set the counter to 0
        // B (EMPTY) get field IntRange.endInclusive (duplicated) and IntRange.start (duplicated)
        // C (endInclusive, endInclusive, start, start) store start into local var i
        // D (endInclusive, endInclusive, start) if start > endInclusive jump to END
        // TOP OF LOOP (endInclusive) // MUST BE THE SAME STACK AS END
        // E (endInclusive) loop body (popping the final result if not void)
        // F (endInclusive) increment counter local variable (if there is one)
        // G (endInclusive) increment i
        // H (endInclusive) DUP
        // I (endInclusive, endInclusive) load i
        // J (endInclusive, endInclusive, i) if i <= endInclusive goto TOP OF LOOP
        // END (endInclusive)
        // K (endInclusive) POP
        // (EMPTY)

        // A (EMPTY) Set the counter to 0
        counterLocalVariable?.let {
            mv.visitInsn(ICONST_0)
            mv.visitVarInsn(ISTORE, it.index)
        }

        // B (EMPTY) get field IntRange.endInclusive (duplicated) and IntRange.start (duplicated)
        loopExpression.compile(mv)
        // (IntRange)
        mv.visitInsn(DUP) // Calling two GETFIELDS, so we need two of the IntRanges.
        // (IntRange,IntRange)
        mv.visitFieldInsn(GETFIELD, IntRange::class.java.asmName(), "endInclusive", intPrimitive.asmDescriptor())
        // (IntRange, endInclusive)
        mv.visitInsn(DUP_X1)
        // (endInclusive, IntRange, endInclusive)
        mv.visitInsn(SWAP)
        // (endInclusive, endInclusive, IntRange)
        mv.visitFieldInsn(GETFIELD, IntRange::class.java.asmName(), "start", intPrimitive.asmDescriptor())
        // (endInclusive, endInclusive, start)
        mv.visitInsn(DUP)
        // (endInclusive, endInclusive, start, start)

        // C (endInclusive, endInclusive, start, start) store start into local var i
        mv.visitVarInsn(ISTORE, loopVarLocalVariable.index)

        // D (endInclusive, endInclusive, start) if endInclusive < start  jump to END
        mv.visitJumpInsn(IF_ICMPLT, endLabel)

        // TOP OF LOOP (endInclusive) // MUST BE THE SAME STACK AS END
        mv.visitLabel(topOfLoopLabel)

        // If we want to automatically throw an InterruptedException when Thread.interrupted(),
        // then insert that code before the condition is tested, so that it is checked each time
        // through the loop.
        if (throwWhenInterrupted) {
            While.compileCheckInterrupted(mv)
        }

        // E (endInclusive) loop body (popping the final result if not void)
        body.compile(mv)
        popUnusedValue(mv, body.resolvedType()) // Throw away the result of the body expression if it is not void.

        // If the loop contains a "continue" statement, it will jump here.
        mv.visitLabel(block.continueLabel)

        // F (endInclusive) increment counter local variable (if there is one)
        counterLocalVariable?.let {
            mv.visitIincInsn(it.index, 1)
        }

        // G (endInclusive) increment i
        mv.visitIincInsn(loopVarLocalVariable.index, 1)

        // H (endInclusive) DUP
        mv.visitInsn(DUP)

        // I (endInclusive, endInclusive) load i
        mv.visitVarInsn(ILOAD, loopVarLocalVariable.index)

        // J (endInclusive, endInclusive, i) if endInclusive >= i goto TOP OF LOOP
        mv.visitJumpInsn(IF_ICMPGE, topOfLoopLabel)

        // END (endInclusive)
        mv.visitLabel(endLabel)

        // K (endInclusive) POP
        mv.visitInsn(POP)

    }

    private fun compileArray(mv: MethodVisitor) {
        val bodyType = body.resolvedType()
        val loopVarLocalVariable = block.findLocalVariable(loopVarName)!!

        val loadOpCode = when (loopVarLocalVariable.type()) {
            booleanPrimitive -> BALOAD
            charPrimitive -> CALOAD
            bytePrimitive -> BALOAD
            shortPrimitive -> SALOAD
            intPrimitive -> IALOAD
            longPrimitive -> LALOAD
            floatPrimitive -> FALOAD
            doublePrimitive -> DALOAD
            else -> AALOAD
        }


        val counterLocalVariable = if (counterVarName == null) {
            block.addVariable("\$FOR_COUNTER", intPrimitive, false)
        } else {
            block.findLocalVariable(counterVarName)!!
        }

        //loopVarLocalVariable.changeType(elementType)
        block.initialiseLocalVariableIndices()

        val topOfLoopLabel = Label()
        val endLabel = block.breakLabel

        // Here's the pseudo code...
        // The stack is indicated by the brackets.
        //
        // (EMPTY) compile the array
        // (array) get the size of the array
        // (array, size) Set the counter to 0
        // (array, size, 0)
        // TOP OF LOOP (array, size, counter) // MUST BE THE SAME the Goto
        // (array, size, counter) Jump to the end if counter >= size
        // (array) DUP
        // (array, array) Load the counter
        // (array, array, counter ) Get the nth element of the array
        // (array, element) Store it in the loop variable
        // (array) get the size
        // (array, size) load the counter
        // (array, size, counter) Goto TOP OF LOOP
        // (array, size, counter) POP
        // END (array, int)
        // (array, counter) pop2
        // (EMPTY)


        // (EMPTY) get the size of the array
        loopExpression.compile(mv)
        mv.visitInsn(DUP)
        // (array,array)
        mv.visitInsn(ARRAYLENGTH)
        // (array, size)
        mv.visitInsn(ICONST_0)
        mv.visitInsn(DUP)
        // (array, size, 0, 0)
        mv.visitVarInsn(ISTORE, counterLocalVariable.index)

        // (array, size, 0)
        mv.visitLabel(topOfLoopLabel)

        // If we want to automatically throw an InterruptedException when Thread.interrupted(),
        // then insert that code before the condition is tested, so that it is checked each time
        // through the loop.
        if (throwWhenInterrupted) {
            While.compileCheckInterrupted(mv)
        }

        // (array, size, counter) Jump to the end if size <= counter
        mv.visitJumpInsn(IF_ICMPLE, endLabel)

        // (array)
        mv.visitInsn(DUP)
        // (array,array) Load the counter
        counterLocalVariable.load(mv)
        // (array, array, counter) Get the nth element of the array
        mv.visitInsn(loadOpCode)
        // (array, element) Store it in the loop variable
        loopVarLocalVariable.store(mv)
        // (array)

        // Body
        body.compile(mv)
        popUnusedValue(mv, bodyType) // Throw away the result of the body expression if it is not void.

        // (array) get the size
        mv.visitInsn(DUP)
        mv.visitInsn(ARRAYLENGTH)

        mv.visitIincInsn(counterLocalVariable.index, 1)

        // (array, size) load the counter
        counterLocalVariable.load(mv)
        // (array, size, counter) Goto TOP OF LOOP
        mv.visitJumpInsn(GOTO, topOfLoopLabel)

        mv.visitLabel(endLabel)
        // (array)
        mv.visitInsn(POP)

    }

    private fun findIteratorMethod(): Pair<Type, StaticMethodCall>? {
        source.findStaticAliases("iterator").forEach { (klass, realName) ->
            createStaticMethodCall(
                klass,
                realName,
                listOf(loopExpression),
                emptyList(),
                source,
                position
            )?.let { methodCall ->
                val iteratorType = methodCall.method.returnType()

                if (Iterator::class.java.isAssignableFrom(iteratorType.klass())) {
                    val nextMethod = iteratorType.findMethod("next", emptyList(), emptyList())!!
                    val itemType = resolveGenericType(iteratorType, nextMethod.genericReturnType())
                    return Pair(itemType, methodCall)
                }
            }
        }
        return null
    }


    companion object {

        fun firstPassLoopVariableType(source: Source, position: FeatherPosition, loopExpression: StackEntry): Type {
            // During the first pass, the type of the loopExpression may not be knowable, and therefore we
            // can only return an ImpliedType based on the loopExpression's type.
            return try {
                loopVariableType(source, position, loopExpression)
            } catch (e: Exception) {
                object : ImpliedType {
                    override fun calculateType(): Type {
                        return loopVariableType(source, position, loopExpression)
                    }
                    override val position = position
                }
            }
        }

        fun loopVariableType(source: Source, position: FeatherPosition, loopExpression: StackEntry): Type {
            val loopExpressionType = loopExpression.type()

            return if (loopExpressionType === IntRange::class.java) {
                intPrimitive
            } else if (loopExpressionType.isArrayType()) {
                loopExpressionType.shallowestElementTypeOfArray()

            } else if (isAssignable(loopExpressionType, Iterable::class.java)) {

                val iteratorMethod = loopExpressionType.findMethod("iterator", emptyList(), emptyList())!!
                val iteratorType = resolveGenericType(loopExpressionType, iteratorMethod.genericReturnType())
                val nextMethod = iteratorType.findMethod("next", emptyList(), emptyList())!!
                resolveGenericType(iteratorType, nextMethod.genericReturnType())

            } else if (isAssignable(loopExpressionType, Iterator::class.java)) {
                if (loopExpressionType is ParameterizedTypeImplementation) {
                    loopExpressionType.actualTypeArguments.firstOrNull() ?: objectClass
                } else {
                    objectClass
                }
            } else {

                // Ok, so iterableSE is not an Iterable, but maybe there is an extension function
                // called "iterator" instead.
                // NOTE. runtime.Extensions has such a method for CharSequence, so that we can iterate
                // over Strings and other CharSequences.
                findIteratorMethod(source, position, loopExpression)
                    ?.first ?: throw FeatherException.expectedIterable(position, loopExpressionType)

            }
        }


        private fun findIteratorMethod(
            source: Source,
            position: FeatherPosition,
            loopExpression: StackEntry
        ): Pair<Type, StaticMethodCall>? {
            source.findStaticAliases("iterator").forEach { (klass, realName) ->
                createStaticMethodCall(
                    klass,
                    realName,
                    listOf(loopExpression),
                    emptyList(),
                    source,
                    position
                )?.let { methodCall ->
                    val iteratorType = methodCall.method.returnType()

                    if (Iterator::class.java.isAssignableFrom(iteratorType.klass())) {
                        val nextMethod = iteratorType.findMethod("next", emptyList(), emptyList())!!
                        val itemType = resolveGenericType(iteratorType, nextMethod.genericReturnType())
                        return Pair(itemType, methodCall)
                    }
                }
            }
            return null
        }

    }

}
