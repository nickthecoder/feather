package uk.co.nickthecoder.feather.internal.special

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.FeatherException
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.internal.*
import java.lang.reflect.Type

/**
 * Feather's syntax makes constructor calls look like functions.
 * [LazyMethodCall] decides if the call is a real method call, or a constructor call,
 * and if the latter, it will create this.
 *
 * Therefore, it is [LazyMethodCall] which will be found in the CompilerWorker's stack,
 * not this.
 */
internal class ConstructorCall(
    val constructor: ConstructorWrapper,
    val args: List<StackEntry>,
    val typeArgs: List<Type>,
    val position: FeatherPosition

) : StackEntry {

    override fun type(): Type {
        return if (typeArgs.isEmpty()) {
            constructor.owner()
        } else {
            ParameterizedTypeImplementation(constructor.owner().klass(), typeArgs.toTypedArray())
        }
    }

    override fun compile(mv: MethodVisitor) {
        val actualTypeParameters = constructor.owner().typeParameters()
        if (actualTypeParameters.size != typeArgs.size) {
            throw FeatherException.wrongNumberOfTypeParameters(position, actualTypeParameters.size, typeArgs.size)
        }

        val paramTypes = constructor.parameterTypes()
        val klass = constructor.owner()
        mv.visitTypeInsn(Opcodes.NEW, klass.asmName())
        mv.visitInsn(Opcodes.DUP)

        pushArgs(mv, args, paramTypes, constructor.isVarArgs())

        mv.visitMethodInsn(
            Opcodes.INVOKESPECIAL, klass.asmName(), "<init>",
            asmMethodDescription(paramTypes, voidPrimitive), false
        )
    }
}