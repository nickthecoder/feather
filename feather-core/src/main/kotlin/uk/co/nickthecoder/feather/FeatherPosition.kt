package uk.co.nickthecoder.feather

import org.antlr.v4.runtime.ParserRuleContext

/**
 * Identifies a position within a piece of source code, most commonly to indicate the position of an
 * error. See [FeatherException].
 * Note that [line] and [column] are zero based (i.e. the first character is line 0, column 0).
 * The [toString] method adds one to the line and column to make it more human readable.
 */
data class FeatherPosition(
        /**
         * Usually the file's path, but if the compiler was passed a simple block of text to compile,
         * then this will be null (as there is no name to associate it with).
         */
        val source: String?,
        /**
         * Zero based. Note, [toString] add one, to make it human readable
         */
        val line: Int,
        /**
         * Zero based. Note, [toString] add one, to make it human readable
         */
        val column: Int,
        val debug : Boolean
) {
    internal constructor(source: Source, ctx: ParserRuleContext, debug : Boolean)
            : this(source.source, ctx.start.line, ctx.start.charPositionInLine, debug)

    internal constructor(source: Source, line: Int, column: Int)
            : this(source.source, line, column, false)

    internal constructor(source: Source)
            : this(source.source, -1, -1, false)

    /**
     * A human readable representation of this error. The line and column numners are 1-based.
     * i.e. the first character is reported as line 1, column 1, which is NOT how they are store in
     * [line] and [column].
     */
    override fun toString(): String {
        return if (line < 0 || column < 0) {
            "Unknown position"
        } else if (source == null) {
            "(${line + 1}:${column + 1})"
        } else {
            "$source (${line + 1}:${column + 1})"
        }
    }
}
