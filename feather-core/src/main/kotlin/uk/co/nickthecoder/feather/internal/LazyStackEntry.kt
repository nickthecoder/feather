package uk.co.nickthecoder.feather.internal

import org.objectweb.asm.MethodVisitor

/**
 * We also act as a wrapper around the actual StackEntry,
 * so that the stack entry can be determined at a later stage.
 * During the parsing phase actual will be null.
 *
 * In the seconds phase, we can ask about [resolvedType] too, which will use
 * the [actual]s [resolvedType].
 */
internal abstract class LazyStackEntry : CachedTypeStackEntry() {

    private var actual: StackEntry? = null

    fun actual() = actual ?: createActual().apply { actual = this }

    final override fun actualType() = object : UnresolvedType {
        override fun resolve() = actual().type()
    }

    abstract fun createActual(): StackEntry

    final override fun compile(mv: MethodVisitor) {
        actual().compile(mv)
    }
}
