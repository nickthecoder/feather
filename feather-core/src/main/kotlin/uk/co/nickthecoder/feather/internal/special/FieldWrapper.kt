package uk.co.nickthecoder.feather.internal.special

import uk.co.nickthecoder.feather.internal.MetaDataField
import uk.co.nickthecoder.feather.internal.isStatic
import uk.co.nickthecoder.feather.internal.resolveGenericType
import java.lang.reflect.Field
import java.lang.reflect.Modifier
import java.lang.reflect.Type

internal class FieldWrapper private constructor(
    val java: Field?,
    val feather: MetaDataField?
) {
    constructor(field: Field) : this(field, null)
    constructor(field: MetaDataField) : this(null, field)

    fun name() = if (java == null) feather!!.name else java.name
    fun isFinal() = if (java == null) feather!!.isFinal else Modifier.isFinal(java.modifiers)
    fun isStatic() = if (java == null) feather!!.isStatic else java.isStatic()
    fun owner() = if (java == null) feather!!.owner else java.declaringClass
    fun returnType() = if (java == null) feather!!.type() else java.type
    fun genericReturnType() = if (java == null) feather!!.type() else java.genericType
    fun genericReturnType(subject: Type) =
        if (java == null) feather!!.type() else resolveGenericType(subject, java.genericType)

}