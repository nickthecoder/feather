package uk.co.nickthecoder.feather.internal.operators.comparisons

import org.objectweb.asm.Label
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.asmMethodDescription
import uk.co.nickthecoder.feather.internal.intPrimitive
import uk.co.nickthecoder.feather.internal.klass
import uk.co.nickthecoder.feather.internal.objectClass
import java.lang.reflect.Type


internal interface Comparison {
    fun compile(mv: MethodVisitor, commonType: Type?, aType: Type, bType: Type, notEqualLabel: Label): Boolean
}

internal fun compareToComparison(mv: MethodVisitor, opcode : Int, aType: Type, bType: Type, notEqualLabel: Label): Boolean {
    // Use .compareTo if they are Comparable
    val aClass = aType.klass()
    val bClass = bType.klass()
    if (Comparable::class.java.isAssignableFrom(aClass) && Comparable::class.java.isAssignableFrom(bClass)) {
        // TODO We aren't checking that they are the same KIND of comparable!
        mv.visitMethodInsn(
                Opcodes.INVOKEVIRTUAL,
                aClass.asmName(),
                "compareTo",
                asmMethodDescription(listOf(objectClass), intPrimitive),
                aClass.isInterface
        )
        mv.visitJumpInsn(opcode, notEqualLabel)
        return true
    } else {
        return false
    }
}
