package uk.co.nickthecoder.feather

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes.ARRAYLENGTH
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.special.*
import java.lang.reflect.Type

internal class LazyFieldStackEntry(
    val subject: StackEntry,
    val name: String,
    val source: Source,
    val position: FeatherPosition
) : CachedTypeStackEntry(), AssignableStackEntry {

    override fun isFinal() = (actual() as? AssignableStackEntry)?.isFinal() ?: true

    private var actual: StackEntry? = null

    private fun actual(): StackEntry {

        if (actual == null) {
            if (subject is Identifier) {
                actual = findFieldAccess(subject.actual(), name, source, position)
            } else {
                actual = findFieldAccess(subject, name, source, position)
            }
        }

        return actual ?: throw FeatherException.noSuchField(position, name, subject.resolvedType())
    }

    override fun actualType() = actual().type()

    override fun compile(mv: MethodVisitor) = actual().compile(mv)

    override fun compileSet(value: StackEntry, mv: MethodVisitor) {
        if (actual is AssignableStackEntry) {
            (actual as AssignableStackEntry).compileSet(value, mv)
        } else {
            throw FeatherException.expectedAssignable(position)
        }
    }

    override fun adjust(
        mv: MethodVisitor,
        includeOldValue: Boolean,
        includeNewValue: Boolean,
        adjustment: (MethodVisitor) -> Unit
    ) {
        if (actual is AssignableStackEntry) {
            (actual as AssignableStackEntry).adjust(mv, includeOldValue, includeNewValue, adjustment)
        } else {
            throw FeatherException.expectedAssignable(position)
        }
    }
}

internal fun findFieldAccess(
    subject: StackEntry,
    name: String,
    source: Source,
    position: FeatherPosition
): StackEntry? {
    if (subject is ClassConstant) {
        val subjectType = subject.constantValue.resolve()

        subjectType.findField(name)?.let { field ->
            if (!field.isStatic()) {
                throw FeatherException.cannotAccessFromStaticContext(position, subjectType, name)
            }
            return StaticFieldAccess(subjectType, field)
        }
        // Look for getter method with the appropriate name (and maybe a setter too)
        subjectType.findMethod(getterName(name), emptyList(), emptyList())?.let { getter ->
            if (!getter.isStatic()) {
                throw FeatherException.cannotAccessFromStaticContext(position, subjectType, name)
            }
            val setter = subjectType.findMethod(setterName(name), emptyList(), listOf(getter.returnType()))
            if (setter != null && setter.returnType() === voidPrimitive) {
                // A getter and a setter, so it is an AssignableStackEntry
                return StaticGetterSetter(subjectType, getter, setter)
            } else {
                // Just a getter
                return StaticMethodCall(subjectType, emptyList(), emptyList(), getter)
            }
        }
    } else {
        val subjectType = subject.resolvedType()

        // Note, the fake method "size()" can also be used on arrays, to make them look similar to lists.
        if (subjectType.isArrayType() && name == "length") {
            return SimpleStackEntry(intPrimitive) { mv ->
                subject.compile(mv)
                mv.visitInsn(ARRAYLENGTH)
            }
        }

        subjectType.findField(name)?.let { field ->
            if (field.isStatic()) {
                return null
            } else {
                return NonStaticFieldAccess(subject, field)
            }
        }
        subjectType.findMethod(getterName(name), emptyList(), emptyList())?.let { getter ->
            if (getter.isStatic()) {
                return null
            }
            val setter = subjectType.findMethod(setterName(name), emptyList(), listOf(getter.returnType()))
            if (setter == null || setter.isStatic()) {
                return LazyNonStaticMethodCall(subject, getterName(name), emptyList(), emptyList(), source, position)
            } else {
                return NonStaticGetterSetter(subject, getter, setter)
            }
        }

    }
    return null

}



