package uk.co.nickthecoder.feather

import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.CompilerWorker
import uk.co.nickthecoder.feather.internal.MetaDataType
import uk.co.nickthecoder.feather.internal.enumClass
import uk.co.nickthecoder.feather.internal.objectClass
import java.io.File
import java.lang.reflect.Type

internal class Source private constructor(
    internal val worker: CompilerWorker,
    val name: String,
    val source: String?,
    val file: File?
) {

    constructor(worker: CompilerWorker, name: String) : this(worker, name, name, null)

    constructor(worker: CompilerWorker, file: File) : this(worker, file.nameWithoutExtension, file.path, file)

    var packageName: String? = null

    val importPackages = mutableListOf<String>()

    val importClasses = mutableMapOf<String, String>()
    
    /**
     * The key is the (alias) name of the field/methods to import, the value is the list of Class and the actual name.
     * Used by an import static without a .* at the end.
     */
    private val staticImports = mutableMapOf<String, MutableList<Pair<Type, String>>>()

    /**
     * A list of class names, where all static methods are imported.
     * e.g. import foo.Foo.* "foo.Foo" is added to the list.
     * This will NOT contain valid java class names, as these will be added to [staticImports]
     * straight away.
     */
    private val staticImportClasses = mutableListOf<String>()

    internal val classes = mutableListOf<MetaDataType>()

    fun qualifiedName(name: String) = if (packageName.isNullOrEmpty()) name else "$packageName.$name"

    /**
     * Returns the class and the actual (not the alias) for the statically imported field/method.
     */
    fun findStaticAliases(name: String): List<Pair<Type, String>> {
        if ( staticImportClasses.isNotEmpty() ) {
            staticImportClasses.forEach{ className ->
                worker.metaDataMap[className]?.let {
                    importStaticClass(it)
                }
            }
            staticImportClasses.clear()
        }
        return staticImports[name] ?: emptyList()
    }

    fun importStaticClass(className : String) {
        val klass = worker.lookupClassName(className)
        if (klass == null) {
            // The class hasn't been resolved - it may be a feather class
            staticImportClasses.add( className )
        } else {
            importStaticClass( klass )
        }
    }

    private fun importStaticClass(type: Type) {
        if (type is Class<*>) {
            type.methods.filter { it.isStatic() }.groupBy { it.name }.keys.forEach { methodName ->
                this.importStaticAlias(type, methodName, methodName)
            }
            type.fields.filter { it.isStatic() }.forEach { field ->
                this.importStaticAlias(type, field.name, field.name)
            }
        }
        if (type is MetaDataType) {
            type.methods.filter { it.isStatic }.groupBy { it.name }.keys.forEach { methodName ->
                this.importStaticAlias(type, methodName, methodName)
            }
            type.fields.filter { it.isStatic }.forEach { field ->
                this.importStaticAlias(type, field.name, field.name)
            }
        }

        // Also import static methods/fields from the super type(s).
        // I'm not sure if this fits in with Java's way of thinking, but as static fields/methods are the closest
        // thing we have to class methods/variables (in a full OO language, such as smalltalk), then importing
        // the super class's fields/methods is as close as we can get to importing all class methods/variables.
        val superType = type.superType()
        if (superType != null && superType !== type && superType !== objectClass && superType !== enumClass) {
            importStaticClass(superType)
        }
    }

    fun importStaticAlias(type: Type, actualName: String, aliasName: String) {
        var existing = staticImports[aliasName]
        if (existing == null) {
            existing = mutableListOf<Pair<Type, String>>()
            staticImports[aliasName] = existing
        }
        existing.add(Pair(type, actualName))
    }

}


class StaticImportDetails(
        val klass: Class<*>,
        val aliasName: String,
        val realName: String
)
