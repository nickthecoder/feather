package uk.co.nickthecoder.feather.internal.operators

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import org.objectweb.asm.Opcodes.CHECKCAST
import uk.co.nickthecoder.feather.FeatherException
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.Source
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.special.MethodWrapper
import uk.co.nickthecoder.feather.internal.special.findExtensionFunction
import java.lang.reflect.Type
import java.lang.reflect.TypeVariable


internal abstract class BinaryOperator(
    val a: StackEntry,
    val b: StackEntry,
    val operator: String,
    val operatorMethodName: String?,
    val source: Source,
    val position: FeatherPosition
) : CachedTypeStackEntry() {

    override fun actualType(): Type {
        return if (operatorMethod == null) {
            standardType().primitive()
        } else {
            resolveGenericType(a.resolvedType(), operatorMethod!!.genericReturnType())
        }
    }

    abstract fun standardType(): Type

    protected fun applyOperator(mv: MethodVisitor) {
        val method = operatorMethod!!

        if (method.isStatic()) {
            mv.visitMethodInsn(
                    Opcodes.INVOKESTATIC,
                    method.owner().asmName(),
                    method.name(),
                    asmMethodDescription(method.parameterTypes(), method.returnType()),
                    method.owner().isInterface()
            )
        } else {
            val isInterface = method.owner().isInterface()
            mv.visitMethodInsn(
                    if (isInterface) Opcodes.INVOKEINTERFACE else Opcodes.INVOKEVIRTUAL,
                    method.owner().asmName(),
                    method.name(),
                    asmMethodDescription(method.parameterTypes(), method.returnType()),
                    isInterface
            )
        }
        val grt = method.genericReturnType()
        if (grt is TypeVariable<*>) {
            mv.visitTypeInsn(CHECKCAST, resolvedType().asmName())
        }
    }

    protected fun operatorFirstType(): Type {
        val m = operatorMethod ?: return voidPrimitive
        return if (m.isStatic()) {
            m.parameterTypes()[0]
        } else {
            m.owner()
        }
    }

    protected fun operatorSecondType(): Type {
        val m = operatorMethod ?: return voidPrimitive
        return if (m.isStatic()) {
            m.parameterTypes()[1]
        } else {
            m.parameterTypes()[0]
        }
    }

    override fun compile(mv: MethodVisitor) {
        mv.addLineNumber(position)
        if (operatorMethod == null) {
            standardCompile(mv)
        } else {
            a.compileAutoCast(mv, operatorFirstType())
            b.compileAutoCast(mv, operatorSecondType())
            applyOperator(mv)
        }
    }

    abstract fun standardCompile(mv: MethodVisitor)

    private val operatorMethod: MethodWrapper? by lazy {

        if (operatorMethodName == null) {
            null
        } else {
            val normalMethod = a.resolvedType().findMethod(operatorMethodName, emptyList(), listOf(b.resolvedType()))
            normalMethod ?: a.resolvedType().findExtensionFunction(operatorMethodName, listOf(b.resolvedType()), source)
        }
    }

    protected fun unexpectedTypes() = FeatherException.unexpectedTypes(position, operator, a, b)


}
