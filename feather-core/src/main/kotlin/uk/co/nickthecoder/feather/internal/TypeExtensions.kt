package uk.co.nickthecoder.feather.internal

import org.antlr.v4.runtime.Token
import uk.co.nickthecoder.feather.FeatherException
import uk.co.nickthecoder.feather.NullType
import uk.co.nickthecoder.feather.Source
import uk.co.nickthecoder.feather.reflect.Function
import java.lang.reflect.*
import java.util.*

internal val voidPrimitive = Void::class.javaPrimitiveType!!
internal val booleanPrimitive = Boolean::class.javaPrimitiveType!!
internal val bytePrimitive = Byte::class.javaPrimitiveType!!
internal val charPrimitive = Char::class.javaPrimitiveType!!
internal val shortPrimitive = Short::class.javaPrimitiveType!!
internal val intPrimitive = Int::class.javaPrimitiveType!!
internal val longPrimitive = Long::class.javaPrimitiveType!!
internal val floatPrimitive = Float::class.javaPrimitiveType!!
internal val doublePrimitive = Double::class.javaPrimitiveType!!

internal val numberPrimitives =
    listOf(bytePrimitive, shortPrimitive, intPrimitive, longPrimitive, floatPrimitive, doublePrimitive)

internal val voidClass = java.lang.Void::class.java
internal val booleanClass = java.lang.Boolean::class.java
internal val byteClass = java.lang.Byte::class.java
internal val characterClass = java.lang.Character::class.java
internal val shortClass = java.lang.Short::class.java
internal val integerClass = java.lang.Integer::class.java
internal val longClass = java.lang.Long::class.java
internal val floatClass = java.lang.Float::class.java
internal val doubleClass = java.lang.Double::class.java

internal val objectClass = Object::class.java
internal val classClass = Class::class.java
internal val stringClass = String::class.java
internal val enumClass = Enum::class.java
internal val functionClass = uk.co.nickthecoder.feather.reflect.Function::class.java
internal val arrayClass = uk.co.nickthecoder.feather.internal.Array::class.java


/**
 * When a generic type is in the form MyClass<*>, then the "*" is defined as a [StarProjection].
 */
class StarProjection

/**
 * If this is an [UnresolvedType], then resolve it, otherwise, return this.
 *
 * This should only be needed when searching for methods/fields. It is NOT needed inside [StackEntry.compile],
 * because [StackEntry.resolvedType] should never return an [UnresolvedType].
 */
internal fun Type.resolve(): Type =
    when (this) {
        is UnresolvedType -> this.resolve()
        is ParameterizedType -> ParameterizedTypeImplementation(
            rawType.klass(),
            actualTypeArguments.map { it.resolve() }.toTypedArray()
        )
        is ImpliedType -> this.calculateType().resolve()
        else -> this
    }


/**
 * When given two primitive number types, works out which is the "larger", so that an operation between the two
 * will "auto-cast" to the "larger" type so that data is not lost.
 */
fun biggestNumberType(a: Type, b: Type): Class<*> {
    val index = Math.max(numberPrimitives.indexOf(a), numberPrimitives.indexOf(b))
    return numberPrimitives[index]
}

// TODO This will return a primitive type, even when one of the arguments is boxed number.
// This may cause problems if the value is null!
fun bestCommonNumberType(a: Type, b: Type): Class<*>? {
    return if (a.isNumber() && b.isNumber()) {
        val index = Math.max(numberPrimitives.indexOf(a.primitive()), numberPrimitives.indexOf(b.primitive()))
        numberPrimitives[index]
    } else {
        null
    }
}

fun Type.superType(): Type? {
    return when (this) {
        is Class<*> -> superclass
        is MetaDataType -> extendsType
        else -> null
    }
}

fun Type.interfaces(): List<Type> {
    return when (this) {
        is Class<*> -> interfaces.toList()
        is MetaDataType -> this.implementsTypes
        else -> emptyList()
    }
}

internal fun Type.nonPrimitive(): Type =
    when (this) {
        voidPrimitive -> voidClass
        booleanPrimitive -> booleanClass
        bytePrimitive -> byteClass
        charPrimitive -> characterClass
        shortPrimitive -> shortClass
        intPrimitive -> integerClass
        longPrimitive -> longClass
        floatPrimitive -> floatClass
        doublePrimitive -> doubleClass
        else -> this
    }


internal fun Type.primitive() =
    when (this) {
        voidClass -> voidPrimitive
        booleanClass -> booleanPrimitive
        byteClass -> byteClass
        characterClass -> charPrimitive
        shortClass -> shortPrimitive
        integerClass -> intPrimitive
        longClass -> longPrimitive
        floatClass -> floatPrimitive
        doubleClass -> doublePrimitive
        else -> this
    }

internal fun Type.isPrimitive() = this is Class<*> && isPrimitive

internal fun Type.isWholeNumber() =
    this.primitive() in listOf(bytePrimitive, shortPrimitive, intPrimitive, longPrimitive)

internal fun Type.isPrimitiveNumber() =
    this in listOf(bytePrimitive, shortPrimitive, intPrimitive, longPrimitive, floatPrimitive, doublePrimitive)

internal fun Type.isNumber() = this.primitive() in listOf(
    bytePrimitive,
    shortPrimitive,
    intPrimitive,
    longPrimitive,
    floatPrimitive,
    doublePrimitive
)

internal fun Type.niceString() = if (this is Class<*>) this.simpleName else toString()

/**
 * Get the class name for this type. Similar to [Class.name], but also handles [MetaDataType] and [ParameterizedType].
 */
internal fun Type.className(): String {
    return if (this is Class<*>) {
        this.name
    } else if (this is ParameterizedType) {
        (this.rawType as Class<*>).name
    } else if (this is MetaDataType) {
        this.name
    } else if (this is UnresolvedType) {
        this.resolve().className()
    } else {
        throw IllegalArgumentException("Not a Class nor a ParameterizedType : $this : ${this.javaClass}")
    }
}

internal fun Type.klass(): Class<*> {
    return if (this is Class<*>) {
        this
    } else if (this is ParameterizedType) {
        this.rawType as Class<*>
    } else {
        throw IllegalArgumentException("Not a Class nor a ParameterizedType : $this : ${this.javaClass}")
    }
}

internal fun Type.simpleTypeOrNull(): Type? =
    if (this is Class<*> || this is MetaDataType) {
        this
    } else if (this is ParameterizedType) {
        this.rawType
    } else {
        null
    }

internal fun Type.typeParameters(): kotlin.Array<out TypeVariable<out Class<*>>> {
    return if (this is Class<*>) {
        this.typeParameters
    } else {
        emptyArray()
    }
}

internal fun Type.extends(): Type? {
    return when (this) {
        is MetaDataType -> this.extendsType
        else -> this.klass().superclass
    }
}

/**
 * Auto-unbox and auto-cast values to compatible types.
 * If the types are already the same, the return that type unchanged (with no unboxing).
 * Otherwise, attempt to unbox into primitives if possible.
 * Then, if both are number types, return the "larger" of the two.
 * If both a and b are object types, then return [objectClass].
 * Otherwise return null, indicating that the types are not compatible.
 * e.g. a boolean and a number, or a primitive and an object.
 */
internal fun compatiblePrimitiveType(a: Type, b: Type): Type? {
    if (a == b) return a

    val primA = a.primitive()
    val primB = b.primitive()

    if (primA == primB) return primA

    if (primA in numberPrimitives && primB in numberPrimitives) {
        return biggestNumberType(primA, primB)
    }

    if (primA.isPrimitive() || primB.isPrimitive()) {
        return null
    } else {
        return objectClass
    }
}

internal fun canAutoCast(fromType: Type, toType: Type): Boolean {
    if (fromType is NullType && !toType.isPrimitive()) return true
    if (fromType.typeName == toType.typeName) return true
    if (toType == objectClass) return true
    if (fromType is UnresolvedType) return canAutoCast(fromType.resolve(), toType)
    if (toType is UnresolvedType) return canAutoCast(fromType, toType.resolve())

    val fromPrim = fromType.primitive()
    val toPrim = toType.primitive()

    if (fromPrim == toPrim) {
        return true
    }

    if (fromPrim in numberPrimitives && toPrim in numberPrimitives) {
        return true
    }

    return isAssignable(fromType, toType)
}

fun isAssignable( fromType : Type, toType : Type ) : Boolean {
    //println( "isAssignable from $fromType (${fromType.javaClass}) to $toType (${toType.javaClass})" )
    val result = if (fromType is MetaDataType) {
        (fromType.superTypes(false).map { it.resolve() }.contains(toType))
    } else {
        if (toType is MetaDataType) {
            false
        } else {
            if (fromType is ParameterizedType) {
                if (toType is ParameterizedType) {
                    if (isAssignable(fromType.rawType, toType.rawType)) {
                        val fromActuals = fromType.actualTypeArguments
                        val toActuals = toType.actualTypeArguments
                        fun actualsMatch(): Boolean {
                            for (i in fromActuals.indices) {
                                if (fromActuals[i] != toActuals[i]) return false
                            }
                            return true
                        }
                        if (fromActuals.size == toActuals.size) {
                            actualsMatch()
                        } else {
                            false
                        }
                    } else {
                        false
                    }
                } else {
                    // fromType is ParameterizedType, but toType isn't
                    // We can assign is the raw type is assignable
                    isAssignable(fromType.rawType, toType)
                }
            } else if (fromType is Class<*> && toType is Class<*>) {
                toType.isAssignableFrom(fromType)
            } else {
                false
            }
        }
    }
    return result
}

/**
 * Attempts to find a primitive type that both a and b can be cast to.
 * @return A primitive class that both are cast-able to, or [objectClass] if both are objects (with no primitive equivalents).
 * @throw [FeatherException] if they are not cast-able to a common primitive type, and aren't both objects.
 */
internal fun compatiblePrimitiveType(source: Source, token: Token, a: Type, b: Type): Type {
    return compatiblePrimitiveType(a, b) ?: throw FeatherException.unexpectedTypes(source, token, a, b)
}

fun Type.isInterface(): Boolean =
    if (this is ParameterizedType) {
        this.rawType.isInterface()
    } else if (this is Class<*>) {
        this.isInterface
    } else if (this is MetaDataType) {
        !this.isClass
    } else if (this is UnresolvedType) {
        resolve().isInterface()
    } else {
        throw IllegalArgumentException("Not a Class, nor a MetaDataType : $this")
    }

fun Type.isFinal(): Boolean =
    when (this) {
        is Class<*> -> Modifier.isFinal(modifiers)
        else -> false
    }


internal fun Type.isArrayType(): Boolean {
    if (this is MetaDataType) return false
    if (this is UnresolvedType) return false
    if (this.klass() == arrayClass) return true
    return klass().isArray
}

/**
 * Returns the type of element contained in the array Type.
 * If this Type is a multi-dimensional array, then only go one level deep.
 * e.g. for an Array<Array<String>>, the result would be Array<String>
 *
 * @see [deepestElementTypeOfArray]
 */
internal fun Type.shallowestElementTypeOfArray(): Type {
    return if (this is ParameterizedType && this.rawType == arrayClass) {
        this.actualTypeArguments[0]
    } else if (this == arrayClass) {
        objectClass
    } else if (this is GenericArrayType) {
        genericComponentType
    } else {
        klass().componentType
    }
}

/**
 * Returns the type of element contained in the array Type.
 * If this Type is a multi-dimensional array, then go to the deepest level.
 * e.g. for an Array<Array<String>>, the result would be String.
 *
 * @see [shallowestElementTypeOfArray]
 */
internal fun Type.deepestElementTypeOfArray(): Type {
    val shallow = shallowestElementTypeOfArray()
    return if (shallow.isArrayType()) {
        shallow.deepestElementTypeOfArray()
    } else {
        shallow
    }
}

fun Type.dimensionsOfArray(): Int {
    var result = 0
    var foo = this
    while (foo.isArrayType()) {
        result++
        foo = foo.shallowestElementTypeOfArray()
    }
    return result
}

fun Method.isStatic() = Modifier.isStatic(this.modifiers)

fun Field.isStatic() = Modifier.isStatic(this.modifiers)

fun getterName(fieldName: String): String {
    return "get" + fieldName.substring(0, 1).uppercase(Locale.getDefault()) + fieldName.substring(1)
}

fun setterName(fieldName: String): String {
    return "set" + fieldName.substring(0, 1).uppercase(Locale.getDefault()) + fieldName.substring(1)
}

fun functionType(argTypes: List<Type>, returnType: Type): Type {

    val all = Array<Type>(10) { voidClass }
    for (i in argTypes.indices) {
        all[i] = argTypes[i].nonPrimitive()
    }
    all[9] = returnType.nonPrimitive()

    return ParameterizedTypeImplementation(
        Function::class.java,
        all
    )
}

fun functionType(owner: Type, argTypes: List<Type>, returnType: Type): Type {

    val all = Array<Type>(10) { voidClass }
    all[0] = owner
    for (i in argTypes.indices) {
        all[i+1] = argTypes[i].nonPrimitive()
    }
    all[9] = returnType.nonPrimitive()

    return ParameterizedTypeImplementation(
        Function::class.java,
        all
    )
}

fun Type.isFunctionType() = this is ParameterizedType && this.rawType === functionClass

internal fun Type.superTypes(includeThis: Boolean): List<Type> {
    val result = mutableListOf<Type>()

    fun addSuperTypes(from: Type) {

        if (from is UnresolvedType) {
            val resolved = from.resolve()
            if (resolved !== from) {
                addSuperTypes(resolved)
            }
        } else if (from is Class<*>) {
            from.genericSuperclass?.let { superClass ->
                val resolved = resolveGenericType(from, superClass)
                if (!result.contains(resolved)) {
                    result.add(resolved)
                    addSuperTypes(resolved)
                }
            }
            from.genericInterfaces.forEach { superClass ->
                val resolved = resolveGenericType(from, superClass)
                if (!result.contains(resolved)) {
                    result.add(resolved)
                    addSuperTypes(resolved)
                }
            }
        } else if (from is ParameterizedType) {

            val raw = from.rawType
            if (raw is Class<*>) {
                raw.genericSuperclass?.let { superClass ->
                    if (!result.contains(superClass)) {
                        val resolved = resolveGenericType(from, superClass)
                        result.add(resolved)
                        addSuperTypes(resolved)
                    }
                }
                raw.genericInterfaces.forEach { superClass ->
                    if (!result.contains(superClass)) {
                        val resolved = resolveGenericType(from, superClass)
                        result.add(resolved)
                        addSuperTypes(resolved)
                    }
                }
            }

        } else if (from is MetaDataType) {
            from.extendsType?.let {
                if (!result.contains(it)) {
                    result.add(it)
                    addSuperTypes(it)
                }
            }
            from.implementsTypes.forEach {
                if (!result.contains(it)) {
                    result.add(it)
                    addSuperTypes(it)
                }
            }
        }
    }

    if (includeThis) {
        result.add(this)
    }
    addSuperTypes(this)

    return result
}
