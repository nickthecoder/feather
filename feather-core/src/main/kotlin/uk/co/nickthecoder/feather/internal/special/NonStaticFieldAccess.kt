package uk.co.nickthecoder.feather.internal.special

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.internal.*
import java.lang.reflect.TypeVariable

internal class NonStaticFieldAccess(
    val subject: StackEntry,
    val field: FieldWrapper
) : CachedTypeStackEntry(), AssignableStackEntry {

    override fun isFinal() = field.isFinal()

    override fun actualType() = field.genericReturnType(subject.type())

    override fun compile(mv: MethodVisitor) {
        subject.compile(mv)
        mv.visitFieldInsn(Opcodes.GETFIELD, field.owner().asmName(), field.name(), field.returnType().asmDescriptor())
        val grt = field.genericReturnType()
        if (grt is TypeVariable<*>) {
            mv.visitTypeInsn(Opcodes.CHECKCAST, resolvedType().asmName())
        }
    }

    override fun compileSet(value: StackEntry, mv: MethodVisitor) {
        subject.compile(mv)
        value.compileAutoCast(mv, field.genericReturnType(subject.resolvedType()))
        mv.visitFieldInsn(Opcodes.PUTFIELD, field.owner().asmName(), field.name(), field.returnType().asmDescriptor())
    }

    override fun adjust(
        mv: MethodVisitor,
        includeOldValue: Boolean,
        includeNewValue: Boolean,
        adjustment: (MethodVisitor) -> Unit
    ) {
        subject.compile(mv)
        // subject
        mv.visitInsn(Opcodes.DUP)
        // subject subject
        mv.visitFieldInsn(Opcodes.GETFIELD, field.owner().asmName(), field.name(), field.returnType().asmDescriptor())
        // subject oldValue
        if (includeOldValue) {
            mv.dupX1(resolvedType())
        }
        // (oldValue), subject oldValue
        adjustment(mv)
        // (oldValue), subject, newValue
        if (includeNewValue) {
            mv.dupX1(resolvedType())
        }
        // (oldValue), (newValue) subject, newValue
        mv.visitFieldInsn(Opcodes.PUTFIELD, field.owner().asmName(), field.name(), field.returnType().asmDescriptor())
        // (oldValue), (newValue)
    }
}