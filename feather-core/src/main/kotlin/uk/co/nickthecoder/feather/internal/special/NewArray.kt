package uk.co.nickthecoder.feather.internal.special

import org.objectweb.asm.MethodVisitor
import uk.co.nickthecoder.feather.FeatherException
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.internal.StackEntry
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.canAutoCast
import uk.co.nickthecoder.feather.internal.intPrimitive
import java.lang.reflect.Type

internal class NewArray(

    private val elementType: Type,
    private val args: List<StackEntry>,
    val position: FeatherPosition

) : CachedTypeStackEntry() {

    override fun actualType(): Type {
        var result = ParameterizedTypeImplementation(arrayClass, arrayOf(elementType))
        for (i in 0 until args.size - 1) {
            result = ParameterizedTypeImplementation(arrayClass, arrayOf(result))
        }
        return result
    }

    override fun compile(mv: MethodVisitor) {

        if (args.isEmpty()) {
            throw FeatherException.arraySizeMissing(position)
        }

        for (arg in args) {
            if (canAutoCast(intPrimitive, arg.resolvedType())) {
                arg.compileAutoCast(mv, intPrimitive)
            } else {
                throw FeatherException.incompatibleType(position, intPrimitive, arg.resolvedType())
            }
        }

        if (args.size == 1) {
            newArray(mv, resolvedType().shallowestElementTypeOfArray())
        } else {
            mv.visitMultiANewArrayInsn(resolvedType().asmDescriptor(), args.size)
        }
    }
}
