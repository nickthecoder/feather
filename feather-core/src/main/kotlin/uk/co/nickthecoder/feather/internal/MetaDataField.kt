package uk.co.nickthecoder.feather.internal

import uk.co.nickthecoder.feather.FeatherException
import uk.co.nickthecoder.feather.FeatherPosition
import java.lang.reflect.Type


/**
 * An item in [MetaDataType.fields]
 */
internal class MetaDataField(
    owner: MetaDataType,
    name: String,
    type: Type,
    isStatic: Boolean,
    val isFinal: Boolean,
    val isEnum: Boolean,
    val position: FeatherPosition
) : MetaDataFieldOrMethod(owner, name, type, isStatic) {

    /**
     * If the initial value is a constant, then this will have the value of that constant.
     * If it is an expression or no value was specified, then this will be null.
     */
    var initialValue: Any? = null

    /**
     * If there is an initial value specified (either a constant, or an expression),
     * this is the StackEntry for that value.
     * If no initial value was specified, then this will be null.
     */
    var initialExpression: StackEntry? = null

    var annotations: List<AnnotationMetaData> = emptyList()

    private var evaluatingType = false

    fun type(): Type {
        return if (type is ImpliedType) {
            if (evaluatingType) {
                throw FeatherException.recursiveTypeDefinition((type as ImpliedType).position)
            }
            evaluatingType = true
            return try {
                initialExpression?.resolvedType() ?: voidPrimitive
            } finally {
                evaluatingType = false
            }
        } else {
            type
        }
    }
}
