package uk.co.nickthecoder.feather

import org.antlr.v4.runtime.CharStreams
import uk.co.nickthecoder.feather.internal.CompilerWorker
import java.io.File

/**
 * If you want to compile code from a mixture of [File] and [String], then wrap each in
 * [FileScript] / [StringScript], and call [FeatherCompiler.compile] with the list/array of
 * [Script].
 *
 * Normally, you can ignore this, and call either [FeatherCompiler.compile] with an array of [File],
 * [FeatherCompiler.compileFiles] or [FeatherCompiler.compileFromStrings].
 */
interface Script
class FileScript(val file: File) : Script {
    override fun toString() = "File : $file"
}

class StringScript(val name: String, val string: String) : Script {
    override fun toString() = name
}

class FeatherCompiler(
    val configuration: CompilerConfiguration
) {

    constructor() : this(CompilerConfiguration())

    fun compile(scripts: List<Script>): Map<String, ByteArray> {
        val worker = CompilerWorker(configuration.clone())

        scripts.forEach { script ->
            val source = when (script) {
                is FileScript -> Source(worker, script.file)
                is StringScript -> Source(worker, script.name)
                else -> throw IllegalArgumentException("Expected a FileScript or StringScript")
            }
            val stream = when (script) {
                is FileScript -> CharStreams.fromFileName(script.file.path)
                is StringScript -> CharStreams.fromString(script.string)
                else -> throw IllegalArgumentException("Expected a FileScript or StringScript")
            }
            worker.firstPhase(source, stream)
        }

        // Don't try the 2nd phase, if errors were reported in the 1st phase.
        worker.throwAnyErrors()
        val result = worker.secondPhase()
        worker.throwAnyErrors()

        return result
    }

    fun compile(vararg scripts: Script): Map<String, ByteArray> = compile(scripts.toList())

    fun compile(vararg files: File) = compile(files.map { FileScript(it) })


    fun compileFromStrings(codes: List<String>) =
        compile(codes.mapIndexed { index, code -> StringScript("script#$index", code) })


    fun compileFromStrings(vararg codes: String) =
        compile(codes.mapIndexed { index, code -> StringScript("script#$index", code) })

    /**
     * Compiles multiple source files, this will typically be called by first scanning a directory for
     * all ".feather" files (possibly recursively), and then feeding all the found files here.
     * You can also compile a single feather file, but it will NOT be able to import other feather classes,
     * unless they have previously been compiled, and added to the classloader.
     */
    fun compileFiles(files: List<File>) = compile(files.map { FileScript(it) })

    fun compileFromNamedStrings(nameCodePairs: List<Pair<String, String>>) =
        compile(nameCodePairs.map { (name, code) -> StringScript(name, code) })
}
