package uk.co.nickthecoder.feather.internal.operators

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.Source
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.bytePrimitive
import uk.co.nickthecoder.feather.internal.doublePrimitive
import uk.co.nickthecoder.feather.internal.floatPrimitive
import uk.co.nickthecoder.feather.internal.intPrimitive
import uk.co.nickthecoder.feather.internal.longPrimitive
import uk.co.nickthecoder.feather.internal.shortPrimitive


internal class Remainder(
    a: StackEntry,
    b: StackEntry,
    source : Source,
    position: FeatherPosition
) : BinaryMathsOperator(a, b, "%", "rem", source, position) {

    override fun compileApply(mv: MethodVisitor) {
        when (resolvedType()) {
            bytePrimitive, shortPrimitive, intPrimitive -> {
                mv.visitInsn(Opcodes.IREM)
            }
            longPrimitive -> {
                mv.visitInsn(Opcodes.LREM)
            }
            floatPrimitive -> {
                mv.visitInsn(Opcodes.FREM)
            }
            doublePrimitive -> {
                mv.visitInsn(Opcodes.DREM)
            }
            else -> throw unexpectedTypes()
        }
    }
}