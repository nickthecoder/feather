package uk.co.nickthecoder.feather.internal.operators

import org.objectweb.asm.Label
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes.*
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.StackEntry
import uk.co.nickthecoder.feather.internal.objectClass

internal class Elvis(
    val a: StackEntry,
    val b: StackEntry,
    val position: FeatherPosition

) : CachedTypeStackEntry() {

    override fun actualType() =
            findCommonSuperType(a.type(), b.type())

    override fun compile(mv: MethodVisitor) {
        val skip = Label()

        a.compileAutoCast(mv, objectClass)

        mv.visitInsn(DUP)
        mv.visitJumpInsn(IFNONNULL, skip)
        mv.visitInsn(POP)

        b.compileAutoCast(mv, objectClass)

        mv.visitLabel(skip)
    }

}
