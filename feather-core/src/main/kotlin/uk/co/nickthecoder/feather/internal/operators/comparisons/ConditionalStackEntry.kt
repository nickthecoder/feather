package uk.co.nickthecoder.feather.internal.operators.comparisons

import org.objectweb.asm.Label
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes.*
import uk.co.nickthecoder.feather.FeatherException
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.booleanPrimitive
import uk.co.nickthecoder.feather.internal.charPrimitive
import uk.co.nickthecoder.feather.internal.primitive
import java.lang.reflect.Type


internal class ConditionalStackEntry(
    val a: StackEntry,
    val b: StackEntry,
    val operator: String,
    val comparison: Comparison,
    val oppositeComparison: Comparison,
    val position: FeatherPosition

) : StackEntry {

    val notEqualLabel = Label()
    val equalsLabel = Label()

    override fun type() = booleanPrimitive

    private fun prepare(mv: MethodVisitor): Type? {
        val commonType = comparisonCommonType(a.resolvedType(), b.resolvedType())
        a.compile(mv)
        if (commonType != null) autoCast(mv, a.resolvedType(), commonType)
        b.compile(mv)
        if (commonType != null) autoCast(mv, b.resolvedType(), commonType)
        return commonType
    }

    fun compileComparison(mv: MethodVisitor) {
        val commonType = prepare(mv)
        if (!comparison.compile(mv, commonType, a.resolvedType().primitive(), b.type().primitive(), notEqualLabel)) {
            throw FeatherException.comparisonFailed(position)
        }
    }

    fun compileOppositeComparison(mv: MethodVisitor) {
        val commonType = prepare(mv)
        oppositeComparison.compile(mv, commonType, a.resolvedType().primitive(), b.type().primitive(), notEqualLabel)
    }

    override fun compile(mv: MethodVisitor) {

        val commonType = prepare(mv)
        comparison.compile(mv, commonType, a.resolvedType().primitive(), b.type().primitive(), notEqualLabel)
        mv.visitInsn(ICONST_1)
        mv.visitJumpInsn(GOTO, equalsLabel)
        mv.visitLabel(notEqualLabel)
        mv.visitInsn(ICONST_0)
        mv.visitLabel(equalsLabel)
    }

    companion object {

        private fun comparisonCommonType(a: Type, b: Type): Type? {
            val aPrim = a.primitive()
            val bPrim = b.primitive()

            return when (aPrim) {
                booleanPrimitive -> if (bPrim == booleanPrimitive) booleanPrimitive else null
                charPrimitive -> if (bPrim == charPrimitive) charPrimitive else null
                else -> bestCommonNumberType(a, b)
            }
        }

        internal fun isEqual(a: StackEntry, b: StackEntry, position: FeatherPosition) =
                ConditionalStackEntry(a, b, "==", Equal(), NotEqual(), position)

        internal fun isNotEqual(a: StackEntry, b: StackEntry, position: FeatherPosition) =
                ConditionalStackEntry(a, b, "!=", NotEqual(), Equal(), position)

        internal fun isSameObject(a: StackEntry, b: StackEntry, position: FeatherPosition) =
                ConditionalStackEntry(a, b, "===", SameInstance(), NotSameInstance(), position)

        internal fun isNotSameObject(a: StackEntry, b: StackEntry, position: FeatherPosition) =
                ConditionalStackEntry(a, b, "!==", NotSameInstance(), SameInstance(), position)

        internal fun isLessThan(a: StackEntry, b: StackEntry, position: FeatherPosition) =
                ConditionalStackEntry(a, b, "<", LessThan(), GreaterThanOrEqual(), position)

        internal fun isLessThanOrEqual(a: StackEntry, b: StackEntry, position: FeatherPosition) =
                ConditionalStackEntry(a, b, "<=", LessThanOrEqual(), GreaterThan(), position)

        internal fun isGreaterThan(a: StackEntry, b: StackEntry, position: FeatherPosition) =
                ConditionalStackEntry(a, b, ">", GreaterThan(), LessThanOrEqual(), position)

        internal fun isGreaterThanOrEqual(a: StackEntry, b: StackEntry, position: FeatherPosition) =
                ConditionalStackEntry(a, b, ">=", GreaterThanOrEqual(), LessThan(), position)

    }

}
