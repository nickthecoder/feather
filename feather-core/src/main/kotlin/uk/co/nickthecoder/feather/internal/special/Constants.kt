package uk.co.nickthecoder.feather

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.internal.*
import java.lang.reflect.Type

internal abstract class ConstantStackEntry(
    private val type: Type
) : StackEntry {

    override fun type() = type

    abstract val constantValue: Any?

    fun castTo(type: Type): ConstantStackEntry? {
        if (type == resolvedType()) return this
        val cv = constantValue
        if (cv is Number) {
            when (type) {
                charPrimitive -> return CharConstant(cv.toInt().toChar())
                bytePrimitive -> return ByteConstant(cv.toByte())
                shortPrimitive -> return ShortConstant(cv.toShort())
                intPrimitive -> return IntConstant(cv.toInt())
                longPrimitive -> return LongConstant(cv.toLong())
                floatPrimitive -> return FloatConstant(cv.toFloat())
                doublePrimitive -> return DoubleConstant(cv.toDouble())
            }
        }
        return null
    }

    override fun compileAutoCast(mv: MethodVisitor, to: Type) {
        this.castTo(to)?.let {
            it.compile(mv)
            return
        }
        compile(mv)
        autoCast(mv, resolvedType(), to)
    }
}

internal class VoidConstant : ConstantStackEntry(voidPrimitive) {
    override val constantValue: Any
        get() = Unit

    override fun compile(mv: MethodVisitor) {
        // Do nothing
    }

    companion object {
        val instance = VoidConstant()
    }
}

internal class BooleanConstant(
    override val constantValue: Boolean
) : ConstantStackEntry(booleanPrimitive) {

    override fun compile(mv: MethodVisitor) {
        mv.visitInsn(if (constantValue) Opcodes.ICONST_1 else Opcodes.ICONST_0)
    }
}

internal abstract class Constant32Bit(type: Type) :
    ConstantStackEntry(type) {

    fun compile(value: Int, mv: MethodVisitor) {
        intConstant(mv, value)
    }
}

internal class ByteConstant(
    override val constantValue: Byte
) : Constant32Bit(bytePrimitive) {
    override fun compile(mv: MethodVisitor) = compile(constantValue.toInt(), mv)
}

internal class ShortConstant(
    override val constantValue: Short
) : Constant32Bit(shortPrimitive) {
    override fun compile(mv: MethodVisitor) = compile(constantValue.toInt(), mv)
}

internal class IntConstant(
    override val constantValue: Int
) : Constant32Bit(intPrimitive) {
    override fun compile(mv: MethodVisitor) = compile(constantValue, mv)
}

internal class CharConstant(
    override val constantValue: Char
) : Constant32Bit(charPrimitive) {
    override fun compile(mv: MethodVisitor) = compile(constantValue.code, mv)
}

internal class LongConstant(
    override val constantValue: Long
) : ConstantStackEntry(longPrimitive) {

    override fun compile(mv: MethodVisitor) {
        when (constantValue) {
            0L -> mv.visitInsn(Opcodes.LCONST_0)
            1L -> mv.visitInsn(Opcodes.LCONST_1)
            else -> mv.visitLdcInsn(constantValue)
        }
    }
}

internal class FloatConstant(
    override val constantValue: Float
) : ConstantStackEntry(floatPrimitive) {

    override fun compile(mv: MethodVisitor) {
        when (constantValue) {
            0f -> mv.visitInsn(Opcodes.FCONST_0)
            1f -> mv.visitInsn(Opcodes.FCONST_1)
            2f -> mv.visitInsn(Opcodes.FCONST_2)
            else -> mv.visitLdcInsn(constantValue)
        }
    }
}

internal class DoubleConstant(
    override val constantValue: Double
) : ConstantStackEntry(doublePrimitive) {

    override fun compile(mv: MethodVisitor) {
        when (constantValue) {
            0.0 -> mv.visitInsn(Opcodes.DCONST_0)
            1.0 -> mv.visitInsn(Opcodes.DCONST_1)
            else -> mv.visitLdcInsn(constantValue)
        }
    }
}

internal class StringConstant(
    override val constantValue: String
) : ConstantStackEntry(stringClass) {

    override fun compile(mv: MethodVisitor) {
        mv.visitLdcInsn(constantValue)
    }

    override fun toString() = "StringConstant : $constantValue"
}

/**
 * The Type of a null literal.
 * Note, Java does not have such a type, so we have to define our own.
 * We could NOT use Object as the type, because for example we want to assign a null value to
 * String variable. So whatever type "null" is, it must be auto-castable to a String (and Object is not
 * auto-castable to a String).
 */
class NullType : Type {
    companion object {
        val instance = NullType()
    }
}


internal class NullLiteral() : ConstantStackEntry(NullType.instance) {
    override val constantValue: Any?
        get() = null

    override fun compile(mv: MethodVisitor) {
        mv.visitInsn(Opcodes.ACONST_NULL)
    }

    companion object {
        val instance = NullLiteral()
    }
}


internal class ClassConstant(
    override val constantValue: Type
) : ConstantStackEntry(ParameterizedTypeImplementation(classClass, arrayOf(constantValue))) {

    override fun compile(mv: MethodVisitor) {
        val asmType = org.objectweb.asm.Type.getType(constantValue.asmDescriptor())
        mv.visitLdcInsn(asmType)
    }

}
