package uk.co.nickthecoder.feather.internal.operators

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.asmMethodDescription
import uk.co.nickthecoder.feather.internal.objectClass
import uk.co.nickthecoder.feather.internal.pushArgs
import uk.co.nickthecoder.feather.internal.voidPrimitive
import uk.co.nickthecoder.feather.runtime.Duo
import java.lang.reflect.Type

internal class MapTo(
    val a: StackEntry,
    val b: StackEntry
) : StackEntry {

    override fun type(): Type {
        return ParameterizedTypeImplementation(
            Duo::class.java,
            arrayOf(a.type().nonPrimitive(), b.type().nonPrimitive())
        )
    }

    override fun compile(mv: MethodVisitor) {
        val klass = Duo::class.java
        val constructor = klass.getConstructor(objectClass, objectClass)

        val paramTypes = constructor.parameterTypes
        mv.visitTypeInsn(Opcodes.NEW, klass.asmName())
        mv.visitInsn(Opcodes.DUP)

        pushArgs(mv, listOf(a, b), paramTypes.toList(), false)

        mv.visitMethodInsn(
                Opcodes.INVOKESPECIAL,
                klass.asmName(),
                "<init>",
                asmMethodDescription(paramTypes, voidPrimitive),
                false
        )
    }

}
