package uk.co.nickthecoder.feather.internal.keywords

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes.ATHROW
import uk.co.nickthecoder.feather.FeatherException
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.voidPrimitive

internal class Throw(
    val throwable: StackEntry,
    val position: FeatherPosition
) : StackEntry {

    override fun type() = voidPrimitive

    override fun isTerminating() = true

    override fun compile(mv: MethodVisitor) {
        if (Throwable::class.java.isAssignableFrom(throwable.resolvedType().klass())) {
            mv.addLineNumber(position)
            throwable.compile(mv)
            mv.visitInsn(ATHROW)
        } else {
            throw FeatherException.expectedTypeThrowable(position, throwable.resolvedType())
        }
    }
}
