package uk.co.nickthecoder.feather.internal.operators

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.Source
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.bytePrimitive
import uk.co.nickthecoder.feather.internal.charPrimitive
import uk.co.nickthecoder.feather.internal.doublePrimitive
import uk.co.nickthecoder.feather.internal.floatPrimitive
import uk.co.nickthecoder.feather.internal.intPrimitive
import uk.co.nickthecoder.feather.internal.longPrimitive
import uk.co.nickthecoder.feather.internal.shortPrimitive
import java.lang.reflect.Type


internal class Minus(
    a: StackEntry,
    b: StackEntry,
    source : Source,
    position: FeatherPosition
) : BinaryMathsOperator(a, b, "-", "minus", source, position) {

    override fun standardType(): Type =
            if (a.type().primitive() === charPrimitive && b.type().primitive() in listOf(bytePrimitive, shortPrimitive, intPrimitive)) {
                charPrimitive
            } else {
                super.standardType()
            }

    override fun standardCompile(mv: MethodVisitor) {
        when (resolvedType().primitive()) {

            charPrimitive -> {
                a.compile(mv)
                autoCast(mv, a.resolvedType(), intPrimitive)
                b.compile(mv)
                autoCast(mv, b.resolvedType(), intPrimitive)
                mv.visitInsn(Opcodes.ISUB)
                autoCast(mv, intPrimitive, charPrimitive)
            }

            else -> super.standardCompile(mv)
        }
    }

    override fun compileApply(mv: MethodVisitor) {
        when (resolvedType().primitive()) {
            bytePrimitive, shortPrimitive, intPrimitive -> mv.visitInsn(Opcodes.ISUB)
            longPrimitive -> mv.visitInsn(Opcodes.LSUB)
            floatPrimitive -> mv.visitInsn(Opcodes.FSUB)
            doublePrimitive -> mv.visitInsn(Opcodes.DSUB)
            else -> throw unexpectedTypes()
        }
    }
}
