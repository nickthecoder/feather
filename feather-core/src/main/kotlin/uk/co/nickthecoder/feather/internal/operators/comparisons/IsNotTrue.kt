package uk.co.nickthecoder.feather.internal.operators.comparisons

import org.objectweb.asm.Label
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.internal.booleanPrimitive
import java.lang.reflect.Type


internal class IsNotTrue : Comparison {
    override fun compile(mv: MethodVisitor, commonType: Type?, aType: Type, bType: Type, notEqualLabel: Label): Boolean {
        return if (aType == booleanPrimitive) {
            mv.visitJumpInsn(Opcodes.IFNE, notEqualLabel)
            true
        } else {
            false
        }
    }
}
