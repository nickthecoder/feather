package uk.co.nickthecoder.feather.internal.operators

import org.objectweb.asm.MethodVisitor
import uk.co.nickthecoder.feather.FeatherException
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.internal.AssignableStackEntry
import uk.co.nickthecoder.feather.internal.StackEntry
import uk.co.nickthecoder.feather.internal.canAutoCast
import uk.co.nickthecoder.feather.internal.voidPrimitive


internal class Assignment(
    val a: AssignableStackEntry,
    val b: StackEntry,
    val isIntialisation : Boolean,
    val position : FeatherPosition

) : StackEntry {

    override fun type() = voidPrimitive

    override fun compile(mv: MethodVisitor) {
        if (!isIntialisation && a.isFinal()) {
            throw FeatherException.cannotChange(position)
        }
        if (!canAutoCast(b.resolvedType(), a.resolvedType())) {
            throw FeatherException.unexpectedTypes(position, "=", a, b)
        }
        a.compileSet(b, mv)
    }
}
