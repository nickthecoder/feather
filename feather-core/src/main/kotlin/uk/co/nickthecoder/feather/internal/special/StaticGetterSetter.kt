package uk.co.nickthecoder.feather.internal.special

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.internal.*
import java.lang.reflect.Type

/**
 * Allows get/set methods to be seen as a field in feather.
 */
internal class StaticGetterSetter(
    val subjectType: Type,
    val getter: MethodWrapper,
    val setter: MethodWrapper
) : CachedTypeStackEntry(), AssignableStackEntry {

    override fun isFinal() = false

    override fun actualType() = getter.genericReturnType()

    override fun compile(mv: MethodVisitor) {

        mv.visitMethodInsn(
            Opcodes.INVOKESTATIC,
            subjectType.asmName(),
            getter.name(),
            asmMethodDescription(emptyList(), getter.returnType()),
            subjectType.isInterface()
        )
    }

    override fun compileSet(value: StackEntry, mv: MethodVisitor) {
        value.compileAutoCast(mv, getter.returnType())
        mv.visitMethodInsn(
            Opcodes.INVOKESTATIC,
            subjectType.asmName(),
            setter.name(),
            asmMethodDescription(listOf(getter.returnType()), voidPrimitive),
            subjectType.isInterface()
        )
    }

    override fun adjust(
        mv: MethodVisitor,
        includeOldValue: Boolean,
        includeNewValue: Boolean,
        adjustment: (MethodVisitor) -> Unit
    ) {
        mv.visitMethodInsn(
            Opcodes.INVOKESTATIC,
            subjectType.asmName(),
            getter.name(),
            asmMethodDescription(emptyList(), getter.returnType()),
            subjectType.isInterface()
        )

        if (includeOldValue) {
            mv.dup(resolvedType())
        }

        adjustment(mv)

        if (includeNewValue) {
            mv.dup(resolvedType())
        }

        mv.visitMethodInsn(
            Opcodes.INVOKESTATIC,
            subjectType.asmName(),
            setter.name(),
            asmMethodDescription(listOf(getter.returnType()), voidPrimitive),
            subjectType.isInterface()
        )
    }
}