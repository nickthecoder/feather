package uk.co.nickthecoder.feather.internal.special

import uk.co.nickthecoder.feather.FeatherException
import uk.co.nickthecoder.feather.FeatherPosition
import uk.co.nickthecoder.feather.Source
import uk.co.nickthecoder.feather.internal.*
import java.lang.reflect.Type


internal fun createStaticMethodCall(
    subjectType: Type,
    name: String,
    args: List<StackEntry>,
    resolvedTypeArgs: List<Type>,
    source: Source,
    position: FeatherPosition
): StaticMethodCall? {

    val argTypes = args.map { it.resolvedType() }

    subjectType.findMethod(name, resolvedTypeArgs, argTypes)?.let { method ->
        if (!method.isStatic()) {
            throw FeatherException.expectedStaticMethod(position, subjectType, name, argTypes)
        }
        if (method.typeParameters().size != resolvedTypeArgs.size) {
            throw FeatherException.mismatchedTypeParameters(position, name, method.typeParameters().size, resolvedTypeArgs.size)
        }
        return StaticMethodCall(subjectType, args, resolvedTypeArgs, method)
    }

    return null
}

internal fun createNonStaticMethodCall(
    subject: StackEntry,
    name: String,
    args: List<StackEntry>,
    resolvedTypeArgs: List<Type>,
    allowStatic: Boolean,
    source: Source,
    position: FeatherPosition
): StackEntry? {

    val resolvedArgTypes = args.map { it.resolvedType() }
    val resolvedSubjectType = subject.resolvedType()

    resolvedSubjectType.findMethod(name, resolvedTypeArgs, resolvedArgTypes)?.let { method ->
        if (method.isStatic()) {
            if (allowStatic) {
                return StaticMethodCall(subject.resolvedType(), args, resolvedArgTypes, method)
            } else {
                throw FeatherException.unexpectedStaticMethod(position, resolvedSubjectType, name, resolvedArgTypes)
            }
        }
        if (!scopeOkFrom(subject, method.modifiers)) {
            throw FeatherException.inaccessibleMethod(position, resolvedSubjectType, name, resolvedArgTypes)
        }
        return NonStaticMethodCall(subject, args, resolvedTypeArgs, method)
    }

    // If the subjectType is an Interface, then the above won't have found methods on Object, so
    // let's do that now...
    if (resolvedSubjectType.isInterface()) {
        objectClass.findMethod(name, resolvedTypeArgs, resolvedArgTypes)?.let { method ->
            if (!method.isStatic()) {
                return NonStaticMethodCall(ToObjectStackEntry(subject), args, resolvedTypeArgs, method)
            }
        }
    }

    // Let's look for an extension function...
    val extArgs = args.toMutableList().apply { add(0, subject) }
    source.findStaticAliases(name).forEach { (klass, realName) ->
        createStaticMethodCall(klass, realName, extArgs, resolvedTypeArgs, source, position)?.let {
            return it
        }
    }

    return null
}

internal fun Type.findExtensionFunction(name: String, resolvedArgTypes: List<Type>, source: Source): MethodWrapper? {
    val extArgs = resolvedArgTypes.toMutableList()
    extArgs.add(0, this)

    source.findStaticAliases(name).forEach { (klass, realName) ->
        klass.findMethod(realName, emptyList(), extArgs)?.let { method ->
            if (method.isStatic()) {
                return method
            }
        }
    }
    return null
}
