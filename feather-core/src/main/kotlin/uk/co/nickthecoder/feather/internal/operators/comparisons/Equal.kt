package uk.co.nickthecoder.feather.internal.operators.comparisons

import org.objectweb.asm.Label
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.internal.*
import uk.co.nickthecoder.feather.internal.booleanPrimitive
import uk.co.nickthecoder.feather.internal.bytePrimitive
import uk.co.nickthecoder.feather.internal.charPrimitive
import uk.co.nickthecoder.feather.internal.doublePrimitive
import uk.co.nickthecoder.feather.internal.floatPrimitive
import uk.co.nickthecoder.feather.internal.intPrimitive
import uk.co.nickthecoder.feather.internal.longPrimitive
import uk.co.nickthecoder.feather.internal.objectClass
import uk.co.nickthecoder.feather.internal.shortPrimitive
import java.lang.reflect.Type


internal class Equal() : Comparison {
    override fun compile(mv: MethodVisitor, commonType: Type?, aType: Type, bType: Type, notEqualLabel: Label): Boolean {

        return when (commonType) {
            booleanPrimitive -> {
                mv.visitJumpInsn(Opcodes.IF_ICMPNE, notEqualLabel)
                true
            }
            charPrimitive -> {
                mv.visitJumpInsn(Opcodes.IF_ICMPNE, notEqualLabel)
                true
            }
            bytePrimitive, shortPrimitive, intPrimitive -> {
                mv.visitJumpInsn(Opcodes.IF_ICMPNE, notEqualLabel)
                true
            }
            longPrimitive -> {
                mv.visitInsn(Opcodes.LCMP)
                mv.visitJumpInsn(Opcodes.IFNE, notEqualLabel)
                true
            }
            floatPrimitive -> {
                mv.visitInsn(Opcodes.FCMPL)
                mv.visitJumpInsn(Opcodes.IFNE, notEqualLabel)
                true
            }
            doublePrimitive -> {
                mv.visitInsn(Opcodes.DCMPL)
                mv.visitJumpInsn(Opcodes.IFNE, notEqualLabel)
                true
            }
            else -> {

                // When comparing objects, we have to take null into account.
                mv.visitInsn(Opcodes.DUP2)
                mv.visitInsn(Opcodes.SWAP)

                // a, b, b, a
                val nonNull = Label()
                val isEqual = Label()
                val equalExtra2 = Label()

                mv.visitJumpInsn(Opcodes.IFNONNULL, nonNull)
                // a, b, b   : a is null
                mv.visitJumpInsn(Opcodes.IFNULL, equalExtra2)
                // a, b      : a is null and b is non-null
                mv.visitInsn(Opcodes.POP2)
                // ()
                mv.visitJumpInsn(Opcodes.GOTO, notEqualLabel)

                mv.visitLabel(equalExtra2)
                // a, b      : a is null and b is null
                mv.visitInsn(Opcodes.POP2)
                mv.visitJumpInsn(Opcodes.GOTO, isEqual)

                mv.visitLabel(nonNull)
                // a, b, b   : a is non-null
                mv.visitInsn(Opcodes.POP) // Remove the duplicated b value.
                // a, b      : a is non-null


                // We can now safely call Object.equals, as we have a non-null value.
                mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, objectClass.asmName(), "equals", "(Ljava/lang/Object;)Z", false)
                // boolean
                mv.visitJumpInsn(Opcodes.IFEQ, notEqualLabel)
                // Empty stack
                mv.visitLabel(isEqual)
                // Empty stack
                true
            }
        }
    }
}
