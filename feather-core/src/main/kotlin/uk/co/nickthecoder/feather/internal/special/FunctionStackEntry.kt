package uk.co.nickthecoder.feather.internal.special

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import uk.co.nickthecoder.feather.IntConstant
import uk.co.nickthecoder.feather.StringConstant
import uk.co.nickthecoder.feather.internal.*
import java.lang.reflect.Method
import java.lang.reflect.Type

/**
 * [FunctionStackEntry] creates bytecode, which creates an instance of [Function] at runtime.
 */
internal class FunctionStackEntry(val method: MethodWrapper) : StackEntry {

    override fun type(): Type = if (method.isStatic()) {
        functionType(method.genericParameterTypes(), method.genericReturnType())
    } else {
        functionType(method.owner(), method.genericParameterTypes(), method.genericReturnType())
    }

    /**
     * Ideally, Function's constructor would take a Method. But that would require a lot of
     * JavaByte code, so I've opted for the "easy" way, and pass enough information, so
     * that we can find the method in Java code, rather than ByteCode.
     */
    override fun compile(mv: MethodVisitor) {
        val parameterTypes = method.genericParameterTypes()

        mv.visitTypeInsn(Opcodes.NEW, functionClass.asmName())
        mv.visitInsn(Opcodes.DUP)
        // Function, Function (uninitialised)

        // We need to pass a Method to the constructor, but first we need to find the Method's Class...

        StringConstant(method.className()).compile(mv)
        mv.visitMethodInsn( // Class.forName( className : String ) : Class
            Opcodes.INVOKESTATIC,
            classClass.asmName(),
            "forName",
            asmMethodDescription(listOf(stringClass), classClass),
            false
        )

        // Function, Function (uninitialised), Method's Class

        // Let's find the method...

        StringConstant(method.name()).compile(mv)

        // Function, Function (uninitialised), class, methodName

        // Create the Class[] for the parameterTypes argument of Class.getMethod
        IntConstant(parameterTypes.size).compile(mv)
        newArray(mv, classClass)
        // Function, Function (uninitialised), class, methodName, Class[] (unfilled)

        // Call Class.forName on each argType, and put it in the Class[]
        for (i in parameterTypes.indices) {
            // ... Class[]
            mv.visitInsn(Opcodes.DUP)
            // ... Class[] Class[]

            val argType = parameterTypes[i]
            IntConstant(i).compile(mv)
            // ... Class[] Class[] index

            argType.compileForName(mv)
            // ... Class[] Class[] index Class
            mv.visitInsn(Opcodes.AASTORE)
            // ... Class[]
        }
        // Function, Function (uninitialised), class, methodName, Class[] (filled)

        mv.visitMethodInsn( // methodsClass.getMethod( method.name, argTypes[] ) : Method
            Opcodes.INVOKEVIRTUAL,
            classClass.asmName(),
            "getMethod",
            asmMethodDescription(listOf(stringClass, arrayType(classClass)), Method::class.java),
            false
        )

        // Function, Function (uninitialised), Method

        // Call the constructor
        mv.visitMethodInsn(
            Opcodes.INVOKESPECIAL,
            functionClass.asmName(),
            "<init>",
            asmMethodDescription(listOf(Method::class.java), voidPrimitive),
            false
        )
        // Function (initialised)
    }
}

