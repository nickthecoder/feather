package uk.co.nickthecoder.feather.internal

import uk.co.nickthecoder.feather.FeatherException
import uk.co.nickthecoder.feather.FeatherPosition
import java.lang.reflect.Type


/**
 * Created during the first pass when a field/method omits the type,
 * and relies on the compiler working is out based on the expression.
 *
 * Most [ImpliedType]s are [SimpleImpliedType]s. Currently, the only exception is
 * in For, which has an implied type for the loop variable, which is based on the
 * StackEntry for the loop expression.
 */
internal interface ImpliedType : Type {

    override fun getTypeName() = "<ImpliedType>"

    val position : FeatherPosition

    fun calculateType(): Type
}

/**
 * Most [ImpliedType]s are [SimpleImpliedType]s. We have found a feather expression,
 * which has been semi-compiled to a [StackEntry] during the first pass, but the
 * type is unknown (e.g. from a method call, where the method hasn't been compiled yet).
 *
 * During the second pass, we must find out the type, and [calculateType] will do this.
 *
 */
internal class SimpleImpliedType(private val stackEntry: StackEntry, override val position: FeatherPosition) : ImpliedType {

    private var evaluatingType = false

    override fun calculateType(): Type {
        if (evaluatingType) {
            throw FeatherException.recursiveTypeDefinition(position)
        }
        evaluatingType = true
        return try {
            stackEntry.resolvedType()
        } finally {
            evaluatingType = false
        }
    }

}
