// feather-runtime

repositories {
    // My other projects hosted on gitlab.com
    maven {
        name = "glok"
        url = uri("https://gitlab.com/api/v4/projects/46354938/packages/maven")
    }
}

dependencies {
    val glokVersion: String by project
    val antlrVersion: String by project

    implementation(project(":feather-core"))
    implementation("uk.co.nickthecoder:glok-core:$glokVersion")
    implementation("uk.co.nickthecoder:glok-model:$glokVersion")
    implementation("uk.co.nickthecoder:glok-model-jvm:$glokVersion")
    implementation("uk.co.nickthecoder:glok-core-jvm:$glokVersion")
    implementation("org.antlr:antlr4-runtime:$antlrVersion")
}

// For uploading maven artifacts to gitlab.com
publishing {
    publications {
        create<MavenPublication>("feather-glok") {
            from(components["kotlin"])
        }
    }
    repositories {
        maven {
            // 16531834 is the GitLab project ID of project nickthecoder/feather
            url = uri("https://gitlab.com/api/v4/projects/16531834/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = providers.gradleProperty("gitLabPrivateToken").get()
                // The password is stored in ~/.gradle/gradle.properties
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}
